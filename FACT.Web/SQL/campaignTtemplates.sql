/****** Object:  Table [dbo].[ctCampaigns]    Script Date: 12-Jul-17 16:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ctCampaigns](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ThumbNail] [nvarchar](max) NULL,
	[Maml] [nvarchar](max) NULL,
	[Mode] [nvarchar](max) NULL,
	[Feed] [nvarchar](max) NULL,
	[Filter] [nvarchar](max) NULL,
	[ctSubCategoryId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.ctCampaigns] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ctCategories]    Script Date: 12-Jul-17 16:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ctCategories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ThumbNail] [nvarchar](max) NULL,
 CONSTRAINT [PK_dbo.ctCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ctCostCenterctCategories]    Script Date: 12-Jul-17 16:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ctCostCenterctCategories](
	[ctCategory_Id] [bigint] NOT NULL,
	[ctCostCenter_Id] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.ctCostCenterctCategories] PRIMARY KEY CLUSTERED 
(
	[ctCostCenter_Id] ASC,
	[ctCategory_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ctCostCenterctSubCategories]    Script Date: 12-Jul-17 16:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ctCostCenterctSubCategories](
	[ctSubCategory_Id] [bigint] NOT NULL,
	[ctCostCenter_Id] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.ctCostCenterctSubCategories] PRIMARY KEY CLUSTERED 
(
	[ctCostCenter_Id] ASC,
	[ctSubCategory_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ctCostCenters]    Script Date: 12-Jul-17 16:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ctCostCenters](
	[Id] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.ctCostCenters] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ctSubCategories]    Script Date: 12-Jul-17 16:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ctSubCategories](
	[Id] [bigint] IDENTITY(1,1) NOT NULL,
	[Name] [nvarchar](max) NULL,
	[Description] [nvarchar](max) NULL,
	[ThumbNail] [nvarchar](max) NULL,
	[ctCategoryId] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.ctSubCategories] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ctSubscriptionctCampaigns]    Script Date: 12-Jul-17 16:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ctSubscriptionctCampaigns](
	[ctSubscription_Id] [nvarchar](128) NOT NULL,
	[ctCampaign_Id] [bigint] NOT NULL,
 CONSTRAINT [PK_dbo.ctSubscriptionctCampaigns] PRIMARY KEY CLUSTERED 
(
	[ctSubscription_Id] ASC,
	[ctCampaign_Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
/****** Object:  Table [dbo].[ctSubscriptions]    Script Date: 12-Jul-17 16:02:58 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE TABLE [dbo].[ctSubscriptions](
	[Id] [nvarchar](128) NOT NULL,
 CONSTRAINT [PK_dbo.ctSubscriptions] PRIMARY KEY CLUSTERED 
(
	[Id] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON) ON [PRIMARY]
) ON [PRIMARY]
GO
SET IDENTITY_INSERT [dbo].[ctCampaigns] ON 

INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (1, N'Weekly Market Commentary', N'This weekly update offers insights into domestic and global financial markets events, policy actions, and geopolitical impacts.<br/><b>Updated:</b> 11/1/16 <br/><b>Next launch</b>:11/9<br/><b>Channel:</b> Email<br/><b>Drips:</b> Launches every Wednesday', N'https://s3.amazonaws.com/lplmodact/WeeklyMarketThumb.png', N'WeeklyMarketCommentary.maml', N'read', NULL, NULL, 9)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (2, N'Weekly Economic Commentary', N'This weekly update offers insights into the U.S. and global economy.<br/><b>Updated:</b> 11/1/16 <br/><b>Next launch</b>:11/9<br/><b>Channel:</b> Email<br/><b>Drips:</b> Launches every Wednesday<br/><br/><br/>', N'https://s3.amazonaws.com/lplmodact/WeeklyEconomicThumb.png', N'WeeklyEconomicCommentary.maml', N'read', NULL, NULL, 9)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (3, N'Bond Market Perspectives', N'This weekly update offers insights into major news and themes driving fixed income markets and how they affect your clients'' portfolios.<br/><b>Updated:</b> 11/2/16 <br/><b>Next launch</b>:11/4<br/><b>Channel:</b> Email<br/><b>Drips:</b> Launches every Friday', N'https://s3.amazonaws.com/lplmodact/WeeklyBondMarketThumb.png', N'WeeklyBondMarketCommentary.maml', N'read', NULL, NULL, 9)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (4, N'Bond Market Perspectives', N'This weekly update offers insights into major news and themes driving fixed income markets and how they affect your clients'' portfolios.<br/><b>Channel:</b> RSS Email, Social<br/><b>Drips:</b> 2, Ad-hoc', N'https://s3.amazonaws.com/lplmodact/WeeklyBondMarketThumbSocial.png', N'RSS_CM.maml', N'rss', N'http://lpl-research.com/~rss/LPL_Financial_Research_Publications.xml', N'Bond Market Perspectives', 9)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (5, N'Bond Market Perspectives', N'This weekly update offers insights into major news and themes driving fixed income markets and how they affect your clients'' portfolios.<br/><b>Channel:</b> Social Post<br/><b>Drips:</b> 1, Ad-hoc', N'https://s3.amazonaws.com/lplmodact/BMP_SocialThumb.png', N'RSS_Social.maml', N'rss', N'http://lpl-research.com/~rss/LPL_Financial_Research_Publications.xml', N'Bond Market Perspectives', 9)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (6, N'LPL Research Client Letters', N'Send insight and research into current trending topics provided on a timely and ad-hoc basis.<br/><b>Channel:</b> RSS Email<br/><b>Drips:</b> 1, Ad-hoc<br/><br/><br/>', N'https://s3.amazonaws.com/lplmodact/LPLResearchCLThumb.png', N'LinkedInPost.maml', N'rss', N'http://lpl-research.com/~rss/LPL_Financial_Research_Publications.xml', N'Client Letter', 9)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (7, N'Plan Participant Newsletter', N'This quarterly newsletter is focused on issues that matter retirement plan participants. Articles include information on retirement income, building good financial habits, the impact of time on investments, and more.<br/><b>Updated:</b> 10/20/16<br/><b>Channel:</b> Email<br/><b>Drips:</b> 4",', N'https://s3.amazonaws.com/lplmodact/PlanParticipantThumb.png', N'QuarterlyPlanPartners.maml', N'read', NULL, NULL, 10)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (8, N'Plan Sponsor Newsletter', N'This quarterly newsletter is a resource on current issues for plan sponsors and administrators. Topics include a plan sponsor quarterly calendar, FAQs, and industry news related to retirement planning.<br/><b>Updated:</b> 10/20/16<br/><b>Channel:</b> Email<br/><b>Drips:</b> 4', N'https://s3.amazonaws.com/lplmodact/PlanSponsorThumb.png', N'QuarterlyPlanSponsors.maml', N'read', NULL, NULL, 10)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (9, N'Affluent investor campaign', N'These monthly messages communicate to high-net-worth households are sent by email only. The messaging concerns issues such as wealth and tax management, charitable giving, and estate and legacy planning.  <br/><b>Channel:</b> Email<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/AffluentThumbEmail.png', N'MonthlyTopicAffluentAdvisor.maml', N'read', NULL, NULL, 1)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (10, N'Affluent investor campaign', N'These monthly messages communicate to high-net-worth households. For a nominal fee, you can complement your email campaign with a direct mail piece. The messaging concerns issues such as wealth and tax management, charitable giving, and estate and legacy planning.   <br/><b>Channel:</b> Email (2) Direct Mail (1)<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/AffluentThumbCrossMedia.png', N'Affluent_Print_1.maml', N'read', NULL, NULL, 1)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (12, N'Affluent investor campaign', N'These monthly messages communicate to high-net-worth households. For a nominal fee, you can complement your email campaign with a direct mail piece. The messaging concerns issues such as wealth and tax management, charitable giving, and estate and legacy planning.   <br/><b>Channel:</b> Email (2) Social (2) Web(2)<br/><b>Drips:</b> 2', N'https://s3.amazonaws.com/lplmodact/AIThumbSocial.png', N'AI1216.maml', N'read', NULL, NULL, 1)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (13, N'Focused on Retirement Email Campaign', N'A monthly email-only campaign looking at the issues critical to baby boomers. Topics include adjustments retirement savings efforts or investments, market volatility, Social Security issues, and more.  <br/><b>Channel:</b> Email<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/FORThumbEmail.png', N'MonthlyTopicRetirementQ4.maml', N'read', NULL, NULL, 2)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (14, N'Focused on Retirement Cross-Media Campaign', N'A monthly campaign looking at the issues critical to baby boomers. For a nominal fee, you can complement your email campaign with a direct mail piece. Topics include adjustments retirement savings efforts or investments, market volatility, Social Security issues, and more.  <br/><b>Channel:</b> Email (1) Direct Mail (2)<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/FORThumbCrossMedia.png', N'FOR_Print_1.maml', N'read', NULL, NULL, 2)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (15, N'Focused on Retirement Cross-Media Campaign - Jan 2017', N'A monthly campaign looking at the issues critical to baby boomers. Topics include adjustments retirement savings efforts or investments, market volatility, Social Security issues, and more.  <br/><b>Channel:</b> Email (2) Social (2) Web (2)<br/><b>Drips:</b> 2', N'https://s3.amazonaws.com/lplmodact/FORThumbSocial.png', N'FOR117.maml', N'read', NULL, NULL, 2)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (16, N'Investors Building Careers & Family', N'A monthly campaign spotlighting the concerns of those at mid-life. Discussions  include  retirement assumptions vs. reality, college planning, and improving household financial habits.<br/><b>Channel:</b> Email<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/CareerThumb.png', N'MonthlyTopicBuildingCareers.maml', N'read', NULL, NULL, 3)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (18, N'Investors Building Careers & Family', N'A monthly campaign spotlighting the concerns of those at mid-life. Discussions  include  retirement assumptions vs. reality, college planning, and improving household financial habits.<br/><b>Channel:</b> Email (2) Social (2) Web (2)<br/><b>Drips:</b> 2', N'https://s3.amazonaws.com/lplmodact/BCFThumbSocial.png', N'BCF117.maml', N'read', NULL, NULL, 3)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (20, N'Millennial Investors', N'This monthly campaign communicates the value of saving and investing for the future early in life. Messaging seeks to awaken millennials to ways to reduce debt and move financially ahead of their peers. Good lifetime financial habits are emphasized.<br/><b>Channel:</b> Email<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/MILThumbEmail.png', N'MonthlyTopicMillenial.maml', N'read', NULL, NULL, 4)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (21, N'Millennial Investors', N'This monthly campaign communicates the value of saving and investing for the future early in life. Messaging seeks to awaken millennials to ways to reduce debt and move financially ahead of their peers. Good lifetime financial habits are emphasized.<br/><b>Channel:</b> Email (2) Social (2) Web(2)<br/><b>Drips:</b> 2', N'https://s3.amazonaws.com/lplmodact/MILThumbSocial.png', N'MIL117.maml', N'read', NULL, NULL, 4)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (22, N'Why Work With a Financial Advisor?', N'This quarterly email-only campaign explains the value of consulting with a financial advisor. It notes what can potentially be gained from such a relationship - now and over time.<br/><b>Channel:</b> Email<br/><b>Drips:</b> 4', N'https://s3.amazonaws.com/lplmodact/WWWThumbEmail.png', N'FinancialAdvisorFAQ.maml', N'read', NULL, NULL, 11)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (23, N'Why Work With a Financial Advisor?', N'This quarterly email only campaign explains the value of consulting with a financial advisor. For a nominal fee, you can complement your email campaign with a direct mail piece. It notes what can potentially be gained from such a relationship - now and over time.<br/><b>Channel:</b> Email (2) Direct Mail (2)<br/><b>Drips:</b> 4', N'https://s3.amazonaws.com/lplmodact/WWWThumbCrossMedia.png', N'WWW_Print_1.maml', N'read', NULL, NULL, 11)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (24, N'Birthday Email Style 1', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb.png', N'HappyBirthday_Email_1.maml', N'read', NULL, NULL, 5)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (25, N'Birthday Email Style 2', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1"', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb2.png', N'HappyBirthday_Email_2.maml', N'read', NULL, NULL, 5)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (27, N'Birthday Email Style 3', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayThumb3.png', N'HappyBirthday_Email_3.maml', N'read', NULL, NULL, 5)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (28, N'Birthday Email Style 4', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb4.png', N'HappyBirthday_Email_4.maml', N'read', NULL, NULL, 5)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (30, N'Birthday Email Style 5', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb5.png', N'HappyBirthday_Email_5.maml', N'read', NULL, NULL, 5)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (31, N'Happy Birthday Greeting Card Style 1', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb.png', N'HappyBirthday_Print_1.maml', N'read', NULL, NULL, 6)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (32, N'Happy Birthday Greeting Card Style 2', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb2.png', N'HappyBirthday_Print_2.maml', N'read', NULL, NULL, 6)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (33, N'Happy Birthday Greeting Card Style 3', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayThumb3.png', N'HappyBirthday_Print_3.maml', N'read', NULL, NULL, 6)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (34, N'Happy Birthday Greeting Card Style 4', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb4.png', N'HappyBirthday_Print_4.maml', N'read', NULL, NULL, 6)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (35, N'Happy Birthday Greeting Card Style 5', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb5.png', N'HappyBirthday_Print_5.maml', N'read', NULL, NULL, 6)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (36, N'Holiday Email Pack', N'Send various emails throughout the year for New Years, Fourth of July, Thanksgiving, and Holiday Season greetings. <br/><b>Channel:</b> Email <br/><b>Drips:</b> 4', N'https://s3.amazonaws.com/lplmodact/HolidayGreetingsEmailThumb.png', N'EmailOnlyHolidayCampaign.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (37, N'Happy New Year E-Card Style 1', N'Yearly scheduled email for the new year. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb1.png', N'HappyNewYear_Email_1.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (38, N'Happy New Year E-Card Style 2', N'Yearly scheduled email for the new year. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb2.png', N'HappyNewYear_Email_2.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (39, N'Happy New Year E-Card Style 3', N'Yearly scheduled email for the new year. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb3.png', N'HappyNewYear_Email_3.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (40, N'Independence Day E-Card Style 1', N'Yearly scheduled email for the Independence Day. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/IndependenceDayThumb1.png', N'IndependenceDay_Email_1.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (41, N'Independence Day E-Card Style 2', N'Yearly scheduled email for Independence Day. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/IndependenceDayThumb2.png', N'IndependenceDay_Email_2.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (42, N'Happy Thanksgiving E-Card Style 1', N'Yearly scheduled email for the Thanksgiving Holiday. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb1.png', N'HappyThanksgiving_Email_1.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (43, N'Happy Thanksgiving E-Card Style 2', N'Yearly scheduled email for the Thanksgiving Holiday. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb2.png', N'HappyThanksgiving_Email_2.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (44, N'Happy Thanksgiving E-Card Style 3', N'Yearly scheduled email for the Thanksgiving Holiday. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb3.png', N'HappyThanksgiving_Email_3.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (45, N'Happy Holidays E-Card Style 1', N'Yearly scheduled email for the Holiday Season. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHolidaysThumb1.png', N'HappyHolidays.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (46, N'Seasons Greetings E-Card Style 1', N'Yearly scheduled email for the Holiday Season. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/SeasonsGreetingsThumb1.png', N'SeasonsGreetings_Email_1.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (47, N'Happy Hanukkah E-Card Style 1', N'Yearly scheduled email for Hanukkah. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHanukkahThumb.png', N'HappyHanukkah_Email_1.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (48, N'Happy Hanukkah E-Card Style 2', N'Yearly scheduled email for Hanukkah. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHanukkahThumb2.png', N'HappyHanukkah_Email_2.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (50, N'Merry Chirstmas E-Card Style 1', N'Yearly scheduled email for the Christmas season. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/MerryChristmasThumb1.png', N'MerryChristmas_Email_1.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (51, N'Merry Chirstmas E-Card Style 2', N'Yearly scheduled email for the Christmas season. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/MerryChristmasThumb2.png', N'MerryChristmas_Email_2.maml', N'read', NULL, NULL, 7)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (52, N'Happy New Year Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb1.png', N'HappyNewYear_Print_1.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (53, N'Happy New Year Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb2.png', N'HappyNewYear_Print_2.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (54, N'Happy New Year Greeting Card Style 3', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb3.png', N'HappyNewYear_Print_3.mamlread', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (56, N'Independence Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/IndependenceDayThumb1.png', N'IndependenceDay_Print_1.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (57, N'Independence Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/IndependenceDayThumb2.png', N'IndependenceDay_Print_2.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (58, N'Happy Thanksgiving Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb1.png', N'HappyThanks_Print_1.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (59, N'Happy Thanksgiving Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb2.png', N'HappyThanks_Print_2.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (60, N'Happy Thanksgiving Greeting Card Style 3', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb3.png', N'HappyThanks_Print_3.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (61, N'Happy Holidays Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHolidaysThumb1.png', N'HappyHolidays_Print_1.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (62, N'Seasons Greetings Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/SeasonsGreetingsThumb1.png', N'SeasonsGreetings_Print_1.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (63, N'Happy Hanukkah Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHanukkahThumb.png', N'HappyHaunaka_Print_1.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (64, N'Happy Hanukkah Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHanukkahThumb2.png', N'HappyHaunaka_Print_2.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (65, N'Merry Christmas Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/MerryChristmasThumb1.png', N'MerryChristmas_Print_1.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (66, N'Merry Christmas Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/MerryChristmasThumb2.png', N'MerryChristmas_Print_2.maml', N'read', NULL, NULL, 8)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (67, N'LPL Letterhead', N'LPL Letterhead', N'https://s3.amazonaws.com/lplmodact/lpllogo.png', N'LPLLetterCampaign.maml', N'edit', NULL, NULL, 12)
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (68, N'LPL Research Outlook', N'This bi-annual publication includes financial market forecasts, economic insights, and investment guidance for the year ahead.<br/><b>Channel:</b> RSS Email<br/><b>Drips:</b> 1, Bi-annual', N'https://s3.amazonaws.com/lplmodact/LPLResearchOutlookThumb.png', N'LinkedInPost.maml', N'rss', N'http://lpl-research.com/~rss/LPL_Financial_Research_Publications.xml', N'Outlook 2017', 9)
SET IDENTITY_INSERT [dbo].[ctCampaigns] OFF
SET IDENTITY_INSERT [dbo].[ctCategories] ON 

INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (1, N'LPL Research Campaigns', N'LPL Research provides timely, comprehensive publications on a range of subjects from investments, to markets, to the economy.', N'https://s3.amazonaws.com/lplmodact/LPLResearchThumb.png')
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (2, N'Retirement Partners Campaigns', N'Retirement plan communications with helpful tips for plan sponsors and plan participants.', N'https://s3.amazonaws.com/lplmodact/RetirementPartnersThumb.png')
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (3, N'Timely Topics Campaigns', N'Provide timely and relevant content by specific target audiences, including affluent investors, baby boomers, and millennials that address their specific needs.', N'https://s3.amazonaws.com/lplmodact/TimelyTopicsThumb.png')
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (4, N'Why Work With a Financial Advisor Campaigns', N'Campaigns that help explain the value of working with an advisor-and how that partnership can help them achieve their goals throughout their lives.', N'https://s3.amazonaws.com/lplmodact/FinancialAdvisorThumb.png')
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (5, N'Birthday Greetings', N'Never miss another birthday and automatically send birthday emails or printed cards personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/BirthdayGreetingsThumb.png')
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (6, N'Holiday Greetings', N'Remembering special occasions is an important part of building relationships. Select from printed or digital greeting card campaigns personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/HolidayGreetingsThumb.png')
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (7, N'LPL', N'Various LPL Products', N'https://s3.amazonaws.com/lplmodact/lpllogo.png')
SET IDENTITY_INSERT [dbo].[ctCategories] OFF
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplabs')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplabs')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplabs')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplabs')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplabs')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplabs')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccho')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccho')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccho')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccho')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccho')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccho')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplcchov')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccmgp')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccmgp')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccmgp')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccmgp')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccmgp')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccmgp')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrbg')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrbg')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrbg')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrbg')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrbg')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrbg')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplabs')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccho')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (12, N'lplcchov')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccmgp')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrbg')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrecruiting')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrpdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrpias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrpis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccwfsdual')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccwfsias')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccwfsis')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplincadvisors')
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplincadvisors')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplabs')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccdual')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccho')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplcchov')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccias')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccis')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccmgp')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrbg')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrecruiting')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrpdual')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrpias')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrpis')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccwfsdual')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccwfsias')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccwfsis')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccworksiteadmin')
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplincadvisors')
SET IDENTITY_INSERT [dbo].[ctSubCategories] ON 

INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (1, N'Affluent', N'Campaigns communicating to high-net-worth households. The messaging concerns issues such as wealth and tax management, charitable giving, and estate and legacy planning.', N'https://s3.amazonaws.com/lplmodact/AffluentThumb.png', 3)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (2, N'Focused on Retirement', N'Campaigns looking at the issues critical to baby boomers. Topics include adjustments retirement savings efforts or investments, market volatility, Social Security issues, and more.', N'https://s3.amazonaws.com/lplmodact/RetirementThumb.png', 3)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (3, N'Investors Building Careers & Family', N'Campaigns spotlighting the concerns of those at mid-life. Discussions include retirement assumptions vs. reality, college planning, and improving household financial habits.', N'https://s3.amazonaws.com/lplmodact/CareerThumb.png', 3)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (4, N'Millennial Investors', N'These campaigns communicate the value of saving and investing for the future early in life. Messaging seeks to awaken millennials to ways to reduce debt and move financially ahead of their peers. Good lifetime financial habits are emphasized.', N'https://s3.amazonaws.com/lplmodact/MillennialThumb.png', 3)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (5, N'Birthday Greetings<br/> E-CARD', N'Choose from various birthday greetings that can be sent by email and personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/BirthdayGreetingsEmailThumb.png', 5)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (6, N'Birthday Greetings <br/>PRINTED', N'For a nominal fee, send printed birthday cards for your top clients. Cards will automatically be mailed on your behalf, personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/BirthdayGreetingsPrintThumb.png', 5)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (7, N'Holiday Greetings<br/> E-CARD', N'Choose from various holiday greetings that can be sent by email and personalized with your signature. Holidays include Thanksgiving, Christmas, Hannukah, 4th of July, and more!', N'https://s3.amazonaws.com/lplmodact/HolidayGreetingsEmailThumb.png', 6)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (8, N'Holiday Greetings<br/> PRINTED', N'For a nominal fee, send printed holiday cards for your top clients, including Thanksgiving, 4th of July, and more. Cards will automatically be mailed on your behalf, personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/HolidayGreetingsPrintThumb.png', 6)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (9, N'LPL Research Campaigns', N'LPL Research provides timely, comprehensive publications on a range of subjects from investments, to markets, to the economy.', N'https://s3.amazonaws.com/lplmodact/LPLResearchThumb.png', 1)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (10, N'Retirement Partners Campaigns', N'Retirement plan communications with helpful tips for plan sponsors and plan participants.', N'https://s3.amazonaws.com/lplmodact/RetirementPartnersThumb.png', 2)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (11, N'Why Work With a Financial Advisor Campaigns', N'Campaigns that help explain the value of working with an advisor-and how that partnership can help them achieve their goals throughout their lives.', N'https://s3.amazonaws.com/lplmodact/FinancialAdvisorThumb.png', 4)
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (12, N'LPL', N'Various LPL Products', N'https://s3.amazonaws.com/lplmodact/lpllogo.png', 7)
SET IDENTITY_INSERT [dbo].[ctSubCategories] OFF
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 1)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 1)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 2)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 2)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 2)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 3)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 3)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 3)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 4)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 4)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 4)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 5)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 5)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 5)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 6)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 6)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 6)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 7)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 7)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 8)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 8)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 8)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 9)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 9)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 9)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 10)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 10)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 10)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 12)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 12)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 12)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 13)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 13)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 13)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 14)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 14)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 14)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 15)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 15)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 15)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 16)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 16)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 16)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 18)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 18)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 18)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 20)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 20)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 20)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 21)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 21)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 21)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 22)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 22)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 23)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 23)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 24)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 24)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 25)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 25)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 27)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 27)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 28)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 28)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 30)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 30)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 31)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 31)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 31)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 32)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 32)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 32)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 33)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 33)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 33)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 34)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 34)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 34)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 35)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 35)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 35)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 36)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 36)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 36)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 37)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 37)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 37)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 38)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 38)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 38)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 39)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 39)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 39)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 40)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 40)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 40)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 41)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 41)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 41)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 42)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 42)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 42)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 43)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 43)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 43)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 44)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 44)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 44)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 45)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 45)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 45)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 46)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 46)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 46)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 47)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 47)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 47)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 48)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 48)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 48)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 50)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 50)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 50)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 51)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 51)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 51)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 52)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 52)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 52)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 53)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 53)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 53)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 54)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 54)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 54)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 56)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 56)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 56)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 57)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 57)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 57)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 58)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 58)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 58)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 59)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 59)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 59)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 60)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 60)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 60)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 61)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 61)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 61)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 62)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 62)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 62)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 63)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 63)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 63)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 64)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 64)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 64)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 65)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 65)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 65)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 66)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 66)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 66)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 67)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 67)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 68)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 68)
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 68)
INSERT [dbo].[ctSubscriptions] ([Id]) VALUES (N'GOLD')
INSERT [dbo].[ctSubscriptions] ([Id]) VALUES (N'PLATINUM')
INSERT [dbo].[ctSubscriptions] ([Id]) VALUES (N'PLATINUMPLUS')
ALTER TABLE [dbo].[ctCampaigns]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ctCampaigns_dbo.ctSubCategories_ctSubCategoryId] FOREIGN KEY([ctSubCategoryId])
REFERENCES [dbo].[ctSubCategories] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ctCampaigns] CHECK CONSTRAINT [FK_dbo.ctCampaigns_dbo.ctSubCategories_ctSubCategoryId]
GO
ALTER TABLE [dbo].[ctCostCenterctCategories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ctCategoryctCostCenters_dbo.ctCategories_ctCategory_Id] FOREIGN KEY([ctCategory_Id])
REFERENCES [dbo].[ctCategories] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ctCostCenterctCategories] CHECK CONSTRAINT [FK_dbo.ctCategoryctCostCenters_dbo.ctCategories_ctCategory_Id]
GO
ALTER TABLE [dbo].[ctCostCenterctCategories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ctCategoryctCostCenters_dbo.ctCostCenters_ctCostCenter_Id] FOREIGN KEY([ctCostCenter_Id])
REFERENCES [dbo].[ctCostCenters] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ctCostCenterctCategories] CHECK CONSTRAINT [FK_dbo.ctCategoryctCostCenters_dbo.ctCostCenters_ctCostCenter_Id]
GO
ALTER TABLE [dbo].[ctCostCenterctSubCategories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ctSubCategoryctCostCenters_dbo.ctCostCenters_ctCostCenter_Id] FOREIGN KEY([ctCostCenter_Id])
REFERENCES [dbo].[ctCostCenters] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ctCostCenterctSubCategories] CHECK CONSTRAINT [FK_dbo.ctSubCategoryctCostCenters_dbo.ctCostCenters_ctCostCenter_Id]
GO
ALTER TABLE [dbo].[ctCostCenterctSubCategories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ctSubCategoryctCostCenters_dbo.ctSubCategories_ctSubCategory_Id] FOREIGN KEY([ctSubCategory_Id])
REFERENCES [dbo].[ctSubCategories] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ctCostCenterctSubCategories] CHECK CONSTRAINT [FK_dbo.ctSubCategoryctCostCenters_dbo.ctSubCategories_ctSubCategory_Id]
GO
ALTER TABLE [dbo].[ctSubCategories]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ctSubCategories_dbo.ctCategories_ctCategoryId] FOREIGN KEY([ctCategoryId])
REFERENCES [dbo].[ctCategories] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ctSubCategories] CHECK CONSTRAINT [FK_dbo.ctSubCategories_dbo.ctCategories_ctCategoryId]
GO
ALTER TABLE [dbo].[ctSubscriptionctCampaigns]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ctSubscriptionctCampaigns_dbo.ctCampaigns_ctCampaign_Id] FOREIGN KEY([ctCampaign_Id])
REFERENCES [dbo].[ctCampaigns] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ctSubscriptionctCampaigns] CHECK CONSTRAINT [FK_dbo.ctSubscriptionctCampaigns_dbo.ctCampaigns_ctCampaign_Id]
GO
ALTER TABLE [dbo].[ctSubscriptionctCampaigns]  WITH CHECK ADD  CONSTRAINT [FK_dbo.ctSubscriptionctCampaigns_dbo.ctSubscriptions_ctSubscription_Id] FOREIGN KEY([ctSubscription_Id])
REFERENCES [dbo].[ctSubscriptions] ([Id])
ON DELETE CASCADE
GO
ALTER TABLE [dbo].[ctSubscriptionctCampaigns] CHECK CONSTRAINT [FK_dbo.ctSubscriptionctCampaigns_dbo.ctSubscriptions_ctSubscription_Id]
GO
