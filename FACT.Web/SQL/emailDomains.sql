USE [MYCMODB]
GO
SET IDENTITY_INSERT [dbo].[EmailDomains] ON 

INSERT [dbo].[EmailDomains] ([Id], [Domain], [IsBlacklisted]) VALUES (1, N'advisorgroup.com', 0)
INSERT [dbo].[EmailDomains] ([Id], [Domain], [IsBlacklisted]) VALUES (2, N'woodburyfinancial.net', 0)
INSERT [dbo].[EmailDomains] ([Id], [Domain], [IsBlacklisted]) VALUES (3, N'woodburyfinancial.com', 0)
INSERT [dbo].[EmailDomains] ([Id], [Domain], [IsBlacklisted]) VALUES (4, N'spfi.com', 0)
INSERT [dbo].[EmailDomains] ([Id], [Domain], [IsBlacklisted]) VALUES (5, N'royalaa.com', 0)
INSERT [dbo].[EmailDomains] ([Id], [Domain], [IsBlacklisted]) VALUES (6, N'fscadvisor.com', 0)
INSERT [dbo].[EmailDomains] ([Id], [Domain], [IsBlacklisted]) VALUES (7, N'royalalliance.com', 0)
INSERT [dbo].[EmailDomains] ([Id], [Domain], [IsBlacklisted]) VALUES (8, N'fscorp.com ', 0)
INSERT [dbo].[EmailDomains] ([Id], [Domain], [IsBlacklisted]) VALUES (9, N'Sagepointadvisor.com', 0)
SET IDENTITY_INSERT [dbo].[EmailDomains] OFF
