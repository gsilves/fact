SET IDENTITY_INSERT [dbo].[ctCategories] ON 
GO
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (1, N'LPL Research Campaigns', N'LPL Research provides timely, comprehensive publications on a range of subjects from investments, to markets, to the economy.', N'https://s3.amazonaws.com/lplmodact/LPLResearchThumb.png')
GO
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (2, N'Retirement Partners Campaigns', N'Retirement plan communications with helpful tips for plan sponsors and plan participants.', N'https://s3.amazonaws.com/lplmodact/RetirementPartnersThumb.png')
GO
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (3, N'Timely Topics Campaigns', N'Provide timely and relevant content by specific target audiences, including affluent investors, baby boomers, and millennials that address their specific needs.', N'https://s3.amazonaws.com/lplmodact/TimelyTopicsThumb.png')
GO
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (4, N'Why Work With a Financial Advisor Campaigns', N'Campaigns that help explain the value of working with an advisor-and how that partnership can help them achieve their goals throughout their lives.', N'https://s3.amazonaws.com/lplmodact/FinancialAdvisorThumb.png')
GO
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (5, N'Birthday Greetings', N'Never miss another birthday and automatically send birthday emails or printed cards personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/BirthdayGreetingsThumb.png')
GO
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (6, N'Holiday Greetings', N'Remembering special occasions is an important part of building relationships. Select from printed or digital greeting card campaigns personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/HolidayGreetingsThumb.png')
GO
INSERT [dbo].[ctCategories] ([Id], [Name], [Description], [ThumbNail]) VALUES (7, N'LPL', N'Various LPL Products', N'https://s3.amazonaws.com/lplmodact/lpllogo.png')
GO
SET IDENTITY_INSERT [dbo].[ctCategories] OFF
GO
SET IDENTITY_INSERT [dbo].[ctSubCategories] ON 
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (1, N'Affluent', N'Campaigns communicating to high-net-worth households. The messaging concerns issues such as wealth and tax management, charitable giving, and estate and legacy planning.', N'https://s3.amazonaws.com/lplmodact/AffluentThumb.png', 3)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (2, N'Focused on Retirement', N'Campaigns looking at the issues critical to baby boomers. Topics include adjustments retirement savings efforts or investments, market volatility, Social Security issues, and more.', N'https://s3.amazonaws.com/lplmodact/RetirementThumb.png', 3)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (3, N'Investors Building Careers & Family', N'Campaigns spotlighting the concerns of those at mid-life. Discussions include retirement assumptions vs. reality, college planning, and improving household financial habits.', N'https://s3.amazonaws.com/lplmodact/CareerThumb.png', 3)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (4, N'Millennial Investors', N'These campaigns communicate the value of saving and investing for the future early in life. Messaging seeks to awaken millennials to ways to reduce debt and move financially ahead of their peers. Good lifetime financial habits are emphasized.', N'https://s3.amazonaws.com/lplmodact/MillennialThumb.png', 3)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (5, N'Birthday Greetings<br/> E-CARD', N'Choose from various birthday greetings that can be sent by email and personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/BirthdayGreetingsEmailThumb.png', 5)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (6, N'Birthday Greetings <br/>PRINTED', N'For a nominal fee, send printed birthday cards for your top clients. Cards will automatically be mailed on your behalf, personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/BirthdayGreetingsPrintThumb.png', 5)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (7, N'Holiday Greetings<br/> E-CARD', N'Choose from various holiday greetings that can be sent by email and personalized with your signature. Holidays include Thanksgiving, Christmas, Hannukah, 4th of July, and more!', N'https://s3.amazonaws.com/lplmodact/HolidayGreetingsEmailThumb.png', 6)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (8, N'Holiday Greetings<br/> PRINTED', N'For a nominal fee, send printed holiday cards for your top clients, including Thanksgiving, 4th of July, and more. Cards will automatically be mailed on your behalf, personalized with your signature.', N'https://s3.amazonaws.com/lplmodact/HolidayGreetingsPrintThumb.png', 6)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (9, N'LPL Research Campaigns', N'LPL Research provides timely, comprehensive publications on a range of subjects from investments, to markets, to the economy.', N'https://s3.amazonaws.com/lplmodact/LPLResearchThumb.png', 1)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (10, N'Retirement Partners Campaigns', N'Retirement plan communications with helpful tips for plan sponsors and plan participants.', N'https://s3.amazonaws.com/lplmodact/RetirementPartnersThumb.png', 2)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (11, N'Why Work With a Financial Advisor Campaigns', N'Campaigns that help explain the value of working with an advisor-and how that partnership can help them achieve their goals throughout their lives.', N'https://s3.amazonaws.com/lplmodact/FinancialAdvisorThumb.png', 4)
GO
INSERT [dbo].[ctSubCategories] ([Id], [Name], [Description], [ThumbNail], [ctCategoryId]) VALUES (12, N'LPL', N'Various LPL Products', N'https://s3.amazonaws.com/lplmodact/lpllogo.png', 7)
GO
SET IDENTITY_INSERT [dbo].[ctSubCategories] OFF
GO
SET IDENTITY_INSERT [dbo].[ctCampaigns] ON 
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (1, N'Weekly Market Commentary', N'This weekly update offers insights into domestic and global financial markets events, policy actions, and geopolitical impacts.<br/><b>Updated:</b> 11/1/16 <br/><b>Next launch</b>:11/9<br/><b>Channel:</b> Email<br/><b>Drips:</b> Launches every Wednesday', N'https://s3.amazonaws.com/lplmodact/WeeklyMarketThumb.png', N'WeeklyMarketCommentary.maml', N'read', NULL, NULL, 9)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (2, N'Weekly Economic Commentary', N'This weekly update offers insights into the U.S. and global economy.<br/><b>Updated:</b> 11/1/16 <br/><b>Next launch</b>:11/9<br/><b>Channel:</b> Email<br/><b>Drips:</b> Launches every Wednesday<br/><br/><br/>', N'https://s3.amazonaws.com/lplmodact/WeeklyEconomicThumb.png', N'WeeklyEconomicCommentary.maml', N'read', NULL, NULL, 9)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (3, N'Bond Market Perspectives', N'This weekly update offers insights into major news and themes driving fixed income markets and how they affect your clients'' portfolios.<br/><b>Updated:</b> 11/2/16 <br/><b>Next launch</b>:11/4<br/><b>Channel:</b> Email<br/><b>Drips:</b> Launches every Friday', N'https://s3.amazonaws.com/lplmodact/WeeklyBondMarketThumb.png', N'WeeklyBondMarketCommentary.maml', N'read', NULL, NULL, 9)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (4, N'Bond Market Perspectives', N'This weekly update offers insights into major news and themes driving fixed income markets and how they affect your clients'' portfolios.<br/><b>Channel:</b> RSS Email, Social<br/><b>Drips:</b> 2, Ad-hoc', N'https://s3.amazonaws.com/lplmodact/WeeklyBondMarketThumbSocial.png', N'RSS_CM.maml', N'rss', N'http://lpl-research.com/~rss/LPL_Financial_Research_Publications.xml', N'Bond Market Perspectives', 9)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (5, N'Bond Market Perspectives', N'This weekly update offers insights into major news and themes driving fixed income markets and how they affect your clients'' portfolios.<br/><b>Channel:</b> Social Post<br/><b>Drips:</b> 1, Ad-hoc', N'https://s3.amazonaws.com/lplmodact/BMP_SocialThumb.png', N'RSS_Social.maml', N'rss', N'http://lpl-research.com/~rss/LPL_Financial_Research_Publications.xml', N'Bond Market Perspectives', 9)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (6, N'LPL Research Client Letters', N'Send insight and research into current trending topics provided on a timely and ad-hoc basis.<br/><b>Channel:</b> RSS Email<br/><b>Drips:</b> 1, Ad-hoc<br/><br/><br/>', N'https://s3.amazonaws.com/lplmodact/LPLResearchCLThumb.png', N'LinkedInPost.maml', N'rss', N'http://lpl-research.com/~rss/LPL_Financial_Research_Publications.xml', N'Client Letter', 9)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (7, N'Plan Participant Newsletter', N'This quarterly newsletter is focused on issues that matter retirement plan participants. Articles include information on retirement income, building good financial habits, the impact of time on investments, and more.<br/><b>Updated:</b> 10/20/16<br/><b>Channel:</b> Email<br/><b>Drips:</b> 4",', N'https://s3.amazonaws.com/lplmodact/PlanParticipantThumb.png', N'QuarterlyPlanPartners.maml', N'read', NULL, NULL, 10)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (8, N'Plan Sponsor Newsletter', N'This quarterly newsletter is a resource on current issues for plan sponsors and administrators. Topics include a plan sponsor quarterly calendar, FAQs, and industry news related to retirement planning.<br/><b>Updated:</b> 10/20/16<br/><b>Channel:</b> Email<br/><b>Drips:</b> 4', N'https://s3.amazonaws.com/lplmodact/PlanSponsorThumb.png', N'QuarterlyPlanSponsors.maml', N'read', NULL, NULL, 10)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (9, N'Affluent investor campaign', N'These monthly messages communicate to high-net-worth households are sent by email only. The messaging concerns issues such as wealth and tax management, charitable giving, and estate and legacy planning.  <br/><b>Channel:</b> Email<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/AffluentThumbEmail.png', N'MonthlyTopicAffluentAdvisor.maml', N'read', NULL, NULL, 1)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (10, N'Affluent investor campaign', N'These monthly messages communicate to high-net-worth households. For a nominal fee, you can complement your email campaign with a direct mail piece. The messaging concerns issues such as wealth and tax management, charitable giving, and estate and legacy planning.   <br/><b>Channel:</b> Email (2) Direct Mail (1)<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/AffluentThumbCrossMedia.png', N'Affluent_Print_1.maml', N'read', NULL, NULL, 1)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (12, N'Affluent investor campaign', N'These monthly messages communicate to high-net-worth households. For a nominal fee, you can complement your email campaign with a direct mail piece. The messaging concerns issues such as wealth and tax management, charitable giving, and estate and legacy planning.   <br/><b>Channel:</b> Email (2) Social (2) Web(2)<br/><b>Drips:</b> 2', N'https://s3.amazonaws.com/lplmodact/AIThumbSocial.png', N'AI1216.maml', N'read', NULL, NULL, 1)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (13, N'Focused on Retirement Email Campaign', N'A monthly email-only campaign looking at the issues critical to baby boomers. Topics include adjustments retirement savings efforts or investments, market volatility, Social Security issues, and more.  <br/><b>Channel:</b> Email<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/FORThumbEmail.png', N'MonthlyTopicRetirementQ4.maml', N'read', NULL, NULL, 2)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (14, N'Focused on Retirement Cross-Media Campaign', N'A monthly campaign looking at the issues critical to baby boomers. For a nominal fee, you can complement your email campaign with a direct mail piece. Topics include adjustments retirement savings efforts or investments, market volatility, Social Security issues, and more.  <br/><b>Channel:</b> Email (1) Direct Mail (2)<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/FORThumbCrossMedia.png', N'FOR_Print_1.maml', N'read', NULL, NULL, 2)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (15, N'Focused on Retirement Cross-Media Campaign - Jan 2017', N'A monthly campaign looking at the issues critical to baby boomers. Topics include adjustments retirement savings efforts or investments, market volatility, Social Security issues, and more.  <br/><b>Channel:</b> Email (2) Social (2) Web (2)<br/><b>Drips:</b> 2', N'https://s3.amazonaws.com/lplmodact/FORThumbSocial.png', N'FOR117.maml', N'read', NULL, NULL, 2)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (16, N'Investors Building Careers & Family', N'A monthly campaign spotlighting the concerns of those at mid-life. Discussions  include  retirement assumptions vs. reality, college planning, and improving household financial habits.<br/><b>Channel:</b> Email<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/CareerThumb.png', N'MonthlyTopicBuildingCareers.maml', N'read', NULL, NULL, 3)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (18, N'Investors Building Careers & Family', N'A monthly campaign spotlighting the concerns of those at mid-life. Discussions  include  retirement assumptions vs. reality, college planning, and improving household financial habits.<br/><b>Channel:</b> Email (2) Social (2) Web (2)<br/><b>Drips:</b> 2', N'https://s3.amazonaws.com/lplmodact/BCFThumbSocial.png', N'BCF117.maml', N'read', NULL, NULL, 3)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (20, N'Millennial Investors', N'This monthly campaign communicates the value of saving and investing for the future early in life. Messaging seeks to awaken millennials to ways to reduce debt and move financially ahead of their peers. Good lifetime financial habits are emphasized.<br/><b>Channel:</b> Email<br/><b>Drips:</b> 3', N'https://s3.amazonaws.com/lplmodact/MILThumbEmail.png', N'MonthlyTopicMillenial.maml', N'read', NULL, NULL, 4)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (21, N'Millennial Investors', N'This monthly campaign communicates the value of saving and investing for the future early in life. Messaging seeks to awaken millennials to ways to reduce debt and move financially ahead of their peers. Good lifetime financial habits are emphasized.<br/><b>Channel:</b> Email (2) Social (2) Web(2)<br/><b>Drips:</b> 2', N'https://s3.amazonaws.com/lplmodact/MILThumbSocial.png', N'MIL117.maml', N'read', NULL, NULL, 4)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (22, N'Why Work With a Financial Advisor?', N'This quarterly email-only campaign explains the value of consulting with a financial advisor. It notes what can potentially be gained from such a relationship - now and over time.<br/><b>Channel:</b> Email<br/><b>Drips:</b> 4', N'https://s3.amazonaws.com/lplmodact/WWWThumbEmail.png', N'FinancialAdvisorFAQ.maml', N'read', NULL, NULL, 11)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (23, N'Why Work With a Financial Advisor?', N'This quarterly email only campaign explains the value of consulting with a financial advisor. For a nominal fee, you can complement your email campaign with a direct mail piece. It notes what can potentially be gained from such a relationship - now and over time.<br/><b>Channel:</b> Email (2) Direct Mail (2)<br/><b>Drips:</b> 4', N'https://s3.amazonaws.com/lplmodact/WWWThumbCrossMedia.png', N'WWW_Print_1.maml', N'read', NULL, NULL, 11)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (24, N'Birthday Email Style 1', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb.png', N'HappyBirthday_Email_1.maml', N'read', NULL, NULL, 5)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (25, N'Birthday Email Style 2', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1"', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb2.png', N'HappyBirthday_Email_2.maml', N'read', NULL, NULL, 5)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (27, N'Birthday Email Style 3', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayThumb3.png', N'HappyBirthday_Email_3.maml', N'read', NULL, NULL, 5)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (28, N'Birthday Email Style 4', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb4.png', N'HappyBirthday_Email_4.maml', N'read', NULL, NULL, 5)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (30, N'Birthday Email Style 5', N'Send birthday emails to your contacts with birthdays on record. <br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb5.png', N'HappyBirthday_Email_5.maml', N'read', NULL, NULL, 5)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (31, N'Happy Birthday Greeting Card Style 1', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb.png', N'HappyBirthday_Print_1.maml', N'read', NULL, NULL, 6)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (32, N'Happy Birthday Greeting Card Style 2', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb2.png', N'HappyBirthday_Print_2.maml', N'read', NULL, NULL, 6)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (33, N'Happy Birthday Greeting Card Style 3', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayThumb3.png', N'HappyBirthday_Print_3.maml', N'read', NULL, NULL, 6)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (34, N'Happy Birthday Greeting Card Style 4', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb4.png', N'HappyBirthday_Print_4.maml', N'read', NULL, NULL, 6)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (35, N'Happy Birthday Greeting Card Style 5', N'<b>001-199</b> $2.41 each<br/><b>200-499</b>	$2.25 each<br/><b>500 +</b> $2.17 each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyBirthdayPrintThumb5.png', N'HappyBirthday_Print_5.maml', N'read', NULL, NULL, 6)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (36, N'Holiday Email Pack', N'Send various emails throughout the year for New Years, Fourth of July, Thanksgiving, and Holiday Season greetings. <br/><b>Channel:</b> Email <br/><b>Drips:</b> 4', N'https://s3.amazonaws.com/lplmodact/HolidayGreetingsEmailThumb.png', N'EmailOnlyHolidayCampaign.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (37, N'Happy New Year E-Card Style 1', N'Yearly scheduled email for the new year. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb1.png', N'HappyNewYear_Email_1.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (38, N'Happy New Year E-Card Style 2', N'Yearly scheduled email for the new year. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb2.png', N'HappyNewYear_Email_2.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (39, N'Happy New Year E-Card Style 3', N'Yearly scheduled email for the new year. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb3.png', N'HappyNewYear_Email_3.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (40, N'Independence Day E-Card Style 1', N'Yearly scheduled email for the Independence Day. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/IndependenceDayThumb1.png', N'IndependenceDay_Email_1.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (41, N'Independence Day E-Card Style 2', N'Yearly scheduled email for Independence Day. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/IndependenceDayThumb2.png', N'IndependenceDay_Email_2.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (42, N'Happy Thanksgiving E-Card Style 1', N'Yearly scheduled email for the Thanksgiving Holiday. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb1.png', N'HappyThanksgiving_Email_1.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (43, N'Happy Thanksgiving E-Card Style 2', N'Yearly scheduled email for the Thanksgiving Holiday. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb2.png', N'HappyThanksgiving_Email_2.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (44, N'Happy Thanksgiving E-Card Style 3', N'Yearly scheduled email for the Thanksgiving Holiday. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb3.png', N'HappyThanksgiving_Email_3.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (45, N'Happy Holidays E-Card Style 1', N'Yearly scheduled email for the Holiday Season. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHolidaysThumb1.png', N'HappyHolidays.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (46, N'Seasons Greetings E-Card Style 1', N'Yearly scheduled email for the Holiday Season. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/SeasonsGreetingsThumb1.png', N'SeasonsGreetings_Email_1.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (47, N'Happy Hanukkah E-Card Style 1', N'Yearly scheduled email for Hanukkah. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHanukkahThumb.png', N'HappyHanukkah_Email_1.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (48, N'Happy Hanukkah E-Card Style 2', N'Yearly scheduled email for Hanukkah. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHanukkahThumb2.png', N'HappyHanukkah_Email_2.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (50, N'Merry Chirstmas E-Card Style 1', N'Yearly scheduled email for the Christmas season. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/MerryChristmasThumb1.png', N'MerryChristmas_Email_1.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (51, N'Merry Chirstmas E-Card Style 2', N'Yearly scheduled email for the Christmas season. <br/><br/><br/><b>Channel:</b> Email <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/MerryChristmasThumb2.png', N'MerryChristmas_Email_2.maml', N'read', NULL, NULL, 7)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (52, N'Happy New Year Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb1.png', N'HappyNewYear_Print_1.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (53, N'Happy New Year Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb2.png', N'HappyNewYear_Print_2.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (54, N'Happy New Year Greeting Card Style 3', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyNewYearThumb3.png', N'HappyNewYear_Print_3.mamlread', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (56, N'Independence Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/IndependenceDayThumb1.png', N'IndependenceDay_Print_1.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (57, N'Independence Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/IndependenceDayThumb2.png', N'IndependenceDay_Print_2.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (58, N'Happy Thanksgiving Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb1.png', N'HappyThanks_Print_1.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (59, N'Happy Thanksgiving Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb2.png', N'HappyThanks_Print_2.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (60, N'Happy Thanksgiving Greeting Card Style 3', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyThanksThumb3.png', N'HappyThanks_Print_3.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (61, N'Happy Holidays Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHolidaysThumb1.png', N'HappyHolidays_Print_1.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (62, N'Seasons Greetings Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/SeasonsGreetingsThumb1.png', N'SeasonsGreetings_Print_1.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (63, N'Happy Hanukkah Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHanukkahThumb.png', N'HappyHaunaka_Print_1.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (64, N'Happy Hanukkah Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/HappyHanukkahThumb2.png', N'HappyHaunaka_Print_2.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (65, N'Merry Christmas Greeting Card Style 1', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/MerryChristmasThumb1.png', N'MerryChristmas_Print_1.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (66, N'Merry Christmas Greeting Card Style 2', N'<b>001-199</b> <strike>$2.41</strike> <span style=''color:red; font-weight:bold;''>$1.80</span> each<br/><b>200-499</b>	<strike>$2.25</strike> <span style=''color:red; font-weight:bold;''>$1.68</span> each<br/><b>500 +</b> <strike>$2.17</strike> <span style=''color:red; font-weight:bold;''>$1.62</span> each<br/><b>Channel:</b> Direct Mail <br/><b>Drips:</b> 1', N'https://s3.amazonaws.com/lplmodact/MerryChristmasThumb2.png', N'MerryChristmas_Print_2.maml', N'read', NULL, NULL, 8)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (67, N'LPL Letterhead', N'LPL Letterhead', N'https://s3.amazonaws.com/lplmodact/lpllogo.png', N'LPLLetterCampaign.maml', N'edit', NULL, NULL, 12)
GO
INSERT [dbo].[ctCampaigns] ([Id], [Name], [Description], [ThumbNail], [Maml], [Mode], [Feed], [Filter], [ctSubCategoryId]) VALUES (68, N'LPL Research Outlook', N'This bi-annual publication includes financial market forecasts, economic insights, and investment guidance for the year ahead.<br/><b>Channel:</b> RSS Email<br/><b>Drips:</b> 1, Bi-annual', N'https://s3.amazonaws.com/lplmodact/LPLResearchOutlookThumb.png', N'LinkedInPost.maml', N'rss', N'http://lpl-research.com/~rss/LPL_Financial_Research_Publications.xml', N'Outlook 2017', 9)
GO
SET IDENTITY_INSERT [dbo].[ctCampaigns] OFF
GO
INSERT [dbo].[ctSubscriptions] ([Id]) VALUES (N'GOLD')
GO
INSERT [dbo].[ctSubscriptions] ([Id]) VALUES (N'PLATINUM')
GO
INSERT [dbo].[ctSubscriptions] ([Id]) VALUES (N'PLATINUMPLUS')
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 2)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 3)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 4)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 5)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 6)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 8)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 9)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 10)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 12)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 13)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 14)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 15)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 16)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 18)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 20)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 21)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 31)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 32)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 33)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 34)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 35)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 36)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 37)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 38)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 39)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 40)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 41)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 42)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 43)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 44)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 45)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 46)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 47)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 48)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 50)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 51)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 52)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 53)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 54)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 56)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 57)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 58)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 59)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 60)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 61)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 62)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 63)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 64)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 65)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 66)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'GOLD', 68)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 1)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 2)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 3)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 4)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 5)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 6)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 7)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 8)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 9)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 10)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 12)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 13)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 14)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 15)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 16)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 18)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 20)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 21)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 22)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 23)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 24)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 25)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 27)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 28)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 30)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 31)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 32)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 33)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 34)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 35)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 36)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 37)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 38)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 39)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 40)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 41)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 42)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 43)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 44)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 45)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 46)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 47)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 48)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 50)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 51)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 52)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 53)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 54)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 56)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 57)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 58)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 59)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 60)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 61)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 62)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 63)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 64)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 65)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 66)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 67)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUM', 68)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 1)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 2)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 3)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 4)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 5)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 6)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 7)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 8)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 9)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 10)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 12)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 13)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 14)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 15)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 16)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 18)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 20)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 21)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 22)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 23)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 24)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 25)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 27)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 28)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 30)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 31)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 32)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 33)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 34)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 35)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 36)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 37)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 38)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 39)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 40)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 41)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 42)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 43)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 44)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 45)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 46)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 47)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 48)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 50)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 51)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 52)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 53)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 54)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 56)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 57)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 58)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 59)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 60)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 61)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 62)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 63)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 64)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 65)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 66)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 67)
GO
INSERT [dbo].[ctSubscriptionctCampaigns] ([ctSubscription_Id], [ctCampaign_Id]) VALUES (N'PLATINUMPLUS', 68)
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplabs')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccdual')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccho')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplcchov')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccias')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccis')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccmgp')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrbg')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrpias')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccrpis')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenters] ([Id]) VALUES (N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplcchov')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctCategories] ([ctCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplabs')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccho')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (12, N'lplcchov')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccmgp')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrbg')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrecruiting')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrpdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrpias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccrpis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccwfsdual')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccwfsias')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccwfsis')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplccworksiteadmin')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (1, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (2, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (3, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (4, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (5, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (6, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (7, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (8, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (9, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (10, N'lplincadvisors')
GO
INSERT [dbo].[ctCostCenterctSubCategories] ([ctSubCategory_Id], [ctCostCenter_Id]) VALUES (11, N'lplincadvisors')
GO
