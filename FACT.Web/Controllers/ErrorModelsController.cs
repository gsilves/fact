﻿using FACT.Service;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Runtime.InteropServices;
using System.Web;
using System.Web.Mvc;
using static FACT.Web.Models.ErrorViewModel;
using FACT.Web.Business.Mappings;
using System.Data.Entity;

namespace FACT.Web.Controllers
{
    public class ErrorModelsController : Controller
    {
        // GET: ErrorModelsController
        private readonly IFactMainService _factMainService;
        bool disposed = false;

        public ErrorModelsController(IFactMainService factMainService)
        {
            _factMainService = factMainService;
        }

        public ActionResult ShowError(string title, string message, string type, string returnUrl, bool logError = false)
        {
            return RedirectToAction("ErrorView", "ErrorModels", new
            {
                title = title,
                message = message,
                type = type,
                returnUrl = returnUrl,
                logError = logError
            });
        }

        public ActionResult ShowException(Exception e, string returnUrl)
        {
            return RedirectToAction("ErrorView", "ErrorModels", new
            {
                title = "Exception",
                message = e.Message,
                type = "exception",
                returnUrl = returnUrl,
                logError = Convert.ToBoolean(ConfigurationManager.AppSettings["LogErrors"])
            });
        }


        public ActionResult ShowInfo(string title, string message, string type, string returnUrl)
        {
            return RedirectToAction("InfoView", "ErrorModels", new
            {
                title = title,
                message = message,
                type = type,
                returnUrl = returnUrl
            });
        }

        public ActionResult InfoView(string title, string message, string type, string returnUrl)
        {
            ErrorModel model = new ErrorModel(title, message, type, returnUrl);
            return View(model);
        }


        public ActionResult ErrorView(string title, string message, string type, string returnUrl, bool logError = false)
        {
            ErrorModel model = new ErrorModel(title, message, type, returnUrl);
            if (logError)
            {
                _factMainService.AddErrorModel(model.ToErrorModels());
            }
            return View(model);
        }

        // GET: ErrorModels
        public ActionResult Index()
        {
            return View(_factMainService.FindAllErrorModels());
        }

        // GET: ErrorModels/Details/5
        public ActionResult Details(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ErrorModel errorModel = _factMainService.FindErrorModelById(id).ToErrorModel();
            if (errorModel == null)
            {
                return HttpNotFound();
            }
            return View(errorModel);
        }

        // GET: ErrorModels/Create
        public ActionResult Create()
        {
            return View();
        }

        // POST: ErrorModels/Create
        //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Create([Bind(Include = "Id,Title,Message,Type,Timestamp")] ErrorModel errorModel)
        {
            if (ModelState.IsValid)
            {
                _factMainService.AddErrorModel(errorModel.ToErrorModels());
                return RedirectToAction("Index");
            }
            return View(errorModel);
        }

        // GET: ErrorModels/Edit/5
        public ActionResult Edit(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ErrorModel errorModel = _factMainService.FindErrorModelById(id).ToErrorModel();
            if (errorModel == null)
            {
                return HttpNotFound();
            }
            return View(errorModel);
        }

        // POST: ErrorModels/Edit/5
        //To protect from overposting attacks, please enable the specific properties you want to bind to, for 
        //more details see https://go.microsoft.com/fwlink/?LinkId=317598.
        [HttpPost]
        [ValidateAntiForgeryToken()]
        public ActionResult Edit([Bind(Include = "Id,Title,Message,Type,Timestamp")] ErrorModel errorModel)
        {
            if (ModelState.IsValid)
            {
                //db.Entry(errorModel).State = EntityState.Modified;
                _factMainService.UpdateErrorModels(errorModel.ToErrorModels());
                return RedirectToAction("Index");
            }
            return View(errorModel);
        }

        // GET: ErrorModels/Delete/5
        public ActionResult Delete(int id)
        {
            if (id == 0)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            ErrorModel errorModel = _factMainService.FindErrorModelById(id).ToErrorModel();
            if (errorModel == null)
            {
                return HttpNotFound();
            }
            return View(errorModel);
        }

        // POST: ErrorModels/Delete/5
        [HttpPost()]
        [ActionName("Delete")]
        [ValidateAntiForgeryToken()]
        public ActionResult DeleteConfirmed(int id)
        {
            ErrorModel errorModel = _factMainService.FindErrorModelById(id).ToErrorModel();
            _factMainService.DeleteErrorModels(errorModel.ToErrorModels());
            return RedirectToAction("Index");
        }

        protected override void Dispose(bool disposing)
        {
            if (!disposed)
            {
                if (disposing)
                {

                }
                base.Dispose(disposing);
            }
            disposed = true;
        }
    }
}