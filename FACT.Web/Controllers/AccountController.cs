﻿using Dapper;
using FACT.Domain.Models;
using FACT.Domain.Models.EntityModels;
using FACT.Domain.Models.MiscModels;
using FACT.Service;
using FACT.Web.Business;
using FACT.Web.Business.Initialization;
using FACT.Web.Enums;
using FACT.Web.Models;
using FACT.Web.Models.AccountViewModels;
using FACT.Web.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Data.SqlClient;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FACT.Web.Controllers
{
    public class AccountController : Controller
    {
        private ApplicationSignInManager _signInManager;
        private ApplicationUserManager _userManager;
        private IFactMainService _factMainService;

        public string LogInProvider { get; set; }
        public string RedirectUri { get; set; }
        public string UserId { get; set; }
        protected ApplicationDbContext db = new ApplicationDbContext();

        public GlobalX objConnection = new GlobalX();

        public AccountController(ApplicationSignInManager signInManager, ApplicationUserManager userManager, IFactMainService factMainService)
        {
            _signInManager = signInManager;
            _userManager = userManager;
            _factMainService = factMainService;
        }

        public AccountController(string provider, string redirect, string user)
        {
            LogInProvider = provider;
            RedirectUri = redirect;
            UserId = user;
        }

        // GET: Account
        public ActionResult Index()
        {
            return View();
        }

        protected ApplicationUserManager UserManager
        {
            get
            {
                return _userManager ?? HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        protected ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? HttpContext.GetOwinContext().Get<ApplicationSignInManager>();
            }
            set
            {
                _signInManager = value;
            }
        }

        // GET: /Account/Login
        [AllowAnonymous]
        public async Task<ActionResult> Login(string returnUrl)
        {
            SSOAccountService sso = new SSOAccountService();
            string repId = "";
            if (Request.QueryString["repid"] == null || Request.QueryString["action"] == null)
            {
                return View();
            }
            else if (!Request.QueryString["action"].Equals("sso"))
            {
                return View();
            }
            else
            {
                //first step find repid in old database RackSpaceDB in table LPLDataFeed
                //use dapper to connect to the sql server
                repId = Request.QueryString["repid"].ToString();
                ResultViewModel result = new ResultViewModel();
                IDbConnection _db1 = new SqlConnection(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString);
                var settingsResult = _db1.Query("SELECT ApprovalRequired, IsCreditChargeable, IsSubscriptionSignable " +
                                       "FROM Settings ").FirstOrDefault();
                SettingsShared.Id = settingsResult.Id;
                SettingsShared.ApprovalRequired = settingsResult.ApprovalRequired;
                SettingsShared.IsCreditChargeable = settingsResult.IsCreditChargeable;
                SettingsShared.IsSubscriptionSignable = settingsResult.IsSubscriptionSignable;
                IDbConnection _db2 = new SqlConnection(ConfigurationManager.ConnectionStrings["gstrConnLPLDataFeed"].ConnectionString);
                var dapperResult = _db2.Query("SELECT RepId, FirstName, LastName, Address1, Address2, City, State, Zip, CostCenterNumber, Email, RIAName " +
                                       "FROM lpluserswithcostcenter WHERE RepId = @repId", new { RepId = repId }).FirstOrDefault();
                if (dapperResult == DBNull.Value || dapperResult == null)
                {
                    IDbConnection _db23 = new SqlConnection(ConfigurationManager.ConnectionStrings["gstrConnCOM"].ConnectionString);
                    StringBuilder sqlScript = new StringBuilder();
                    sqlScript.Append("Select LoginAccount.Username, Employee.EmpId As RepId, Employee.NameFirstName As FirstName, ");
                    sqlScript.Append("Employee.NameSurname As LastName, Employee.Email, CostCentre.CostCentreNumber As CostCenterNumber, CostCentreAddress.Address1, ");
                    sqlScript.Append("CostCentreAddress.Address2, CostCentreAddress.Suburb As City, CostCentreAddress.[State], CostCentreAddress.Postcode As Zip ");
                    sqlScript.Append("FROM Employee ");
                    sqlScript.Append("INNER JOIN CostCentre On CostCentre.CostCentre_Id = Employee.CostCentre_Id ");
                    sqlScript.Append("INNER JOIN Customer On Customer.Customer_Id = CostCentre.Customer_Id ");
                    sqlScript.Append("INNER JOIN LoginAccount On LoginAccount.LoginAccount_Id = Employee.LoginAccount_Id ");
                    sqlScript.Append("LEFT OUTER JOIN CostCentreAddress On CostCentreAddress.Employee_Id =Employee.Employee_Id ");
                    sqlScript.Append("WHERE Employee.EmpId Is Not NULL And Employee.Active = //Yes// AND LoginAccount.Username = //" + repId + "//");
                    SqlConnection conn = new SqlConnection(ConfigurationManager.ConnectionStrings["gstrConnCOM"].ConnectionString);
                    conn.Open();
                    SqlCommand cmd = new SqlCommand(sqlScript.ToString(), conn);
                    cmd.CommandType = CommandType.Text;
                    using (SqlDataReader rdr = cmd.ExecuteReader())
                    {
                        while (rdr.Read())
                        {
                            result.Username = rdr["Username"] == DBNull.Value ? "" : rdr["Username"].ToString();
                            result.RepId = rdr["Username"] == DBNull.Value ? "" : rdr["Username"].ToString();
                            result.CostCenterNumber = rdr["CostCenterNumber"] == DBNull.Value ? "" : rdr["CostCenterNumber"].ToString();
                            result.Email = rdr["Email"] == DBNull.Value ? "" : rdr["Email"].ToString();
                            result.FirstName = rdr["FirstName"] == DBNull.Value ? "" : rdr["FirstName"].ToString();
                            result.LastName = rdr["LastName"] == DBNull.Value ? "" : rdr["LastName"].ToString();
                            result.Address1 = rdr["Address1"] == DBNull.Value ? "" : rdr["Address1"].ToString();
                            result.Address2 = rdr["Address2"] == DBNull.Value ? "" : rdr["Address2"].ToString();
                            result.City = rdr["City"] == DBNull.Value ? "" : rdr["City"].ToString();
                            result.State = rdr["State"] == DBNull.Value ? "" : rdr["State"].ToString();
                            result.Zip = rdr["Zip"] == DBNull.Value ? "" : rdr["Zip"].ToString();
                        }
                    }
                }
                else
                {
                    result.Username = dapperResult.Username;
                    result.RepId = dapperResult.RepId;
                    result.CostCenterNumber = dapperResult.CostCenterNumber;
                    result.Email = dapperResult.Email;
                    result.FirstName = dapperResult.FirstName;
                    result.LastName = dapperResult.LastName;
                    result.Address1 = dapperResult.Address1;
                    result.Address2 = dapperResult.Address2;
                    result.City = dapperResult.City;
                    result.State = dapperResult.State;
                    result.Zip = dapperResult.Zip;
                    result.RIAName = dapperResult.RIAName;
                }
                LoginViewModel loginM = null;
                if (result != null)
                {
                    //user has been found in old database proceed to search user ( in the Amazon DB Server
                    string userAmazonDb = result.CostCenterNumber + result.RepId;
                    ApplicationUser samlUser = UserManager.FindByName(userAmazonDb);
                    if (samlUser != null)
                    {
                        //user found in Amazon DB goto Account/Index
                        loginM = new LoginViewModel
                        {
                            Email = samlUser.Email,
                            Password = userAmazonDb + "_Connect101",
                            RememberMe = true
                        };
                        var resultSignIn = SignInManager.PasswordSignIn(userAmazonDb, loginM.Password, loginM.RememberMe, shouldLockout: false);
                        switch (resultSignIn)
                        {
                            case SignInStatus.Success:
                                return RedirectToAction("Index", "Home");
                            default:
                                ModelState.AddModelError("", "Login Failed");
                                break;
                        }
                    }
                    else
                    {
                        //user not in Amazon DB proceed to Create SSO User
                        string strPassword = userAmazonDb + "_Connect101";
                        Profiles ssoModel = new Profiles();
                        ssoModel.Email = result.Email;
                        ssoModel.RepCode = result.RepId;
                        ssoModel.FirstName = result.FirstName;
                        ssoModel.LastName = result.LastName;
                        ssoModel.MailingAddressLine1 = result.Address1;
                        ssoModel.MailingAddressLine2 = result.Address2;
                        ssoModel.MailingCity = result.City;
                        ssoModel.MailingState = result.State;
                        ssoModel.MailingZip = result.Zip;
                        ssoModel.ApprovedRIAName = result.RIAName;
                        ssoModel.UserName = userAmazonDb;
                        ssoModel.AGUserId = userAmazonDb;
                        ssoModel.UserType = "Registered Advisor";
                        ssoModel.ExternalID = result.RepId;
                        bool aspNetUsersModified = false;
                        bool profileModified = false;
                        bool authorizeNetModified = false;
                        bool mindfireModified = false;
                        bool contactsModified = false;
                        bool companyLogoUploaded = false;
                        bool companyLogoSelected = false;
                        bool profilePhotoUploaded = false;
                        bool signatureImageUploaded = false;
                        List<KeyValuePair<string, string>> list = new List<KeyValuePair<String, String>>();
                        SSOAccountResult resultSSO = new SSOAccountResult();
                        ApplicationUser user = new ApplicationUser()
                        {
                            UserName = userAmazonDb,
                            Email = result.Email
                        };
                        AuthorizeNetService authorizeNetService = new AuthorizeNetService();
                        AmazonS3Service awsService = new AmazonS3Service();
                        MindfireFunctions objMF = new MindfireFunctions();
                        int? intSubaccount = -1;
                        try
                        {
                            // AspNetUsers user Creation
                            IdentityResult createUserResult;
                            createUserResult = await UserManager.CreateAsync(user, strPassword);
                            if (createUserResult.Succeeded)
                            {
                                aspNetUsersModified = true;
                                ssoModel.UserId = user.Id;
                                ssoModel.DisclosureText = ControllerFunctions.generateDisclosure(ssoModel, _factMainService);
                                //Making sure that model.ApprovedRIAName is empty if IAType is "Registered Representative" or "Corporate IA"
                                if (ssoModel.IAType == null || ssoModel.IAType == (int)IAType.NONE || ssoModel.IAType == (int)IAType.CRIA)
                                {
                                    ssoModel.ApprovedRIAName = "";
                                }
                                // Profile creation
                                ssoModel.Email = result.Email;
                                ssoModel.RepCode = userAmazonDb.Substring(userAmazonDb.Length - 4);
                                _factMainService.AddProfile(ssoModel);
                                profileModified = true;
                                //AuthorizeNet CreateCustomer
                                var anResult = authorizeNetService.CreateCustomer(ssoModel.Email, user.Id);
                                if (anResult.messages.resultCode == AuthorizeNet.Api.Contracts.V1.messageTypeEnum.Ok)
                                {
                                    authorizeNetModified = true;
                                    user.AuthorizeNetId = Convert.ToInt32(anResult.customerProfileId);
                                    await UserManager.UpdateAsync(user);
                                    bool isMFUser = objMF.CheckMindfireUser(ssoModel.Email, strPassword);
                                    if (!isMFUser)
                                    {
                                        //Create Mindfire account
                                        //*** add web config key to LPL Financial or Database
                                        intSubaccount = (int)objMF.CreateMFSubAccount("LPL Financial-" + ssoModel.AGUserId + ssoModel.RepCode,
                                            ssoModel.Email, ssoModel.FirstName, ssoModel.LastName, strPassword);
                                        if (intSubaccount != null)
                                        {
                                            mindfireModified = true;
                                            ssoModel.MFAccountID = intSubaccount.ToString();
                                            objMF.SetAuthorizedPartner();
                                            //*** same for advisormailbox put into some kind of configuration
                                            objMF.AddDomain(ssoModel.MFAccountID + ".advisormailbox.com");
                                        }
                                        else
                                        {
                                            ModelState.AddModelError("", "Campaign Engine Account Error. Please contact support for assistance.");
                                            intSubaccount = -1;
                                        }
                                    }
                                    if (ModelState.IsValid)
                                    {
                                        objMF.AuthenticateAccount(ssoModel.Email, strPassword, ref ssoModel);
                                        //upload images to S3
                                        if (ssoModel.IAType == null)
                                        {
                                            ssoModel.IAType = (int)IAType.NONE;
                                        }
                                        if (!string.IsNullOrEmpty(ssoModel.CompanyLogo))
                                        {
                                            string[] stringSeparators = { AmazonS3Service.Bucket + "/" };
                                            var a = ssoModel.CompanyLogo.Split(stringSeparators, StringSplitOptions.None);
                                            if (a.Length > 1)
                                            {
                                                ssoModel.CompanyLogo = a[1];
                                            }
                                            companyLogoUploaded = true;
                                            companyLogoSelected = true;
                                        }
                                        else if (ssoModel.CompanyLogoFile != null)
                                        {
                                            var resultA = awsService.UploadCompanyLogoImage(ssoModel.CompanyLogoFile);
                                            if (resultA.Successful)
                                            {
                                                ssoModel.CompanyLogo = resultA.Path;
                                                companyLogoUploaded = true;
                                            }
                                            else
                                            {
                                                ModelState.AddModelError("", resultA.Message);
                                            }
                                        }
                                        else
                                        {
                                        }
                                        if (ssoModel.ProfilePhotoFile != null)
                                        {
                                            var resultA = awsService.UploadProfileImage(ssoModel.ProfilePhotoFile);
                                            if (resultA.Successful)
                                            {
                                                ssoModel.ProfilePhoto = resultA.Path;
                                                profilePhotoUploaded = true;
                                            }
                                            else
                                            {
                                                ModelState.AddModelError("", resultA.Message);
                                            }
                                        }
                                        if (ssoModel.SignatureImageFile != null)
                                        {
                                            var resultA = awsService.UploadSignatureImage(ssoModel.SignatureImageFile);
                                            if (resultA.Successful)
                                            {
                                                ssoModel.SignatureImage = resultA.Path;
                                                signatureImageUploaded = true;
                                            }
                                            else
                                            {
                                                ModelState.AddModelError("", resultA.Message);
                                            }
                                        }
                                        //Update profile properties in DB
                                        _factMainService.UpdateProfiles(ssoModel);
                                        //Create contact with same info as new User
                                        MyContactsModel myContactsModel = new MyContactsModel();
                                        myContactsModel.Email = (ssoModel.Email == null) ? "" : ssoModel.Email;
                                        myContactsModel.AccountID = (ssoModel.MFAccountID == null) ? "" : ssoModel.MFAccountID;
                                        myContactsModel.Address1 = (ssoModel.BranchAddressLine1 == null) ? "" : ssoModel.BranchAddressLine1;
                                        myContactsModel.Address2 = (ssoModel.BranchAddressLine2 == null) ? "" : ssoModel.BranchAddressLine2;
                                        myContactsModel.City = (ssoModel.BranchCity == null) ? "" : ssoModel.BranchCity;
                                        myContactsModel.Company = (ssoModel.Company == null) ? "" : ssoModel.Company;
                                        myContactsModel.FacebookAccount = (ssoModel.FacebookURL == null) ? "" : ssoModel.FacebookURL;
                                        myContactsModel.Fax = (ssoModel.BranchFaxNumber == null) ? "" : ssoModel.BranchFaxNumber;
                                        myContactsModel.FirstName = (ssoModel.FirstName == null) ? "" : ssoModel.FirstName;
                                        myContactsModel.Gender = (ssoModel.Gender == null) ? "" : ssoModel.Gender;
                                        myContactsModel.LastName = (ssoModel.LastName == null) ? "" : ssoModel.LastName;
                                        myContactsModel.LinkedInAccount = (ssoModel.LinkedInURL == null) ? "" : ssoModel.LinkedInURL;
                                        myContactsModel.MiddleName = (ssoModel.MiddleName == null) ? "" : ssoModel.MiddleName;
                                        myContactsModel.Phone = (ssoModel.BranchPhone == null) ? "" : ssoModel.BranchPhone;
                                        myContactsModel.PhotoURL = (ssoModel.ProfilePhoto == null) ? "" : ssoModel.ProfilePhoto;
                                        myContactsModel.State = (ssoModel.BranchState == null) ? "" : ssoModel.BranchState;
                                        myContactsModel.Title = (ssoModel.JobTitle == null) ? "" : ssoModel.JobTitle.ToString();
                                        myContactsModel.TwitterAccount = (ssoModel.TwitterURL == null) ? "" : ssoModel.TwitterURL;
                                        myContactsModel.Website = (ssoModel.Website == null) ? "" : ssoModel.Website;
                                        myContactsModel.Zip = (ssoModel.BranchZip == null) ? "" : ssoModel.BranchZip;
                                        myContactsModel.PURL = (ssoModel.FirstName == null) ? "" : (ssoModel.FirstName + ssoModel.LastName == null) ? "" : ssoModel.LastName;
                                        foreach (PropertyInfo p in myContactsModel.GetType().GetProperties())
                                        {
                                            if (p.CanRead)
                                            {
                                                var val = p.GetValue(myContactsModel);
                                                string name = p.Name;
                                                KeyValuePair<string, string> pair = new KeyValuePair<String, String>(name, val.ToString());
                                                list.Add(pair);
                                            }
                                        }
                                        var x = objMF.CreateContact(ssoModel.MFTicket, strPassword, ssoModel.MFMasterPartnerTicket, list);
                                        if (String.IsNullOrEmpty(x["Result"]["ErrorCode"].ToString()))
                                        {
                                            contactsModified = true;
                                            JToken contactId = x["ContactID"];
                                            JToken purl = x["Purl"];
                                        }
                                        else
                                        {
                                            string errMsg = x["Result"]["ErrorMessage"].ToString();
                                            ModelState.AddModelError("", errMsg);
                                        }
                                        user.AuthorizeNetId = Convert.ToInt32(anResult.customerProfileId);
                                        user.Active = true;
                                        await UserManager.UpdateAsync(user);
                                        await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                                        return RedirectToAction("Index", "Home");
                                    }
                                    else
                                    {
                                        //Profile and AspNetUsers records created, AuthorizeNet Customer created
                                        if (profileModified)
                                        {
                                            _factMainService.DeleteProfile(ssoModel);
                                        }
                                        if (aspNetUsersModified)
                                        {
                                            await UserManager.DeleteAsync(user);
                                        }
                                        var response = authorizeNetService.DeleteCustomer(Convert.ToInt32(anResult.customerProfileId));
                                        return View("acs", ssoModel);
                                    }
                                }
                                else
                                {
                                    //AuthorizeNet.CreateCustomer Unsuccessfull. 
                                    //Profile and AspNetUsers records created
                                    if (profileModified)
                                    {
                                        _factMainService.DeleteProfile(ssoModel);
                                    }
                                    if (aspNetUsersModified)
                                    {
                                        await UserManager.DeleteAsync(user);
                                    }
                                    AddErrors(new IdentityResult(new List<String>
                                    {
                                        anResult.messages.resultCode.ToString()
                                    }));
                                    return View();
                                }
                            }
                            else
                            {
                                //AspNetUsers new user creation failed.
                                //No other changes yet.
                                AddErrors(createUserResult);
                                return View();
                            }
                        }
                        catch (Exception ex)
                        {
                            //ModelState.AddModelError("", ex.StackTrace)
                            if (authorizeNetModified)
                            {
                                authorizeNetService.DeleteCustomer((int)user.AuthorizeNetId);
                            }
                            if (profileModified)
                            {
                                _factMainService.DeleteProfile(ssoModel);
                            }
                            if (aspNetUsersModified)
                            {
                                UserManager.Delete(user);
                            }
                            if (contactsModified)
                                objMF.DeleteContact(ssoModel.MFTicket, 
                                    ssoModel.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])),
                                    ssoModel.MFMasterPartnerTicket, list);
                            if (companyLogoUploaded)
                            {
                                if (!companyLogoSelected)
                                {
                                    awsService.DeleteCompanyLogoImage(ssoModel.CompanyLogo);
                                }
                                ssoModel.CompanyLogo = "";
                            }
                            if (profilePhotoUploaded)
                            {
                                awsService.DeleteProfileImage(ssoModel.ProfilePhoto);
                                ssoModel.ProfilePhoto = "";
                            }
                            if (signatureImageUploaded)
                            {
                                awsService.DeleteSignatureImage(ssoModel.SignatureImage);
                                ssoModel.SignatureImage = "";
                            }
                            if (mindfireModified)
                            {
                                ModelState.AddModelError("", "Mindfire Account Created is not linked to any Profile! MF Account Id: " + intSubaccount);
                            }
                            ModelState.AddModelError("", ex);
                            // *** return errController.ShowException(ex, "back");
                            return RedirectToAction("ErrorView", "ErrorModels", new
                            {
                                title = "Exception",
                                message = ex.Message,
                                type = "exception",
                                returnUrl = returnUrl,
                                logError = Convert.ToBoolean(ConfigurationManager.AppSettings["LogErrors"])
                            });
                        }
                    }
                }
                else
                {
                    //user not found show error
                    ModelState.AddModelError("", "Login Failed");
                }
            }
            ViewData["ReturnUrl"] = returnUrl;
            Session.Clear();
            return View();
        }

        [AllowAnonymous]
        public async Task<JsonResult> LoginDelegateAjax(string Username)
        {
            ApplicationUser userId = await UserManager.FindByNameAsync(Username);
            if (userId != null)
            {
                bool delegateExists = _factMainService.FindDelegatesById(userId.Id) != null ? true : false;
                if (delegateExists)
                {
                    List<Delegates> delegates = _factMainService.FindAllDelegates();
                    int delegateCount = delegates.Count();
                    if (delegateCount > 1)
                    {
                        List<Profiles> profiles = _factMainService.FindAllProfiles();
                        // ***
                        //var userList = from d in delegates
                        //               join u in db.Users on d.ParentId equals u.Id
                        //               join p in profiles on p.UserId equals u.Id
                        //               where d.AccountId = userId.Id
                        //               select new { Id = u.Id, UserName = p.FirstName + " " + p.LastName };
                        var userList = from d in delegates
                                       from u in db.Users.Where(q => q.Id == d.ParentId)
                                       from p in profiles.Where(q => q.UserId == u.Id)
                                       where d.AccountId == userId.Id
                                       select new
                                       {
                                           Id = u.Id,
                                           UserName = p.FirstName + " " + p.LastName
                                       };
                        return Json(new { Success = true, List = userList }, JsonRequestBehavior.AllowGet);
                    }
                }
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(new { Success = false }, JsonRequestBehavior.AllowGet);
            }
        }

        // POST: /Account/Login
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Login(LoginViewModel model, string returnUrl)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // This doesn't count login failures towards account lockout
            // To enable password failures to trigger account lockout, change to shouldLockout := True
            List<ApplicationUser> Users = new List<ApplicationUser>();
            var userActive = Users.SingleOrDefault(u => u.UserName == model.Username).Active;
            if (userActive)
            {
                var result = await SignInManager.PasswordSignInAsync(model.Username, model.Password, model.RememberMe, shouldLockout: false);
                switch (result)
                {
                    case SignInStatus.Success:
                        var userId = await UserManager.FindByNameAsync(model.Username);
                        List<Delegates> delegates = _factMainService.FindAllDelegates();
                        bool delegateExists = delegates.Count() > 0 ? true : false;
                        if (delegateExists)
                        {
                            int delegateCount = delegates.Count(p => p.AccountId == userId.Id);
                            Delegates objDelegate;
                            if (model.ParentIdSelected != null)
                            {
                                objDelegate = delegates.Where(p => p.AccountId == userId.Id).Where(p => p.ParentId == model.ParentIdSelected).SingleOrDefault();
                            }
                            else
                            {
                                objDelegate = delegates.SingleOrDefault(p => p.AccountId == userId.Id);
                            }
                            ApplicationUser masterUser = await UserManager.FindByIdAsync(objDelegate.ParentId);
                            Profiles objProfile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == masterUser.Id);
                            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                            string strPAssword = objProfile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"]));
                            //Below is constant pasword of Dannicum, we need logic to make password
                            var delegateResult = await SignInManager.PasswordSignInAsync(masterUser.UserName, strPAssword, true, shouldLockout: false);
                            switch (delegateResult)
                            {
                                case SignInStatus.Success:
                                    Session["Delegate"] = true;
                                    if (!User.IsInRole("HomeOffice") && userId.AuthorizeNetId != null && 
                                        !ControllerFunctions.HasSubscriptions(userId.AuthorizeNetId.ToString()))
                                    {
                                        if (userId.Created == null)
                                        {
                                            userId.Created = DateTime.Now;
                                        }
                                        if (DateTime.Now > new DateTime(2018, 3, 31))
                                        {
                                            DateTime date1 = (DateTime)userId.Created;
                                            int diff = (int)((date1 - DateTime.Now).TotalDays);
                                            if (diff > Convert.ToInt32(ConfigurationManager.AppSettings["DeadlineForSubscription"]))
                                            {
                                                ControllerFunctions.StopAllCampaigns(Convert.ToInt64(userId.Id), _factMainService);
                                                string message = "<h1>You don't have any active subscription for 60 days, all your campaigns are stoped!</p>";
                                                await objConnection.SendMail("helpdesk@agmycmo.com", "MyCMO Notifications", 
                                                    "You don't have any active subscription.", false, message, userId.Email, null);
                                            }
                                        }
                                    }
                                    return RedirectToLocal(returnUrl);
                                default:
                                    ModelState.AddModelError("", "Invalid delegate login attempt.");
                                    return View(model);
                            }
                        }
                        else
                        {
                            Session["Delegate"] = false;
                            if (!User.IsInRole("HomeOffice") && userId.AuthorizeNetId != null && 
                                !ControllerFunctions.HasSubscriptions(userId.AuthorizeNetId.ToString()))
                            {
                                if (userId.Created == null)
                                {
                                    userId.Created = DateTime.Now;
                                }
                                if (DateTime.Now > new DateTime(2018, 3, 31))
                                {
                                    DateTime date1 = (DateTime)userId.Created;
                                    int diff = (int)(date1 - DateTime.Now).TotalDays;
                                    if (diff > Convert.ToInt32(ConfigurationManager.AppSettings["DeadlineForSubscription"]))
                                    {
                                        ControllerFunctions.StopAllCampaigns(Convert.ToInt64(userId.Id), _factMainService);
                                        string message = "<h1>You don't have any active subscription for 60 days, all your campaigns are stoped!</p>";
                                        await objConnection.SendMail("helpdesk@agmycmo.com", "MyCMO Notifications", "You don't have any active subscription.",
                                            false, message, userId.Email, null);
                                    }
                                }
                            }
                            return RedirectToLocal(returnUrl);
                        }
                    case SignInStatus.LockedOut:
                        return View("Lockout");
                    case SignInStatus.RequiresVerification:
                        return RedirectToAction("SendCode", new
                        {
                            ReturnUrl = returnUrl,
                            RememberMe = model.RememberMe
                        });
                    default:
                        ModelState.AddModelError("", "Invalid login attempt.");
                        return View(model);
                }
            }
            else
            {
                ModelState.AddModelError("", "User isn't active");
                return View(model);
            }
        }

        // GET: /Account/VerifyCode
        [AllowAnonymous]
        public async Task<ActionResult> VerifyCode(string provider, string returnUrl, bool rememberMe)
        {
            // Require that the user has already logged in via username/password or external login
            if (!await SignInManager.HasBeenVerifiedAsync())
            {
                return View("Error");
            }
            return View(new VerifyCodeViewModel()
            {
                Provider = provider,
                ReturnUrl = returnUrl,
                RememberMe = rememberMe
            });
        }

        // POST: /Account/VerifyCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> VerifyCode(VerifyCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            // The following code protects for brute force attacks against the two factor codes. 
            // If a user enters incorrect codes for a specified amount of time then the user account 
            // will be locked out for a specified amount of time. 
            // You can configure the account lockout settings in IdentityConfig
            var result = await SignInManager.TwoFactorSignInAsync(model.Provider, model.Code, isPersistent: model.RememberMe, 
                rememberBrowser: model.RememberBrowser);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(model.ReturnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                default:
                    ModelState.AddModelError("", "Invalid code.");
                    return View(model);
            }
        }

        // GET: /Account/Register
        [AllowAnonymous]
        public ActionResult Register()
        {
            return View();
        }

        // POST: /Account/Register
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> Register(RegisterViewModel model)
        {
            if (ModelState.IsValid)
            {
                if (objConnection.CheckDomain(model.Email, _factMainService) > 0)
                {
                    var user = new ApplicationUser()
                    {
                        UserName = model.Email,
                        Email = model.Email
                    };
                    var createUserResult = await UserManager.CreateAsync(user, model.Password);
                    //If the creation of local user account has worked properly, we try to create an account on Auth.net.
                    if (createUserResult.Succeeded)
                    {
                        AuthorizeNetService authorizeNetService = new AuthorizeNetService();
                        var anResult = authorizeNetService.CreateCustomer(model.Email, user.Id);
                        // If user creation on Auth.net has failed, we need to delete local account and show an error to user.
                        if (!(anResult.messages.resultCode == AuthorizeNet.Api.Contracts.V1.messageTypeEnum.Ok))
                        {
                            await UserManager.DeleteAsync(user);
                            AddErrors(new IdentityResult(new List<String>
                        {
                            anResult.messages.resultCode.ToString()
                        }));
                            return View(model);
                        }
                        else
                        {
                            user.AuthorizeNetId = Convert.ToInt32(anResult.customerProfileId);
                            await UserManager.UpdateAsync(user);
                            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                            return RedirectToAction("Index", "Home");
                        }
                    }
                    AddErrors(createUserResult);
                }
                else
                {
                    AddErrors(new IdentityResult("Email does not contain valid domain."));
                }
            }
            //If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/ConfirmEmail
        [AllowAnonymous]
        public async Task<ActionResult> ConfirmEmail(string userId, string code)
        {
            if (userId == null || code == null)
            {
                return View("Error");
            }
            var result = await UserManager.ConfirmEmailAsync(userId, code);
            return View(result.Succeeded ? "ConfirmEmail" : "Error");
        }

        // GET: /Account/ForgotPassword
        [AllowAnonymous]
        public ActionResult ForgotPassword()
        {
            return View();
        }

        // POST: /Account/ForgotPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ForgotPassword(ForgotPasswordViewModel model)
        {
            if (ModelState.IsValid)
            {
                var user = await UserManager.FindByNameAsync(model.Email.ToString());
                if (user == null || !(await UserManager.IsEmailConfirmedAsync(user.Id)))
                {
                    // Don't reveal that the user does not exist or is not confirmed
                    return View("ForgotPasswordConfirmation");
                }
            }
            // If we got this far, something failed, redisplay form
            return View(model);
        }

        // GET: /Account/ForgotPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ForgotPasswordConfirmation()
        {
            return View();
        }

        // GET: /Account/ResetPassword
        [AllowAnonymous]
        public ActionResult ResetPassword(string code)
        {
            return code == null ? View("Error") : View();
        }

        // POST: /Account/ResetPassword
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ResetPassword(ResetPasswordViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View(model);
            }
            var user = await UserManager.FindByNameAsync(model.Email.ToString());
            if (user == null)
            {
                // Don't reveal that the user does not exist
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            var result = await UserManager.ResetPasswordAsync(user.Id, model.Code.ToString(), model.Password.ToString());
            if (result.Succeeded)
            {
                return RedirectToAction("ResetPasswordConfirmation", "Account");
            }
            AddErrors(result);
            return View();
        }

        // GET: /Account/ResetPasswordConfirmation
        [AllowAnonymous]
        public ActionResult ResetPasswordConfirmation()
        {
            return View();
        }

        // POST: /Account/ExternalLogin
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public ActionResult ExternalLogin(string provider, string returnUrl)
        {
            // Request a redirect to the external login provider
            return new ChallengeResult(provider, Url.Action("ExternalLoginCallback", "Account", new
            {
                ReturnUrl = returnUrl
            }));
        }

        // GET: /Account/SendCode
        [AllowAnonymous]
        public async Task<ActionResult> SendCode(string returnUrl, bool rememberMe)
        {
            var userId = await SignInManager.GetVerifiedUserIdAsync();
            if (userId == null)
            {
                return View("Error");
            }
            var userFactors = await UserManager.GetValidTwoFactorProvidersAsync(userId);
            var factorOptions = userFactors.Select(purpose => new SelectListItem()
            {
                Text = purpose,
                Value = purpose
            }).ToList();
            return View(new SendCodeViewModel()
            {
                Providers = factorOptions,
                ReturnUrl = returnUrl,
                RememberMe = rememberMe
            });
        }

        // POST: /Account/SendCode
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> SendCode(SendCodeViewModel model)
        {
            if (!ModelState.IsValid)
            {
                return View();
            }
            // Generate the token and send it
            if (!await SignInManager.SendTwoFactorCodeAsync(model.SelectedProvider))
            {
                return View("Error");
            }
            return RedirectToAction("VerifyCode", new
            {
                Provider = model.SelectedProvider,
                ReturnUrl = model.ReturnUrl,
                RememberMe = model.RememberMe
            });
        }

        // GET: /Account/ExternalLoginCallback
        [AllowAnonymous]
        public async Task<ActionResult> ExternalLoginCallback(string returnUrl)
        {
            var loginInfo = await AuthenticationManager.GetExternalLoginInfoAsync();
            if (loginInfo == null)
            {
                return RedirectToAction("Login");
            }
            // Sign in the user with this external login provider if the user already has a login
            var result = await SignInManager.ExternalSignInAsync(loginInfo, isPersistent: false);
            switch (result)
            {
                case SignInStatus.Success:
                    return RedirectToLocal(returnUrl);
                case SignInStatus.LockedOut:
                    return View("Lockout");
                case SignInStatus.RequiresVerification:
                    return RedirectToAction("SendCode", new
                    {
                        ReturnUrl = returnUrl,
                        RememberMe = false
                    });
                default:
                    // If the user does not have an account, then prompt the user to create an account
                    ViewData["ReturnUrl"] = returnUrl;
                    ViewData["LoginProvider"] = loginInfo.Login.LoginProvider;
                    return View("ExternalLoginConfirmation", new ExternalLoginConfirmationViewModel()
                    {
                        Email = loginInfo.Email
                    });
            }
        }

        [HttpPost]
        public async Task<ActionResult> LoginUser(string name)
        {
            var user = await UserManager.FindByNameAsync(name);
            if (user != null)
            {
                await SignInManager.SignInAsync(user, true, true);
            }
            return RedirectToAction("Index", "Home");
        }

        // POST: /Account/ExternalLoginConfirmation
        [HttpPost]
        [AllowAnonymous]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> ExternalLoginConfirmation(ExternalLoginConfirmationViewModel model, string returnUrl)
        {
            if (User.Identity.IsAuthenticated)
            {
                return RedirectToAction("Index", "Manage");
            }
            if (ModelState.IsValid)
            {
                // Get the information about the user from the external login provider
                var info = await AuthenticationManager.GetExternalLoginInfoAsync();
                if (info == null)
                {
                    return View("ExternalLoginFailure");
                }
                var userInfo = new ApplicationUser()
                {
                    UserName = model.Email,
                    Email = model.Email
                };
                var result = await UserManager.CreateAsync(userInfo);
                if (result.Succeeded)
                {
                    result = await UserManager.AddLoginAsync(userInfo.Id, info.Login);
                    if (result.Succeeded)
                    {
                        await SignInManager.SignInAsync(userInfo, isPersistent: false, rememberBrowser: false);
                        return RedirectToLocal(returnUrl);
                    }
                }
                AddErrors(result);
            }
            ViewData["ReturnUrl"] = returnUrl;
            return View(model);
        }

        // POST: /Account/LogOff
        [HttpPost]
        [ValidateAntiForgeryToken]
        public ActionResult LogOff()
        {
            AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
            return RedirectToAction("Index", "Home");
        }

        // GET: /Account/ExternalLoginFailure
        [AllowAnonymous]
        public ActionResult ExternalLoginFailure()
        {
            return View();
        }

        // GET: /Account/ChooseAdvisor
        [AllowAnonymous]
        public ActionResult SelectAdvisor(IEnumerable<Delegates> userList)
        {
            return View(userList);
        }

        #region Helpers
        // Used for XSRF protection when adding external logins
        private const string XsrfKey = "XsrfId";

        public IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
            set
            {
                AuthenticationManager = value;
            }
        }

        private void AddErrors(IdentityResult result)
        {
            foreach (var error in result.Errors)
            {
                ModelState.AddModelError("", error);
            }
        }

        private ActionResult RedirectToLocal(string returnUrl)
        {
            if (Url.IsLocalUrl(returnUrl))
            {
                return Redirect(returnUrl);
            }
            return RedirectToAction("Index", "Home");
        }

        internal class ChallengeResult : HttpUnauthorizedResult
        {
            public ChallengeResult(string provider, string redirectUri) : this(provider, redirectUri, null)
            {
            }

            public ChallengeResult(string provider, string redirect, string user)
            {
                LoginProvider = provider;
                RedirectUri = redirect;
                UserId = user;
            }

            public string LoginProvider { get; set; }
            public string RedirectUri { get; set; }
            public string UserId { get; set; }

            public override void ExecuteResult(ControllerContext context)
            {
                var properties = new AuthenticationProperties()
                {
                    RedirectUri = RedirectUri
                };
                if (UserId == null)
                {
                    properties.Dictionary[XsrfKey] = UserId;
                }
                context.HttpContext.GetOwinContext().Authentication.Challenge(properties, LoginProvider);
            }
        }
        #endregion
    }
}