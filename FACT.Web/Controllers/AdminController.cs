﻿using CsvHelper;
using FACT.Domain.Models;
using FACT.Domain.Models.Controllers;
using FACT.Domain.Models.EntityModels;
using FACT.Domain.Models.MiscModels;
using FACT.Service;
using FACT.Web.Business;
using FACT.Web.CSV;
using FACT.Web.Enums;
using FACT.Web.Models;
using FACT.Web.Models.Filters;
using FACT.Web.Services;
using Microsoft.AspNet.Identity;
using Microsoft.VisualBasic.FileIO;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml;

namespace FACT.Web.Controllers
{
    public class AdminController : Controller
    {
        private readonly IFactMainService _factMainService;
        private AmazonS3Service S3Service = new AmazonS3Service();
        private GlobalX objConnection = new GlobalX();
        public string strImportData;
        public string strImportHeader = "FirstName,LastName,Company,Mobile,Address1,Address2,City,State,Zip,Email,Website,BirthDate,Anniversary";
        private List<ContactsModel> listOfContacts;
        public int intCSVLimit = 500;
        public int intContactLimit = 500;
        private List<SelectListItem> listOfSelectItems;
        private int intContactCount;
        private CRMFunctions objCRM = new CRMFunctions();

        public AdminController(IFactMainService factMainService)
        {
            _factMainService = factMainService;
        }

        // GET: Admin
        public ActionResult Index()
        {
            return RedirectToAction("Personas");
        }

        // GET: Admin/Personas
        [Authorize]
        public ActionResult Personas()
        {
            var userId = User.Identity.GetUserId();
            var model = _factMainService.FindPersonasById(userId);
            return View(model);
        }

        // GET: Admin/AddPersona
        public ActionResult AddPersona()
        {
            Personas persona = new Personas();
            long userId = Convert.ToInt64(User.Identity.GetUserId());
            Profiles profile = _factMainService.FindProfileById(userId);
            persona.Address = profile.BranchAddressLine1;
            persona.City = profile.BranchCity;
            persona.State = Convert.ToInt32(profile.BranchState);
            persona.Zip = profile.BranchZip;
            persona.PhoneNumber = profile.BranchPhone;
            persona.ApprovedDBAName = profile.BranchName;
            persona.BDID = profile.BDID;
            persona.AGUserId = profile.AGUserId;
            ViewBag.companyLogoList = S3Service.GetFolderContent("AssetFiles/CompanyLogos/" +
                ControllerFunctions.decodeBDID(profile.BDID) + "/");
            return View(persona);
        }

        //GET: Admin/Transactions'
        public ActionResult Transactions()
        {
            var transactionsList = _factMainService.FindAllSubscriptions();
            return View(transactionsList);
        }

        public ActionResult Settings()
        {
            Settings _settings = _factMainService.FindAllSettings().FirstOrDefault();
            SettingsViewModel model = new SettingsViewModel()
            {
                Id = _settings.Id,
                ApprovalRequired = _settings.ApprovalRequired,
                IsCreditChargeable = _settings.IsCreditChargeable,
                IsSubscriptionSignable = _settings.IsSubscriptionSignable
            };
            return View(model);
        }

        public JsonResult generatePersonaDisclosure(string _IAType, string _BDID, string _RIA)
        {
            Personas objPersona = new Personas();
            objPersona.IAType = Convert.ToInt32(_IAType);
            objPersona.BDID = _BDID;
            objPersona.ApprovedRIAName = _RIA;
            objPersona.UserId = User.Identity.GetUserId();
            Profiles objProfile = _factMainService.FindProfileById(Convert.ToInt64(objPersona.UserId));
            return Json(ControllerFunctions.generatePersonaDisclosure(objPersona, objProfile, _factMainService), JsonRequestBehavior.AllowGet);
        }

        // POST: Admin/AddPersona
        [HttpPost]
        [ValidateAntiForgeryToken]
        public async Task<ActionResult> AddPersona(Personas personaForm)
        {
            MindfireFunctions objMF = new MindfireFunctions();
            if (personaForm.PersonaType == (int)PersonaType.Individual)
            {
                personaForm.TeamName = null;
            }
            int emailStatus = objConnection.CheckDomain(personaForm.Email, _factMainService);
            ViewBag.emailStatus = emailStatus;
            if (emailStatus == 0)
            {
                ModelState.AddModelError("", "Email does not contain valid domain.");
            }
            if (personaForm.PersonaType.ToString() == "Individual" && string.IsNullOrEmpty(personaForm.FirstName) || string.IsNullOrEmpty(personaForm.LastName))
            {
                ModelState.AddModelError("", "Individual Persona Type requires first and last name values.");
            }
            if (personaForm.PersonaType.ToString() == "Company" && string.IsNullOrEmpty(personaForm.TeamName))
            {
                ModelState.AddModelError("", "Company Persona Type requires Team Name value.");
            }
            // check if uploaded files are images and add errors to model if they are not
            if (personaForm.SignatureImageFile != null && !(ControllerFunctions.IsImage(personaForm.SignatureImageFile)))
            {
                ModelState.AddModelError("SignatureImageFile", "Signature image must be an image file!");
            }
            if (personaForm.ProfilePhotoFile != null && !(ControllerFunctions.IsImage(personaForm.ProfilePhotoFile)))
            {
                ModelState.AddModelError("ProfilePhotoFile", "Profile photo must be an image file!");
            }
            if (personaForm.CompanyLogoFile != null && !(ControllerFunctions.IsImage(personaForm.CompanyLogoFile)))
            {
                ModelState.AddModelError("CompanyLogoFile", "Company logo must be an image file!");
            }
            if (ModelState.IsValid)
            {
                AmazonS3Service awsService = new AmazonS3Service();
                if (personaForm.SignatureImageFile != null)
                {
                    var result = awsService.UploadSignatureImage(personaForm.SignatureImageFile);
                    if (result.Successful)
                    {
                        personaForm.SignatureImage = result.Path;
                    }
                    else
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                    }
                }
                else
                {
                    personaForm.SignatureImage = "1x1.png";
                }
                if (personaForm.ProfilePhotoFile != null)
                {
                    var result = awsService.UploadProfileImage(personaForm.ProfilePhotoFile);
                    if (result.Successful)
                    {
                        personaForm.ProfilePhoto = result.Path;
                    }
                    else
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                    }
                }
                else
                {
                    personaForm.ProfilePhoto = "1x1.png";
                }
                if (personaForm.CompanyLogoFile != null)
                {
                    var result = awsService.UploadCompanyLogoImage(personaForm.CompanyLogoFile);
                    if (result.Successful)
                    {
                        personaForm.CompanyLogo = result.Path;
                    }
                    else
                    {
                        return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                    }
                }
                else
                {
                }
                personaForm.UserId = User.Identity.GetUserId();
                Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == personaForm.UserId);
                if (profile != null)
                {
                    personaForm.DisclosureText = ControllerFunctions.generatePersonaDisclosure(personaForm, profile, _factMainService);
                }
                if (personaForm.DefaultPersona == true)
                {
                    var personas = _factMainService.FindAllPersonas().Where(p => p.UserId == personaForm.UserId);
                    foreach (Personas pers in personas)
                    {
                        pers.DefaultPersona = false;
                    }
                }
                _factMainService.AddPersona(personaForm);
                var strPassword = profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"]));
                List<KeyValuePair<string, string>> list = new List<KeyValuePair<string, string>>();
                MyContactsModel myContactsModel = new MyContactsModel();
                myContactsModel.Email = personaForm.Email == null ? "" : personaForm.Email;
                myContactsModel.AccountID = profile.MFAccountID == null ? "" : profile.MFAccountID + DateTime.Now.Millisecond.ToString();
                myContactsModel.Address1 = personaForm.Address == null ? "" : personaForm.Address;
                myContactsModel.Address2 = "";
                myContactsModel.City = personaForm.City == null ? "" : personaForm.City;
                myContactsModel.Company = personaForm.Company == null ? "" : personaForm.Company;
                myContactsModel.FacebookAccount = personaForm.FacebookURL == null ? "" : personaForm.FacebookURL;
                myContactsModel.Fax = "";
                myContactsModel.FirstName = personaForm.FirstName == null ? "" : personaForm.FirstName;
                myContactsModel.Gender = "";
                myContactsModel.LastName = personaForm.LastName == null ? "" : personaForm.LastName;
                myContactsModel.LinkedInAccount = personaForm.LinkedInURL == null ? "" : personaForm.LinkedInURL;
                myContactsModel.MiddleName = "";
                myContactsModel.Phone = personaForm.PhoneNumber == null ? "" : personaForm.PhoneNumber;
                myContactsModel.PhotoURL = personaForm.ProfilePhoto == null ? "" : personaForm.ProfilePhoto;
                myContactsModel.State = personaForm.State == null ? "" : personaForm.State.ToString();
                myContactsModel.Title = personaForm.JobTitle == null ? "" : personaForm.JobTitle.ToString();
                myContactsModel.TwitterAccount = personaForm.TwitterURL == null ? "" : personaForm.TwitterURL;
                myContactsModel.Website = personaForm.Website == null ? "" : personaForm.Website;
                myContactsModel.Zip = personaForm.Zip == null ? "" : personaForm.Zip;
                myContactsModel.PURL = personaForm.Email;
                foreach (PropertyInfo p in myContactsModel.GetType().GetProperties())
                {
                    if (p.CanRead)
                    {
                        var val = p.GetValue(myContactsModel);
                        string name = p.Name;
                        KeyValuePair<string, string> pair = new KeyValuePair<string, string>(name, val.ToString());
                        list.Add(pair);
                    }
                }
                var x = objMF.CreateContact(profile.MFTicket, strPassword, profile.MFMasterPartnerTicket, list);
                if (string.IsNullOrEmpty(x["Result"]["ErrorCode"].ToString()))
                {
                    var contactId = x["ContactID"];
                    var purl = x["Purl"];
                }
                else
                {
                    JToken errMsg = x["Result"]["ErrorMessage"];
                    ModelState.AddModelError("", new Exception(errMsg.ToString()));
                }
                return RedirectToAction("Personas");
            }
            return View(personaForm);
        }

        // GET: Admin/EditPersona/:id
        public async Task<ActionResult> EditPersona(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            Personas persona = _factMainService.FindPersonasById(id.ToString());
            if (persona == null)
            {
                return HttpNotFound();
            }
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindProfileById(Convert.ToInt64(userId));
            persona.AGUserId = profile.AGUserId;
            if (persona.CompanyLogo != null)
            {
                ViewBag.CompanyLogo = "'<img src='" + ConfigurationManager.AppSettings["S3UrlPrefix"] + "/" + persona.CompanyLogo + "' class='file-preview-image CompanyLogoImg'/>'";
            }
            if (persona.ProfilePhoto != null)
            {
                ViewBag.ProfilePhoto = "'<img src='" + ConfigurationManager.AppSettings["S3UrlPrefix"] + "/" + persona.ProfilePhoto + "' class='file-preview-image ProfilePhotoImg'/>'";
            }
            if (persona.SignatureImage != null)
            {
                ViewBag.SignatureImage = "'<img src='" + ConfigurationManager.AppSettings["S3UrlPrefix"] + "/" + persona.SignatureImage + "' class='file-preview-image SignatureImageImg'/>'";
            }
            if (!persona.emailSPFApproved)
            {
                int emailStatus = objConnection.CheckDomain(persona.Email, _factMainService);
                ViewBag.emailStatus = emailStatus;
            }
            else
            {
                ViewBag.emailStatus = 2;
            }
            ViewBag.companyLogoList = S3Service.GetFolderContent("AssetFiles/CompanyLogos/" + ControllerFunctions.decodeBDID(profile.BDID) + "/");
            return View(persona);
        }

        // POST: Admin/EditPersona
        [HttpPost, ValidateAntiForgeryToken]
        public async Task<ActionResult> EditPersona(Personas personaForm)
        {

            if (personaForm.PersonaType == (int)PersonaType.Individual)
            {
                personaForm.TeamName = null;
            }
            int emailStatus;
            if (!personaForm.emailSPFApproved)
            {
                emailStatus = objConnection.CheckDomain(personaForm.Email, _factMainService);
            }
            else
            {
                emailStatus = 2;
            }
            bool CompanyLogoIsModified = true;
            bool ProfilePhotoIsModified = true;
            bool SignatureImageIsModified = true;
            ViewBag.emailStatus = emailStatus;
            if (emailStatus == 0)
            {
                ModelState.AddModelError("", "Email does not contain valid domain.");
            }
            if (personaForm.PersonaType.ToString() == "Individual" && string.IsNullOrEmpty(personaForm.FirstName) || string.IsNullOrEmpty(personaForm.LastName))
            {
                ModelState.AddModelError("", "Individual Persona Type requires first and last name values.");
            }
            if (personaForm.PersonaType.ToString() == "Company" && string.IsNullOrEmpty(personaForm.TeamName))
            {
                ModelState.AddModelError("", "Company Persona Type requires Team Name value.");
            }
            AmazonS3Service awsService = new AmazonS3Service();
            if (personaForm.SignatureImageDelete)
            {
                awsService.DeleteSignatureImage(personaForm.SignatureImage);
            }
            else if (personaForm.SignatureImageFile != null)
            {
                var result = awsService.UploadSignatureImage(personaForm.SignatureImageFile);
                if (result.Successful)
                {
                    personaForm.SignatureImage = result.Path;
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                SignatureImageIsModified = false;
            }
            if (personaForm.ProfilePhotoDelete)
            {
                awsService.DeleteProfileImage(personaForm.ProfilePhoto);
            }
            else if (personaForm.ProfilePhotoFile != null)
            {
                var result = awsService.UploadProfileImage(personaForm.ProfilePhotoFile);
                if (result.Successful)
                {
                    personaForm.ProfilePhoto = result.Path;
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.InternalServerError);
                }
            }
            else
            {
                ProfilePhotoIsModified = false;
            }
            if (ModelState.IsValid)
            {
                // Validate that the persona being edited actually belongs to the logged in user.
                // Without this check a user could maliciously change other users data by submitting 
                // form with manually set persona ID.
                var userId = User.Identity.GetUserId();
                var personaExists = _factMainService.FindAllPersonas().Any(p => p.Id == personaForm.Id && p.UserId == userId);
                if (personaExists)
                {
                    personaForm.UserId = userId;
                    _factMainService.UpdatePersonas(personaForm);
                    // *** these false are reflecting that they are considered to be modified ? what to do?
                    //if (!CompanyLogoIsModified)
                    //{
                    //    personaForm.CompanyLogo = false;
                    //}
                    //if (!ProfilePhotoIsModified)
                    //{
                    //    personas.ProfilePhoto = false;
                    //}
                    //if (!SignatureImageIsModified)
                    //{
                    //    personaForm.SignatureImage = false;
                    //}
                    Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == personaForm.UserId);
                    if (profile != null)
                    {
                        personaForm.DisclosureText = ControllerFunctions.generatePersonaDisclosure(personaForm, profile, _factMainService);
                    }
                    if (personaForm.DefaultPersona == true)
                    {
                        List<Personas> personas = _factMainService.FindAllPersonas().Where(p => p.UserId == personaForm.UserId && p.Id != personaForm.Id).ToList();
                        foreach (Personas pers in personas)
                        {
                            pers.DefaultPersona = false;
                            _factMainService.UpdatePersonas(pers);
                        }
                    }
                    return RedirectToAction("Personas");
                }
                else
                {
                    return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
                }
            }
            return View(personaForm);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateAntiForgeryToken()]
        public ActionResult UploadCSV(HttpPostedFileBase CSVFile)
        {
            ErrorModelsController errController = new ErrorModelsController(_factMainService);
            string byteString = "";
            int intCSVCount = 1;
            string allRows;
            string fullFile = CSVFile.FileName.Replace("'", "");
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == userId);
            MindfireFunctions objMFFunctions = new MindfireFunctions();
            StringBuilder errorSB = new StringBuilder();
            try
            {
                // fail the method and return error if file extension is not valid
                if (Path.GetExtension(fullFile) != ".csv")
                {
                    string message = "The file must have .csv extension, " + Path.GetExtension(fullFile) + " provided";
                    return errController.ShowError("Wrong format", message, "input error", "~/Admin/UploadContacts");
                }
                string fileDateTime = "CSVImport" + Convert.ToString(3303) + DateTime.Now.ToString("MMddyyyy") + "_" + DateTime.Now.ToString("HHmmss");
                var createdField = objMFFunctions.CreateCustomMFFields(profile.MFTicket, profile.AGUserId +
                    profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])));
                if (!createdField)
                {
                    string message = "Could not create a custom field";
                    return errController.ShowError("Custom field is not created", message, "error", "back");
                }
                var csvConfig = new CsvHelper.Configuration.Configuration.CsvConfiguration
                {
                    Delimiter = ",",
                    DetectColumnCountChanges = true,
                    HasHeaderRecord = true,
                    IgnoreReferences = true,
                    WillThrowOnMissingField = false,
                    IgnoreBlankLines = true,
                    TrimFields = true,
                    ThrowOnBadData = true,
                    IgnoreQuotes = true,
                    SkipEmptyRecords = true
                };
                StreamReader sr = new StreamReader(CSVFile.InputStream);
                CsvReader csvReader = new CsvReader(sr, csvConfig);
                var records = csvReader.GetRecords<CSVUpload>().ToList();
                sr.Close();
                // Temp folder must already exist
                StreamWriter sw = new StreamWriter(Server.MapPath("/temp/" + fileDateTime + ".csv"));
                CsvWriter csvWriter = new CsvWriter(sw, csvConfig);
                foreach (CSVUpload use in records)
                {
                    intCSVCount = intCSVCount + 1;
                    use.GroupName = Request.Form["GroupName"];
                    use.ContactSource = "CSVUpload";
                    if (use.Email == "")
                    {
                        errorSB.Append("One or more csv records missing required value for Email Address.<br/>");
                    }
                    if (use.Email == null)
                    {
                        errorSB.Append("Required column Email Address missing from csv file.<br/>");
                    }
                    if (use.FirstName == "")
                    {
                        errorSB.Append("One or more csv records missing required value for First Name.<br/>");
                    }
                    if (use.FirstName == null)
                    {
                        errorSB.Append("Required column First Name missing from csv file.<br/>");
                    }
                    if (use.LastName == "")
                    {
                        errorSB.Append("One or more csv records missing required value for Last Name.<br/>");
                    }
                    if (use.LastName == null)
                    {
                        errorSB.Append("Required column Last Name missing from csv file.<br/>");
                    }
                }
                if (errorSB.Length > 0)
                {
                    return errController.ShowError("Input error", errorSB.ToString(), "input error", "back", Convert.ToBoolean(ConfigurationManager.AppSettings["LogErrors"]));
                }
                //Finally write file if passed validation
                csvWriter.WriteRecords(records);
                sw.Close();
                intCSVCount = intCSVCount - 1;
                StreamReader fileSR = new StreamReader(Server.MapPath("/temp/" + fileDateTime + ".csv"));
                allRows = fileSR.ReadToEnd();
                var emails = objMFFunctions.ContactsEmails(profile);
                List<string> emails2 = new List<String>();
                var rows = allRows.Split('\n');
                foreach (string r in rows)
                {
                    if (r.Contains("\n"))
                    {
                        if (r.Split('\n').Length > 1)
                        {
                            r = r.Split('\n')(1);
                        }
                        else
                        {
                            r = "";
                        }
                    }
                    if (r != "")
                    {
                        emails2.Add(r.Split(',')(9));
                    }
                }
                emails2.Remove("Email");
                var distinctEmails = emails.Concat(emails2).Distinct();
                var distinctEmailsCount = distinctEmails.Count();
                if (distinctEmailsCount > Convert.ToInt32(ConfigurationManager.AppSettings["MaxContacts"]))
                {
                    string message = "Try to add too much contacts";
                    return errController.ShowError("Too much contacts", message, "limit error", "~/Admin/UploadContacts");
                }
                byte[] csvToBytes = GlobalFunctions.Convert64(allRows);
                for (int x = 0; x <= csvToBytes.Length - 1; x++)
                {
                    byteString = byteString + csvToBytes[x] + ",";
                    byteString = byteString + Environment.NewLine;
                }
                byteString = byteString.Remove(byteString.LastIndexOf(","));
                byte[] listCallData3 = Encoding.UTF8.GetBytes(@"{" + @"""FileName"":""" + fileDateTime + ".csv" + @"""," + @"""Filterable"":true," +
                    @"""Mapping"":""[FirstName][FirstName];[LastName][LastName];[Company][Company];[Mobile][Mobile];[Address1][Address1];[Address2][Address2];[City][City];[State][State];[Zip][Zip];[Email][Email];[Website][Website];[BirthDate][BirthDate];[Anniversary][Anniversary];[ContactSource][ContactSource];[GroupName][GroupName]""," +
                    @"""Mode"":1," + @"""Name"":""" + Request.Form["ListName"] + @"""," + @"""NotificationEmail"":""" + Request.Form["NotificationEmail"] + @"""," + @"""CSV"":[" + byteString + "]," +
                    @"""Credentials"":{" + @"""IsEncrypted"":false," + @"""PartnerPassword"":""""," + @"""PartnerTicket"":""""," + @"""Ticket"":""" + profile.MFTicket + @"""," +
                    @"""UserPassword"":""" + profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) + @"""" + @"}," + @"""CsvFormat"":1" + @"}");
                Object response = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/ImportContacts", listCallData3, "application/json", "POST", "", "");
                UploadCSVConfirmation uploadCSVConfirmation1 = new UploadCSVConfirmation();
                if (response == null)
                {
                    string message = "Could not upload data";
                    return errController.ShowError("Could not upload data", message, "upload error", "back");
                }
                else
                {
                    List<SegmentationFilterCriteria> filterList = new List<SegmentationFilterCriteria>();
                    filterList.Add(new SegmentationFilterCriteria()
                    {
                        Row = 1,
                        Field = "ID",
                        Operation = "Equal",
                        Value = response["ID"]
                    });
                    JObject responseList = objMFFunctions.GetFilteredImportList(profile.MFTicket, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), null);
                    Object uploadedCSV = null;
                    foreach (JToken item in responseList["ImportList"])
                    {
                        if (item["ID"] == response["ID"])
                        {
                            uploadedCSV = item;
                            break;
                        }
                    }
                    if (uploadedCSV != null)
                    {
                        uploadCSVConfirmation1.FileName = uploadedCSV["FileName"].ToString();
                        uploadCSVConfirmation1.StatusName = uploadedCSV["StatusName"];
                        uploadCSVConfirmation1.RejectedRecords = uploadedCSV["RejectedRecords"];
                        uploadCSVConfirmation1.ProcessedRecords = uploadedCSV["ProcessedRecords"];
                        uploadCSVConfirmation1.ModeName = uploadedCSV["ModeName"];
                        uploadCSVConfirmation1.ID = uploadedCSV["ID"];
                        uploadCSVConfirmation1.Mode = uploadedCSV["Mode"];
                        uploadCSVConfirmation1.SuccessfulRecords = uploadedCSV["SuccessfulRecords"];
                        uploadCSVConfirmation1.Status = uploadedCSV["Status"];
                    }
                }
                return View("UploadConfirmation", uploadCSVConfirmation1);
            }
            catch (Exception ex)
            {
                return errController.ShowError("Error", ex.Message, "error", "back");
            }
        }

        // GET: Admin/PersonaDetails/:id
        public async Task<ActionResult> PersonaDetails(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userId = User.Identity.GetUserId();
            Personas persona = _factMainService.FindAllPersonas().SingleOrDefault(p => p.Id == id.ToString() && p.UserId == userId);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View(persona);
        }

        public ActionResult PreviewContacts(string RTTagGroups)
        {
            PreviewContacts prevContacts = new PreviewContacts();
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == userId);
            prevContacts.NotificationEmail = profile.Email;
            try
            {
                prevContacts.RTTagGroups = RTTagGroups;
                prevContacts.CSV = PullRedtailContacts(ConfigurationManager.AppSettings["RedtailApiKey"] + ":" + profile.RedtailUsername + ":" + profile.RedtailPassword, RTTagGroups);
            }
            catch (Exception ex)
            {
                ErrorModelsController errController = new ErrorModelsController(_factMainService);
                return errController.ShowError("Error", ex.Message, "error", "~/RedTailCRM");
            }
            prevContacts.ListOfContacts = listOfContacts;
            return View(prevContacts);
        }

        [AcceptVerbs(HttpVerbs.Post)]
        [ValidateAntiForgeryToken()]
        public ActionResult PreviewContacts(PreviewContacts prevContacts)
        {
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == userId);
            prevContacts.CSV = PullRedtailContacts(ConfigurationManager.AppSettings["RedtailApiKey"] + ":" + profile.RedtailUsername + ":" + profile.RedtailPassword, prevContacts.RTTagGroups);
            prevContacts.ListOfContacts = listOfContacts;
            string byteString = "";
            int intCSVCount;
            string allRows = "";
            MemoryStream fullFileData = new MemoryStream(Encoding.UTF8.GetBytes(prevContacts.CSV));
            MindfireFunctions objMFFunctions = new MindfireFunctions();
            string FileDateTime = "RTImport" + profile.MFAccountID + DateTime.Now.ToString("MMddyyyy") + "_" + DateTime.Now.ToString("HHmm");
            try
            {
                if (objMFFunctions.CreateCustomMFFields(profile.MFTicket, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"]))))
                {
                    objMFFunctions.AddCustomFieldValues(fullFileData, FileDateTime + ".csv", prevContacts.GroupName, "Redtail");
                    var MyReader = new TextFieldParser(Server.MapPath("/temp/") + FileDateTime + ".csv");
                    MyReader.TextFieldType = FieldType.Delimited;
                    MyReader.SetDelimiters(",");
                    allRows = MyReader.ReadToEnd();
                    var emails = objMFFunctions.ContactsEmails(profile);
                    List<string> emails2 = new List<String>();
                    var rows = allRows.Split('\n');
                    foreach (var r in rows)
                    {
                        if (r.Contains("\n"))
                        {
                            if (r.Split('\n').Length > 1)
                            {
                                r = r.Split('\n')(1);
                            }
                            else
                            {
                                r = "";
                            }
                        }
                        if (r != "")
                        {
                            emails2.Add(r.Split(',')(9));
                        }
                    }
                    emails2.Remove("Email");
                    var distinctEmails = emails.Concat(emails2).Distinct();
                    var distinctEmailsCount = distinctEmails.Count();
                    if (distinctEmailsCount > Convert.ToInt32(ConfigurationManager.AppSettings["MaxContacts"]))
                    {
                        if (string.IsNullOrEmpty(profile.RedtailUsername))
                        {
                            return RedirectToAction("RedTailCRMLogin");
                        }
                        else
                        {
                            PullRedtailTagGroups(ConfigurationManager.AppSettings["RedtailApiKey"] + ":" + profile.RedtailUsername + ":" + profile.RedtailPassword);
                            ViewBag.listOfSelectItems = listOfSelectItems;
                            ModelState.AddModelError("", "Too much contacts!");
                            return View("RedTailCRM");
                        }
                    }
                    intCSVCount = objConnection.lineCount(Server.MapPath("/temp/") + FileDateTime + ".csv", true);
                    if (intCSVCount > intCSVLimit)
                    {
                        ModelState.AddModelError("", "CSV record count exceeds single contact list limit of " + intCSVLimit + ". Record count is " + intCSVCount + ".");
                    }
                    else if ((intCSVCount + intContactCount) > intContactLimit)
                    {
                        ModelState.AddModelError("", "You have exceeded the total contact allowance of " + intContactLimit + ". You currently have a record count of " + intContactCount + ".");
                    }
                    var csvToBytes = GlobalFunctions.Convert64(allRows);
                    for (int x = 0; x <= csvToBytes.Length - 1; x++)
                    {
                        byteString = byteString + csvToBytes[x] + ",";
                        byteString = byteString + Environment.NewLine;
                    }
                    byteString = byteString.Remove(byteString.LastIndexOf(","));
                    byte[] listCallData3 = Encoding.UTF8.GetBytes(@"{" + @"""FileName"":""" + FileDateTime + ".csv" + @"""," + @"""Filterable"":true," +
                        @"""Mapping"":""[FirstName][FirstName];[LastName][LastName];[AccountID][AccountID];[Company][Company];[Mobile][Mobile];[Address1][Address1];[Address2][Address2];[City][City];[State][State];[Zip][Zip];[Email][Email];[Website][Website];[BirthDate][BirthDate];[Anniversary][Anniversary];[GroupName][GroupName];[ContactSource][ContactSource]""," +
                        @"""Mode"":1," + @"""Name"":""" + prevContacts.MFListName + @"""," + @"""NotificationEmail"":""" + prevContacts.NotificationEmail + @"""," + @"""CSV"":[" + byteString + "]," +
                        @"""Credentials"":{" + @"""IsEncrypted"":false," + @"""PartnerPassword"":""""," + @"""PartnerTicket"":""""," + @"""Ticket"":""" + profile.MFTicket + @"""," +
                        @"""UserPassword"":""" + profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) + @"""" + @"}," + @"""CsvFormat"":1" + @"}");
                    JObject result_upload = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/ImportContacts", listCallData3,
                        "application/json", "POST", "", ""));
                    UploadCSVConfirmation uploadCSVConfirmation1 = new UploadCSVConfirmation();
                    if (result_upload["Result"]["ErrorMessage"].ToString() != "")
                    {
                        ModelState.AddModelError("", result_upload["Result"]["ErrorMessage"].ToString());
                        return View(prevContacts);
                    }
                    else
                    {
                        List<SegmentationFilterCriteria> filterList = new List<SegmentationFilterCriteria>();
                        filterList.Add(new SegmentationFilterCriteria()
                        {
                            Row = 1,
                            Field = "ID",
                            Operation = "Equal",
                            Value = result_upload["ID"].ToString()
                        });
                        var responseList = objMFFunctions.GetFilteredImportList(profile.MFTicket, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), null);
                        Object uploadedCSV = null;
                        foreach (Object item in responseList["ImportList"])
                        {
                            if (item["ID"].ToString() == result_upload["ID"].ToString())
                            {
                                uploadedCSV = item;
                                break;
                            }
                        }
                        if (uploadedCSV != null)
                        {
                            uploadCSVConfirmation1.FileName = uploadedCSV["FileName"];
                            uploadCSVConfirmation1.StatusName = uploadedCSV("StatusName");
                            uploadCSVConfirmation1.RejectedRecords = uploadedCSV("RejectedRecords");
                            uploadCSVConfirmation1.ProcessedRecords = uploadedCSV("ProcessedRecords");
                            uploadCSVConfirmation1.ModeName = uploadedCSV("ModeName");
                            uploadCSVConfirmation1.ID = uploadedCSV("ID");
                            uploadCSVConfirmation1.Mode = uploadedCSV("Mode");
                            uploadCSVConfirmation1.SuccessfulRecords = uploadedCSV("SuccessfulRecords");
                            uploadCSVConfirmation1.Status = uploadedCSV("Status");
                        }
                    }
                    objMFFunctions = null;
                    MyReader.Close();
                    MyReader = null;
                    return View("ListSubmit", uploadCSVConfirmation1);
                }
                else
                {
                    return View(prevContacts);
                }
            }
            catch (Exception ex)
            {
                ErrorModelsController errController = new ErrorModelsController(_factMainService);
                return errController.ShowError("Error", ex.Message, "error", "~/PreviewContacts");
            }
        }

        public ActionResult ListSubmit()
        {
            return View();
        }

        // GET: /Admin/DeletePersona/:id
        public ActionResult DeletePersonaConfirmation(long id)
        {
            string userId = User.Identity.GetUserId();
            Personas persona = _factMainService.FindAllPersonas().SingleOrDefault(p => p.Id == id.ToString() && p.UserId == userId);
            if (persona == null)
            {
                return HttpNotFound();
            }
            return View("DeletePersona", persona);
        }

        // POST: /Admin/DeletePersona
        public ActionResult DeletePersona(long id)
        {
            ErrorModelsController errController = new ErrorModelsController();
            // We delete an entry only if we can locate it for the currently logged in user!
            // It is not enaugh to just find it by ID, that way any user could delete anything from DB.
            string userId = User.Identity.GetUserId();
            Personas persona = _factMainService.FindAllPersonas().SingleOrDefault(p => p.Id == id.ToString() && p.UserId == userId);
            AmazonS3Service awsService = new AmazonS3Service();
            if (persona == null)
            {
                return HttpNotFound();
            }
            Profiles profile = _factMainService.FindAllProfiles().Where(p => p.UserId == userId).SingleOrDefault();
            MindfireFunctions objMF = new MindfireFunctions();
            objMF.AuthenticateAccount(profile.Email, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), ref profile);
            byte[] progCallData = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":""" + profile.MFMasterPartnerTicket + @""",""Ticket"":""" +
                profile.MFTicket + @""",""UserPassword"":""" + profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) +
                @"""},""SearchString"":"""",""MaxRows"":500,""SortExpression"":"""",""StartRowIndex"":0,""State"":255}");
            JObject progResultPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/GetProgramList", progCallData,
                "application/json", "POST", "", ""));
            string programId = "";
            if (progResultPost["Result"]["ErrorMessage"].ToString() != "")
            {
                //TODO Report Error                    
                return errController.ShowError("Mindfire API Error", progResultPost["Result"]["ErrorMessage"].ToString(), "api error", "back",
                    Convert.ToBoolean(ConfigurationManager.AppSettings["LogErrors"]));
            }
            else
            {
                List<Campaigns> camps = _factMainService.FindAllCampaigns().Where(x => x.PersonaId == id).ToList();
                foreach (var c in camps)
                {
                    c.Status = "Stop";
                    for (int i = 0; i <= progResultPost["ProgramList"].Count() - 1; i++)
                    {
                        string pName = "";
                        pName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                        if (c.CampaignName == pName)
                        {
                            programId = progResultPost["ProgramList"][i]["ProgramID"].ToString();
                            string mamlXML = "";
                            string byteString = "";
                            byte[] campaignCallData = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + profile.MFTicket +
                                @""",""UserPassword"":""" + profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) + @"""},""MamlFormat"":0,""ProgramID"":" +
                                programId + @"}");
                            JObject campaignResultPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/CheckoutProgram",
                                campaignCallData, "application/json", "POST", "", ""));
                            if (campaignResultPost["Result"]["ErrorMessage"].ToString() != "")
                            {
                                return errController.ShowError("Mindfire API Error", progResultPost["Result"]["ErrorMessage"].ToString(), "api error", "back",
                                    Convert.ToBoolean(ConfigurationManager.AppSettings["LogErrors"]));
                            }
                            else
                            {
                                for (int x = 0; x <= campaignResultPost["Maml"].Count() - 1; i++)
                                {
                                    mamlXML = mamlXML + Convert.ToChar(Convert.ToInt32(campaignResultPost["Maml"][x]));
                                }
                                XmlDocument campXML = new XmlDocument();
                                campXML.LoadXml(mamlXML);
                                XmlElement root = campXML.DocumentElement;
                                var strProgramState = root.Attributes["State"];
                                XmlAttribute strCampaignState = root.Item["Campaign"].Attributes["State"];
                                strProgramState.Value = "Stop";
                                strCampaignState.Value = "Stop";
                                var encodedData = GlobalFunctions.Convert64(campXML.InnerXml);
                                for (int x = 0; x <= encodedData.Length - 1; x++)
                                {
                                    byteString = byteString + encodedData[x] + ",";
                                    byteString = byteString + Environment.NewLine;
                                }
                                byteString = byteString.Remove(byteString.LastIndexOf(","));
                                byte[] progCallData2 = Encoding.UTF8.GetBytes(@"{""Maml"":[" + byteString +
                                    @"],""MamlFormat"":0,""CheckoutAfterCheckin"":false,""Comments"":""Edited Campaign"",""Credentials"":{""IsEncrypted"":false,
                                ""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + profile.MFTicket + @""",""UserPassword"":""" +
                                profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) + @"""}}");
                                JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/CheckinProgram", progCallData2, "application/json", "POST", "", ""));
                                if (campaignResultPost["Result"]["ErrorMessage"].ToString() != "")
                                {
                                    return errController.ShowError("Mindfire API Error", progResultPost["Result"]["ErrorMessage"].ToString(), "api error", "back",
                                        Convert.ToBoolean(ConfigurationManager.AppSettings["LogErrors"]));
                                }
                                break;
                            }
                        }
                        _factMainService.UpdateProfiles(profile);
                    }
                }
            }
            // TODO: delete persona images from S3
            if (persona.ProfilePhoto != null)
            {
                awsService.DeleteProfileImage(persona.ProfilePhoto);
                persona.ProfilePhoto = null;
            }
            if (persona.CompanyLogo != null)
            {
                awsService.DeleteCompanyLogoImage(persona.CompanyLogo);
                persona.CompanyLogo = null;
            }
            if (persona.SignatureImage != null)
            {
                awsService.DeleteSignatureImage(persona.SignatureImage);
                persona.SignatureImage = null;
            }
            _factMainService.DeletePersonas(persona);
            return RedirectToAction("Personas");
        }

        public ActionResult UploadContacts()
        {
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == userId);
            ViewBag.Email = profile.Email;
            return View();
        }

        // GET: Reports
        [Authorize]
        public ActionResult Reports()
        {
            MindfireFunctions objMF = new MindfireFunctions();
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == userId);
            objMF.AuthenticateAccount(profile.Email, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), ref profile);
            string token = objMF.setReportingTokenSession(profile.MFAccountID, profile.Email, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])));
            ViewBag.IframeURL = "https://studio.dashboard.mdl.io/api/Report/GetReportView?rn=home&si=0" +
                "&ai=" + profile.MFAccountID + "&st=201&fd=3/14/2014&td=" + DateTime.Now.ToString("MM/dd/yyyy") + "&filter=false&pb=true&seed=false&menu=True&token=" + token;
            return View();
        }

        [Authorize]
        public ActionResult Support()
        {
            return View();
        }

        [Authorize]
        public ActionResult RedTailCRM()
        {
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == userId);
            if (string.IsNullOrEmpty(profile.RedtailUsername))
            {
                return RedirectToAction("RedTailCRMLogin");
            }
            else
            {
                try
                {
                    PullRedtailTagGroups(ConfigurationManager.AppSettings["RedtailApiKey"] + ":" + profile.RedtailUsername + ":" + profile.RedtailPassword);
                    ViewBag.listOfSelectItems = listOfSelectItems;
                }
                catch (Exception)
                {
                    ModelState.AddModelError("", "Redtail Login Error. Please check your username and password.");
                    return View("RedtailCRMLogin", profile);
                }
                return View();
            }
        }

        public ActionResult RedTailCRMLogin()
        {
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == userId);
            return View(profile);
        }

        [Authorize]
        public ActionResult MyContacts()
        {
            MindfireFunctions objMF = new MindfireFunctions();
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == userId);
            //*** temporary
            string authCode = "00Df4000001e49q!ARQAQJsAQ2s4SX4k2Pn0qnf67zT1iXr4RBh_IOqOAhL96f6GOWB.4ESyUP4tKALRLkqZ2Rj0Ci4fw0KIqPayazk759W7Pf0_";
            string refreshToken = "5Aep861UTWIWNgl0kd1Ab.T.RLgNpu5Wfco44D_HGpoq4.Re9cNSI.rqvaJLdHz5YzSamlxPhuJ4gbEH3WOtDTR";
            profile.SalesforceAuthToken = authCode;
            profile.SalesforceRefreshToken = refreshToken;
            _factMainService.UpdateProfiles(profile);
            objMF.AuthenticateAccount(profile.Email, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), ref profile);
            string token = objMF.setReportingTokenSession(profile.MFAccountID, profile.Email, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])));
            ViewBag.iframeurl = "https://studio.dashboard.mdl.io/api/Report/GetReportView?rn=contact-management&si=0" + "&ai=" + profile.MFAccountID + "&st=201&fd=3/14/2014&td=" +
                DateTime.Now.ToString("MM/dd/yyyy") + "&seed=false&pb=true&menu=true&token=" + token;
            if (Request.QueryString["code"] != "" && Request.QueryString["display"] == "page")
            {
                objCRM.getSFAuthToken(Request.QueryString["code"].ToString(), "http://localhost:50721/Admin/MyContacts", profile, _factMainService);
            }
            return View(profile);
        }

        public Action SalesForceImports()
        {
            // if not authenticated, return authentication form
            SalesForceImportViewModel sfImports = new SalesForceImportViewModel();
            sfImports.jWrap = false;
            sfImports.jWrapE = false;
            sfImports.CCFormValidated = false;
            sfImports.contactResult = false;
            sfImports.ListSubmitVisible = true;
            sfImports.contactResult = false;
            sfImports.excludeNoEmail = true;
            sfImports.Contacts = new List<ContactImportAdmin>();
            Session["SalesforceAuthToken"] = "00Df4000001e49q!ARQAQJsAQ2s4SX4k2Pn0qnf67zT1iXr4RBh_IOqOAhL96f6GOWB.4ESyUP4tKALRLkqZ2Rj0Ci4fw0KIqPayazk759W7Pf0_";
            Session["SalesforceRefreshToken"] = "5Aep861UTWIWNgl0kd1Ab.T.RLgNpu5Wfco44D_HGpoq4.Re9cNSI.rqvaJLdHz5YzSamlxPhuJ4gbEH3WOtDTR";
            Session["SalesforceInstance"] = "https://na46.salesforce.com";
            sfImports = FillCustomFields(sfImports);
            if (SessionManagerAdmin.SalesForceAdmin.AuthenticationAdmin == null)
            {
                sfImports.SFAuth = false;
                return View(sfImports);
            }
            else
            {
                sfImports.SFAuth = true;
            }
            sfImports.CCFormValidated = true;
            return View(sfImports);
        }

        public ActionResult MyCampaigns(string home)
        {
            var userId = User.Identity.GetUserId();
            Profiles Profile = _factMainService.FindAllProfiles().Where(p => p.UserId == userId).SingleOrDefault();
            MindfireFunctions objMF = new MindfireFunctions();
            objMF.AuthenticateAccount(Profile.Email, Profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), Profile);
            byte[] progCallData = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":""" + Profile.MFMasterPartnerTicket +
                @""",""Ticket"":""" + Profile.MFTicket + @""",""UserPassword"":""" + Profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) +
                @"""},""SearchString"":"""",""MaxRows"":500,""SortExpression"":"""",""StartRowIndex"":0,""State"":255}");
            JObject progResultPost = JObject.FromObject(objConnection.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/GetProgramList", progCallData, "application/json", "POST", "", ""));
            List<MyCampaignsModel> listOfMyCampaignModels = new List<MyCampaignsModel>();
            List<Campaigns> campaignsDb = _factMainService.FindAllCampaigns().ToList();
            List<Personas> personas = _factMainService.FindAllPersonas();
            List<Campaigns> savedCampaigns = campaignsDb.Where(p => p.UserId == userId && p.Status == "Saved").ToList();
            AmazonS3Service awsService = new AmazonS3Service();
            var mamlController = new MamlProgramModelsController();
            foreach (var s in savedCampaigns)
            {
                AmazonResponse ar = awsService.DownloadMaml(s.CampaignFileURL);
                if (ar.Successful)
                {
                    var mamlXML = ar.Message;
                    myCampaignsModel myCampaignsModel = new MyCampaignsModel();
                    myCampaignsModel.Status = "";
                    myCampaignsModel.DbStatus = s.Status;
                    myCampaignsModel.CampaignName = s.CampaignName;
                    myCampaignsModel.DateSubmitted = s.DateSubmitted;
                    myCampaignsModel.Maml = mamlXML;
                    myCampaignsModel.Program = mamlController.FromMAMLString(mamlXML);
                    if (s.PersonaId == null || s.PersonaId == 0)
                    {
                        myCampaignsModel.CreatorName = Profile.FirstName + " " + Profile.LastName;
                    }
                    else
                    {
                        Personas persona = personas.SingleOrDefault(p => p.Id == s.PersonaId);
                        if (persona != null)
                        {
                            if (persona.PersonaType == PersonaType.Individual)
                            {
                                myCampaignsModel.CreatorName = persona.FirstName + " " + persona.LastName;
                            }
                            else
                            {
                                myCampaignsModel.CreatorName = persona.TeamName;
                            }
                            myCampaignsModel.PersonaId = persona.Id;
                        }
                        else
                        {
                            myCampaignsModel.CreatorName = Profile.FirstName + " " + Profile.LastName;
                        }
                    }
                    myCampaignsModel.Url = s.CampaignFileURL;
                    for (int i = 0; i <= progResultPost["ProgramList"].Count() - 1; i++)
                    {
                        string pName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                        if (s.CampaignName == pName)
                        {
                            myCampaignsModel.Id = progResultPost["ProgramList"][i]["ProgramID"];
                        }
                    }
                    listOfMyCampaignModels.Add(myCampaignsModel);
                }
            }
            if (progResultPost["Result"]["ErrorMessage"].ToString() != "")
            {
            }
            else
            {
                List<int> outboundIds = new List<Integer>();
                for (int i = 0; i <= progResultPost["ProgramList"].Count() - 1; i++)
                {
                    string creatorId = progResultPost["ProgramList"][i]["CreatorID"].ToString();
                    if (creatorId == Profile.MFUserID)
                    {
                        MyCampaignsModel myCampaignsModel = new MyCampaignsModel();
                        string pName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                        try
                        {
                            Campaigns dbCampaign = _factMainService.FindAllCampaigns().Where(x => x.UserId == Profile.UserId && x.CampaignName == pName).First();
                            myCampaignsModel.Maml = dbCampaign.CampaignFileURL;
                            myCampaignsModel.Url = dbCampaign.CampaignFileURL;
                            myCampaignsModel.Token = dbCampaign.ApprovalToken;
                            myCampaignsModel.DbStatus = dbCampaign.Status;
                        }
                        catch (Exception)
                        {
                            myCampaignsModel.Maml = "";
                            myCampaignsModel.Token = "";
                            myCampaignsModel.DbStatus = progResultPost["ProgramList"][i]["StateName"].ToString();
                        }
                        myCampaignsModel.CampaignName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                        var campaignDb = campaignsDb.Where(p => p.UserId == userId && p.CampaignName == myCampaignsModel.CampaignName).FirstOrDefault();
                        if (campaignDb != null && campaignDb.Status != "Saved")
                        {
                            if (campaignDb.PersonaId == null || campaignDb.PersonaId == 0)
                            {
                                myCampaignsModel.CreatorName = Profile.FirstName + " " + Profile.LastName;
                            }
                            else
                            {
                                Personas persona = personas.SingleOrDefault(p => p.Id == campaignDb.PersonaId);
                                if (persona == null)
                                {
                                    myCampaignsModel.CreatorName = Profile.FirstName + " " + Profile.LastName;
                                }
                                else
                                {
                                    if (persona.PersonaType == PersonaType.Individual)
                                    {
                                        myCampaignsModel.CreatorName = persona.FirstName + " " + persona.LastName;
                                    }
                                    else
                                    {
                                        myCampaignsModel.CreatorName = persona.TeamName;
                                    }
                                }
                            }
                            myCampaignsModel.Id = Convert.ToInt32(progResultPost["ProgramList"][i]["ProgramID"].ToString());
                            if (progResultPost["ProgramList"][i]["StateName"].ToString() == "Start")
                            {
                                var programInfoRes = objMF.GetProgramInfo(Profile.MFTicket, Profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])),
                                    progResultPost["ProgramList"][i]["ProgramID"]);
                                var outbId = programInfoRes["Campaigns"][0]["Outbounds"][0]["Outbound_ID"];
                                outboundIds.Add(Convert.ToInt32(outbId));
                            }
                            myCampaignsModel.Status = progResultPost["ProgramList"][i]["StateName"].ToString();
                            myCampaignsModel.DateSubmitted = Convert.ToDateTime(progResultPost["ProgramList"][i]["PublishDate"].ToString());
                            listOfMyCampaignModels.Add(myCampaignsModel);
                        }
                    }
                }
                ViewBag.Events = objMF.GetSchedules(outboundIds, Profile.MFTicket, Profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"]))).ToList();
            }
            if (home == "active")
            {
                return View(listOfMyCampaignModels.Where(p => p.Status == "Start" && p.DbStatus == "Start" || p.DbStatus == "Approved"));
            }
            else if (home == "complete")
            {
                return View(listOfMyCampaignModels.Where(p => p.Status == "Stop" && p.DbStatus == "Start" || p.DbStatus == "Approved"));
            }
            else if (home == "wait")
            {
                return View(listOfMyCampaignModels.Where(p => p.Status == "Stop" && p.DbStatus == "WaitingApproval"));
            }
            return View(listOfMyCampaignModels.OrderByDescending(x => x.DateSubmitted));
        }

        public ActionResult CampaignDetailsActions(string JsonModel)
        {
            var mamlProgramCtr = new MamlProgramModelsController();
            CampaignViewModel campaignView = new CampaignViewModel();
            var userId = User.Identity.GetUserId();
            campaignView.userProfile = _factMainService.FindAllProfiles().Where(p => p.UserId == userId).FirstOrDefault();
            var userToken = campaignView.userProfile.OAUserToken;
            OneAllFunctions oneAllService = new OneAllFunctions();
            campaignView.htmlContents = new List<ListItem>();
            campaignView.socialUser = oneAllService.GetUser(userToken);
            campaignView.myCampaignsModel = JsonConvert.DeserializeObject<MyCampaignsModel>(JsonModel);
            MindfireFunctions objMF = new MindfireFunctions();
            objMF.AuthenticateAccount(campaignView.userProfile.Email, campaignView.userProfile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])),
                campaignView.userProfile);
            byte[] reportCallData = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" +
                campaignView.userProfile.MFTicket + @""",""UserPassword"":""" + campaignView.userProfile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) +
                @"""},""MamlFormat"":0,""ProgramID"":" + campaignView.myCampaignsModel.Id + "}");
            JObject reportResultPost = JObject.FromObject(objConnection.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/GetProgram", reportCallData,
                "application/json", "POST", "", ""));
            string mamlXML = "";
            for (int x = 0; x <= reportResultPost["Maml"].Count() - 1; x++)
            {
                mamlXML = mamlXML + Chr(Convert.ToInt32(reportResultPost["Maml"][x].ToString()));
            }
            campaignView.myCampaignsModel.Program = mamlProgramCtr.FromMAMLString(mamlXML);
            foreach (var element in campaignView.myCampaignsModel.Program.Campaign.CampaignElements)
            {
                if (element.Type == "Email" && element.Properties != null && element.Properties.Messages != null && element.Properties.Messages.Message != null &&
                    element.Properties.Messages.Message.HtmlContent_URL != null)
                {
                    string sourceString;
                    try
                    {
                        sourceString = new WebClient().DownloadString(element.Properties.Messages.Message.HtmlContent_URL);
                    }
                    catch (Exception)
                    {
                        sourceString = element.Properties.Messages.Message.HtmlContent_URL;
                    }
                    // TODO: these replacements need to be configured for CMO after we have advisors set up in profile.
                    sourceString = sourceString.Replace("https://campaigns.lplmod.com/files/CompanyLogos/", "https://s3.amazonaws.com/lplmodact/AdvisorFiles/CompanyLogos/");
                    sourceString = sourceString.Replace("https://campaigns.lplmod.com/files/SignatureImages/", "https://lplmodact.s3.amazonaws.com/AdvisorFiles/SignatureImages/");
                    sourceString = sourceString.Replace("https://campaigns.lplmod.com/files/ProfilePhotos/", "https://lplmodact.s3.amazonaws.com/AdvisorFiles/ProfilePhotos/");
                    campaignView.htmlContents.Add(new ListItem
                    {
                        Value = element.Id,
                        Text = sourceString
                    });
                }
                else if (element.Type == "Microsite")
                {
                    var url = element.ChildElements.Where(x => x.Properties.IsHomePage == "True").First();
                    string sourceString;
                    try
                    {
                        sourceString = new WebClient().DownloadString(url.Properties.HtmlContent);
                    }
                    catch (Exception)
                    {
                        sourceString = url.Properties.HtmlContent;
                    }
                    campaignView.htmlContents.Add(new ListItem
                    {
                        Value = element.Id,
                        Text = sourceString
                    });
                }
                else
                {
                    campaignView.htmlContents.Add(new ListItem
                    {
                        Value = element.Id,
                        Text = "No html content"
                    });
                }
            }
            return View(campaignView);
        }

        [ValidateInput(false)]
        public JsonResult StopStartCampaign(string programID, string campaignName)
        {
            string mamlXML = "";
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().Where(p => p.UserId == userId).SingleOrDefault();
            MindfireFunctions objMF = new MindfireFunctions();
            objMF.AuthenticateAccount(profile.Email, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), ref profile);
            var campaignResultPost = objMF.CheckOutProgram(profile.MFTicket, profile.GetMFPassword(CBool(ConfigurationManager.AppSettings["MFPasswordMODAct"])), programID);
            if (campaignResultPost["Result"]["ErrorMessage"].ToString() != "")
            {
                return Json(new { message = campaignResultPost["Result"]["ErrorMessage"].ToString() }, JsonRequestBehavior.AllowGet);
            }
            else
            {
                for (int x = 0; x <= campaignResultPost["Maml"].Count() - 1; x++)
                {
                    mamlXML = mamlXML + Chr(Convert.ToInt32(campaignResultPost["Maml"][x].ToString()));
                }
                XmlDocument campXML = new XmlDocument();
                campXML.LoadXml(mamlXML);
                XmlElement rootLoad = campXML.DocumentElement;
                if (rootLoad.Attributes["State"].Value == "Start")
                {
                    rootLoad.Attributes["State"].Value = "Stop";
                    rootLoad.Item["Campaign"].Attributes["State"].Value = "Stop";
                    Campaigns campaignChangedStatus = _factMainService.FindAllCampaigns().Where(c => c.CampaignName == campaignName && c.UserId == userId).FirstOrDefault();
                    if (campaignChangedStatus != null)
                    {
                        campaignChangedStatus.Status = "Stop";
                    }
                    else
                    {
                        AmazonS3Service AService = new AmazonS3Service();
                        string fileDateTime = DateTime.Now.ToString("MMddyyyy") + "_" + DateTime.Now.ToString("HHmm");
                        AmazonResponse amazonResponse;
                        var mamlName = "Campaign_" + profile.MFAccountID + "_" + fileDateTime;
                        amazonResponse = AService.UploadMaml(mamlXML, mamlName);
                        Campaigns newCampaign = new Campaigns();
                        newCampaign.UserId = User.Identity.GetUserId();
                        newCampaign.CampaignName = campaignName;
                        newCampaign.LaunchDate = DateTime.Now;
                        newCampaign.CampaignFileURL = amazonResponse.Path;
                        newCampaign.DateSubmitted = DateTime.Now;
                        newCampaign.ApprovalToken = Guid.NewGuid().ToString();
                        newCampaign.Status = "Stop";
                        _factMainService.AddCampaigns(newCampaign);
                    }
                }
                else
                {
                    rootLoad.Attributes["State"].Value = "Start";
                    rootLoad.Item["Campaign"].Attributes["State"].Value = "Start";
                    Campaigns campaignChangedStatus = _factMainService.FindAllCampaigns().Where(c => c.CampaignName == campaignName && c.UserId == userId).FirstOrDefault();
                    if (campaignChangedStatus != null)
                    {
                        campaignChangedStatus.Status = "Start";
                    }
                    else
                    {
                        AmazonS3Service AService = new AmazonS3Service();
                        string fileDateTime = DateTime.Now.ToString("MMddyyyy") + "_" + DateTime.Now.ToString("HHmm");
                        AmazonResponse amazonResponse;
                        string mamlName = "Campaign_" + profile.MFAccountID + "_" + fileDateTime;
                        amazonResponse = AService.UploadMaml(mamlXML, mamlName);
                        Campaigns newCampaign = new Campaigns();
                        newCampaign.UserId = User.Identity.GetUserId();
                        newCampaign.CampaignName = campaignName;
                        newCampaign.LaunchDate = DateTime.Now;
                        newCampaign.CampaignFileURL = amazonResponse.Path;
                        newCampaign.DateSubmitted = DateTime.Now;
                        newCampaign.ApprovalToken = Guid.NewGuid().ToString();
                        newCampaign.Status = "Start";
                        _factMainService.AddCampaigns(newCampaign);
                    }
                }
                var campaignStopPost = objMF.CheckInProgram(campXML.InnerXml, profile.MFTicket, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])));
                if (campaignStopPost["Result"]["ErrorMessage"].ToString() != "")
                {
                    return Json(new { message = campaignStopPost["Result"]["ErrorMessage"].ToString(), status = rootLoad.Attributes["State"].Value }, JsonRequestBehavior.AllowGet);
                }
                else
                {
                    return Json(new { message = "Campaign Status Saved", status = rootLoad.Attributes["State"].Value }, JsonRequestBehavior.AllowGet);
                }
            }
        }

        public async Task<ActionResult> CampaignDetails(long? id)
        {
            if (id == null)
            {
                return new HttpStatusCodeResult(HttpStatusCode.BadRequest);
            }
            var userId = User.Identity.GetUserId;
            var campaign = await _factMainService.FindAllCampaigns().SingleOrDefault(p => p.Campaign_Id == id && p.UserId == userId);
            if (campaign == null)
            {
                return HttpNotFound();
            }
            return View(campaign);
        }

        [HttpPost]
        [AllowAnonymous]
        public async Task<ActionResult> RedTailCRMLogin(FormCollection form)
        {
            var userId = User.Identity.GetUserId();
            Profiles profile = _factMainService.FindAllProfiles().SingleOrDefault(p => p.UserId == userId);
            profile.RedtailUsername = form.Item["RedtailUsername"];
            profile.RedtailPassword = form.Item("RedtailPassword");
            _factMainService.UpdateProfiles(profile);
            return RedirectToAction("RedTailCRM");
        }

        public JsonResult checkDomain(string email)
        {
            return Json(objConnection.CheckDomain(email), JsonRequestBehavior.AllowGet);
        }

        public string PullRedtailContacts(string authString, string tagGroup)
        {
            GlobalX objConnection = new GlobalX();
            JObject redtailAuth = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://api2.redtailtechnology.com/crm/v1/rest/taggroups/" +
                tagGroup + "/contacts", null, "text/json", "GET", authString, "Basic"));
            List<ContactsModel> listOfContacts = new List<ContactsModel>();
            if (redtailAuth != null)
            {
                if (redtailAuth["TagMembers"] != null)
                {
                    for (int i = 0; i <= redtailAuth["TagMembers"].Count() - 1; i++)
                    {
                        ContactsModel contact = new ContactsModel();
                        JObject redtailGetIdData = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://api2.redtailtechnology.com/crm/v1/rest/contacts/" +
                            redtailAuth["TagMembers"][i]["ContactID"]) + "/master", null, "text/json", "GET", authString, "Basic");
                        contact.FirstName = objConnection.cleanString(redtailGetIdData["ContactRecord"]["Firstname"].ToString());
                        contact.LastName = objConnection.cleanString(redtailGetIdData["ContactRecord"]["Lastname"].ToString());
                        Random rnd = new Random(6001 * DateTime.Now.Millisecond);
                        contact.AccountID = Convert.ToInt32(rnd);
                        if (redtailGetIdData["ContactRecord"]["Company"].Count() > 0)
                        {
                            contact.Company = objConnection.cleanString(redtailGetIdData["ContactRecord"]["Company"].ToString());
                        }
                        else
                        {
                            contact.Company = "";
                        }
                        //Phone
                        for (int w = 0; w <= redtailGetIdData["Phone"].Count() - 1; w++)
                        {
                            switch (redtailGetIdData["Phone"][w]["Type"].ToString())
                            {
                                case "Cellular":
                                    contact.CellPhone = objConnection.cleanString(redtailGetIdData["Phone"][w]["Number"].ToString());
                                    break;
                                default:
                                    break;
                            }
                        }
                        //Address 1
                        if (redtailGetIdData["Address"].Count() > 0)
                        {
                            contact.Address1 = objConnection.cleanString(redtailGetIdData["Address"][0]["Address1"].ToString());
                        }
                        else
                        {
                            contact.Address1 = "";
                        }
                        //Address 2
                        if (redtailGetIdData["Address"].Count() > 0)
                        {
                            contact.Address2 = objConnection.cleanString(redtailGetIdData["Address"][0]["Address2"].ToString());
                        }
                        else
                        {
                            contact.Address2 = "";
                        }
                        //City
                        if (redtailGetIdData["Address"].Count() > 0)
                        {
                            contact.City = objConnection.cleanString(redtailGetIdData["Address"][0]["City"].ToString());
                        }
                        else
                        {
                            contact.City = "";
                        }
                        //State
                        if (redtailGetIdData["Address"].Count() > 0)
                        {
                            contact.State = objConnection.cleanString(redtailGetIdData["Address"][0]["State"].ToString());
                        }
                        else
                        {
                            contact.State = "";
                        }
                        //Zip
                        if (redtailGetIdData["Address"].Count() > 0)
                        {
                            contact.Zip = objConnection.cleanString(redtailGetIdData["Address"][0]["Zip"].ToString());
                        }
                        else
                        {
                            contact.Zip = "";
                        }
                        //Internet Addresses
                        for (int x = 0; x <= redtailGetIdData["Internet"].Count() - 1; x++)
                        {
                            switch (redtailGetIdData["Internet"][x]["Type"].ToString())
                            {
                                case "Website 1":
                                    contact.WebSite = objConnection.cleanString(redtailGetIdData["Internet"][x]["Address"].ToString());
                                    break;
                                case "Email 1":
                                    {
                                        contact.Email = objConnection.cleanString(redtailGetIdData["Internet"][x]["Address"].ToString());
                                        break;
                                    }
                                default:
                                    break;
                            }
                        }
                        // redtail returns incorrect birth date when it's empty on their side
                        var dateToken = redtailGetIdData["ContactRecord"]["DateOfBirth"];
                        if (dateToken.Type == JTokenType.Date)
                        {
                            contact.Birthdate = Convert.ToDateTime(redtailGetIdData["ContactRecord"]["DateOfBirth"].ToString());
                        }
                        listOfContacts.Add(contact);
                        ViewBag.ContactsList = listOfContacts;
                    }
                }
                else
                {
                }
                ViewBag.ContactsList = listOfContacts;
            }
            else
            {
            }
            foreach (ContactsModel item in listOfContacts)
            {
                strImportData = strImportData + item.FirstName + "," + item.LastName + "," + item.AccountID + "," + item.Company + "," + item.CellPhone + "," +
                    item.Address1 + "," + item.Address2 + "," + item.City + "," + item.State + "," + item.Zip + "," + item.Email + "," +
                    item.WebSite + "," + item.Birthdate + "," + item.Anniversary + Environment.NewLine;
            }
            strImportData = strImportData.Substring(0, strImportData.Length - 1);
            strImportHeader = "FirstName,LastName,AccountID,Company,Mobile,Address1,Address2,City,State,Zip,Email,Website,BirthDate,Anniversary";
            var strFinal = strImportHeader + Environment.NewLine + strImportData;
            strFinal = strFinal.Replace("\n", "");
            return strFinal;
        }

        private void PullRedtailTagGroups(string strAuth)
        {
            listOfSelectItems = new List<SelectListItem>();
            JArray redtailAuth = JArray.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://api2.redtailtechnology.com/crm/v1/rest/settings/taggroups", null, "text/json", "GET", strAuth, "Basic"));
            for (int i = 0; i <= redtailAuth.Count() - 1; i++)
            {
                SelectListItem newListItem = new SelectListItem();
                newListItem.Text = redtailAuth.Children().ElementAt(i).ElementAt(1).ToString();
                newListItem.Value = redtailAuth.Children().ElementAt(i).ElementAt(4).ToString();
                listOfSelectItems.Add(newListItem);
            }
        }
    }
}