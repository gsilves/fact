﻿using FACT.Domain.Models;
using FACT.Domain.Models.EntityModels;
using FACT.Domain.Models.OneAllModels;
using FACT.Service;
using FACT.Web.Business;
using FACT.Web.Services;
using Microsoft.AspNet.Identity;
using Microsoft.Owin.Security;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;

namespace FACT.Web.Controllers
{
    public class HomeController : Controller
    {
        private readonly IFactMainService _factMainService;

        public HomeController(IFactMainService factMainService)
        {
            _factMainService = factMainService;
        }

        protected IAuthenticationManager AuthenticationManager
        {
            get
            {
                return HttpContext.GetOwinContext().Authentication;
            }
        }

        [Authorize]
        public ActionResult Index()
        {
            var userId = User.Identity.GetUserId();
            var Profile = _factMainService.FindProfileById(Convert.ToInt64(userId));
            if (Profile == null)
            {
                AuthenticationManager.SignOut(DefaultAuthenticationTypes.ApplicationCookie);
                return RedirectToAction("Index", "Home");
            }
            MindfireFunctions objMF = new MindfireFunctions();
            objMF.AuthenticateAccount(Profile.Email, Profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), ref Profile);
            List<MyCampaignsModel> listOfMyCampaignModels = ControllerFunctions.GetMyCampaigns(Profile, _factMainService).Take(4).ToList();
            ViewBag.NumOfActiveCampaigns = ControllerFunctions.GetMyCampaigns(Profile, _factMainService).
                Where(p => p.State == 0 && (p.DbStatus == "Start" || p.DbStatus == "Approved")).Count();
            ViewBag.NumOfWaitingCampaigns = ControllerFunctions.GetMyCampaigns(Profile, _factMainService).
                Where(p => p.State == 1 && p.DbStatus == "WaitingApproval").Count();
            ViewBag.NumOfCompletedCampaigns = ControllerFunctions.GetMyCampaigns(Profile, _factMainService).
                Where(p => p.State == 1 && (p.DbStatus == "Start" || p.DbStatus == "Approved")).Count();
            OneAllFunctions oaf = new OneAllFunctions();
            string OAuserToken = Profile.OAUserToken;
            // check if social account token has expired
            if (OAuserToken != null)
            {
                OneAllUser userSocials = oaf.GetUser(OAuserToken);
                if (userSocials != null)
                {
                    List<Identity> identities = userSocials.identities;
                    //Dim profileLinkedinLastUpdate As DateTime = Nothing
                    StringBuilder personaViewBagString = new StringBuilder();
                    // check if profile has expired social account tokens
                    if (identities != null)
                    {
                        DateTime now = DateTime.Now;
                        foreach (Identity identity in identities)
                        {
                            if (identity.provider == "linkedin")
                            {
                                JObject identityToken = oaf.ReturnIdentityDetails(identity.identity_token);
                                if (identityToken != null)
                                {
                                    DateTime exp_date = Convert.ToDateTime(identityToken["response"]["result"]["data"]["identity"]["source"]["access_token"]["date_expiration"].ToString());
                                    double diff = (exp_date - DateTime.Now).TotalDays;
                                    if (diff <= 14 && diff >= 0)
                                    {
                                        personaViewBagString.Append("LinkedIn account: " + identity.displayName + " for advisor: " + Profile.FirstName + 
                                            " " + Profile.LastName + " will expire for " + diff + "days! /n");
                                    }
                                    else
                                    {
                                        if (diff < 0)
                                        {
                                            personaViewBagString.Append("LinkedIn account: " + identity.displayName + " for advisor: " + 
                                                Profile.FirstName + " " + Profile.LastName + " has been expired before" + (-diff) + "days! /n");
                                        }
                                    }
                                }
                                else
                                {
                                    personaViewBagString.Append("LinkedIn account: " + identity.date_creation + " for advisor: " + Profile.FirstName + " " + Profile.LastName +
                                                                " is not found ! /n");
                                }
                            }
                        }
                    }
                    // check if personas has expired social account tokens
                    List<Personas> personas = _factMainService.FindAllPersonas().Where(p => p.UserId == userId).ToList();
                    foreach (Personas persona in personas)
                    {
                        //Dim OAuserToken1 = persona.OAUserToken
                        OneAllUser userSocials1 = oaf.GetUser(OAuserToken);
                        if (userSocials1 != null || userSocials.identities != null)
                        {
                            List<Identity> identities1 = userSocials.identities;
                            DateTime now = DateTime.Now;
                            foreach (Identity identity in identities1)
                            {
                                if (identity.provider == "linkedin")
                                {
                                    JObject identityToken = oaf.ReturnIdentityDetails(identity.identity_token);
                                    if (identityToken != null)
                                    {
                                        DateTime exp_date = Convert.ToDateTime(identityToken["response"]["result"]["data"]["identity"]["source"]["access_token"]["date_expiration"].ToString());
                                        double diff = (exp_date - DateTime.Now).TotalDays;
                                        if (diff <= 14 && diff >= 0)
                                        {
                                            personaViewBagString.Append("LinkedIn account: " + identity.displayName + " for persona: " + 
                                                Profile.FirstName + " " + Profile.LastName + " will expire for " + diff + "days! /n");
                                        }
                                        else
                                        {
                                            if (diff < 0)
                                            {
                                                personaViewBagString.Append("LinkedIn account: " + identity.displayName + " for persona: " + 
                                                    Profile.FirstName + " " + Profile.LastName + "has been expired before" + (-diff) + "days! /n");
                                            }
                                        }
                                    }
                                    else
                                    {
                                        personaViewBagString.Append("LinkedIn account: " + identity.date_creation + " for persona: " + 
                                            Profile.FirstName + " " + Profile.LastName + " is not found ! /n");
                                    }
                                }
                            }
                        }
                        else
                        {
                            personaViewBagString.Append("LinkedIn account for persona: " + persona.Name + "has been expiried or not valid! /n");
                        }
                    }
                }
            }
            return View(listOfMyCampaignModels);
        }

        [Authorize]
        public ActionResult About()
        {
            ViewData["Message"] = "Your application description page.";
            return View();
        }

        [Authorize]
        public ActionResult Contact()
        {
            ViewData["Message"] = "";
            return View();
        }

        [Authorize]
        public ActionResult Branding()
        {
            ViewData["Message"] = "Your branding page.";
            return View();
        }

        [Authorize]
        public ActionResult Touchpoints()
        {
            ViewData["Message"] = "Your Touchpoints page.";
            return View();
        }

        [Authorize]
        public ActionResult Admin()
        {
            ViewData["Message"] = "Your Admin page.";
            return View();
        }
    }
}