﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FACT.Web.Models
{
    public class SettingsViewModel
    {
        public long Id { get; set; }

        [Display(Name = "Approval Requiered"]
        public bool ApprovalRequired { get; set; }

        public bool IsCreditChargeable { get; set; }
        public bool IsSubscriptionSignable { get; set; }
    }
}