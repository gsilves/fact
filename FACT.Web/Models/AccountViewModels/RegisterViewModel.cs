﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FACT.Web.Models.AccountViewModels
{
    public class RegisterViewModel
    {
        [Required]
        public long Profile_Id { get; set; }

        [Required]
        [EmailAddress]
        [MaxLength(200)]
        [DataType(DataType.EmailAddress)]
        [Display(Name = "Email")]
        public string Email { get; set; }

        [Required]
        [StringLength(100, ErrorMessage = "The {0} must be at least {2} characters long.", MinimumLength = 6)]
        [DataType(DataType.Password)]
        [Display(Name = "Password")]
        public string Password { get; set; }

        [DataType(DataType.Password)]
        [MaxLength(50)]
        [Display(Name = "Confirm password")]
        [Compare("Password", ErrorMessage = "The password and confirmation password do not match.")]
        public string ConfirmPassword { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "First name")]
        public string FirstName { get; set; }

        [Required]
        [MaxLength(50)]
        [Display(Name = "Last name")]
        public string LastName { get; set; }

        [Phone]
        [MaxLength(50)]
        [Required]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Phone number")]
        public string PhoneNumber { get; set; }

        [Phone]
        [MaxLength(50)]
        [DataType(DataType.PhoneNumber)]
        [Display(Name = "Alternate Phone number")]
        public string AlternatePhoneNumber { get; set; }

        [MaxLength(50)]
        public string Company { get; set; }
    }
}