﻿using FACT.Domain.Models.MiscModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace FACT.Web.Models
{
    public class SalesForceImportViewModel
    {
        public string intCSVLimit { get; set; }
        public string intContactLimit { get; set; }
        public string okResponse { get; set; }
        public string ErrorResponse { get; set; }
        public string jResponseX { get; set; }
        public string jResponseY { get; set; }
        public string CustomFieldValue { get; set; }
        public string MFListName { get; set; }
        public string GroupName { get; set; }

        [Display(Name = "Filter by Custom Field")]
        public string SelectedContact { get; set; }

        [Display(Name = "Filter by Lead")]
        public string SelectedLead { get; set; }

        public ICollection<SelectListItem> CustomFieldsContact { get; set; }
        public ICollection<SelectListItem> CustomFieldsLead { get; set; }
        public bool SFAuth { get; set; }
        public bool CCFormValidated { get; set; }
        public string SFState { get; set; }
        public string SFAccount { get; set; }
        public string SFLeadSource { get; set; }
        public bool jWrap { get; set; }
        public bool jWrapE { get; set; }
        public bool ListSubmitVisible { get; set; }
        public bool contactResult { get; set; }
        public bool excludeNoEmail { get; set; }
        public string ImportDataField { get; set; }
        public List<ContactImportAdmin> Contacts { get; set; }
        public string SegOptions { get; set; }
    }
}