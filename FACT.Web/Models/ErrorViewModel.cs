﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Configuration;
using System.Linq;
using System.Web;
using System.Web.UI.WebControls;

namespace FACT.Web.Models
{
    public class ErrorViewModel
    {
        public class ErrorModel
        {
            public ErrorModel(string title, string message, string type, string returnUrl)
            {
                Title = title;
                Message = message;
                Type = type;
                ReturnUrl = returnUrl.Replace("~", ConfigurationManager.AppSettings["RootURL"]);
                Timestamp = DateTime.Now;
            }

            public int Id { get; set; }

            [Display(Name = nameof(Table))]
            public string Title { get; set; }

            public string Message { get; set; }

            [NotMapped]
            [Display(Name = "Return Url")]
            public string ReturnUrl { get; set; }

            [Display(Name = nameof(Type))]
            public string Type { get; set; }

            [Display(Name = nameof(Timestamp))]
            public DateTime Timestamp { get; set; }
        }
    }
}