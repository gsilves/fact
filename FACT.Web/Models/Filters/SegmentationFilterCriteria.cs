﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FACT.Web.Models.Filters
{
    [XmlRoot("Criteria")]
    public class SegmentationFilterCriteria
    {
        [XmlAttribute]
        public int Row { get; set; }

        [XmlAttribute]
        public string Field { get; set; }

        [XmlAttribute("Operator")]
        public string Operation { get; set; }

        [XmlAttribute]
        public string Value { get; set; }
    }
}