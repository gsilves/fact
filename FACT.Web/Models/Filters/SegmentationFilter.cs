﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Xml.Serialization;

namespace FACT.Web.Models.Filters
{
    [XmlRoot("Filter")]
    public class SegmentationFilter
    {
        [XmlElement("Criteria")]
        public SegmentationFilterCriteria[] Criterias { get; set; }

        [XmlAttribute]
        public string CriteriaJoinOperator { get; set; }
    }
}