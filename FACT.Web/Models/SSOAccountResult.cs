﻿using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.Models
{
    public class SSOAccountResult
    {
        public Profiles ProfileModel { get; set; }
        public string ResultStr { get; set; }
        public Exception ExErr { get; set; }
    }
}