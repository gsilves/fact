﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Xml;
using System.Xml.Serialization;

namespace FACT.Web.Helpers
{
    public class XmlHelper
    {
        public static string Serialize<T>(T inputObject)
        {
            XmlWriterSettings settings = new XmlWriterSettings();
            settings.OmitXmlDeclaration = true;
            settings.ConformanceLevel = ConformanceLevel.Auto;
            XmlSerializerNamespaces namespaces = new XmlSerializerNamespaces();
            namespaces.Add("", "");
            XmlSerializer serializer = new XmlSerializer(inputObject.GetType());
            using (StringWriter stringWriterObject = new StringWriter())
            {
                XmlWriter xmlWriterObject = XmlWriter.Create(stringWriterObject, settings);
                serializer.Serialize(xmlWriterObject, inputObject, namespaces);
                return stringWriterObject.ToString();
            }
        }
    }
}