﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FACT.Web.CSV
{
    public class UploadCSVConfirmation
    {
        [Display(Name = "Status Name:")]
        public string StatusName { get; set; }

        [Display(Name = "Rejected Records:")]
        public string RejectedRecords { get; set; }

        [Display(Name = "Processed Records:")]
        public string ProcessedRecords { get; set; }

        [Display(Name = "File Name:")]
        public string FileName { get; set; }

        [Display(Name = "Mode Name:")]
        public string ModeName { get; set; }

        [Display(Name = "Successful Records:")]
        public string SuccessfulRecords { get; set; }

        public string Status { get; set; }
        public string Mode { get; set; }
        public string ID { get; set; }
    }
}