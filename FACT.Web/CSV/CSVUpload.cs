﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CSV
{
    public class CSVUpload
    {
        public string FirstName
        {
            get
            {
                return m_FirstName;
            }
            set
            {
                m_FirstName = value;
            }
        }

        private string m_FirstName { get; set; }

        public string LastName
        {
            get
            {
                return m_LastName;
            }
            set
            {
                m_LastName = value;
            }
        }

        private string m_LastName { get; set; }

        public string Company
        {
            get
            {
                return m_Company;
            }
            set
            {
                m_Company = value;
            }
        }

        private string m_Company { get; set; }

        public string Mobile
        {
            get
            {
                return m_Mobile;
            }
            set
            {
                m_Mobile = value;
            }
        }

        private string m_Mobile { get; set; }

        public string Address1
        {
            get
            {
                return m_Address1;
            }
            set
            {
                m_Address1 = value;
            }
        }

        private string m_Address1 { get; set; }

        public string Address2
        {
            get
            {
                return m_Address2;
            }
            set
            {
                m_Address2 = value;
            }
        }

        private string m_Address2 { get; set; }

        public string City
        {
            get
            {
                return m_City;
            }
            set
            {
                m_City = value;
            }
        }

        private string m_City { get; set; }

        public string State
        {
            get
            {
                return m_State;
            }
            set
            {
                m_State = value;
            }
        }


        private string m_State { get; set; }

        public string Zip
        {
            get
            {
                return m_Zip;
            }
            set
            {
                m_Zip = value;
            }
        }

        private string m_Zip { get; set; }

        public string Email
        {
            get
            {
                return m_Email;
            }
            set
            {
                m_Email = value;
            }
        }

        private string m_Email { get; set; }

        public string Website
        {
            get
            {
                return m_Website;
            }
            set
            {
                m_Website = value;
            }
        }

        private string m_Website { get; set; }

        public string BirthDate
        {
            get
            {
                return m_BirthDate;
            }
            set
            {
                m_BirthDate = value;
            }
        }

        private string m_BirthDate { get; set; }

        public string Anniversary
        {
            get
            {
                return m_Anniversary;
            }
            set
            {
                m_Anniversary = value;
            }
        }

        private string m_Anniversary { get; set; }

        public string ContactSource
        {
            get
            {
                return m_ContactSource;
            }
            set
            {
                m_ContactSource = value;
            }
        }

        private string m_ContactSource { get; set; }

        public string GroupName
        {
            get
            {
                return m_GroupName;
            }
            set
            {
                m_GroupName = value;
            }
        }

        private string m_GroupName { get; set; }
    }
}