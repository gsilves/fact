﻿var
	ckeConfig = {

		skin: 'moono-lisa',
		contentsCss: CKEDITOR.getUrl('contents_mod.min.css'),

		removePlugins: 'showblocks,templates',
		extraPlugins: '_ans_showblocks,_ans_templates',

		//autoEmbed_widget: 'customEmbed',
		forcePasteAsPlainText: true,
		tabSpaces: 0,
		htmlEncodeOutput: true,
		startupOutlineBlocks: true,
		height: '30em',
		linkShowAdvancedTab: false,
		linkShowTargetTab: true,
		linkJavaScriptLinksAllowed: false,
		keystrokes: [
			[122/*F11*/, 'maximize'],
			[CKEDITOR.CTRL + 49 /*Ctrl+1*/, 'h1'],
			[CKEDITOR.CTRL + 50 /*Ctrl+2*/, 'h2'],
			[CKEDITOR.CTRL + 51 /*Ctrl+3*/, 'h3'],
			[CKEDITOR.CTRL + 52 /*Ctrl+4*/, 'h4'],
			[CKEDITOR.CTRL + 53 /*Ctrl+5*/, 'h5'],
			[CKEDITOR.CTRL + 54 /*Ctrl+6*/, 'h6'],
		],
		toolbar: [
			['Styles', 'Format'],
			['Cut', 'Copy', 'PasteText', '-', 'Undo', 'Redo'],
			['Link', 'Unlink', 'Anchor'],
			['Image', 'Table', 'SpecialChar'],
			['Find', 'Replace'],
			['Maximize'],
			'/',
			['Bold', 'Italic', 'Underline', 'Strike', 'Subscript', 'Superscript', '-', 'RemoveFormat'],
			['NumberedList', 'BulletedList', '-', 'Outdent', 'Indent', '-', 'Blockquote', '-', 'JustifyLeft', 'JustifyCenter', 'JustifyRight'],
			['Source', '-', 'Templates'],
		],

		format_tags: 'h1;h2;h3;h4;h5;h6;p;div',

		stylesSet: [
			/* Block Styles */
			{
				name: 'Container',
				element: 'div',
				styles: {
					padding: '5px 10px',
					background: '#eee',
					border: '1px solid #ccc'
				}
			},
			/* Inline Styles */
			{ name: 'Large', element: 'span', attributes: { 'class': 'text-large' } },
			{ name: 'Small', element: 'span', attributes: { 'class': 'text-small' } },
			{ name: 'Computer Code', element: 'code' },
			{ name: 'Deleted Text', element: 'del' },
			{ name: 'Inserted Text', element: 'ins' },
			/* Object Styles */
			{ name: 'Styled image (left)', element: 'img', attributes: { 'class': 'left' } },
			{ name: 'Styled image (right)', element: 'img', attributes: { 'class': 'right' } },
			{ name: 'Table doc', element: 'table', attributes: { 'class': 'table table-doc' } },
			{ name: 'Table auto', element: 'table', attributes: { 'class': 'table table-auto' } },
		],

		addTemplates: {},

	};


(function ($) {

	$(".form-ckeditor").ckeditor(ckeConfig);

})(jQuery);
