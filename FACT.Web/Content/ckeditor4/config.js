/**
 * @license Copyright (c) 2003-2017, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function (config) {
    // Define changes to default configuration here. For example:
    //config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.height = 700;
    config.toolbarCanCollapse = true;
    config.toolbarStartupExpanded = false;
    config.removeButtons = 'About,Language,Underline,JustifyCenter,Flash,NewPage,Templates,InsertMediaEmbed,iFrame,Form,TextField,Button,Save,Checkbox,Textarea,RadioButton,HiddenField,Select';
    config.skin = 'icy_orange';
    config.startupShowBorders = false;
    config.startupOutlineBlocks = false;
    config.autoGrow_onStartup = true;
    config.fullPage = true;
};
