﻿(function () {
	CKEDITOR.plugins.add('_ans_templates', {
		requires: 'dialog',
		lang: 'en,ru',
		icons: 'templates,templates-rtl',
		hidpi: true,
		init: function (editor) {
			CKEDITOR.dialog.add('templates', CKEDITOR.getUrl(this.path + 'dialogs/templates.js'));
			editor.addCommand('templates', new CKEDITOR.dialogCommand('templates'));

			editor.ui.addButton && editor.ui.addButton('Templates', {
				label: editor.lang.templates.button,
				command: 'templates',
				toolbar: 'doctools,10'
			});
		}
	});

	var templates = {},
		loadedTemplatesFiles = {};

	CKEDITOR.addTemplates = function (name, definition) {
		templates[name] = definition;
	};

	CKEDITOR.getTemplates = function (name) {
		return templates[name];
	};

	CKEDITOR.loadTemplates = function (templateFiles, callback) {
		// Holds the templates files to be loaded.
		var toLoad = [];

		// Look for pending template files to get loaded.
		for (var i = 0, count = templateFiles.length; i < count; i++) {
			if (!loadedTemplatesFiles[templateFiles[i]]) {
				toLoad.push(templateFiles[i]);
				loadedTemplatesFiles[templateFiles[i]] = 1;
			}
		}

		if (toLoad.length)
			CKEDITOR.scriptLoader.load(toLoad, callback);
		else
			setTimeout(callback, 0);
	};
})();



/**
 * The templates definition set to use. It accepts a list of names separated by
 * comma. It must match definitions loaded with the {@link #templates_files} setting.
 *
 *		config.templates = 'my_templates';
 *
 * @cfg {String} [templates='default']
 * @member CKEDITOR.config
 */

/**
 * The list of templates definition files to load.
 *
 *		config.templates_files = [
 *			'/editor_templates/site_default.js',
 *			'http://www.example.com/user_templates.js
 *		];
 *
 * @cfg
 * @member CKEDITOR.config
 */
CKEDITOR.config.templates_files = [
	CKEDITOR.getUrl('plugins/_ans_templates/templates/default.js')
];

/**
 * Whether the "Replace actual contents" checkbox is checked by default in the
 * Templates dialog.
 *
 *		config.templates_replaceContent = false;
 *
 * @cfg
 * @member CKEDITOR.config
 */
CKEDITOR.config.templates_replaceContent = true;
