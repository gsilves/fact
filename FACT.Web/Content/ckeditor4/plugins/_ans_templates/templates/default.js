﻿CKEDITOR.addTemplates('default', {

	imagesPath: CKEDITOR.getUrl(CKEDITOR.plugins.getPath('_ans_templates') + 'templates/images/'),

	templates: [
		{
			title: 'Иллюстрация и название',
			image: 'template1.gif',
			description: 'One main image with a title and text that surround the image.',
			html:
				'<h3><img src=" " alt="" />Название</h3>' +
				'<p>Тест</p>'
		},
		{
			title: 'Таблица',
			image: 'template2.gif',
			description: 'A template that defines two colums, each one with a title, and some text.',
			html: '<table cellspacing="0" cellpadding="0" style="width:100%" border="0">' +
				'<tr>' +
					'<td style="width:50%">' +
						'<h3>Title 1</h3>' +
					'</td>' +
					'<td></td>' +
					'<td style="width:50%">' +
						'<h3>Title 2</h3>' +
					'</td>' +
				'</tr>' +
				'<tr>' +
					'<td>' +
						'Text 1' +
					'</td>' +
					'<td></td>' +
					'<td>' +
						'Text 2' +
					'</td>' +
				'</tr>' +
				'</table>' +
				'<p>' +
				'More text goes here.' +
				'</p>'
		},
		{
			title: 'Текст и таблица',
			image: 'template3.gif',
			description: 'A title with some text and a table.',
			html: '<div style="width: 80%">' +
				'<h3>' +
					'Title goes here' +
				'</h3>' +
				'<table style="width:150px;float: right" cellspacing="0" cellpadding="0" border="1">' +
					'<caption style="border:solid 1px black">' +
						'<strong>Table title</strong>' +
					'</caption>' +
					'<tr>' +
						'<td>&nbsp;</td>' +
						'<td>&nbsp;</td>' +
						'<td>&nbsp;</td>' +
					'</tr>' +
					'<tr>' +
						'<td>&nbsp;</td>' +
						'<td>&nbsp;</td>' +
						'<td>&nbsp;</td>' +
					'</tr>' +
					'<tr>' +
						'<td>&nbsp;</td>' +
						'<td>&nbsp;</td>' +
						'<td>&nbsp;</td>' +
					'</tr>' +
				'</table>' +
				'<p>' +
					'Type the text here' +
				'</p>' +
				'</div>'
		}
	]
});
