﻿//
//	config.ansShowblocksMode = true;
//
(function () {
	'use strict';

	var commandDefinition = {
		readOnly: 1,
		preserveState: true,
		editorFocus: false,

		exec: function (editor) {
			this.toggleState();
			this.refresh(editor);
		},

		refresh: function (editor) {
			if (editor.document) {
				var showBlocks = (this.state == CKEDITOR.TRISTATE_ON
					&& (editor.elementMode != CKEDITOR.ELEMENT_MODE_INLINE
						|| editor.focusManager.hasFocus));
				var funcName = showBlocks ? 'attachClass' : 'removeClass';
				editor.editable()[funcName]('cke_show_blocks');
			}
		}
	};

	CKEDITOR.plugins.add('_ans_showblocks', {
		lang: 'en,ru',
		icons: 'showblocks,showblocks-rtl',
		hidpi: true,

		onLoad: function () {
			var
				tags = [
					'th', 'td', 'ul', 'ol', 'li', 'dl', 'dt', 'dd',
					'p', 'div', 'pre', 'address', 'blockquote',
					'h1', 'h2', 'h3', 'h4', 'h5', 'h6'
				],
				cssStd, cssImg, cssLtr, cssRtl,
				path = CKEDITOR.getUrl(this.path),
				supportsNotPseudoclass = !(CKEDITOR.env.ie && CKEDITOR.env.version < 9),
				notDisabled = supportsNotPseudoclass ? ':not([contenteditable=false]):not(.cke_show_blocks_off)' : '',
				tag, trailing;
			cssStd = cssImg = cssLtr = cssRtl = '';
			while ((tag = tags.pop())) {
				trailing = tags.length ? ',' : '';
				cssStd += '.cke_show_blocks ' + tag + notDisabled + trailing;
				cssLtr += '.cke_show_blocks.cke_contents_ltr ' + tag + notDisabled + trailing;
				cssRtl += '.cke_show_blocks.cke_contents_rtl ' + tag + notDisabled + trailing;
				cssImg += '.cke_show_blocks ' + tag + notDisabled + '{' + 'background-image:url(' + CKEDITOR.getUrl(path + 'images/block_' + tag + '.png') + ')' + '}';
			}
			cssStd += '{background-repeat:no-repeat;border:1px dotted #ccc;padding-top:6px;}';
			cssLtr += '{background-position:top left;padding-left:6px;}';
			cssRtl += '{background-position:top right;padding-right:6px;}';
			CKEDITOR.addCss(cssStd.concat(cssImg, cssLtr, cssRtl));
			if (!supportsNotPseudoclass) {
				CKEDITOR.addCss(
					'.cke_show_blocks [contenteditable=false],.cke_show_blocks .cke_show_blocks_off{' +
						'border:none;padding-top:0;background-image:none;}' +
					'.cke_show_blocks.cke_contents_rtl [contenteditable=false],.cke_show_blocks.cke_contents_rtl .cke_show_blocks_off{' +
						'padding-right:0;}' +
					'.cke_show_blocks.cke_contents_ltr [contenteditable=false],.cke_show_blocks.cke_contents_ltr .cke_show_blocks_off{' +
						'padding-left:0;}'
				);
			}
		},

		init: function (editor) {
			if (editor.blockless)
				return;
			var command = editor.addCommand('_ans_showblocks', commandDefinition);
			command.canUndo = false;
			if (editor.config.startupOutlineBlocks)
				command.setState(CKEDITOR.TRISTATE_ON);

			//editor.ui.addButton && editor.ui.addButton('ShowBlocks', {
			//	label: editor.lang.showblocks.toolbar,
			//	command: 'showblocks',
			//	toolbar: 'tools,20'
			//});

			editor.on('mode', function () {
				if (command.state != CKEDITOR.TRISTATE_DISABLED)
					command.refresh(editor);
			});
			if (editor.elementMode == CKEDITOR.ELEMENT_MODE_INLINE) {
				editor.on('focus', onFocusBlur);
				editor.on('blur', onFocusBlur);
			}
			editor.on('contentDom', function () {
				if (command.state != CKEDITOR.TRISTATE_DISABLED)
					command.refresh(editor);
			});
			function onFocusBlur() {
				command.refresh(editor);
			}
		}
	});

})();
