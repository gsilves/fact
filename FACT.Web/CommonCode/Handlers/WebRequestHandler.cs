﻿using FACT.Web.CommonCode.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Net.Http;
using System.Text;

namespace FACT.Web.CommonCode.Handlers
{
    public class WebRequestHandler : IWebRequestHandler
    {
        /// <summary>
        /// The valid HTTP error codes.
        /// </summary>
        private ICollection<HttpStatusCode> validHttpErrorCodes = new HashSet<HttpStatusCode>();

        /// <summary>
        /// Sets the valid HTTP error codes.
        /// </summary>
        /// <param name="validHttpErrorCodes">The valid HTTP error codes.</param>
        public void SetValidHttpErrorCodes(params HttpStatusCode[] validHttpErrorCodes)
        {
            this.validHttpErrorCodes = new HashSet<HttpStatusCode>(validHttpErrorCodes);
        }

        /// <summary>
        /// Processes the get request.
        /// </summary>
        /// <param name="uriString">The URI string.</param>
        /// <param name="requestHeaders">The request headers.</param>
        /// <returns>Response string.</returns>
        public string ProcessGetRequest(string uriString, IEnumerable<KeyValuePair<string, string>> requestHeaders)
        {
            Uri uri = new Uri(uriString);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = FACT.Web.CommonCode.Constants.HttpMethod.HttpGet;
            this.AddRequestHeaders(request, requestHeaders);
            return this.GetResponseString(request);
        }

        /// <summary>
        /// Processes the post form request.
        /// </summary>
        /// <param name="uriString">The URI string.</param>
        /// <param name="requestContent">Content of the request.</param>
        /// <param name="requestHeaders">The request headers.</param>
        /// <returns>Response string.</returns>
        public string ProcessPostFormRequest(string uriString, IEnumerable<KeyValuePair<string, string>> requestContent, IEnumerable<KeyValuePair<string, string>> requestHeaders)
        {
            Uri uri = new Uri(uriString);
            HttpWebRequest request = (HttpWebRequest)WebRequest.Create(uri);
            request.Method = FACT.Web.CommonCode.Constants.HttpMethod.HttpPost;
            this.AddFormRequestBody(request, requestContent);
            this.AddRequestHeaders(request, requestHeaders);
            return this.GetResponseString(request);
        }

        /// <summary>
        /// Adds body to the request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="requestContent">Content of the request.</param>
        private void AddFormRequestBody(HttpWebRequest request, IEnumerable<KeyValuePair<string, string>> requestContent)
        {
            if (requestContent != null)
            {
                using (var formContent = new FormUrlEncodedContent(requestContent))
                {
                    var requestBodyString = formContent.ReadAsStringAsync().Result;
                    this.WriteRequestBody(request, requestBodyString);
                }
            }
        }

        /// <summary>
        /// Adds the request headers to the provided request object.
        /// </summary>
        /// <param name="request">The request object.</param>
        /// <param name="requestHeaders">The request headers.</param>
        private void AddRequestHeaders(HttpWebRequest request, IEnumerable<KeyValuePair<string, string>> requestHeaders)
        {
            if (requestHeaders != null)
            {
                foreach (var pair in requestHeaders)
                {
                    var headerName = pair.Key;
                    var headerValue = pair.Value;
                    switch (headerName)
                    {
                        case "Accept":
                            request.Accept = headerValue;
                            break;
                        case "Connection":
                            request.Connection = headerValue;
                            break;
                        case "Content-Length":
                            request.ContentLength = long.Parse(headerValue);
                            break;
                        case "Content-Type":
                            request.ContentType = headerValue;
                            break;
                        case "Date":
                            request.Date = DateTime.Parse(headerValue);
                            break;
                        case "Expect":
                            request.Expect = headerValue;
                            break;
                        case "Host":
                            request.Host = headerValue;
                            break;
                        case "If-Modified-Since":
                            request.IfModifiedSince = DateTime.Parse(headerValue);
                            break;
                        case "Referer":
                            request.Referer = headerValue;
                            break;
                        case "Transfer-Encoding":
                            request.TransferEncoding = headerValue;
                            break;
                        case "User-Agent":
                            request.UserAgent = headerValue;
                            break;
                        default:
                            // *** need to find a way to make the following line work Item is giving an error
                            //request.Headers.Item[headerName] = headerValue;
                            break;
                    }
                }
            }
        }

        /// <summary>
        /// Writes the request body.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <param name="requestBodyString">The request body string.</param>
        private void WriteRequestBody(HttpWebRequest request, string requestBodyString)
        {
            var requestBodyBytes = Encoding.UTF8.GetBytes(requestBodyString);
            using (var requestStream = request.GetRequestStream())
            {
                requestStream.Write(requestBodyBytes, 0, requestBodyBytes.Length);
            }
        }

        /// <summary>
        /// Gets the response string for the provided web request.
        /// </summary>
        /// <param name="request">The request.</param>
        /// <returns>Response string.</returns>
        private string GetResponseString(HttpWebRequest request)
        {
            try
            {
                using (HttpWebResponse response = (HttpWebResponse)request.GetResponse())
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            var responseString = reader.ReadToEnd();
                            return responseString;
                        }
                    }
                }
            }
            catch (WebException exception)
            {
                using (HttpWebResponse response = (HttpWebResponse)exception.Response)
                {
                    using (Stream responseStream = response.GetResponseStream())
                    {
                        using (StreamReader reader = new StreamReader(response.GetResponseStream()))
                        {
                            var responseString = reader.ReadToEnd();
                            if (this.validHttpErrorCodes.Contains(response.StatusCode))
                            {
                                return responseString;
                            }
                            else
                            {
                                throw new WebException($"HTTP response code: {Convert.ToInt32(response.StatusCode)}; Response body: {responseString}.", exception);
                            }
                        }
                    }
                }
            }
        }
    }
}