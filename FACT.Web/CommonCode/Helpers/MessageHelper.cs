﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.Helpers
{
    /// <summary>
    /// Represents helper module for message retrieval.
    /// </summary>
    public static class MessageHelper
    {
        /// <summary>
        /// Gets the current request information message format string and parameters.
        /// </summary>
        /// <returns>Current request information message format string and parameters.</returns>
        public static Tuple<string, string> GetCurrentRequestInformationMessageFormat()
        {
            return new Tuple<String, Object[]>("URL='{1}'{0}HTTP Method='{2}'{0}Referer='{3}'{0}Email='{4}'{0}OpsUsername='{5}'", new Object[]
        {
            Environment.NewLine, HttpContext.Current.Request.Url.AbsoluteUri, HttpContext.Current.Request.RequestType, HttpContext.Current.Request.UrlReferrer?.AbsoluteUri,
            SessionManager.UserInformation.Email, SessionManager.UserInformation.OpsUsername});
        }
    }
}