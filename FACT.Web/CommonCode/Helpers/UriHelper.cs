﻿using System;
using System.Collections.Generic;
using System.Collections.Specialized;
using System.Linq;
using System.Text;
using System.Web;

namespace FACT.Web.CommonCode.Helpers
{
    /// <summary>
    /// Represents helper module to work with URIs.
    /// </summary>
    public static class UriHelper
    {
        /// <summary>
        /// Gets the fully qualified path.
        /// </summary>
        /// <param name="basePath">The base path.</param>
        /// <param name="relativePath">The relative path.</param>
        /// <param name="queryStringValues">The query string values.</param>
        /// <returns>Fully qualified path with all query string values if provided.</returns>
        public static string GetFullyQualifiedPath(string basePath, string relativePath, params KeyValuePair<string, string>[] queryStringValues)
        {
            UriBuilder initialBuilder = new UriBuilder(basePath);
            Uri uri = new Uri(initialBuilder.Uri, relativePath);
            if (queryStringValues == null || queryStringValues.Length == 0)
            {
                return uri.AbsoluteUri;
            }
            NameValueCollection queryCollection = HttpUtility.ParseQueryString(uri.Query);
            foreach (var pair in queryStringValues)
            {
                queryCollection.Add(pair.Key, pair.Value);
            }
            StringBuilder resultQueryString = new StringBuilder();
            foreach (string key in queryCollection.AllKeys)
            {
                string encodedKey = HttpUtility.UrlEncode(key);
                string[] values = queryCollection.GetValues(key);
                foreach (string value in values)
                {
                    string encodedValue = HttpUtility.UrlEncode(value);
                    resultQueryString.Append($"{encodedKey}={encodedValue}");
                }
            }
            UriBuilder finalBuilder = new UriBuilder(uri.AbsoluteUri);
            finalBuilder.Query = resultQueryString.ToString();
            finalBuilder.Fragment = uri.Fragment;
            return finalBuilder.Uri.AbsoluteUri;
        }
    }
}