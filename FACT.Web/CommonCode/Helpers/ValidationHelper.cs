﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Web;

namespace FACT.Web.CommonCode.Helpers
{
    /// <summary>
    /// Represents helper module for validation functionality.
    /// </summary>
    public static class ValidationHelper
    {
        /// <summary>
        /// Contains message templates.
        /// </summary>
        public static class MessageTemplate
        {
            /// <summary>
            /// {0} should have a value.
            /// </summary>
            public const string ShouldHaveValue = "{0} should have a value.";
        }

        /// <summary>
        /// The message parameter names.
        /// </summary>
        private static readonly ICollection<string> MessageParameterNames = new HashSet<String>( { "message", "msg", "m", "textString", "s", "reason" } );

        /// <summary>
        /// Specifies a precondition contract
        /// and throws an exception of provided type with the provided message if the condition fails.
        /// </summary>
        /// <typeparam name="T">The exception to throw if the condition failed.</typeparam>
        /// <param name="condition">The conditional expression to test.</param>
        /// <param name="messageTemplate">The message template for exception if the condition is false.</param>
        /// <param name="arguments">An object array that contains zero or more objects to format the message.</param>
        /// <exception cref="System.Exception">
        /// If the provided condition is false, the exception of provided type with the provided message will be thrown.
        /// </exception>
        public static void Requirement<T>(bool condition, string messageTemplate, params Object[] arguments)
        {
            // if condition is true then no action needed
            if (condition)
            {
                return;
            }

            string message = String.Format(messageTemplate, arguments);

            Exception exceptionToThrow = null;
            ConstructorInfo twoParametersConstructor = (T.GetType()).GetConstructor(new Type[]
            {
                typeof(String),
                typeof(String)
            });

            if (twoParametersConstructor == null)
            {
                ConstructorInfo singleParameterConstructor = (T.GetType()).GetConstructor(new Type[] { typeof(String)});
                if (singleParameterConstructor != null && MessageParameterNames.Contains(singleParameterConstructor.GetParameters()[0].Name))
                {
                    exceptionToThrow = singleParameterConstructor.Invoke(new Object[] { message }) as Exception);
                }
                else
                {
                    ParameterInfo[] constructorParameters = twoParametersConstructor.GetParameters();
                    if (MessageParameterNames.Contains(constructorParameters[0].Name))
                    {
                        exceptionToThrow = twoParametersConstructor.Invoke(new Object[] { message, null }) as Exception);
                    }
                    else if (MessageParameterNames.Contains(constructorParameters[1].Name))
                    {
                        exceptionToThrow = twoParametersConstructor.Invoke(new Object[] { null, message }) as Exception);
                    }
                }
                if (exceptionToThrow == null)
                {
                    throw new ArgumentException("Constructor for exception not found.", new Exception(message));
                }
                throw exceptionToThrow;
            }
        }
    }
}