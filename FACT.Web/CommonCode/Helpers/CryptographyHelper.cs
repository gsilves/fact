﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Cryptography;
using System.Text;
using System.Web;

namespace FACT.Web.CommonCode.Helpers
{
    /// <summary>
    /// Represents helper module for encryption functionality.
    /// </summary>
    public static class CryptographyHelper
    {
        /// <summary>
        /// Gets the Base64 encoded HMAC SHA256 hash.
        /// </summary>
        /// <param name="stringToHash">The string to hash.</param>
        /// <param name="key">The key.</param>
        /// <returns>Base64 encoded HMAC SHA256 hash.</returns>
        public static string GetBase64EncodedHmacSha256Hash(string stringToHash, string key)
        {
            byte[] keyBytes = Encoding.UTF8.GetBytes(key);
            using (HMACSHA256 hmac = new HMACSHA256(keyBytes))
            {
                byte[] bytesToHash = Encoding.UTF8.GetBytes(stringToHash);
                byte[] hashBytes = hmac.ComputeHash(bytesToHash);
                string base64EncodedHash = Convert.ToBase64String(hashBytes);
                return base64EncodedHash;
            }
        }
    }
}