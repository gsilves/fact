﻿using FACT.Web.CommonCode.Interfaces;
using NLog;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.Loggers
{
    /// <summary>
    /// Implements event logger functionality.
    /// </summary>
    /// <seealso cref="MyCMO.CommonCode.Interfaces.IEventLogger" />
    public static class EventLogger : IEventLogger
    {
        /// <summary>
        /// The NLog logger.
        /// </summary>
        private static Logger logger = LogManager.GetLogger("EventLogger");

        /// <summary>
        /// Writes the diagnostic message at the Error level using the specified parameters.
        /// </summary>
        /// <param name="message">A string containing format items.</param>
        /// <param name="arguments">Arguments to format.</param>
        public static void LogError(string message, params Object[] arguments)
        {
            logger.Error(message, arguments);
        }

        /// <summary>
        /// Writes the exception and the diagnostic message at the Error level using the specified parameters.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A string containing format items.</param>
        /// <param name="arguments">Arguments to format.</param>
        public static void LogError(Exception exception, string message, params Object[] arguments)
        {
            logger.Error(exception, message, arguments);
        }

        /// <summary>
        /// Writes the diagnostic message at the Info level using the specified parameters.
        /// </summary>
        /// <param name="message">A string containing format items.</param>
        /// <param name="arguments">Arguments to format.</param>
        public static void LogInfo(string message, params Object[] arguments)
        {
            logger.Info(message, arguments);
        }

        /// <summary>
        /// Writes the diagnostic message at the Debug level using the specified parameters.
        /// </summary>
        /// <param name="message">A string containing format items.</param>
        /// <param name="arguments">Arguments to format.</param>
        public static void LogDebug(string message, params Object[] arguments)
        {
            logger.Debug(message, arguments);
        }
    }
}