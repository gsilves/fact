﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Web.CommonCode.Interfaces
{
    /// <summary>
    /// Contains methods for communication with sales force API.
    /// </summary>
    public interface ISalesForceService
    {
        /// <summary>
        /// Gets the authentication information with the provided refresh token.
        /// </summary>
        /// <param name="refreshToken">The refresh token.</param>
        /// <returns>Authentication information.</returns>
        AuthenticationInformation GetAuthenticationInformation(string refreshToken);


        /// <summary>
        /// Gets the authentication information with the provided UserName and password.
        /// </summary>
        /// <param name="username">The username.</param>
        /// <param name="password">The password.</param>
        /// <returns>Authentication information.</returns>
        LoginInformation Login(string username, string password);


        /// <summary>
        /// Gets the contacts.
        /// </summary>
        /// <param name="authentication">The authentication information.</param>
        /// <param name="input">The input parameters.</param>
        /// <returns>Collection of contacts to import.</returns>
        ICollection<ContactImport> GetContacts(AuthenticationInformation authentication, GetContactsInput input);

        /// <summary>
        /// Gets the leads.
        /// </summary>
        /// <param name="authentication">The authentication information.</param>
        /// <param name="input">The input parameters.</param>
        /// <returns>Collection of leads to import.</returns>
        ICollection<ContactImport> GetLeads(AuthenticationInformation authentication, GetContactsInput input);

        /// <summary>
        /// Gets the custom fields.
        /// </summary>
        /// <param name="authentication">The authentication information.</param>
        /// <param name="tableNames">The table names.</param>
        /// <returns>Collection of custom fields.</returns>
        ICollection<CustomField> GetCustomFields(AuthenticationInformation authentication, params string[] tableNames);

        /// <summary>
        /// Create Case Object using refreshToken authorization.
        /// </summary>
        /// <param name="authentication">The authentication information.</param>
        /// <param name="body">Request body.</param>
        /// <returns>Collection of custom fields.</returns>
        RestResponsePost CreateCase(AuthenticationInformation authentication, CreateCaseBody body);

        /// <summary>
        /// Create Case Object using Username/Password authorization.
        /// </summary>
        /// <param name="body">Request body.</param>
        /// <returns>Collection of custom fields.</returns>
        RestResponsePost CreateCaseUP(string repCode, CreateCaseBody body, IEnumerable<KeyValuePair<string, string>> additionalParams);

        /// <summary>
        /// Gets records for query.
        /// </summary>
        /// <param name="query">Request query.</param>
        /// <param name="accessToken">Access Token.</param>
        /// <param name="serviceUrl">Service Url.</param>
        /// <returns>Collection of custom fields.</returns>
        QueryResponseGet GetQuery(string query, string accessToken, string serviceUrl);
    }
}
