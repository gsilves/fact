﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;

namespace FACT.Web.CommonCode.Interfaces
{
    /// <summary>
    /// Contains methods for sending different web requests and return the responses.
    /// </summary>
    public interface IWebRequestHandler
    {
        /// <summary>
        /// Sets the valid HTTP error codes.
        /// </summary>
        /// <param name="validHttpErrorCodes">The valid HTTP error codes.</param>
        void SetValidHttpErrorCodes(params HttpStatusCode[] validHttpErrorCodes);

        /// <summary>
        /// Processes the get request.
        /// </summary>
        /// <param name="uriString">The URI string.</param>
        /// <param name="requestHeaders">The request headers.</param>
        /// <returns>Response string.</returns>
        string ProcessGetRequest(string uriString, IEnumerable<KeyValuePair<string, string>> requestHeaders);

        /// <summary>
        /// Processes the post form request.
        /// </summary>
        /// <param name="uriString">The URI string.</param>
        /// <param name="requestContent">Content of the request.</param>
        /// <param name="requestHeaders">The request headers.</param>
        /// <returns>Response string.</returns>
        string ProcessPostFormRequest(string uriString, IEnumerable<KeyValuePair<string, string>> requestContent, IEnumerable<KeyValuePair<string, string>> requestHeaders);
    }
}