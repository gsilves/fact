﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Web.CommonCode.Interfaces
{
    /// <summary>
    /// Contains methods for logging events.
    /// </summary>
    public interface IEventLogger
    {
        /// <summary>
        /// Writes the exception and the diagnostic message at the Error level using the specified parameters.
        /// </summary>
        /// <param name="exception">An exception to be logged.</param>
        /// <param name="message">A string containing format items.</param>
        /// <param name="arguments">Arguments to format.</param>
        void LogError(Exception exception, string message, params Object[] arguments);

        /// <summary>
        /// Writes the diagnostic message at the Error level using the specified parameters.
        /// </summary>
        /// <param name="message">A string containing format items.</param>
        /// <param name="arguments">Arguments to format.</param>
        void LogError(string message, params Object[] arguments);

        /// <summary>
        /// Writes the diagnostic message at the Info level using the specified parameters.
        /// </summary>
        /// <param name="message">A string containing format items.</param>
        /// <param name="arguments">Arguments to format.</param>
        void LogInfo(string message, params Object[] arguments);

        /// <summary>
        /// Writes the diagnostic message at the Debug level using the specified parameters.
        /// </summary>
        /// <param name="message">A string containing format items.</param>
        /// <param name="arguments">Arguments to format.</param>
        void LogDebug(string message, params Object[] arguments);
    }
}
