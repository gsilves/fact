﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Web.CommonCode.SalesForce.Model
{
    public class QueryResponseGet
    {
        [JsonProperty("totalSize")]
        public int TotalSize { get; set; }

        [JsonProperty("done")]
        public bool Done { get; set; }

        [JsonProperty("records")]
        public List<QueryResponseGet> Records { get; set; }
    }
}
