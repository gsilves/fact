﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Model
{
    /// <summary>
    /// Represents authentication information.
    /// </summary>
    public class AuthenticationInformation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="AuthenticationInformation"/> class.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        /// <param name="refreshToken">The refresh token.</param>
        /// <param name="instanceUrl">The instance URL.</param>
        public AuthenticationInformation(string accessToken, string refreshToken, string instanceUrl)
        {
            this.AccessToken = accessToken;
            this.RefreshToken = refreshToken;
            this.InstanceUrl = instanceUrl;
        }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets the refresh token.
        /// </summary>
        public string RefreshToken { get; set; }

        /// <summary>
        /// Gets or sets the instance URL.
        /// </summary>
        public string InstanceUrl { get; set; }
    }
}