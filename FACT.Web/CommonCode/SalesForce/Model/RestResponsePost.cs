﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Model
{
    public class RestResponsePost
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("errors")]
        public List<string> Errors { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }
    }
}