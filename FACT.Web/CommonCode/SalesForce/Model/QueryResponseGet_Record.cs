﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Model
{
    public class QueryResponseGet_Record
    {
        [JsonProperty("attributes")]
        public QueryResponseGet_Attribute Attributes { get; set; }

        [JsonProperty("Id")]
        public string Id { get; set; }

        [JsonProperty("Name")]
        public string Name { get; set; }

        [JsonProperty("Rep_Code__c")]
        public string RepCodeC { get; set; }

        [JsonProperty("BDRepCode__c")]
        public string BDRepCodeC { get; set; }
    }
}