﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Model
{
    /// [summary]
    /// Represents refresh token response.
    /// [/summary]
    public class RepCodeCResponse
    {
        /// [summary]
        /// Gets or sets the access token.
        /// [/summary]
        [JsonProperty("repcode__c")]
        public string RepcodeC { get; set; }

        /// [summary]
        /// Gets or sets the Subject.
        /// [/summary]
        public string Subject { get; set; }

        /// [summary]
        /// Gets or sets the Recordtypeid.
        /// [/summary]
        public string Recordtypeid { get; set; }

        /// [summary]
        /// Gets or sets the instance URL.
        /// [/summary]
        [JsonProperty("Campaign_details__c")]
        public string CampaignDetailsC { get; set; }

        /// [summary]
        /// Gets or sets the identifier.
        /// [/summary]
        public string Origin { get; set; }

        /// [summary]
        /// Gets or sets the type of the token.
        /// [/summary]
        public string Status { get; set; }
    }
}