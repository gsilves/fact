﻿using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Model
{
    public class QueryResponseGet_Attribute
    {
        [JsonProperty("type")]
        public string Type { get; set; }

       [JsonProperty("url")]
        public string Url { get; set; }
    }
}