﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Model
{
    public class LoginInformation
    {
        /// <summary>
        /// Initializes a new instance of the <see cref="LoginInformation"/> class.
        /// </summary>
        /// <param name="accessToken">The access token.</param>
        /// <param name="instanceUrl">The instance URL.</param>
        /// <param name="id">The Id.</param>
        /// <param name="tokenType">The Token Type.</param>
        /// <param name="issuedAt">The Issued at.</param>
        /// <param name="signature">The Signature.</param>
        public LoginInformation(string accessToken, string instanceUrl, string id, string tokenType, string issuedAt, string signature)
        {
            this.AccessToken = accessToken;
            this.InstanceUrl = instanceUrl;
            this.Id = id;
            this.TokenType = tokenType;
            this.IssuedAt = issuedAt;
            this.Signature = signature;
        }

        /// <summary>
        /// Initializes a new instance of the <see cref="LoginInformation"/> class.
        /// </summary>
        /// <param name="Token">The Refresh token.</param>
        public LoginInformation(RefreshTokenResponse token)
        {
            this.AccessToken = token.AccessToken;
            this.InstanceUrl = token.InstanceUrl;
            this.Id = token.Id;
            this.TokenType = token.TokenType;
            this.IssuedAt = token.IssuedAt;
            this.Signature = token.Signature;
        }

        /// <summary>
        /// Gets or sets the access token.
        /// </summary>
        public string AccessToken { get; set; }

        /// <summary>
        /// Gets or sets the instance URL.
        /// </summary>
        public string InstanceUrl { get; set; }

        /// <summary>
        /// Gets or sets the Id.
        /// </summary>
        public string Id { get; set; }

        /// <summary>
        /// Gets or sets the Token Type.
        /// </summary>
        public string TokenType { get; set; }

        /// <summary>
        /// Gets or sets the Issued At.
        /// </summary>
        public string IssuedAt { get; set; }

        /// <summary>
        /// Gets or sets the Signature.
        /// </summary>
        public string Signature { get; set; }
    }
}