﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Model.InputParameters
{
    /// <summary>
    /// Represents input parameters for <see cref="SalesForceService.CreateCase"/> method.
    /// </summary>
    public class CreateCaseBody
    {
        /// <summary>
        /// Gets or sets the Type. The type of case, such as Feature Request or Question.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Gets or sets the Origin. The source of the case, such as Email, Phone, or Web. Label is Case Origin.
        /// </summary>
        public string Origin { get; set; }

        /// <summary>
        /// Gets or sets the Reason. The reason why the case was created, such as Instructions not clear, or User didn’t attend training.
        /// </summary>
        public string Reason { get; set; }

        /// <summary>
        /// Gets or sets a Status. The status of the case, such as “New,” “Closed,” or “Escalated.” This field directly controls the IsClosed flag. 
        /// Each predefined Status value implies an IsClosed flag value. For more information, see CaseStatus.
        /// </summary>
        public string Status { get; set; }

        /// <summary>
        /// Gets or sets a Subject. The subject of the case. Limit: 255 characters.
        /// </summary>
        public string Subject { get; set; }

        /// <summary>
        /// Gets or sets OwnerId. ID of the contact who owns the case.
        /// </summary>
        public string OwnerId { get; set; }

        /// <summary>
        /// Gets or sets ParentId. The ID of the parent case in the hierarchy. The label is Parent Case.
        /// </summary>
        public string ParentId { get; set; }

        /// <summary>
        /// Gets or sets Priority. The importance or urgency of the case, such as High, Medium, or Low.
        /// </summary>
        public string Priority { get; set; }

        /// <summary>
        /// Gets or sets AccountId. ID of the account associated with this case.
        /// </summary>
        public string AccountId { get; set; }

        /// <summary>
        /// Gets or sets ContactId. ID of the associated Contact.
        /// </summary>
        public string ContactId { get; set; }

        /// <summary>
        /// Gets or sets Description. A text description of the case. Limit: 32 KB.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Gets or sets IsEscalated. Indicates whether the case has been escalated (true) or not. A case's escalated state does not affect how you can use a case, 
        /// or whether you can query, delete, or update it.You can set this flag via the API. Label is Escalated.
        /// </summary>
        public bool IsEscalated { get; set; }

        /// <summary>
        /// Gets or sets SuppliedName. The name that was entered when the case was created. This field can't be updated after the case has been created.Label is Name.
        /// </summary>
        public string SuppliedName { get; set; }

        /// <summary>
        /// Gets or sets SuppliedEmail. The email address that was entered when the case was created. This field can't be updated after the case has been created.
        /// Label is Email.If your organization has an active auto-response rule, SuppliedEmail is required when creating a case via the API.
        /// Auto-response rules use the email in the contact specified by ContactId.If no email address is in the contact record, the email specified here is used.
        /// </summary>
        public string SuppliedEmail { get; set; }

        /// <summary>
        /// Gets or sets SuppliedPhone. The phone number that was entered when the case was created. This field can't be updated after the case has been created.Label is Phone.
        /// </summary>
        public string SuppliedPhone { get; set; }

        /// <summary>
        /// Gets or sets SuppliedCompany. The company name that was entered when the case was created. This field can't be updated after the case has been created.Label is Company.
        /// </summary>
        public string SuppliedCompany { get; set; }
    }
}