﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Model.InputParameters
{
    /// <summary>
    /// Represents input parameters for <see cref="SalesForceService.GetContacts"/> and <see cref="SalesForceService.GetLeads"/> methods.
    /// </summary>
    public class GetContactsInput
    {
        /// <summary>
        /// Gets or sets the state.
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Gets or sets the account name.
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Gets or sets the lead source.
        /// </summary>
        public string LeadSource { get; set; }

        /// <summary>
        /// Gets or sets a value indicating whether to exclude contact with no email address.
        /// </summary>
        public bool ExcludeNoEmail { get; set; }

        /// <summary>
        /// Gets or sets the custom field identifier.
        /// </summary>
        public string CustomFieldId { get; set; }

        /// <summary>
        /// Gets or sets the custom field value.
        /// </summary>
        public string CustomFieldValue { get; set; }
    }
}