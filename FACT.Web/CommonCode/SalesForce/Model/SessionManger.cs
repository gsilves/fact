﻿using FACT.Web.CommonCode.Constants;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Model
{
    /// <summary>
    /// Implements strongly-typed access to session variables.
    /// </summary>
    public static class SessionManger
    {
        /// <summary>
        /// Contains user-specific session values.
        /// </summary>
        public static class UserInformation
        {
            /// <summary>
            /// Gets or sets the email.
            /// </summary>
            public static string Email
            {
                get
                {
                    return HttpContext.Current.Session[SessionKey.UserInformation.Email].ToString();
                }
                set
                {
                    HttpContext.Current.Session[SessionKey.UserInformation.Email] = value;
                }
            }

            public static string OpsUsername
            {
                get
                {
                    return HttpContext.Current.Session[SessionKey.UserInformation.OpsUsername].ToString();
                }
                set
                {
                    HttpContext.Current.Session[SessionKey.UserInformation.OpsUsername] = value;
                }
            }
        }

        /// <summary>
        /// Contains session values associated with SalesForce service.
        /// </summary>
        public static class SalesForce
        {
            /// <summary>
            /// Gets or sets the authenticaton.
            /// </summary>
            public static AuthenticationInformation Authentication
            {
                get
                {
                    if (SalesForce.AccessTokenAvailable || SalesForce.RefreshTokenAvailable)
                    {
                        return new AuthenticationInformation(SalesForce.AccessToken, SalesForce.RefreshToken, SalesForce.InstanceUrl);
                    }
                    else
                    {
                        return null;
                    }
                }
                set
                {
                    SalesForce.AccessToken = value.AccessToken;
                    SalesForce.RefreshToken = value.RefreshToken;
                    SalesForce.InstanceUrl = value.InstanceUrl;
                }
            }

            /// <summary>
            /// Gets a value indicating whether access token is available.
            /// </summary>
            public static bool AccessTokenAvailable
            {
                get
                {
                    return !string.IsNullOrWhiteSpace(SalesForce.AccessToken) && !string.IsNullOrWhiteSpace(SalesForce.InstanceUrl);
                }
            }

            /// <summary>
            /// Gets a value indicating whether refresh token is available.
            /// </summary>
            public static bool RefreshTokenAvailable
            {
                get
                {
                    return !string.IsNullOrWhiteSpace(SalesForce.RefreshToken);
                }
            }

            /// <summary>
            /// Gets or sets the access token.
            /// </summary>
            private static string AccessToken
            {
                get
                {
                    return HttpContext.Current.Session[SessionKey.SalesForce.AccessToken].ToString();
                }
                set
                {
                    HttpContext.Current.Session[SessionKey.SalesForce.AccessToken] = value;
                }
            }

            /// <summary>
            /// Gets or sets the refresh token.
            /// </summary>
            private static string RefreshToken
            {
                get
                {
                    return HttpContext.Current.Session[SessionKey.SalesForce.RefreshToken].ToString();
                }
                set
                {
                    HttpContext.Current.Session[SessionKey.SalesForce.RefreshToken] = value;
                }
            }

            /// <summary>
            /// Gets or sets the instance URL.
            /// </summary>
            private static string InstanceUrl
            {
                get
                {
                    return HttpContext.Current.Session[SessionKey.SalesForce.InstanceUrl].ToString();
                }
                set
                {
                    HttpContext.Current.Session[SessionKey.SalesForce.InstanceUrl] = value;
                }
            }
        }
    }
}