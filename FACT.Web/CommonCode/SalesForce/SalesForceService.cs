﻿using FACT.Domain.Models.MiscModels;
using FACT.Web.CommonCode.Constants;
using FACT.Web.CommonCode.Interfaces;
using FACT.Web.CommonCode.SalesForce.Model;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;

namespace FACT.Web.CommonCode.SalesForce
{
    /// <summary>
    /// Implements methods for communication with sales force API.
    /// </summary>
    public class SalesForceService : ISalesForceService
    {
        /// <summary>
        /// The web request handler.
        /// </summary>
        private readonly IWebRequestHandler requestHandler;

        /// <summary>
        /// Initializes a new instance of the <see cref="SalesForceService"/> class.
        /// </summary>
        /// <param name="requestHandler">The request handler.</param>
        public SalesForceService(IWebRequestHandler requestHandler)
        {
            this.requestHandler = requestHandler;
        }

        /// <summary>
        /// The client identifier.
        /// </summary>
        private readonly string ClientId = ConfigurationManager.AppSettings["SFClientId"];

        /// <summary>
        /// The SecurityToken identifier.
        /// </summary>
        private readonly string SecurityToken = ConfigurationManager.AppSettings["SFClientSecurityToken"];

        /// <summary>
        /// The client secret.
        /// </summary>
        private readonly string ClientUsername = ConfigurationManager.AppSettings["SFClientUserName"];
        /// <summary>
        /// The client secret.
        /// </summary>
        private readonly string ClientPassword = ConfigurationManager.AppSettings["SFClientPassword"];
        /// <summary>
        /// The client secret.
        /// </summary>
        private readonly string ClientSecret = ConfigurationManager.AppSettings["SFClientSecret"];

        /// <summary>
        /// The non quoted data types.
        /// </summary>
        //private readonly ICollection<string> NonQuotedDataTypes = new HashSet<String>( { "Number", "Checkbox", "Currency", "Date", "DateTime", "Percent" } );
        // *** HashSet error

        /// <summary>
        /// The unsupported data types.
        /// </summary>
        //private readonly ICollection<string> UnsupportedDataTypes = new HashSet<String>({"Location"});


        public string RESTPost(string serviceUrl, string endPoint, IEnumerable<KeyValuePair<string, string>> headers, IEnumerable<KeyValuePair<string, string>> queryParams,
            IEnumerable<KeyValuePair<string, string>> requestBody, string contentType)
        {
            try
            {
                RestClient service = new RestClient(serviceUrl);
                RestRequest request = new RestRequest(endPoint, Method.POST);
                request.JsonSerializer.ContentType = contentType + "; charset=utf-8";
                request.RequestFormat = contentType.ToUpper().IndexOf("XML") >= 0 ? DataFormat.Xml : DataFormat.Json;
                if (headers != null && headers.Count() > 0)
                {
                    foreach (KeyValuePair<string, string> headerLine in headers)
                    {
                        request.AddHeader(headerLine.Key, headerLine.Value);
                    }
                }
                if (requestBody != null && requestBody.Count() > 0)
                {
                    JObject jObj = new JObject();
                    foreach (KeyValuePair<string, string> reqBodyItem in requestBody)
                    {
                        jObj[reqBodyItem.Key] = reqBodyItem.Value;
                    }
                    request.AddParameter(contentType, jObj.ToString(), ParameterType.RequestBody);
                }
                if (queryParams != null && queryParams.Count() > 0)
                {
                    foreach (KeyValuePair<string, string> queryParam in queryParams)
                    {
                        request.AddQueryParameter(queryParam.Key, queryParam.Value);
                    }
                }
                var response = service.Execute(request);
                return response.Content;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string RESTGet(string serviceUrl, string endPoint, IEnumerable<KeyValuePair<string, string>> headers, IEnumerable<KeyValuePair<string, string>> requestParams)
        {
            try
            {
                RestClient client = new RestClient(serviceUrl);
                RestRequest request = new RestRequest(endPoint, Method.GET);
                foreach (KeyValuePair<string, string> headerItem in headers)
                {
                    request.AddHeader(headerItem.Key, headerItem.Value);
                }
                if (requestParams != null)
                {
                    foreach (KeyValuePair<string, string> requestParam in requestParams)
                    {
                        request.AddQueryParameter(requestParam.Key, requestParam.Value);
                    }
                }
                return client.Execute(request).Content;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string GetProfile()
        {
            var logininfo = Login(ClientUsername, ClientPassword);
            List<KeyValuePair<string, string>> requestHeaders = new List<KeyValuePair<String, String>>();
            requestHeaders.Add(new KeyValuePair<String, String>("Authorization", "Bearer " + logininfo.AccessToken));
            requestHeaders.Add(new KeyValuePair<String, String>("Content-Type", ContentType.Application.Json + "; charset=utf-8"));
            requestHeaders.Add(new KeyValuePair<String, String>("Accept", ContentType.Application.Json));
            string responseString = RESTGet(logininfo.InstanceUrl, ApiPath.Tooling.SObjects.Profile, requestHeaders, null);
            this.ValidateApiResponseForError(responseString);
            return responseString;
        }

        /// <summary>
        /// Create Case Object using refreshToken authorization.
        /// </summary>
        /// <param name="authentication">The authentication information.</param>
        /// <param name="body">The request body.</param>
        /// <returns>Collection of contacts to import.</returns>
        public RestResponsePost CreateCase(AuthenticationInformation authentication, CreateCaseBody body)
        {
            ValidationHelper.Requirement<ArgumentNullException>((authentication != null), MessageTemplate.ShouldHaveValue, nameof(authentication));
            ValidationHelper.Requirement<ArgumentNullException>((body != null), MessageTemplate.ShouldHaveValue, nameof(body));
            this.ValidateAuthenticationInformation(authentication);
            string isEscalated = body.IsEscalated ? "true" : "false";
            List<KeyValuePair<string, string>> requestBody = new List<KeyValuePair<String, String>>();
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Type, body.Type));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Origin, body.Origin));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Reason, body.Reason));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Status, body.Status));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.OwnerId, body.OwnerId));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Subject, body.Subject));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.ParentId, body.ParentId));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Priority, body.Priority));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.AccountId, body.AccountId));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.ContactId, body.ContactId));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Description, body.Description));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.IsEscalated, isEscalated));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.SuppliedName, body.SuppliedName));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.SuppliedEmail, body.SuppliedEmail));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.SuppliedPhone, body.SuppliedPhone));
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.SuppliedCompany, body.SuppliedCompany));
            List<KeyValuePair<string, string>> requestHeaders = GetAuthenticationRequestHeaders(authentication);
            requestHeaders.Add(new KeyValuePair<String, String>("Content-Type", ContentType.Application.Json + "; charset=utf-8"));
            requestHeaders.Add(new KeyValuePair<String, String>("Accept", ContentType.Application.Json));
            requestHeaders.Add(new KeyValuePair<String, String>("Sforce-Auto-Assign", "true"));
            this.requestHandler.SetValidHttpErrorCodes(HttpStatusCode.BadRequest);
            string responseString = RESTPost(authentication.InstanceUrl, ApiPath.Tooling.SObjects.Case, requestHeaders, null, requestBody, ContentType.Application.Json);
            this.ValidateApiResponseForError(responseString);
            RestResponsePost response = JsonConvert.DeserializeObject<RestResponsePost>(responseString);
            return response;
        }

        /// <summary>
        /// Gets records for query.
        /// </summary>
        /// <param name="query">The request body.</param>
        /// <returns>Collection of contacts to import.</returns>
        public QueryResponseGet GetQuery(string query, string accessToken, string serviceUrl)
        {
            ValidationHelper.Requirement<ArgumentNullException>(query != null, MessageTemplate.ShouldHaveValue, nameof(query));
            List<KeyValuePair<string, string>> header = new List<KeyValuePair<String, String>>();
            header.Add(new KeyValuePair<String, String>("Accept-Encoding", "gzip,deflate"));
            header.Add(new KeyValuePair<String, String>("Authorization", "Bearer " + accessToken));
            List<KeyValuePair<string, string>> param = new List<KeyValuePair<String, String>>();
            param.Add(new KeyValuePair<String, String>("q", query));
            string responseString = RESTGet(serviceUrl, ApiPath.Query, header, param);
            QueryResponseGet response = JsonConvert.DeserializeObject<QueryResponseGet>(responseString);
            return response;
        }

        /// <summary>
        /// Create Case Object using Username/Password authorization.
        /// </summary>
        /// <param name="body">The request body.</param>
        /// <returns>Collection of contacts to import.</returns>
            public Function CreateCaseUP(repCode As String, body As CreateCaseBody, additionalParams As IEnumerable(Of KeyValuePair<String, String>)) As RestResponsePost _
            Implements ISalesForceService.CreateCaseUP
            ValidationHelper.Requirement(Of ArgumentNullException)(Not IsNothing(body), MessageTemplate.ShouldHaveValue, NameOf(body))
            if Not(String.IsNullOrEmpty(ClientUsername) OrElse String.IsNullOrEmpty(ClientPassword)) Then
                '1. We get access token and instance url
                var logininfo = Login(ClientUsername, ClientPassword)
                'var profileStr = GetProfile()
                var repcodeC As String = ""
                '2. we goet RepCodeId for current advisor
                var q = GetQuery("SELECT id,Name,Rep_Code__c,bdrepcode__c FROM Repcode__c WHERE BDRepCode__c='" & repCode & "'", logininfo.AccessToken, logininfo.InstanceUrl)
                if q.Records.Count > 0 Then
                    repcodeC = q.Records(0).Id
                    var isEscalated As String = If(body.IsEscalated, "true", "false")
                    var requestBody = new List(Of KeyValuePair<String, String>)
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Type, body.Type))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Origin, body.Origin))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Reason, body.Reason))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Status, body.Status))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Subject, body.Subject))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Priority, body.Priority))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.Description, "Approval Request"))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.IsEscalated, isEscalated))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.SuppliedName, body.SuppliedName))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.SuppliedEmail, body.SuppliedEmail))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.SuppliedPhone, body.SuppliedPhone))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseKey.SuppliedCompany, body.SuppliedCompany))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseAdditionalKey.RepCodeC, repcodeC))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseAdditionalKey.Recordtypeid, ConfigurationManager.AppSettings("SFRecordTypeId_InUse")))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseAdditionalKey.CampaignDetailsC, body.Description))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseAdditionalKey.ADSubStatusC, "new Submission"))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseAdditionalKey.RecordKeepingOnlyMaterialC, "NO"))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseAdditionalKey.ADProductsC, "Marketing/Brand Awareness"))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseAdditionalKey.ADIntendedAudienceC, "OTHER"))
                    requestBody.Add(new KeyValuePair<String, String>(RequestBodyCreateCaseAdditionalKey.ADCreatorOfTheMaterialC, "Third Party"))
                    If(Not IsNothing(additionalParams)) AndAlso(additionalParams.Count > 0) Then
                      requestBody.AddRange(additionalParams)
                    End If
                    var requestHeaders As List(Of KeyValuePair<String, String>) = new List(Of KeyValuePair<String, String>)
                    requestHeaders.Add(new KeyValuePair<String, String>("Authorization", "Bearer " & logininfo.AccessToken))
                    requestHeaders.Add(new KeyValuePair<String, String>("Content-Type", ContentType.Application.Json & "; charset=utf-8"))
                    requestHeaders.Add(new KeyValuePair<String, String>("Accept", ContentType.Application.Json))
                    requestHeaders.Add(new KeyValuePair<String, String>("Sforce-Auto-Assign", "true"))
                    this.requestHandler.SetValidHttpErrorCodes(HttpStatusCode.BadRequest)
                    '3. We create Case object
                    var responseString As String = RESTPost(logininfo.InstanceUrl, ApiPath.Tooling.SObjects.Case, requestHeaders, Nothing, requestBody, ContentType.Application.Json)
                    this.ValidateApiResponseForError(responseString)
                    return JsonConvert.DeserializeObject(Of RestResponsePost)(responseString)
                 Else
                    var response = new RestResponsePost()
                    response.Id = ""
                    response.Errors = new List(Of String)
                    response.Errors.Add("Rep Code " & repCode & " not found.")
                    response.Success = False
                    return response
                End If
            Else
                var response = new RestResponsePost()
                response.Id = ""
                response.Errors = new List(Of String)
                response.Errors.Add("Client Username or Password are empty or not defined!")
                response.Success = False
                return response
            End If
        End Function

        /// <summary>
        /// Gets the authentication information with the provided refresh token.
        /// </summary>
        /// <param name="username">The usernathis.</param>
        /// <param name="password">The password.</param>
        /// <returns>Authentication information.</returns>
            public Function Login(username As String, password As String) As LoginInformation _
            Implements ISalesForceService.Login
            ValidationHelper.Requirement(Of ArgumentNullException)(Not String.IsNullOrWhiteSpace(username), MessageTemplate.ShouldHaveValue, NameOf(username))
            ValidationHelper.Requirement(Of ArgumentNullException)(Not String.IsNullOrWhiteSpace(password), MessageTemplate.ShouldHaveValue, NameOf(password))
            var requestParams = new List(Of KeyValuePair<String, String>)
            requestParams.Add(new KeyValuePair<String, String>(RequestBodyKey.GrantType, RequestBodyKey.Password))
            requestParams.Add(new KeyValuePair<String, String>(RequestBodyKey.ClientId, ClientId))
            requestParams.Add(new KeyValuePair<String, String>(RequestBodyKey.ClientSecret, ClientSecret))
            requestParams.Add(new KeyValuePair<String, String>(RequestBodyKey.UserName, username))
            requestParams.Add(new KeyValuePair<String, String>(RequestBodyKey.Password, password + SecurityToken))
            var requestHeaders = new List(Of KeyValuePair<String, String>)
            requestHeaders.Add(new KeyValuePair<String, String>("Accept-Encoding", "gzip,deflate"))
            requestHeaders.Add(new KeyValuePair<String, String>("Content-Type", ContentType.Application.Json & "; charset=utf-8"))
            this.requestHandler.SetValidHttpErrorCodes(HttpStatusCode.BadRequest)
            var responseString As String = RESTPost(ApiPath.BasePath.QA, ApiPath.RefreshToken, requestHeaders, requestParams, Nothing, ContentType.Application.Json)
            this.ValidateAuthResponseForError(responseString)
            var response = JsonConvert.DeserializeObject(Of RefreshTokenResponse)(responseString)
             this.VerifyAuthenticationSignature(response)
            return new LoginInformation(response)
        End Function

        /// <summary>
        /// Gets the authentication information with the provided refresh token.
        /// </summary>
        /// <param name="refreshToken">The refresh token.</param>
        /// <returns>Authentication information.</returns>
            public Function GetAuthenticationInformation(refreshToken As String) As AuthenticationInformation _
            Implements ISalesForceService.GetAuthenticationInformation
            ValidationHelper.Requirement(Of ArgumentNullException)(Not String.IsNullOrWhiteSpace(refreshToken),
                                                                   MessageTemplate.ShouldHaveValue, NameOf(refreshToken))
            var requestUri = UriHelper.GetFullyQualifiedPath(ApiPath.BasePath.Login, ApiPath.RefreshToken)
            var requestBody = new List(Of KeyValuePair<String, String>)
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyKey.GrantType, RequestBodyKey.RefreshToken))
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyKey.ClientId, ClientId))
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyKey.ClientSecret, ClientSecret))
            requestBody.Add(new KeyValuePair<String, String>(RequestBodyKey.RefreshToken, refreshToken))
            var requestHeaders = new List(Of KeyValuePair<String, String>)
            requestHeaders.Add(new KeyValuePair<String, String>("Content-Type", ContentType.Application.WwwFormUrlEncoded))
            this.requestHandler.SetValidHttpErrorCodes(HttpStatusCode.BadRequest)
            var responseString = requestHandler.ProcessPostFormRequest(requestUri, requestBody.ToArray(), requestHeaders.ToArray())
            this.ValidateAuthResponseForError(responseString)
            var response = JsonConvert.DeserializeObject(Of RefreshTokenResponse)(responseString)
             this.VerifyAuthenticationSignature(response)
            return new AuthenticationInformation(response.AccessToken, refreshToken, response.InstanceUrl)
        End Function

        /// <summary>
        /// Gets the contacts.
        /// </summary>
        /// <param name="authentication">The authentication information.</param>
        /// <param name="input">The input parameters.</param>
        /// <returns>Collection of contacts to import.</returns>
            public Function GetContacts(authentication As AuthenticationInformation, input As GetContactsInput) As ICollection(Of ContactImport) _
            Implements ISalesForceService.GetContacts
            this.ValidateAuthenticationInformation(authentication)
            var whereExpressions As new List(Of String)
            if Not String.IsNullOrWhiteSpace(input.State) Then
                whereExpressions.Add($"MailingState = '{this.EncodeInputString(input.State)}'")
            End If
            if Not String.IsNullOrWhiteSpace(input.AccountName) Then
                whereExpressions.Add($"Account.Name = '{this.EncodeInputString(input.AccountName)}'")
            End If
            if Not String.IsNullOrWhiteSpace(input.LeadSource) Then
                whereExpressions.Add($"LeadSource = '{this.EncodeInputString(input.LeadSource)}'")
            End If
            if input.ExcludeNoEmail Then
                whereExpressions.Add("Email != ''")
            End If
            var customFieldExpression = this.GetCustomFieldWhereExpression(authentication, input.CustomFieldId, input.CustomFieldValue)
            if Not String.IsNullOrWhiteSpace(customFieldExpression) Then
                whereExpressions.Add(customFieldExpression)
            End If
            whereExpressions.Add("DoNotCall = false")
            var soqlQuery = "SELECT FirstName, LastName, Email, Name, MobilePhone, MailingAddress, Birthdate FROM Contact"
            if whereExpressions.Any() Then
                soqlQuery += " WHERE " & String.Join(" AND ", whereExpressions)
            End If
            var requestUri = UriHelper.GetFullyQualifiedPath(authentication.InstanceUrl, ApiPath.Query,
                                                             new KeyValuePair<String, String>(QueryStringKey.SoqlQuery, soqlQuery))
            var requestHeaders = this.GetAuthenticationRequestHeaders(authentication)
            this.requestHandler.SetValidHttpErrorCodes(HttpStatusCode.Unauthorized)
            var responseString As String = requestHandler.ProcessGetRequest(requestUri, requestHeaders)
            this.ValidateApiResponseForError(responseString)
            var contacts As ICollection(Of ContactImport) = this.ParseContacts(responseString)
            return contacts
        End Function

        /// <summary>
        /// Gets the leads.
        /// </summary>
        /// <param name="authentication">The authentication information.</param>
        /// <param name="input">The input parameters.</param>
        /// <returns>Collection of leads to import.</returns>
            public Function GetLeads(authentication As AuthenticationInformation, input As GetContactsInput) As ICollection(Of ContactImport) _
            Implements ISalesForceService.GetLeads
            this.ValidateAuthenticationInformation(authentication)
            var whereExpressions As new List(Of String)
            if Not String.IsNullOrWhiteSpace(input.State) Then
                whereExpressions.Add($"State = '{this.EncodeInputString(input.State)}'")
            End If
            if Not String.IsNullOrWhiteSpace(input.AccountName) Then
                whereExpressions.Add($"Name = '{this.EncodeInputString(input.AccountName)}'")
            End If
            if Not String.IsNullOrWhiteSpace(input.LeadSource) Then
                whereExpressions.Add($"LeadSource = '{this.EncodeInputString(input.LeadSource)}'")
            End If
            if input.ExcludeNoEmail Then
                whereExpressions.Add("Email != ''")
            End If
            var customFieldExpression = this.GetCustomFieldWhereExpression(authentication, input.CustomFieldId, input.CustomFieldValue)
            if customFieldExpression IsNot Nothing Then
                whereExpressions.Add(customFieldExpression)
            End If
            var soqlQuery = "SELECT FirstName, LastName, Email, Name, Phone, Street, City, State, PostalCode FROM Lead"
            if whereExpressions.Any() Then
                soqlQuery += " WHERE " & String.Join(" AND ", whereExpressions)
            End If
            var requestUri = UriHelper.GetFullyQualifiedPath(authentication.InstanceUrl, ApiPath.Query,
                                                             new KeyValuePair<String, String>(QueryStringKey.SoqlQuery, soqlQuery))
            var requestHeaders = this.GetAuthenticationRequestHeaders(authentication)
            this.requestHandler.SetValidHttpErrorCodes(HttpStatusCode.Unauthorized)
            var responseString As String = requestHandler.ProcessGetRequest(requestUri, requestHeaders)
            this.ValidateApiResponseForError(responseString)
            var contacts As ICollection(Of ContactImport) = this.ParseLeads(responseString)
            return contacts
        End Function

        /// <summary>
        /// Gets the custom fields.
        /// </summary>
        /// <param name="authentication">The authentication information.</param>
        /// <param name="tableNames">The table names.</param>
        /// <returns>Collection of custom fields.</returns>
            public Function GetCustomFields(authentication As AuthenticationInformation, ParamArray tableNames() As String) As ICollection(Of CustomField) _
            Implements ISalesForceService.GetCustomFields
            this.ValidateAuthenticationInformation(authentication)
            ValidationHelper.Requirement(Of ArgumentNullException)(tableNames IsNot Nothing AndAlso tableNames.Length > 0,
                                                                    MessageTemplate.ShouldHaveValue, NameOf(tableNames))
            var tablesList = String.Join(", ", tableNames.Select(Function(tableName) "'" & tableName & "'"))
            var soqlQuery = $"SELECT Id, TableEnumOrId, DeveloperName, Metadata, FullName FROM CustomField WHERE TableEnumOrId IN ({tablesList})"
            var requestUri = UriHelper.GetFullyQualifiedPath(authentication.InstanceUrl, ApiPath.Tooling.Query,
                                                             new KeyValuePair<String, String>(QueryStringKey.SoqlQuery, soqlQuery))
            var requestHeaders = this.GetAuthenticationRequestHeaders(authentication)
            this.requestHandler.SetValidHttpErrorCodes(HttpStatusCode.Unauthorized)
            var responseString As String = requestHandler.ProcessGetRequest(requestUri, requestHeaders)
            this.ValidateApiResponseForError(responseString)
            var customFields As ICollection(Of CustomField) = this.ParseCustomFields(responseString)
            return customFields
        End Function

        /// <summary>
        /// Gets the custom field by its identifier.
        /// </summary>
        /// <param name="authentication">The authentication.</param>
        /// <param name="id">The identifier.</param>
        /// <returns>Custom field.</returns>
            public Function GetCustomFieldById(authentication As AuthenticationInformation, id As String) As CustomField
            this.ValidateAuthenticationInformation(authentication)
            ValidationHelper.Requirement(Of ArgumentNullException)(Not String.IsNullOrWhiteSpace(id),
                                                                   MessageTemplate.ShouldHaveValue, NameOf(id))
            var requestUri = UriHelper.GetFullyQualifiedPath(authentication.InstanceUrl, ApiPath.Tooling.SObjects.CustomField & id)
            var requestHeaders = this.GetAuthenticationRequestHeaders(authentication)
            this.requestHandler.SetValidHttpErrorCodes(HttpStatusCode.Unauthorized)
            var responseString As String = requestHandler.ProcessGetRequest(requestUri, requestHeaders)
            this.ValidateApiResponseForError(responseString)
            var customField As CustomField = this.ParseCustomField(responseString)
            return customField
        End Function

        /// <summary>
        /// Gets the authorization request headers.
        /// </summary>
        /// <param name="authentication">The authentication.</param>
        /// <returns>Request headers collection.</returns>
            private Function GetAuthenticationRequestHeaders(authentication As AuthenticationInformation) As IEnumerable(Of KeyValuePair<String, String>)
            var requestHeaders = new List(Of KeyValuePair<String, String>)
            requestHeaders.Add(new KeyValuePair<String, String>(RequestHeader.Authorization, $"Bearer {authentication.AccessToken}"))
            return requestHeaders
        End Function

        /// <summary>
        /// Parses the contacts.
        /// </summary>
        /// <param name="responseString">The response string.</param>
        /// <returns>Collection of contacts to import.</returns>
            private Function ParseContacts(responseString As String) As ICollection(Of ContactImport)
            var response = JsonConvert.DeserializeObject(Of ContactsQueryResponse)(responseString)
            var contacts As IEnumerable(Of ContactImport) = response.Records _
                .Select(Function(record) new ContactImport() With
                    {.FirstName = record.FirstName,
                    .LastName = record.LastName,
                    .Email = record.Email,
                    .Company = record.Company,
                    .Phone = record.Phone,
                    .Address1 = record.MailingAddress?.Address1,
                    .City = record.MailingAddress?.City,
                    .State = record.MailingAddress?.State,
                    .Zip = record.MailingAddress?.Zip,
                    .BirthDate = record.BirthDate}) _
.ToArray()
return contacts
End Function

        /// <summary>
        /// Parses the leads.
        /// </summary>
        /// <param name="responseString">The response string.</param>
        /// <returns>Collection of leads to import.</returns>
            private Function ParseLeads(responseString As String) As ICollection(Of ContactImport)
            var response = JsonConvert.DeserializeObject(Of LeadsQueryResponse)(responseString)
            var contacts As IEnumerable(Of ContactImport) = response.Records _
                .Select(Function(record) new ContactImport() With
                    {.FirstName = record.FirstName,
                    .LastName = record.LastName,
                    .Email = record.Email,
                    .Company = record.Company,
                    .Phone = record.Phone,
                    .Address1 = record.Address1,
                    .City = record.City,
                    .State = record.State,
                    .Zip = record.Zip}) _
.ToArray()
return contacts
End Function

        /// <summary>
        /// Parses the custom fields.
        /// </summary>
        /// <param name="responseString">The response string.</param>
        /// <returns>Collection of custom fields.</returns>
            private Function ParseCustomFields(responseString As String) As ICollection(Of CustomField)
            var customFieldQueryResponse = JsonConvert.DeserializeObject(Of CustomFieldQueryResponse)(responseString)
            var customFields As IEnumerable(Of CustomField) = customFieldQueryResponse.Records _
                .Select(Function(record) this.GetCustomFieldFromResponse(record)) _
                .ToArray()
            return customFields
        End Function

        /// <summary>
        /// Parses the custom field.
        /// </summary>
        /// <param name="responseString">The response string.</param>
        /// <returns>Custom field.</returns>
            private Function ParseCustomField(responseString As String) As CustomField
            var customFieldRecord = JsonConvert.DeserializeObject(Of CustomFieldRecord)(responseString)
            var customField = this.GetCustomFieldFromResponse(customFieldRecord)
            return customField
        End Function

        /// <summary>
        /// Gets the custom field from response object.
        /// </summary>
        /// <param name="record">The record.</param>
        /// <returns>Custom field.</returns>
            private Function GetCustomFieldFromResponse(record As CustomFieldRecord) As CustomField
            return new CustomField() With
                {.Id = record.Id,
                .FullyQualifiedName = record.FullName,
                .Name = record.DeveloperName,
                .Table = record.TableEnumOrId,
                .Label = record.Metadata?.Label,
                .Type = record.Metadata?.Type}
            End Function

        /// <summary>
        /// Verifies the authentication signature.
        /// </summary>
        /// <param name="authentication">The authentication.</param>
        /// <exception cref="SalesForceAuthException">Signature is not valid.</exception>
            private Sub VerifyAuthenticationSignature(authentication As RefreshTokenResponse)
            if Not String.IsNullOrWhiteSpace(authentication.Signature) Then
                var stringToSign As String = authentication.Id & authentication.IssuedAt
                var signedString As String = CryptographyHelper.GetBase64EncodedHmacSha256Hash(stringToSign, ClientSecret)
                if signedString <> authentication.Signature Then
                     Throw new SalesForceAuthException(AuthErrorCodes.SignatureValidationError, "Signature is not valid.")
                End If
            End If
        End Sub

        /// <summary>
        /// Validates the authentication response for error.
        /// </summary>
        /// <param name="responseString">The response string.</param>
        /// <exception cref="SalesForceAuthException">API responded with invalid grant type/value or client ID/secret.</exception>
        /// <exception cref="SalesForceException">API responded with other error.</exception>
            private Sub ValidateAuthResponseForError(responseString As String)
            var response = JsonConvert.DeserializeObject(Of AuthenticationErrorResponse)(responseString)
            if Not String.IsNullOrWhiteSpace(response.ErrorCode) Then
                if response.ErrorCode = AuthErrorCodes.InvalidGrantValue _
                OrElse response.ErrorCode = AuthErrorCodes.InvalidGrantType _
                OrElse response.ErrorCode = AuthErrorCodes.InvalidClientId _
                OrElse response.ErrorCode = AuthErrorCodes.InvalidClientSecret Then
                    Throw new SalesForceAuthException(response.ErrorCode, response.ErrorDescription)
                Else
                    Throw new SalesForceException(response.ErrorCode, response.ErrorDescription)
                End If
            End If
        End Sub

        /// <summary>
        /// Validates the API response for error.
        /// </summary>
        /// <param name="responseString">The response string.</param>
        /// <exception cref="SalesForceSessionException">API responded with invalid session error.</exception>
        /// <exception cref="SalesForceException">API responded with other error.</exception>
            private Sub ValidateApiResponseForError(responseString As String)
            var jsonResponse = JToken.Parse(responseString)
            var response As ErrorResponse
            if TypeOf jsonResponse Is JArray Then
                response = jsonResponse.First.ToObject(Of ErrorResponse)
            Else
                response = jsonResponse.ToObject(Of ErrorResponse)
            End If
            if response IsNot Nothing AndAlso Not String.IsNullOrWhiteSpace(response.ErrorCode) Then
                if response.ErrorCode = ApiErrorCodes.InvalidSession Then
                    Throw new SalesForceSessionException(response.ErrorCode, response.Message)
                Else
                    Throw new SalesForceException(response.ErrorCode, response.Message)
                End If
            End If
        End Sub

        /// <summary>
        /// Validates the authentication information.
        /// </summary>
        /// <param name="authentication">The authentication.</param>
            private Sub ValidateAuthenticationInformation(authentication As AuthenticationInformation)
            ValidationHelper.Requirement(Of ArgumentNullException)(authentication IsNot Nothing,
                                                                    MessageTemplate.ShouldHaveValue, NameOf(authentication))
            ValidationHelper.Requirement(Of ArgumentNullException)(Not String.IsNullOrWhiteSpace(authentication.AccessToken),
                                                                   MessageTemplate.ShouldHaveValue, NameOf(authentication.AccessToken))
            ValidationHelper.Requirement(Of ArgumentNullException)(Not String.IsNullOrWhiteSpace(authentication.InstanceUrl),
                                                                   MessageTemplate.ShouldHaveValue, NameOf(authentication.InstanceUrl))
        End Sub

        /// <summary>
        /// Encodes the input string.
        /// </summary>
        /// <param name="input">The input string.</param>
        /// <returns>Encoded string.</returns>
            private Function EncodeInputString(input As String) As String
            return If(input Is Nothing, DirectCast(Nothing, String), input.Replace("'", "\'"))
        End Function

        /// <summary>
        /// Gets the custom field where expression.
        /// </summary>
        /// <param name="authentication">The authentication.</param>
        /// <param name="customFieldId">The custom field identifier.</param>
        /// <param name="customFieldValue">The custom field value.</param>
        /// <returns>The custom field where expression.</returns>
            private Function GetCustomFieldWhereExpression(authentication As AuthenticationInformation,
                                                       customFieldId As String,
                                                       customFieldValue As String) As String
            var result As String = Nothing
            if Not String.IsNullOrWhiteSpace(customFieldId) _
            AndAlso Not String.IsNullOrWhiteSpace(customFieldValue) Then
                var customField As CustomField = this.GetCustomFieldById(authentication, customFieldId)
                if customField IsNot Nothing _
                AndAlso Not UnsupportedDataTypes.Contains(customField.Type, StringComparer.InvariantCultureIgnoreCase) Then
                    var rightOperand As String
                    if customField.Type = "Date" Then
                        rightOperand = this.EncodeInputString(Date.Parse(customFieldValue).ToString("yyyy-MM-dd"))
                    Elseif customField.Type = "DateTime" Then
                        rightOperand = this.EncodeInputString(Date.Parse(customFieldValue).ToString("yyyy-MM-ddTHH:mm:ssZ"))
                    Elseif NonQuotedDataTypes.Contains(customField.Type, StringComparer.InvariantCultureIgnoreCase) Then
                        rightOperand = this.EncodeInputString(customFieldValue)
                    Else
                        rightOperand = $"'{this.EncodeInputString(customFieldValue)}'"
                    End If
                    result = $"{customField.FullyQualifiedName} = {rightOperand}"
                End If
            End If
            return result
        End Function
    }
}