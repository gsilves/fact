﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Constants
{
    /// <summary>
    /// Contains SalesForce SOQL data table names.
    /// </summary>
    public static class SoqlDataTable
    {
        /// <summary>
        /// Contacts table.
        /// </summary>
        public const string Contact = "Contact";

        /// <summary>
        /// Leads table.
        /// </summary>
        public const string Lead = "Lead";
    }
}