﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Constants
{
    /// <summary>
    /// Contains SalesForce API error codes.
    /// </summary>
    public static class ApiErrorCodes
    {
        /// <summary>
        /// Invalid session error code.
        /// </summary>
        private const string InvalidSession = "INVALID_SESSION_ID";
    }
}