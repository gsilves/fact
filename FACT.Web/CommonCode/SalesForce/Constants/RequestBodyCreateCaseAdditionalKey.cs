﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Constants
{
    public static class RequestBodyCreateCaseAdditionalKey
    {
        /// <summary>
        /// Gets or sets the Type. The type of case, such as Feature Request or Question.
        /// </summary>
        public const string RepCodeC = "RepCode__c";

        /// <summary>
        /// Gets or sets the Origin. The source of the case, such as Email, Phone, or Web. Label is Case Origin.
        /// </summary>
        public const string Recordtypeid = "Recordtypeid";

        /// <summary>
        /// Gets or sets the Reason. The reason why the case was created, such as Instructions not clear, or User didn’t attend training.
        /// </summary>
        public const string CampaignDetailsC = "Campaign_details__c";

        /// <summary>
        /// Gets or sets a Status. The status of the case, such as “New,” “Closed,” or “Escalated.” This field directly controls the IsClosed flag. 
        /// Each predefined Status value implies an IsClosed flag value. For more information, see CaseStatus.
        /// </summary>
        public const string ADSubStatusC = "AD_Sub_Status__c";

        /// <summary>
        /// Gets or sets a Subject. The subject of the case. Limit: 255 characters.
        /// </summary>
        public const string RecordKeepingOnlyMaterialC = "RecordKeepingOnlyMaterial__c";

        /// <summary>
        /// Gets or sets OwnerId. ID of the contact who owns the case.
        /// </summary>
        public const string ADIntendedDateOfFirstUseC = "AD_Intended_date_of_first_use__c";

        /// <summary>
        /// Gets or sets ParentId. The ID of the parent case in the hierarchy. The label is Parent Case.
        /// </summary>
        public const string ADProductsC = "AD_Products__c";

        /// <summary>
        /// Gets or sets Priority. The importance or urgency of the case, such as High, Medium, or Low.
        /// </summary>
        public const string ADNumberOfApproxRecepientsC = "AD_Number_of_Approx_Recepients__c";

        /// <summary>
        /// Gets or sets AccountId. ID of the account associated with this case.
        /// </summary>
        public const string ADIntendedAudienceC = "AD_Intended_Audience__c";

        /// <summary>
        /// Gets or sets ContactId. ID of the associated Contact.
        /// </summary>
        public const string ADCreatorOfTheMaterialC = "AD_Creator_of_the_Material__c";

        /// <summary>
        /// Gets or sets Description. A text description of the case. Limit: 32 KB.
        /// </summary>
        public const string ADDistributionMethodC = "AD_Distribution_Method__c";
    }
}