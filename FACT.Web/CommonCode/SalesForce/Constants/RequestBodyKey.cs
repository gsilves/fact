﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Constants
{
    /// <summary>
    /// Contains request body form keys.
    /// </summary>
    public static class RequestBodyKey
    {
        /// <summary>
        /// The grant type.
        /// </summary>
        public const string GrantType = "grant_type";

        /// <summary>
        /// The client identifier.
        /// </summary>
        public const string ClientId = "client_id";

        /// <summary>
        /// The client secret.
        /// </summary>
        public const string ClientSecret = "client_secret";

        /// <summary>
        /// The refresh token.
        /// </summary>
        public const string RefreshToken = "refresh_token";

        /// <summary>
        /// The password token.
        /// </summary>
        public const string Password = "password";

        /// <summary>
        /// The UserName token.
        /// </summary>
        public const string UserName = "username";
    }
}