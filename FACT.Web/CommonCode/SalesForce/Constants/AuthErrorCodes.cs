﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Constants
{
    /// <summary>
    /// Contains SalesForce authentication error codes.
    /// </summary>
    public static class AuthErrorCodes
    {
        /// <summary>
        /// The invalid grant type
        /// </summary>
        public const string InvalidGrantType = "unsupported_grant_type";

        /// <summary>
        /// The invalid client identifier
        /// </summary>
        public const string InvalidClientId = "invalid_client_id";

        /// <summary>
        /// The invalid client secret
        /// </summary>
        public const string InvalidClientSecret = "invalid_client";

        /// <summary>
        /// The invalid grant value
        /// </summary>
        public const string InvalidGrantValue = "invalid_grant";

        /// <summary>
        /// The signature validation error
        /// </summary>
        public const string SignatureValidationError = "signature_validation_error";
    }
}