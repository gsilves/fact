﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Constants
{
    /// <summary>
    /// Represents input parameters for <see cref="SalesForceService.CreateCase"/> method.
    /// </summary>
    public static class RequestBodyCreateCaseKey
    {
        /// <summary>
        /// Gets or sets the Type. The type of case, such as Feature Request or Question.
        /// </summary>
        public const string Type = "Type";

        /// <summary>
        /// Gets or sets the Origin. The source of the case, such as Email, Phone, or Web. Label is Case Origin.
        /// </summary>
        public const string Origin = "Origin";

        /// <summary>
        /// Gets or sets the Reason. The reason why the case was created, such as Instructions not clear, or User didn’t attend training.
        /// </summary>
        public const string Reason = "Reason";

        /// <summary>
        /// Gets or sets a Status. The status of the case, such as “New,” “Closed,” or “Escalated.” This field directly controls the IsClosed flag. 
        /// Each predefined Status value implies an IsClosed flag value. For more information, see CaseStatus.
        /// </summary>
        public const string Status = "Status";

        /// <summary>
        /// Gets or sets a Subject. The subject of the case. Limit: 255 characters.
        /// </summary>
        public const string Subject = "Subject";

        /// <summary>
        /// Gets or sets OwnerId. ID of the contact who owns the case.
        /// </summary>
        public const string OwnerId = "OwnerId";

        /// <summary>
        /// Gets or sets ParentId. The ID of the parent case in the hierarchy. The label is Parent Case.
        /// </summary>
        public const string ParentId = "ParentId";

        /// <summary>
        /// Gets or sets Priority. The importance or urgency of the case, such as High, Medium, or Low.
        /// </summary>
        public const string Priority = "Priority";

        /// <summary>
        /// Gets or sets AccountId. ID of the account associated with this case.
        /// </summary>
        public const string AccountId = "AccountId";

        /// <summary>
        /// Gets or sets ContactId. ID of the associated Contact.
        /// </summary>
        public const string ContactId = "ContactId";

        /// <summary>
        /// Gets or sets Description. A text description of the case. Limit: 32 KB.
        /// </summary>
        public const string Description = "Description";

        /// <summary>
        /// Gets or sets IsEscalated. Indicates whether the case has been escalated (true) or not. A case's escalated state does not affect how you can use a case, 
        /// or whether you can query, delete, or update it.You can set this flag via the API. Label is Escalated.
        /// </summary>
        public const string IsEscalated = "IsEscalated";

        /// <summary>
        /// Gets or sets SuppliedName. The name that was entered when the case was created. This field can't be updated after the case has been created.Label is Name.
        /// </summary>
        public const string SuppliedName = "SuppliedName";

        /// <summary>
        /// Gets or sets SuppliedEmail. The email address that was entered when the case was created. This field can't be updated after the case has been created.Label is Email.
        /// If your organization has an active auto-response rule, SuppliedEmail is required when creating a case via the API.Auto-response rules use the email in the contact 
        /// specified by ContactId.If no email address is in the contact record, the email specified here is used.
        /// </summary>
        public const string SuppliedEmail = "SuppliedEmail";

        /// <summary>
        /// Gets or sets SuppliedPhone. The phone number that was entered when the case was created. This field can't be updated after the case has been created.Label is Phone.
        /// </summary>
        public const string SuppliedPhone = "SuppliedPhone";

        /// <summary>
        /// Gets or sets SuppliedCompany. The company name that was entered when the case was created. This field can't be updated after the case has been created.Label is Company.
        /// </summary>
        public const string SuppliedCompany = "SuppliedCompany";
    }
}