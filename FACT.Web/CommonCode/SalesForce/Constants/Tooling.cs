﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Constants
{
    /// <summary>
    /// Contains paths to tooling api endpoints.
    /// </summary>
    public static class Tooling
    {
        /// <summary>
        /// Path to query endpoint.
        /// </summary>
        public const string Query = "/services/data/v32.0/tooling/query/";

        /// <summary>
        /// Contains paths to tooling api objects and their metadata.
        /// </summary>
        public static class SObjects
        {
            /// <summary>
            /// Path to custom field object endpoint.
            /// </summary>
            public const string CustomField = "/services/data/v32.0/tooling/sobjects/CustomField/";
            /// <summary>
            /// Path to Case object endpoint.
            /// </summary>
            public const string Case = "/services/data/v39.0/sobjects/Case";

            public const string Profile = "/services/data/v39.0/sobjects/Profile";
        }
    }
}