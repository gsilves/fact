﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.constants
{
    /// <summary>
    /// Contains SalesForce API paths.
    /// </summary>
    public static class ApiPath
    {
        /// <summary>
        /// Path to refresh token endpoint.
        /// </summary>
        public const string RefreshToken = "/services/oauth2/token";

        /// <summary>
        /// Path to query endpoint.
        /// </summary>
        public const string Query = "/services/data/v40.0/query/";

        /// <summary>
        /// Contains base API paths.
        /// </summary>
        public static class BasePath

            /// <summary>
            /// Base path to login endpoints.
            /// </summary>
        public const string Login = "https://login.salesforce.com/";

        /// <summary>
        /// Base path to test endpoints.
        /// </summary>
        public const string Test = "https://test.salesforce.com/";

        /// <summary>
        /// Base path to Dev endpoints.
        /// </summary>
        public const string Dev = "https://advisorgroup--dev2015.cs7.my.salesforce.com/";
        public const string QA = "https://advisorgroup--QA.cs1.my.salesforce.com/";
        public const string Prod = "https://advisorgroup.my.salesforce.com/";
    }
}