﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Constants
{
    /// <summary>
    /// Contains SalesForce API query string keys.
    /// </summary>
    public static class QueryStringKey
    {
        /// <summary>
        /// The SOQL query.
        /// </summary>
        public const string SoqlQuery = "q";
    }
}