﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.SalesForce.Constants
{
    /// <summary>
    /// Contains SalcesForce API request headers.
    /// </summary>
    public static class RequestHeader
    {
        /// <summary>
        /// Authorization header.
        /// </summary>
        public const string Authorization = "Authorization";
    }
}