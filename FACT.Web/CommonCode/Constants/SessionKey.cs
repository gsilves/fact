﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.Constants
{
    ///<summary>
    ///Contains session key constants.
    ///</summary>
    public static class SessionKey
    {
        ///<summary>
        ///Contains user-specific session keys.
        ///</summary>
        public static class UserInformation
        {
            ///<summary>
            ///The email.
            ///</summary>
            public const string Email = "Email";

            /// <summary>
            /// The OPS username.
            /// </summary>
            public const string OpsUsername = "OPSUsername";
        }

        /// <summary>
        /// Contains session keys associated with SalesForce service.
        /// </summary>
        public static class SalesForce
        {
            /// <summary>
            /// The Sales force access token.
            /// </summary>
            public const string AccessToken = "SalesforceAuthToken";

            /// <summary>
            /// The sales force refresh token.
            /// </summary>
            public const string RefreshToken = "SalesfoceRefreshToken";

            /// <summary>
            /// the sales force instance URL.
            /// </summary>
            public const string InstanceUrl = "SalesforceInstance";
        }
    }
}