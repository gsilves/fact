﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.Constants
{
    /// <summary>
    /// Contains constants for content types.
    /// </summary>
    public static class ContentType
    {
        /// <summary>
        /// The form-data
        /// </summary>
        public const string FormData = "form-data";
        
        /// <summary>
        /// text content types.
        /// </summary>
        public static class Text
        {
            /// <summary>
            /// The html.
            /// </summary>
            public const string html = "text/html; charset=utf-8";
        }

        /// <summary>
        /// Multipart content types.
        /// </summary>
        public static class Multipart
        {
            /// <summary>
            /// The form-data.
            /// </summary>
            public const string FormDataWB = "multipart/form-data; boundary=---------------------------974767299852498929531610575";
            public const string FormData = "multipart/form-data";
        }

        /// <summary>
        /// Application content types.
        /// </summary>
        public static class Application
        {
            /// <summary>
            /// The WWW form URL encoded.
            /// </summary>
            public const string WwwFormUrlEncoded = "application/x-www-form-urlencoded";

            /// <summary>
            /// Json.
            /// </summary>
            public const string Json = "application/json";
        }
    }
}