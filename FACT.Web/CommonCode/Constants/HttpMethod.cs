﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.CommonCode.Constants
{
    /// <summary>
    /// Represents valid HTTP methods.
    /// </summary>
    public static class HttpMethod
    {
        /// <summary>
        /// The GET HTTP method.
        /// </summary>
        public const string HttpGet = "GET";

        /// <summary>
        /// The POST HTTP method.
        /// </summary>
        public const string HttpPost = "POST";
    }
}