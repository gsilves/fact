﻿using FACT.Domain.Models;
using FACT.Domain.Models.EntityModels;
using FACT.Web.Services;
using Newtonsoft.Json;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace FACT.Web.Business
{
    public static class GlobalFunctions
    {
        public static string ToBase64(Byte[] data)
        {
            if (data == null)
            {
                throw new ArgumentNullException("data");
            }
            return Convert.ToBase64String(data);
        }

        public static Byte[] Convert64(string inputFile)
        {
            Byte[] Bytes = Encoding.UTF8.GetBytes(inputFile);
            var encodedText = Convert.ToBase64String(Bytes);
            var decoded = Convert.FromBase64String(encodedText);
            return decoded;
        }

        public static Object ProcessJSONHTTPRequest(string uri, Byte[] jsonDataBytes, string contentType, string method, string headers, string authType)
        {
            string jsonDataString = string.Empty;
            if (jsonDataBytes == null)
            {
                jsonDataString = string.Empty;
            }
            else
            {
                jsonDataString = Encoding.UTF8.GetString(jsonDataBytes);
            }
            switch (method)
            {
                case "POST":
                    return PostJSONHTTPRequestSync(uri, jsonDataString, contentType, headers, authType);
                case "GET":
                    return GetJSONHTTPRequestSync(uri, jsonDataString, contentType, headers, authType);
                default:
                    return null;
            }
        }

        public static Object PostJSONHTTPRequestSync(string uri, string requestBody, string contentType, string headers, string authType)
        {
            Uri xURI = new Uri(uri);
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                RestClient client = new RestClient(xURI);
                RestRequest request = new RestRequest("", Method.POST);
                SetupRequestHeader(ref request, contentType, headers, authType);
                request.RequestFormat = contentType.ToUpper().IndexOf("XML") >= 0 ? DataFormat.Xml : DataFormat.Json;
                if (requestBody != "")
                {
                    request.AddParameter(contentType, requestBody, ParameterType.RequestBody);
                }
                var response = client.Execute(request);
                JsonSerializerSettings jcSettings = new JsonSerializerSettings();
                jcSettings.NullValueHandling = NullValueHandling.Ignore;
                jcSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
                return JsonConvert.DeserializeObject(response.Content, jcSettings);
            }
            catch (Exception ex)
            {
                MessageError Message = new MessageError();
                Message.ErrorText = ex.Message;
                return Message;
            }
        }

        public static Object GetJSONHTTPRequestSync(string uri, string requestBody, string contentType, string headers, string authType)
        {
            Uri xURI = new Uri(uri);
            if (xURI.ToString().IndexOf("file:///") < 0)
            {
                try
                {
                    ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                    RestClient client = new RestClient(xURI);
                    RestRequest request = new RestRequest("", Method.GET);
                    SetupRequestHeader(ref request, contentType, headers, authType);
                    if (requestBody != "")
                    {
                        request.AddParameter(contentType, requestBody, ParameterType.RequestBody);
                    }
                    var response = client.Execute(request);
                    JsonSerializerSettings jcSettings = new JsonSerializerSettings();
                    jcSettings.NullValueHandling = NullValueHandling.Ignore;
                    jcSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
                    return JsonConvert.DeserializeObject(response.Content, jcSettings);
                }
                catch (Exception ex)
                {
                    showPrompt(ex.Message, true);
                    return null;
                }
            }
            else
            {
                return ReadLocalFile(uri, contentType);
            }
        }

        private static void SetupRequestHeader(ref RestRequest request, string contentType, string headers, string authType)
        {
            request.JsonSerializer.ContentType = contentType + "; charset=utf-8";
            request.AddHeader("Accept", contentType);
            request.AddHeader("Content-Type", contentType + "; charset=utf-8");
            if (headers != "")
            {
                switch (authType)
                {
                    case "Basic":
                        request.AddHeader("Authorization", "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(headers)));
                        break;
                    case "Bearer":
                        request.AddHeader("Authorization", "Bearer " + headers);
                        break;
                    default:
                        break;
                }
            }
        }

        public static void showPrompt(string strText, bool blnError)
        {
            Page cPage = (Page)HttpContext.Current.Handler;
            MasterPage pageMaster = cPage.Master;
            Panel okPanel = (Panel)pageMaster.FindControl("jWrap");
            Label okTextBox = (Label)pageMaster.FindControl("okResponse");
            Panel errorPanel = (Panel)pageMaster.FindControl("jWrapE");
            Label errorTextBox = (Label)pageMaster.FindControl("errorResponse");
            if (!blnError)
            {
                okPanel.Visible = true;
                okTextBox.Text = @"<i class=""fa fa-check""></i> " + strText;
            }
            else
            {
                errorPanel.Visible = true;
                errorTextBox.Text = @"<i class=""fa fa-exclamation-circle""></i>  " + strText;
            }
        }

        public static Object ReadLocalFile(string file, string contentType)
        {
            if (String.IsNullOrEmpty(contentType))
            {
            }
            var text = System.IO.File.ReadAllText(file);
            JsonSerializerSettings jcSettings = new JsonSerializerSettings();
            jcSettings.NullValueHandling = NullValueHandling.Ignore;
            jcSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
            return JsonConvert.DeserializeObject(text, jcSettings);
        }

        public static XmlDocument loadXML(string xmlData)
        {
            XmlDocument doc = new XmlDocument();
            doc.Load(xmlData);
            return doc;
        }

        public static string adjustCentralTime(string strTime)
        {
            //TODO make this independent of OS Locale.
            CultureInfo MyCultureInfo = new CultureInfo("en-US");
            DateTime time = DateTime.Parse(strTime, MyCultureInfo);
            TimeZoneInfo tst = TimeZoneInfo.FindSystemTimeZoneById("Central Standard Time");
            var tstTime = TimeZoneInfo.ConvertTime(time, TimeZoneInfo.Local, tst);
            return tstTime.ToString("MM/dd/yyyy hh:mm:ss tt");
        }
    }
}