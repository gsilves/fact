﻿using System;

namespace FACT.Web.Business
{
    public class TContactActivity
    {
        public string mfTicket { get; set; }
        public string mfPassword { get; set; }
        public int contactId { get; set; }
        public int activitiesPerPage { get; set; }
        public int activityStartRowIndex { get; set; }
        public DateTime fromDate { get; set; }
        public DateTime toDate { get; set; }
        public string sortExpression { get; set; }
    }
}