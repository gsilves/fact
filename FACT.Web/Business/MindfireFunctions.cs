﻿using RestSharp;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Web;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System.IO;
using CsvHelper;
using FACT.Service;
using System.Configuration;
using FACT.Domain.Models;
using System.Net;
using System.Xml;
using FACT.Domain.Models.EntityModels;
using FACT.Web.Helpers;
using FACT.Web.Models.Filters;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using FACT.Domain.Models.Controllers;
using System.Text.RegularExpressions;

namespace FACT.Web.Business
{
    public class MindfireFunctions : IDisposable
    {
        public Byte[] progCallData;
        private bool disposed = false;

        public enum ContactListStatus
        {
            [Description("Waiting for process")]
            WAITING,
            [Description("In process")]
            PROCESS,
            [Description("Complete")]
            COMPLETE,
            [Description("Failed")]
            FAILED,
            [Description("Canceled")]
            CANCELED,
            [Description("All")]
            All
        };

        public const string filterAll = "" + @"<Filter CriteriaJoinOperator=\"" |\""\/>" + "";

        public string Post(string serviceUrl, string endPoint, string requestBody, string contentType, string headers, string authType)
        {
            try
            {
                RestClient service = new RestClient(serviceUrl);
                RestRequest request = new RestRequest(endPoint, Method.POST);
                request.JsonSerializer.ContentType = contentType + "; charset=utf-8";
                request.AddHeader("Accept", contentType);
                request.AddHeader("Content-Type", contentType + "; charset=utf-8");
                if (headers != "")
                {
                    switch (authType)
                    {
                        case "Basic":
                            request.AddHeader("Authorization", "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(headers)));
                            break;
                        case "Bearer":
                            request.AddHeader("Authorization", "Bearer " + headers);
                            break;
                        default:
                            break;
                    }
                }
                if (contentType.ToUpper().IndexOf("XML") >= 0)
                {
                    request.RequestFormat = DataFormat.Xml;
                }
                else
                {
                    request.RequestFormat = DataFormat.Json;
                }
                if (requestBody != "")
                {
                    request.AddParameter(contentType, requestBody, ParameterType.RequestBody);
                }
                var response = service.Execute(request);
                return response.Content;
            }
            catch (Exception)
            {
                return "";
            }
        }

        public string CallService(string endPoint, Object requestBody, string contentType, string headers, string authType)
        {
            try
            {
                RestClient service = new RestClient("http://studio.mdl.io/REST/");
                RestRequest request = new RestRequest(endPoint, Method.POST);
                request.JsonSerializer.ContentType = contentType + "; charset=utf-8";
                request.AddHeader("Accept", contentType);
                request.AddHeader("Content-Type", contentType + "; charset=utf-8");
                if (headers != "")
                {
                    switch (authType)
                    {
                        case "Basic":
                            request.AddHeader("Authorization", "Basic " + Convert.ToBase64String(Encoding.Default.GetBytes(headers)));
                            break;
                        case "Bearer":
                            request.AddHeader("Authorization", "Bearer " + headers);
                            break;
                        default:
                            break;
                    }
                }
                if (contentType.ToUpper().IndexOf("XML") >= 0)
                {
                    request.RequestFormat = DataFormat.Xml;
                }
                else
                {
                    request.RequestFormat = DataFormat.Json;
                }
                if (requestBody != null)
                {
                    request.AddJsonBody(requestBody);
                }
                var response = service.Execute(request);
                return response.Content;
            }
            catch (Exception)
            {
                return null;
            }
        }

        public IEnumerable<TContactActivity> GetContactActivitiesByPage(string mfTicket, string mfPassword, int contactId, int activitiesPerPage, int activityStartRowIndex,
            DateTime fromDate, DateTime toDate, string sortExpression = "")
        {
            TContactActivity obj = new TContactActivity();
            List<TContactActivity> result = new List<TContactActivity>();
            List<string> fieldNames = new List<string>();
            StringBuilder strFNames = new StringBuilder();
            strFNames.Append("[");
            //TODO fill fieldNames with property names of TContactActivity
            PropertyInfo[] info = obj.GetType().GetProperties();
            foreach (PropertyInfo prop in info)
            {
                strFNames.Append(prop.Name + ",");
                fieldNames.Add(prop.Name);
            }
            string sfn = strFNames.ToString();
            sfn = sfn.Substring(0, sfn.Length - 1) + "]";
            DateTime constDate = Convert.ToDateTime("01/01/1970");
            var requestBody = new
            {
                Credentials = new
                {
                    IsEncrypted = false,
                    Ticket = mfTicket,
                    UserPassword = mfPassword
                },
                ContactID = contactId,
                SortExpression = sortExpression,
                MaxRows = activitiesPerPage,
                StartRowIndex = activityStartRowIndex,
                FromDate = "/Date(" + ((constDate - fromDate).TotalSeconds * 1000).ToString() + "-300)/",
                ToDate = "/Date(" + Convert.ToString((constDate - toDate).TotalSeconds * 1000) + "-300)/",
                FieldNames = fieldNames.ToArray()
            };
            string serviceResult = CallService("contactservice/GetContactActivitiesByPage", requestBody, "application/json", "", "");
            if (!string.IsNullOrEmpty(serviceResult))
            {
                JObject jRes = JObject.Parse(serviceResult);
                if (jRes["Result"]["ErrorCode"].ToString() == "")
                {
                    string strActivities = jRes["ContactActivities"].ToString();
                    //TODO strActivities is csv with contract activities
                    StringReader sr = new StringReader(strActivities);
                    CsvParser parser = new CsvParser(sr);
                    CsvReader reader = new CsvReader(parser);
                    while (reader.Read())
                    {
                        result.Add((TContactActivity)reader.GetRecord(obj.GetType()));
                    }
                }
                else
                { }
            }
            return result;
        }

        public JObject CreateContact(string mFTicket, string mFPassword, string mfPartnerTicket, List<KeyValuePair<string, string>> contact)
        {
            JArray keyValueList = new JArray();
            foreach (KeyValuePair<string, string> field in contact)
            {
                JObject kvpair = new JObject();
                kvpair["Key"] = field.Key;
                kvpair["Value"] = field.Value;
                keyValueList.Add(kvpair);
            }
            string kvListstring = keyValueList.ToString();
            byte[] listCallData3 = Encoding.UTF8.GetBytes(@"{" + @"""KeyValueList"":" + kvListstring + "," + @"""Credentials"":{" + @"""IsEncrypted"":false," +
                @"""PartnerPassword"":""" + ConfigurationManager.AppSettings["MFPartnerPassword"].ToString() + @"""," + @"""PartnerTicket"":""" + mfPartnerTicket + @"""," +
                @"""Ticket"":""" + mFTicket + @"""," + @"""UserPassword"":""" + mFPassword + @"""" + "}" + "}");
            return JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/CreateContact", listCallData3, "application/json", "POST", "", ""));
        }

        public JObject UpdateContact(string mFTicket, string mFPassword, string mfPartnerTicket, List<KeyValuePair<string, string>> contact)
        {
            JArray keyValueList = new JArray();
            foreach (KeyValuePair<string, string> field in contact)
            {
                JObject kvpair = new JObject();
                kvpair["Key"] = field.Key;
                kvpair["Value"] = field.Value;
                keyValueList.Add(kvpair);
            }
            string kvListstring = keyValueList.ToString();
            byte[] listCallData3 = Encoding.UTF8.GetBytes(@"{" + @"""KeyValueList"":" + kvListstring + "," + @"""Credentials"":{" + @"""IsEncrypted"":false," + 
                @"""PartnerPassword"":""fe64c0656ca0472e9dade99c24084cb1""," + @"""PartnerTicket"":""" + mfPartnerTicket + @"""," + @"""Ticket"":""" + mFTicket + @"""," + 
                @"""UserPassword"":""" + mFPassword + @"""" + "}," + @"""DeleteContact"":false" + "}");
            return JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/UpdateContact", listCallData3, "application/json", "POST", "", ""));
        }

        public JObject DeleteContact(string mFTicket, string mFPassword, string mfPartnerTicket, List<KeyValuePair<string, string>> contact)
        {
            JArray keyValueList = new JArray();
            foreach (KeyValuePair<string, string> field in contact)
            {
                JObject kvpair = new JObject();
                kvpair["Key"] = field.Key;
                kvpair["Value"] = field.Value;
                keyValueList.Add(kvpair);
            }
            string kvListstring = keyValueList.ToString();
            byte[] listCallData3 = Encoding.UTF8.GetBytes(@"{" + @"""KeyValueList"":" + kvListstring + "," + @"""Credentials"":{" + @"""IsEncrypted"":false," +
                @"""PartnerPassword"":""fe64c0656ca0472e9dade99c24084cb1""," + @"""PartnerTicket"":""" + mfPartnerTicket + @"""," + @"""Ticket"":""" + mFTicket + @"""," +
                @"""UserPassword"":""" + mFPassword + @"""" + "}," + @"""DeleteContact"":true" + "}");
            JObject delObj = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/UpdateContact", listCallData3,
                "application/json", "POST", "", ""));
            return delObj;
        }

        public string[,] AddCustomFieldValues(Stream strInputFile, string newFileName, string strGroupName, string strSource)
        {
            try
            {
                int y;
                int x;
                StreamReader filestring = new StreamReader(strInputFile);
                string[] FileRows = filestring.ReadToEnd().Split(Convert.ToChar(Environment.NewLine));
                int NumberOfRows;
                string[] FileColumns = FileRows[0].Split(',');
                int NumberOfColumns = FileColumns.GetUpperBound(0);
                if (strSource != "CSVUpload")
                {
                    NumberOfRows = FileRows.GetUpperBound(0) - 1;
                }
                else
                {
                    NumberOfRows = FileRows.GetUpperBound(0);
                }
                string[,] XArray = new string[NumberOfRows, NumberOfColumns + 2];
                XArray[0, NumberOfColumns + 1] = "GroupName";
                XArray[0, NumberOfColumns + 2] = "ContactSource";
                for (x = 0; x <= NumberOfRows; x++)  //Cycle through all the rows
                {
                    FileColumns = FileRows[x].Split(','); //Split row into columns
                    for (y = 0; y <= NumberOfColumns; y++)  //Now cycle through all the columns on that row
                    {
                        XArray[x, y] = FileColumns[y]; //Set each piece of data into the array.
                        if (x != 0)
                        {
                            XArray[x, NumberOfColumns + 1] = strGroupName;
                            XArray[x, NumberOfColumns + 2] = strSource;
                        }
                    }
                }
                WriteCSVFile(HttpContext.Current.Server.MapPath("/temp/" + newFileName), XArray);
                filestring.Close();
                return XArray;
            }
            catch (Exception ex)
            {
                HttpContext.Current.Response.Write(ex.Message);
                return null;
            }
        }

        public void WriteCSVFile(string varFileName, Object[,] Array)
        {
            StreamWriter Filenew = new StreamWriter(varFileName);
            int NumberOfRows = Array.GetUpperBound(0); // Count number of rows.
            int NumberOfColumns = Array.GetUpperBound(1); // Count number of columns.
            string[] FileRows = new string[NumberOfRows];
            //The idea here is to make up the row first by adding all of the columns together then write each row to the file.
            for (int i0 = 0; i0 <= NumberOfRows; i0++) //Loop through all of the Rows
            {
                for (int i1 = 0; i1 <= NumberOfColumns; i1++) //Loop through all of the Columns
                {
                    if (FileRows[i0] == "") //Check if it is the first column then we dont need the ","
                    {
                        FileRows[i0] = Array[i0, i1].ToString(); //if it is the first column we start the row with the first column
                    }
                    else
                    {
                        FileRows[i0] = FileRows[i0] + "," + Array[i0, i1]; //Now we take what we have so far and add "," and add the the next column
                    }
                } // We repeat this process untill of of the columns are written to a row.
                Filenew.WriteLine(FileRows[i0]); // Now we write that row to the file.
            } // Move on to the next row.
            Filenew.Close();
        }

        public bool CreateCustomMFFields(string strTicket, string password)
        {
            //Set up custom fields
            bool blnFieldExists = false;
            byte[] checkFields = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":""fe64c0656ca0472e9dade99c24084cb1"",""PartnerTicket"":"""",""Ticket"":""" +
                strTicket + @""",""UserPassword"":""" + password + @"""},""Type"":2}");
            Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactFieldList", checkFields, "application/json", "POST", "", "");
            if (tmp.GetType() == typeof(MessageError))
            {
                return false;
            }
            else
            {
                JObject checkFields_result = JObject.FromObject(tmp);
                if (checkFields_result["Result"]["ErrorMessage"].ToString() != "")
                {
                    return false;
                }
                else
                {
                    if (checkFields_result["FieldList"].HasValues)
                    {
                        blnFieldExists = true;
                        return true;
                    }
                }
                if (!blnFieldExists)
                {
                    byte[] fieldData = Encoding.UTF8.GetBytes(@"{""Category_ID"":1,""DataType_ID"":1,""DedupGroupIndex"":0,""Description"":null,""ID"":1,""IsEncrypted"":false,""IsRequired"":false,""MaxLength"":0,""Name"":""ContactSource"",""PURLIndex"":0,""Type"":2,""ValidationRegex"":null,""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":""fe64c0656ca0472e9dade99c24084cb1"",""PartnerTicket"":"""",""Ticket"":""" +
                        strTicket + @""",""UserPassword"":""" + password + @"""}}");
                    byte[] fieldData2 = Encoding.UTF8.GetBytes(@"{""Category_ID"":1,""DataType_ID"":1,""DedupGroupIndex"":0,""Description"":null,""ID"":1,""IsEncrypted"":false,""IsRequired"":false,""MaxLength"":0,""Name"":""GroupName"",""PURLIndex"":0,""Type"":2,""ValidationRegex"":null,""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":""fe64c0656ca0472e9dade99c24084cb1"",""PartnerTicket"":"""",""Ticket"":""" + 
                        strTicket + @""",""UserPassword"":""" + password + @"""}}");
                    Object tmp1 = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/SetContactField", fieldData, "application/json", "POST", "", "");
                    if (tmp1.GetType() == typeof(MessageError))
                    {
                        return false;
                    }
                    else
                    {
                        JObject fieldData_result = JObject.FromObject(tmp1);
                        if (fieldData_result["Result"]["ErrorMessage"].ToString() != "")
                        {
                            return false;
                        }
                        else
                        {
                            Object tmp2 = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/SetContactField", fieldData2, "application/json", "POST", "", "");
                            if (tmp2.GetType() == typeof(MessageError))
                            {
                                return false;
                            }
                            else
                            {
                                return true;
                            }
                        ;
                        }
                    }
                }
                return true;
            }
        }

        public string LaunchCampaign(string programData, string strMFTicket, string strMFPassword)
        {
            try
            {
                byte[] progCallData2 = Encoding.UTF8.GetBytes(@"{""Maml"":[" + programData +
                                                             @"],""MamlFormat"":0,""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" +
                                                             strMFTicket + @""",""UserPassword"":""" + strMFPassword + @"""}}");
                Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/PublishProgram", progCallData2, "application/json", "POST", "", "");
                if (tmp.GetType() == typeof(MessageError))
                {
                    MessageError msg = new MessageError();
                    msg = (MessageError)tmp;
                    return msg.ErrorText;
                }
                else
                {
                    JObject callPublish = JObject.FromObject(tmp);
                    return callPublish.ToString();
                }
            }
            catch (WebException ex)
            {
                return ex.Response.ToString();
            }
        }

        public XmlDocument GetCampaignForm(string cId, string maml, string costCenterId)
        {
            XmlDocument campXMLLoad = new XmlDocument();
            if (string.IsNullOrEmpty(cId))
            {
            }
            if (File.Exists(HttpContext.Current.Server.MapPath("/Maml/BaseMAML/") + costCenterId + maml))
            {
                campXMLLoad = GlobalFunctions.loadXML(HttpContext.Current.Server.MapPath("/Maml/BaseMAML/") + costCenterId + maml);
            }
            else
            {
                campXMLLoad = GlobalFunctions.loadXML(HttpContext.Current.Server.MapPath("/Maml/BaseMAML/") + maml);
            }
            return campXMLLoad;
        }

        public void AuthenticateAccount(string strEmail, string strPassword, ref Profiles objModel)
        {
            var param = new {
                Email = strEmail,
                IsEncrypted = false,
                PartnerGuid = ConfigurationManager.AppSettings["MFPartnerGUID"],
                PartnerPassword = ConfigurationManager.AppSettings["MFPartnerPassword"],
                Password = strPassword,
                SelectedAccountID = (int?)null
            };
            RestClient client = new RestClient("https://studio.mdl.io");
            RestRequest request = new RestRequest("REST/userservice/Authenticate", Method.POST);
            request.AddHeader("Content-type", "application/json");
            request.AddJsonBody(param);
            // Execute the request and parse JSON result
            var response = client.Execute(request);
            JObject result = JObject.Parse(response.Content);
            if (result["Result"]["ErrorCode"].ToString() == "")
            {
                if (!string.IsNullOrEmpty(result["AvailableAccountList"][0]["Key"].ToString()))
                {
                    int acctID = Convert.ToInt32((result["AvailableAccountList"][0]["Key"].ToString()));
                    var params1 = new
                    {
                        Email = strEmail,
                        IsEncrypted = false,
                        PartnerGuid = ConfigurationManager.AppSettings["MFPartnerGUID"],
                        PartnerPassword = ConfigurationManager.AppSettings["MFPartnerPassword"],
                        Password = strPassword,
                        SelectedAccountID = acctID
                    };
                    RestRequest request1 = new RestRequest("REST/userservice/Authenticate", Method.POST);
                    request1.AddHeader("Content-type", "application/json");
                    request1.AddJsonBody(params1);
                    // Execute the request and parse JSON result
                    var response1 = client.Execute(request1);
                    result = JObject.Parse(response1.Content);
                    if (result["Result"]["ErrorCode"].ToString() == "")
                    {
                        objModel.MFTicket = result["Credentials"]["Ticket"].ToString();
                        objModel.MFMasterPartnerTicket = result["Credentials"]["PartnerTicket"].ToString();
                        objModel.MFAccountID = acctID.ToString();
                        objModel.MFDomain = acctID + ".agmycmo.com";
                        objModel.MFParentAccountID = result["ParentAccountID"].ToString();
                        objModel.MFUserID = result["UserID"].ToString();
                        objModel.MFUserToken = result["UserToken"].ToString();
                    }
                    else
                    {
                        throw new Exception("MyCMO Authenticate MFAccount failed." + "\r\n" + "Additional info:" + "\r\n" + result["Result"]["ErrorMessage"].ToString());
                    }
                }
                else
                {
                    throw new Exception("MyCMO Authenticate MFAccount failed." + "\r\n" + "Additional info: None available account found for selected Partner");
                }
            }
            else
            {
                throw new Exception("MyCMO Authenticate MFAccount failed." + "\r\n" + "Additional info:" + "\r\n" + result["Result"]["ErrorMessage"].ToString());
            }
        }

        private List<int> DecodeContacts(List<int> contacts)
        {
            List<int> result = new List<int>();
            StringBuilder strE = new StringBuilder();
            if ((contacts.Count() >= 5) && (contacts[0] == 73) && (contacts[1] == 100))
            {
                int i;
                for (i = 4; i <= contacts.Count() - 1; i++)
                {
                    if (contacts[i] == 13)
                    {
                        if (strE.Length > 0)
                        {
                            result.Add(int.Parse(strE.ToString()));
                            strE.Clear();
                        }
                        else if (contacts[i] != 10)
                        {
                            strE.Append(Convert.ToInt32(contacts[i].ToString()));
                        }
                    }
                }
            }
            return result;
        }

        private JArray BuildContactsArray(string contacts)
        {
            int i;
            int j;
            JArray result = new JArray();
            string[] strData = contacts.Split(new string[] { "\r\n" }, System.StringSplitOptions.None);
            if (strData.Length > 1)
            {
                string[] fieldNames = strData[0].Split(',');
                for (i = 1; i <= strData.Length - 1; i++)
                {
                    string[] fieldValues = strData[i].Replace("\n", "").Replace("\r", "").Split(',');
                    if (fieldValues.Length == fieldNames.Length)
                    {
                        JObject jObj = new JObject();
                        for (j = 0; j <= fieldValues.Length - 1; j++)
                        {
                            jObj[fieldNames[j]] = fieldValues[j];
                        }
                        result.Add(jObj);
                    }
                }
            }
            return result;
        }

        public List<string> ContactsEmails(Profiles profile)
        {
            SegmentationFilter filter = new SegmentationFilter();
            filter.CriteriaJoinOperator = "|";
            string filterXml = XmlHelper.Serialize(filter);
            StringBuilder requestBody = new StringBuilder();
            requestBody.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + profile.MFTicket +
                               @""",""UserPassword"":""" + profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"].ToString())) +
                               @"""},""Filter"":" + JsonConvert.ToString(filterXml) + "}");
            byte[] strListData = Encoding.UTF8.GetBytes(requestBody.ToString());
            GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactListCount", strListData, "application/json", "POST", "", "");
            JArray contactList = GetFilteredContactArray(profile.MFTicket, profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"].ToString())), filter);
            List<string> emails = new List<string>();
            if (contactList != null)
            {
                foreach (JToken contact in contactList.Children())
                {
                    JToken p = contact["Email"];
                    emails.Add(p.ToString());
                }
            }
            return emails;
        }

        public SelectList GetContactGroups(string strMFTicket, string strMFPassword)
        {
            StringBuilder requestBody = new StringBuilder();
            string joinOp = @"<Filter CriteriaJoinOperator=\"" |\""\/> ";
            requestBody.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + strMFTicket +
                                   @""",""UserPassword"":""" + strMFPassword +
                                   @"""},""FieldNames"":[""GroupName""],""Filter"":""" + joinOp + @""",""OutputType"":1}");
            byte[] strListData = Encoding.UTF8.GetBytes(requestBody.ToString());
            Object tmp1 = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactList", strListData, "application/json", "POST", "", "");
            if (tmp1.GetType() == typeof(MessageError))
            {
            }
            else
            {
                JObject result_post = new JObject(tmp1);
                if (result_post["Result"]["ErrorMessage"].ToString() != "")
                {
                    //TODO what here?
                    return null;
                }
                else
                {
                    StringBuilder strResult = new StringBuilder();
                    for (int i = 0; i <= result_post["Contacts"].Count() - 1; i++)
                    {
                        strResult.Append(Convert.ToByte(result_post["Contacts"][i]));
                    }
                    string strContacts = strResult.ToString();
                    string[] tmpStr = strContacts.Split(new string[] { "\r\n" }, System.StringSplitOptions.None);
                    List<string> list = new List<string>();
                    foreach (string t in tmpStr)
                    {
                        string p = t.ToString().Trim();
                        if (p != "GroupName" && p != "")
                        {
                            list.Add(p);
                        }
                    }
                    SelectList result1 = new SelectList(list.Distinct());
                    return result1;
                }
            }
            return null;
        }

        public string GetContactFieldList(string strMFTicket, string strMFPassword)
        {
            int i;
            string strFieldNames = "";
            StringBuilder requestBody = new StringBuilder();
            // first we get ContactFieldList
            requestBody.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + strMFTicket +
                @""",""UserPassword"":""" + strMFPassword + @"""},""Type"":0}");
            byte[] strListData = Encoding.UTF8.GetBytes(requestBody.ToString());
            Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactFieldList", strListData, "application/json", "POST", "", "");
            if (tmp.GetType() == typeof(MessageError))
            {
                MessageError msg = new MessageError();
                msg = (MessageError)tmp;
                return msg.ErrorText;
            }
            else
            {
                JObject result_post = JObject.FromObject(tmp);
                if (result_post["Result"]["ErrorMessage"].ToString() != "")
                {
                    //TODO what here?
                    return null;
                }
                else
                {
                    for (i = 0; i <= result_post["FieldList"].Count() - 1; i++)
                    {
                        strFieldNames = strFieldNames + @"""" + result_post["FieldList"][i]["Name"].ToString() + @"""";
                        if (i < result_post["FieldList"].Count() - 1)
                        {
                            strFieldNames = strFieldNames + ",";
                        }
                    }
                    if (strFieldNames != "")
                    {
                        strFieldNames = "," + strFieldNames;
                    }
                    strFieldNames = @"""Id""" + strFieldNames;
                    return strFieldNames;
                }
            }
        }

        public JObject GetContact(string strMFTicket, string strMFPassword, int contactId)
        {
            JArray contacts = GetContactList(strMFTicket, strMFPassword);
            if ((contacts != null) && (contacts.Count() > 0))
            {
                for (int i = 0; i <= contacts.Count() - 1; i++)
                {
                    if (Convert.ToInt32(contacts[i]["Id"].ToString()) == contactId)
                    {
                        return (JObject)contacts[i];
                    }
                }
            }
            return null;
        }

        public JArray GetContactList(string strMFTicket, string strMFPassword)
        {
            string fieldNames = GetContactFieldList(strMFTicket, strMFPassword);
            StringBuilder requestBody = new StringBuilder();
            string joinOp = @"<Filter CriteriaJoinOperator=\"" |\""\/> ";
            requestBody.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + strMFTicket +
                               @""",""UserPassword"":""" + strMFPassword +
                               @"""},""FieldNames"":[" + fieldNames + @"],""OutputType"":1, ""Filter"":""" + joinOp + @"""}");
            Byte[] strListData = Encoding.UTF8.GetBytes(requestBody.ToString());
            Object tmp1 = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactList", strListData, "application/json", "POST", "", "");
            if (tmp1.GetType() == typeof(MessageError))
            {
                return (JArray)false;
            }
            else
            {
                JObject result_post = JObject.FromObject(tmp1);
                if (result_post["Result"]["ErrorMessage"].ToString() != "")
                {
                    return null;
                }
                else
                {
                    //Dim contactsStr = result_post("ContactsStr")
                    StringBuilder strResult = new StringBuilder();
                    for (int i = 0; i <= result_post["Contacts"].Count() - 1; i++)
                    {
                        strResult.Append(Convert.ToByte(result_post["Contacts"][i].ToString()));
                    }
                    string strContacts = strResult.ToString();
                    JArray contacts = BuildContactsArray(strContacts);
                    return contacts;
                }
            }
        }

        public JArray GetFilteredContactArray(string strMFTicket, string strMFPassword, SegmentationFilter filter)
        {
            int i;
            string strFieldNames = string.Empty;
            StringBuilder requestBody = new StringBuilder();
            requestBody.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + strMFTicket + @""",""UserPassword"":""" +
                               strMFPassword + @"""},""Type"":0}");
            byte[] strListData = Encoding.UTF8.GetBytes(requestBody.ToString());
            Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactFieldList", strListData, "application/json", "POST", "", "");
            if (tmp.GetType() == typeof(MessageError))
            {
                MessageError msg = new MessageError();
                msg = (MessageError)tmp;
                return (JArray)msg.ErrorText;
            }
            else
            {
                JObject result_post = JObject.FromObject(tmp);
                if (result_post["Result"]["ErrorMessage"].ToString() != "")
                {
                    //TODO what here?
                    return null;
                }
                else
                {
                    for (i = 0; i <= result_post["FieldList"].Count() - 1; i++)
                    {
                        strFieldNames += @"""" + result_post["FieldList"][i]["Name"].ToString() + @"""";
                        if (i < result_post["FieldList"].Count() - 1)
                        {
                            strFieldNames = strFieldNames + ",";
                        }
                    }
                    if (strFieldNames != "")
                    {
                        strFieldNames = "," + strFieldNames;
                    }
                    strFieldNames = @"""Id""" + strFieldNames;
                }
                // Now we get contacts
                string filterXml = XmlHelper.Serialize(filter);
                StringBuilder strResult = new StringBuilder();
                requestBody.Clear();
                requestBody.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + strMFTicket +
                                   @""",""UserPassword"":""" + strMFPassword +
                                   @"""},""FieldNames"":[" + strFieldNames + @"],""Filter"":" + JsonConvert.ToString(filterXml) + @",""OutputType"":1}");
                strListData = Encoding.UTF8.GetBytes(requestBody.ToString());
                Object tmp1 = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactList", strListData, "application/json", "POST", "", "");
                if (tmp1.GetType() == typeof(MessageError))
                {
                    MessageError msg = new MessageError();
                    msg = (MessageError)tmp1;
                    return (JArray)msg.ErrorText;
                }
                else
                {
                    result_post = JObject.FromObject(tmp1);
                    if (result_post["Result"]["ErrorMessage"].ToString() != "")
                    {
                        //TODO what here?
                        return null;
                    }
                    else
                    {
                        //Dim contactsStr = result_post("ContactsStr")
                        for (i = 0; i <= result_post["Contacts"].Count() - 1; i++)
                        {
                            strResult.Append(Convert.ToByte(result_post["Contacts"][i].ToString()));
                        }
                        string strContacts = strResult.ToString();
                        JArray contacts = BuildContactsArray(strContacts);
                        return contacts;
                    }
                }
            }
        }

        public int[] GetFilteredContactList(string strMFTicket, string strMFPassword, SegmentationFilter filter)
        {
            int i;
            string strFieldNames = "";
            StringBuilder requestBody = new StringBuilder();
            // first we get ContactFieldList
            requestBody.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + strMFTicket + @""",""UserPassword"":""" +
                               strMFPassword + @"""},""Type"":0}");
            byte[] strListData = Encoding.UTF8.GetBytes(requestBody.ToString());
            Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactFieldList", strListData, "application/json", "POST", "", "");
            if (tmp.GetType() == typeof(MessageError))
            {
                return null;
            }
            else
            {
                JObject result_post = JObject.FromObject(tmp);
                if (result_post["Result"]["ErrorMessage"].ToString() != "")
                {
                    //TODO what here?
                    return null;
                }
                else
                {
                    for (i = 0; i <= result_post["FieldList"].Count() - 1; i++)
                    {
                        strFieldNames = strFieldNames + @"""" + result_post["FieldList"][i]["Name"].ToString() + @"""";
                        if (i < result_post["FieldList"].Count() - 1)
                        {
                            strFieldNames = strFieldNames + ",";
                        }
                    }
                    if (strFieldNames != "")
                    {
                        strFieldNames = "," + strFieldNames;
                    }
                    strFieldNames = @"""Id""" + strFieldNames;
                }
                List<int> res = new List<int>();
                string filterXml = XmlHelper.Serialize(filter);
                StringBuilder strResult = new StringBuilder();
                requestBody.Clear();
                requestBody.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + strMFTicket +
                               @""",""UserPassword"":""" + strMFPassword +
                               @"""},""FieldNames"":[" + strFieldNames + @"],""Filter"":" + JsonConvert.ToString(filterXml) + @",""OutputType"":1}");
                strListData = Encoding.UTF8.GetBytes(requestBody.ToString());
                Object tmp1 = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactList", strListData, "application/json", "POST", "", "");
                if (tmp1.GetType() == typeof(MessageError))
                {
                    return null;
                }
                else
                {
                    result_post = JObject.FromObject(tmp1);
                    if (result_post["Result"]["ErrorMessage"].ToString() != "")
                    {
                        //TODO what here?
                        return null;
                    }
                    else
                    {
                        //Dim contactsStr = result_post("ContactsStr")
                        for (i = 0; i <= result_post["Contacts"].Count() - 1; i++)
                        {
                            strResult.Append(Convert.ToByte(result_post["Contacts"][i].ToString()));
                            res.Add(Convert.ToByte(result_post["Contacts"][i]));
                        }
                        string strContacts = strResult.ToString();
                        BuildContactsArray(strContacts);
                        return DecodeContacts(res).ToArray();
                    }
                }
            }
        }

        public string GetFilteredContactNumber(string strMFTicket, string strMFPassword, SegmentationFilter filter)
        {
            string filterXml = XmlHelper.Serialize(filter);
            StringBuilder requestBody = new StringBuilder();
            requestBody.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + strMFTicket +
                               @""",""UserPassword"":""" + strMFPassword +
                               @"""},""Filter"":" + JsonConvert.ToString(filterXml) + "}");
            byte[] strListData = Encoding.UTF8.GetBytes(requestBody.ToString());
            Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactListCount", strListData, "application/json", "POST", "", "");
            if (tmp.GetType() == typeof(MessageError))
            {
                MessageError msg = new MessageError();
                msg = (MessageError)tmp;
                return msg.ErrorText;
            }
            else
            {
                JObject result_post = JObject.FromObject(tmp);
                return result_post["Count"].ToString();
            }
        }

        public string CheckTargetRecords(string strMFTicket, string strMFPassword, SegmentationFilter filter)
        {
            int rf = 0;
            string s = GetFilteredContactNumber(strMFTicket, strMFPassword, filter);
            if (int.TryParse(s, out rf))
            {
            }
            return GetFilteredContactNumber(strMFTicket, strMFPassword, filter) + " Records Found";
        }

        ////// <summary>
        ////// Reads all the imported lists of contacts from Mindfire and returns an MVC SelectList populated with data.
        ////// </summary>
        ////// <param name="partnerTicket"></param>
        ////// <param name="ticket"></param>
        ////// <param name="userPassword"></param>
        ////// <returns>A SelectList populated with names of imported contact groups</returns>
        public SelectList GetImportedContactsLists(string partnerTicket, string ticket, string userPassword)
        {
            // We arrange all parameters into an object that will be converted into JSON body for API request.
            var param = new
            {
                Credentials = new
                {
                    IsEncrypted = false,
                    PartnerPassword = "",
                    PartnerTicket = partnerTicket,
                    Ticket = ticket,
                    UserPassword = userPassword
                },
                MaxRows = 500,
                Searchstring = "",
                SortExpression = "",
                StartRowIndex = 0,
                Status = 2
            };
            // Set up the rest client
            RestClient client = new RestClient("https://studio.mdl.io");
            RestRequest request = new RestRequest("REST/contactservice/GetImportList", Method.POST);
            request.AddHeader("Content-type", "application/json");
            request.AddJsonBody(param);
            // Execute the request and parse JSON result
            var response = client.Execute(request);
            JObject result = JObject.Parse(response.Content);
            // Extract the information we need, and fill it into a list.
            List<ListItem> list = new List<ListItem>();
            foreach (var item in result["ImportList"])
            {
                if (Convert.ToInt32(item["Mode"].ToString()) != 4 && item["Filterable"].ToString() == "true")
                    {
                    list.Add(new ListItem {
                        Text = item["Name"].ToString() + " (" + item["SuccessfulRecords"].ToString() + ")",
                        Value = item["Name"].ToString()
                    });
                };
            }
            return new SelectList(list);
        }

        public string setReportingTokenSession(string strAccId, string strEmail, string strPass)
        {
            byte[] dashAuthCall = Encoding.UTF8.GetBytes(@"{""AccountID"":" + strAccId + @",""UserEmail"":""" + strEmail + @""",""UserPass"":""" + strPass + @"""}");
            JObject dash_post = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.dashboard.mdl.io/api/Report/Authenticate",
                dashAuthCall, "application/json", "POST", "", "Basic"));
            return dash_post["Token"].ToString();
        }

        public string GenerateEmailPreview(string strEmailBody, string strEmailFrom, string strEmailFromName, string strEmailSubject, string strNotificationEmail)
        {
            string newstring = strEmailBody.Replace("\r", "").Replace("\n", "").Replace("\n", "").Replace("\r", "").Replace(@"""", "//");
            try
            {
                byte[] dashAuthCall = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":""" +
                    HttpContext.Current.Session["MFMasterPartnerTicket"] + @""",""Ticket"":""" + HttpContext.Current.Session["Email"] +
                    @""",""UserPass"":""" + HttpContext.Current.Session["MFTicket"] + @""",""UserPassword"":""" + HttpContext.Current.Session["Password"] +
                    @"""},""EmailBody"":""" + newstring + @""",""EmailFrom"":""" + strEmailFrom + @""",""EmailFromName"":""" + strEmailFromName + @""",""EmailSubject"":""" +
                    strEmailSubject + @""",""NotificationEmail"":""" + strNotificationEmail + @"""}");
                Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/CreateEmailPreview", dashAuthCall, "application/json", "POST", "", "Basic");
                if (tmp.GetType() == typeof(MessageError))
                {
                    MessageError msg = new MessageError();
                    msg = (MessageError)tmp;
                    return msg.ErrorText;
                }
                else
                {
                    return "Email preview generation now processing. A link will be sent toy our inbox with a link to the previews. Preview processing can take up to 10 minutes.";
                }
            }
            catch (WebException ex)
            {
                return ex.Message;
            }
        }

        public string AddDomain(string domainName)
        {
            MasterMFAccount masterMFA = GetAuthorizedPartner();
            if (masterMFA != null)
            {
                byte[] dashAuthCall = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":""" +
                    masterMFA.PartnerTicket + @""",""Ticket"":""" + masterMFA.Ticket + @""",""UserPassword"":""" + masterMFA.Password + @"""},""Name"":""" + domainName +
                    @""",""Description"":""" + domainName + @""",""IsHidden"":false,""OnlyWithSSL"":true}");
                Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/configurationservice/AddDomain",
                    (byte[])(dashAuthCall), "application/json", "POST", "", "Basic");
                if (tmp.GetType() == typeof(MessageError))
                {
                    MessageError msg = new MessageError();
                    msg = (MessageError)tmp;
                    return msg.ErrorText;
                }
                else
                {
                    return string.Empty;
                }
            }
            else
            {
                return null;
            }
        }

        public int? CreateMFSubAccount(string strAccountName, string strEmail, string strFirstName, string strLastName, string strPassword)
        {
            MasterMFAccount masterMFA = GetAuthorizedPartner();
            if (masterMFA != null)
            {
                byte[] accData = Encoding.UTF8.GetBytes(@"{""Name"":""" + strAccountName +
                    @""",""PasswordRenewalFrequency"":5,""PasswordStrengthLevel"":1,""ServicePackage_ID"":1,""StatementDay"":1,""Status"":1,""TimeoutMin"":20,""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":""" +
                    masterMFA.PartnerPassword + @""",""PartnerTicket"":""" + masterMFA.PartnerTicket + @""",""Ticket"":""" + masterMFA.Ticket +
                    @""",""UserPassword"":""" + strPassword + @"""},""InheritFromAccountID"":16425,""PowerUserEmail"":""" + strEmail +
                    @""",""PowerUserFirstName"":""" + strFirstName + @""",""PowerUserLastName"":""" + strLastName + @""",""PowerUserPassword"":""" + strPassword + @"""}");
                JObject accData_result = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/accountservice/CreateAccount", accData,
                    "application/json", "POST", "", ""));
                if (accData_result["Result"]["ErrorMessage"].ToString() != "")
                {
                    return null;
                }
                else
                {
                    return Convert.ToInt32(accData_result["AccountID"]);
                }
            }
            return null;
        }

        public int? DeactivateMFSubAccount(string strAccountName, string strEmail, string strFirstName, string strLastName, string strPassword)
        {
            MasterMFAccount masterMFA = GetAuthorizedPartner();
            if (masterMFA != null)
            {
                byte[] accData = Encoding.UTF8.GetBytes(@"{""Name"":""" + strAccountName +
                    @""",""PasswordRenewalFrequency"":5,""PasswordStrengthLevel"":1,""ServicePackage_ID"":1,""StatementDay"":1,""Status"":2,""TimeoutMin"":20,""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":""" +
                    masterMFA.PartnerPassword + @""",""PartnerTicket"":""" + masterMFA.PartnerTicket + @""",""Ticket"":""" + masterMFA.Ticket +
                    @""",""UserPassword"":""" + strPassword + @"""},""InheritFromAccountID"":16425,""PowerUserEmail"":""" + strEmail + @""",""PowerUserFirstName"":""" +
                    strFirstName + @""",""PowerUserLastName"":""" + strLastName + @""",""PowerUserPassword"":""" + strPassword + @"""}");
                JObject accData_result = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/accountservice/UpdateAccount", accData,
                    "application/json", "POST", "", ""));
                if (accData_result["Result"]["ErrorMessage"].ToString() != "")
                {
                    return null;
                }
                else
                {
                    return Convert.ToInt32(accData_result["AccountID"]);
                }
            }
            else
            {
                return null;
            }
        }

        public int? ActivateMFSubAccount(string strAccountName, string strEmail, string strFirstName, string strLastName, string strPassword)
        {
            MasterMFAccount masterMFA = GetAuthorizedPartner();
            if (masterMFA != null)
            {
                byte[] accData = Encoding.UTF8.GetBytes(@"{""Name"":""" + strAccountName +
                    @""",""PasswordRenewalFrequency"":5,""PasswordStrengthLevel"":1,""ServicePackage_ID"":1,""StatementDay"":1,""Status"":1,""TimeoutMin"":20,""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":""" +
                    masterMFA.PartnerPassword + @""", ""PartnerTicket"": """ + masterMFA.PartnerTicket + @""",""Ticket"":""" + masterMFA.Ticket +
                    @""",""UserPassword"":""" + strPassword + @"""},""InheritFromAccountID"":16425,""PowerUserEmail"":""" + strEmail + @""",""PowerUserFirstName"":""" +
                    strFirstName + @""",""PowerUserLastName"":""" + strLastName + @""",""PowerUserPassword"":""" + strPassword + @"""}");
                JObject accData_result = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/accountservice/UpdateAccount",
                    accData, "application/json", "POST", "", ""));
                if (accData_result["Result"]["ErrorMessage"].ToString() != "")
                {
                    return null;
                }
                else
                {
                    return Convert.ToInt32(accData_result["AccountID"]);
                }
            }
            else
            {
                return null;
            }
        }

        public MasterMFAccount GetAuthorizedPartner()
        {
            var param = new
            {
                Email = ConfigurationManager.AppSettings["MFPartnerEmail"].ToString(),
                IsEncrypted = Convert.ToBoolean(ConfigurationManager.AppSettings["MFPartnerIsEncrypted"]),
                PartnerGuid = ConfigurationManager.AppSettings["MFPartnerGUID"].ToString(),
                PartnerPassword = ConfigurationManager.AppSettings["MFPartnerPassword"].ToString(),
                Password = ConfigurationManager.AppSettings["MFPartnerEmailPassword"].ToString(),
                SelectedAccountID = Convert.ToInt32(ConfigurationManager.AppSettings["MFPartnerSelectedAccountID"].ToString())
            };
            RestClient client = new RestClient("https://studio.mdl.io");
            RestRequest request = new RestRequest("REST/userservice/Authenticate", Method.POST);
            request.AddHeader("Content-type", "application/json");
            request.AddJsonBody(param);
            // Execute the request and parse JSON result
            var response = client.Execute(request);
            JObject result = JObject.Parse(response.Content);
            if (result["Result"]["ErrorCode"].ToString() == "")
            {
                MasterMFAccount authorizedPartner = new MasterMFAccount();
                authorizedPartner.Email = param.Email;
                authorizedPartner.PartnerGuid = param.PartnerGuid; ;
                authorizedPartner.PartnerPassword = param.PartnerPassword;
                authorizedPartner.Password = param.Password;
                if (result["ParentAccountID"] != null)
                {
                    authorizedPartner.ParentAccountId = result["ParentAccountID"].ToString();
                }
                authorizedPartner.AccountId = result["AccountID"].ToString();
                authorizedPartner.PartnerTicket = result["Credentials"]["PartnerTicket"].ToString();
                authorizedPartner.Ticket = result["Credentials"]["Ticket"].ToString();
                authorizedPartner.UserId = result["UserID"].ToString();
                authorizedPartner.UserToken = result["UserToken"].ToString();
                return authorizedPartner;
            }
            else
            {
                return null;
            }
        }

        public void SetAuthorizedPartner()
        {
            byte[] authData = Encoding.UTF8.GetBytes(@"{""Email"":""jfroehlke@rangedelivers.com"",""IsEncrypted"":false,""PartnerGuid"":""RangePrintingAPIUser"",""PartnerPassword"":""fe64c0656ca0472e9dade99c24084cb1"",""Password"":""LPLConnect101!"",""SelectedAccountID"":16425}");
            JObject authData_result = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/userservice/AuThenticate",
                authData, "application/json", "POST", "", ""));
            if (authData_result["Result"]["ErrorMessage"].ToString() != "")
            {
                HttpContext.Current.Response.Write(authData_result["Result"]["ErrorMessage"].ToString());
            }
            else
            {
                HttpContext.Current.Session["MFMasterPartnerTicket"] = authData_result["Credentials"]["PartnerTicket"].ToString();
                HttpContext.Current.Session["MFMasterTicket"] = authData_result["Credentials"]["Ticket"].ToString();
                HttpContext.Current.Session["MFMasterPartnerPassword"] = authData_result["Credentials"]["PartnerPassword"].ToString();
            }
        }

        public bool CheckMindfireUser(string strEmail, string mfPassword)
        {
            byte[] authUser = Encoding.UTF8.GetBytes(@"{""Email"":""" + strEmail +
                @""",""IsEncrypted"":false,""PartnerGuid"":""RangePrintingAPIUser"",""PartnerPassword"":""fe64c0656ca0472e9dade99c24084cb1"",""Password"":""" +
                mfPassword + @""",""SelectedAccountID"":null,""UserToken"":""""}");
            Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/userservice/Authenticate", authUser, "application/json", "POST", "", "");
            if (tmp.GetType() == typeof(MessageError))
            {
                return false;
            }
            else
            {
                JObject authUser_result = JObject.FromObject(tmp);
                if ((authUser_result["UserToken"].ToString() == "") || (authUser_result["UserToken"] == null))
                {
                    return false;
                }
                else
                {
                    return true;
                }
            }
        }

        public string GetOutboundList(string strFilter, string strTitle, Profiles profile)
        {
            HttpContext.Current.Response.Clear();
            HttpContext.Current.Response.ContentType = "text/csv";
            HttpContext.Current.Response.AddHeader("content-disposition", "attachment;filename=RecipientList_" + strTitle.Replace(" ", "") + ".csv");
            StringBuilder strE = new StringBuilder();
            int i;
            string joinOp = @"<Filter CriteriaJoinOperator=\"" + amp;\"" > ";
            int critCount = strFilter.Split("Criteria".ToArray()).Length - 1;
            if (critCount > 1)
            {
                joinOp = @"<Filter CriteriaJoinOperator=\"" |\"" > ";
            }
            string strFilterVal = joinOp + strFilter.Replace(((char)34).ToString(), @"\" + ((char)34).ToString() + " </ Filter > ");
            byte[] listCallData = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":""" + profile.MFMasterPartnerTicket +
                @" "",""Ticket"":""" + profile.MFTicket + @""",""UserPassword"":""" + profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) +
                @"""},""FieldNames"":[""Email"",""FirstName"",""LastName"",""Id"",""GroupName"",""ContactSource"",""Address1"",""Address2"",""Company"",""Purl"",""BirthDate""],""Filter"":""" +
                strFilterVal + @""",""OutputType"":1}");
            Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/contactservice/GetContactList", listCallData, "application/json", "POST", "", "");
            if (tmp.GetType() == typeof(MessageError))
            {
                MessageError msg = new MessageError();
                msg = (MessageError)tmp;
                return msg.ErrorText;
            }
            else
            {
                JObject result_post = JObject.FromObject(tmp);
                if (result_post["Result"]["ErrorMessage"].ToString() != "")
                {
                    strE.Append("Data Error");
                }
                else
                {
                    if (result_post["Contacts"].Count() > 0)
                    {
                        for (i = 0; i <= result_post["Contacts"].Count() - 1; i++)
                        {
                            strE.Append(result_post["Contacts"][i].ToString());
                        }
                    }
                    else
                    {
                        strE.Append("No Contact Records returned");
                    }
                }
                HttpContext.Current.Response.Write(strE.ToString());
                HttpContext.Current.Response.End();
                return strE.ToString();
            }
        }

        public string EmailHTMLFillContentVariables(string HTMLContent, XmlElement mamlRootElement, XmlElement mamlElement, JObject contact)
        {
            string strRuleGroupSet = mamlElement.Attributes["RuleGroup"].Value;
            XmlElement ruleGroupContentXML = mamlRootElement.SelectSingleNode("//RuleGroup[@Name=//" + strRuleGroupSet + "//]")["ContentVariables"];
            XmlNodeList ruleGroupSetXML = ruleGroupContentXML.GetElementsByTagName("ContentVariable");
            for (int intFieldCountXML = 0; intFieldCountXML <= ruleGroupSetXML.Count - 1; intFieldCountXML++)
            {
                var strSessionTag = ruleGroupSetXML[intFieldCountXML].Attributes["Name"].Value;
                HTMLContent = HTMLContent.Replace("##" + strSessionTag + "##", ruleGroupSetXML[intFieldCountXML].InnerText);
            }
            HTMLContent = HTMLContent.Replace("##firstname##", contact["FirstName"].ToString());
            return HTMLContent;
        }

        public string GenerateEmailTest(string programData, int intCampaignId, JArray aryContacts, int intOutboundMamlID, int selectedElement, Profiles advisor)
        {
            if ((intCampaignId == 0) || (intOutboundMamlID == 0))
            {
            }
            if (selectedElement > 0)
            {
                try
                {
                    JObject joCredentials = new JObject();
                    //TODO DEBUG!!!!
                    joCredentials.Add("Email", advisor.Email);
                    joCredentials.Add("PartnerGuid", "RangePrintingAPIUser");
                    joCredentials.Add("PartnerPassword", "fe64c0656ca0472e9dade99c24084cb1");
                    joCredentials.Add("Password", advisor.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])));
                    joCredentials.Add("SelectedAccountID", Convert.ToInt32(advisor.MFAccountID));
                    joCredentials.Add("PartnerTicket", "");
                    joCredentials.Add("Ticket", advisor.MFTicket);
                    for (int i = 0; i <= aryContacts.Count() - 1; i++)
                    {
                        JObject contact = (JObject)aryContacts[i];
                        int contactId = Convert.ToInt32(contact["Id"]);
                        JObject joAuthCall = new JObject();
                        joAuthCall.Add("ContactID", contactId);
                        joAuthCall.Add("Credentials", joCredentials);
                        joAuthCall.Add("ServiceTypeID", 201);
                        XmlDocument mamlXML = new XmlDocument();
                        mamlXML.LoadXml(programData);
                        XmlElement rootLoad = mamlXML.DocumentElement;
                        string properties = "";
                        XmlNodeList nodeList = mamlXML.SelectNodes(".//CampaignElements/CampaignElement");
                        string HtmlContent;
                        XmlElement node = (XmlElement)nodeList.Item(selectedElement);
                        XmlNode propertiesNode = node["Properties"];
                        var msgNode = propertiesNode["Messages"]["Message"];
                        if (msgNode["HtmlContent"]["URL"] == null)
                        {
                            HtmlContent = new WebClient().DownloadString(msgNode["HtmlContent"]["URL"].Value);
                            if (HtmlContent == null)
                            {
                                HtmlContent = "";
                                throw new Exception("");
                            }
                        }
                        else
                        {
                            HtmlContent = msgNode["HtmlContent"].InnerText;
                        }
                        msgNode["HtmlContent"].InnerText = EmailHTMLFillContentVariables(HtmlContent, rootLoad, node, contact);
                        properties = propertiesNode.OuterXml;
                        string dbgStr = properties;
                        string propertiesXML = properties;
                        joAuthCall.Add("PropertiesXML", propertiesXML);
                        dbgStr = joAuthCall.ToString().Replace("/r/n", "").Replace("/n", "").Replace("/r/n", "").Replace("\n", "").Replace("\r", "");
                        byte[] dashAuthCall = Encoding.UTF8.GetBytes(dbgStr);
                        Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/SendAppTest", dashAuthCall, "application/json", "POST", "", "");
                        if (tmp.GetType() == typeof(MessageError))
                        {
                            MessageError msg = new MessageError();
                            msg = (MessageError)tmp;
                            return msg.ErrorText;
                        }
                        else
                        {
                            JObject result_post = JObject.FromObject(tmp);
                            if (result_post["Result"]["ErrorMessage"].ToString() != "")
                            {
                                //TODO what here?
                                return "Test Email for ContactId" + aryContacts[i].ToString() + " not sent! Error: " + result_post["Result"]["ErrorMessage"].ToString();
                            }
                            else
                            {
                            }
                        }
                    }
                    return "";
                }
                catch (WebException ex)
                {
                    return ex.Message;
                }
            }
            else
            {
                return "Invalid template!";
            }
        }

        public string SetEmail(string email, Profiles _profile)
        {
            try
            {
                byte[] progCallData = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" +
                    _profile.MFTicket + @""",""UserPassword"":""" + _profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) +
                    @"""}," + @"{""newEmail"":""" + email + @"""}");
                Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/userservice/ChangeUserEmail", progCallData, "application/json", "POST", "", "");
                if (tmp.GetType() == typeof(MessageError))
                {
                    MessageError msg = new MessageError();
                    msg = (MessageError)tmp;
                    return msg.ErrorText;
                }
                else
                {
                    JObject result_post = JObject.FromObject(tmp);
                    if (result_post["Result"]["ErrorMessage"].ToString() != "")
                    {
                        return result_post["Result"]["ErrorMessage"].ToString();
                    }
                    else
                    {
                        return "";
                    }
                }
            }
            catch (Exception ex)
            {
                return ex.Message;
            }
        }

        public List<BagEvent> GetSchedules(List<int> outboundIds, string mfTicket, string userPassword)
        {
            List<BagEvent> result = new List<BagEvent>();
            StringBuilder strCallData = new StringBuilder();
            strCallData.Append(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + mfTicket +
                @""",""UserPassword"":""" + userPassword + @"""},""OutboundList"":[");
            foreach (int progId in outboundIds)
            {
                strCallData.Append(@"{""Key"":" + progId + @",""Value"":null}");
                strCallData.Append(",");
            }
            if (outboundIds.Count() > 0)
            {
                strCallData.Length -= 1;
            }
            strCallData.Append("]}");
            byte[] calCallData = Encoding.UTF8.GetBytes(strCallData.ToString());
            JObject calResultPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/GetOutboundsScheduleList", calCallData,
                "application/json", "POST", "", ""));
            if (calResultPost["Result"]["ErrorMessage"].ToString() != "")
            {
                //Error ...
            }
            else
            {
                if (calResultPost["OutboundScheduleList"] != null)
                {
                    for (int i = 0; i <= calResultPost["OutboundScheduleList"].Count() - 1; i++)
                    {
                        BagEvent o = new BagEvent();
                        o.title = Regex.Replace(calResultPost["OutboundScheduleList"][i]["Name"].ToString(), @"[^A-Za-z0-9\-/]", "");
                        o.start = GlobalFunctions.adjustCentralTime(calResultPost["OutboundScheduleList"][i]["ScheduleDate"].ToString());
                        result.Add(o);
                    }
                }
            }
            return result;
        }

        public string postToMFService(string endPoint, Object param)
        {
            RestClient client = new RestClient("https://studio.mdl.io/REST");
            RestRequest request = new RestRequest(endPoint, Method.POST);
            request.AddHeader("Content-type", "application/json");
            request.AddJsonBody(param);
            var response = client.Execute(request);
            return response.Content;
        }

        public JObject GetFilteredImportList(string strMFTicket, string strMFPassword, SegmentationFilter filter)
        {
            string filterstring = "";
            if (filter != null)
            {
                string filterXml = XmlHelper.Serialize(filter);
                filterstring = JsonConvert.ToString(filterXml);
            }
            var param = new {
                Credentials = new {
                    IsEncrypted = false,
                    PartnerPassword = "",
                    PartnerTicket = "",
                    Ticket = strMFTicket,
                    UserPassword = strMFPassword
                },
                MaxRows = 500,
                Searchstring = filterstring,
                SortExpression = "",
                StartRowIndex = 0,
                Status = 255
            };
            var Res = JObject.Parse(postToMFService("contactservice/GetImportList", param));
            List<ListItem> list = new List<ListItem>();
            foreach (var item in Res["ImportList"])
            {
                if (Convert.ToInt32(item["Mode"]) != 4 && item["Filterable"].ToString() == "true")
                {
                    list.Add(new ListItem
                    {
                        Text = item["Name"] + " (" + item["SuccessfulRecords"] + ")",
                        Value = item["Name"].ToString()
                    });
                }
            }
            return Res;
        }

        public JObject CheckInProgram(string campXML, string mfTicket, string mfPassword)
        {
            string bytestring = "";
            byte[] encodedData = GlobalFunctions.Convert64(campXML);
            for (int x = 0; x <= encodedData.Length - 1; x++)
            {
                bytestring = bytestring + encodedData[x] + ",";
                bytestring = bytestring + Environment.NewLine;
            }
            bytestring = bytestring.Remove(bytestring.LastIndexOf(","));
            Byte[] progCallData2 = Encoding.UTF8.GetBytes(@"{""Maml"":[" + bytestring +
                @"],""MamlFormat"":0,""CheckoutAfterCheckin"":false,""Comments"":""Edited Campaign"",""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" +
                mfTicket + @""",""UserPassword"":""" + mfPassword + @"""}}");
            JObject campaignStopPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/CheckinProgram", progCallData2,
                "application/json", "POST", "", ""));
            return campaignStopPost;
        }


        public JObject CheckOutProgram(string mfTicket, string mfPassword, string programID)
        {
            Byte[] campaignCallData = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + mfTicket +
                @""",""UserPassword"":""" + mfPassword + @"""},""MamlFormat"":0,""ProgramID"":" + programID + @"}");
            JObject campaignResultPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/CheckoutProgram", campaignCallData,
                "application/json", "POST", "", ""));
            return campaignResultPost;
        }

        public JObject GetProgramInfo(string mfTicket, string mfPassword, string programId)
        {
            Byte[] progCallData2 = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" + mfTicket +
                @""",""UserPassword"":""" + mfPassword + @"""},""Program_ID"":" + programId + @"}");
            JObject campaignStopPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/GetProgramInfo", progCallData2,
                "application/json", "POST", "", ""));
            return campaignStopPost;
        }

        #region "IDisposable Support"
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
        }
        #endregion
    }
}