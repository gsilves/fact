﻿using AutoMapper;
using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static FACT.Web.Models.ErrorViewModel;

namespace FACT.Web.Business.Mappings
{
    /// <summary>
    /// Maps tables or views properties to another table or view
    /// </summary>
    public static class ModelMapper
    {
        public static ErrorModels ToErrorModels(this ErrorModel viewModel)
        {
            return Mapper.Map<ErrorModel, ErrorModels>(viewModel);
        }

        public static ErrorModel ToErrorModel(this ErrorModels viewModel)
        {
            return Mapper.Map<ErrorModels, ErrorModel>(viewModel);
        }
    }
}