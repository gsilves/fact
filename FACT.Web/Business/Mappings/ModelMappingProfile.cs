﻿using AutoMapper;
using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static FACT.Web.Models.ErrorViewModel;

namespace FACT.Web.Business.Mappings
{
    /// <summary>
    /// 
    /// </summary>
    public class ModelMappingProfile : Profile
    {
        public ModelMappingProfile()
        {
            CreateMap<ErrorModel, ErrorModels>();
        }
    }
}