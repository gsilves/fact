﻿using AutoMapper;
using FACT.Domain.Models.EntityModels;
using FACT.Web.Business.Mappings;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using static FACT.Web.Models.ErrorViewModel;

namespace FACT.Web.Business.Initialization
{
    public class AutoMapper
    {
        public static void Execute()
        {
            Mapper.Initialize(cfg =>
            {
                cfg.AddProfile<ModelMappingProfile>();
                //cfg.CreateMap<ErrorModel, ErrorModels>();
            });
        }
    }
}