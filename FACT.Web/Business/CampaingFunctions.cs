﻿using FACT.Domain.Models;
using FACT.Domain.Models.EntityModels;
using FACT.Service;
using FACT.Web.Services;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;

namespace FACT.Web.Business
{
    public class CampaingFunctions
    {
        private readonly IFactMainService _factMainService;

        public CampaingFunctions(IFactMainService factMainService)
        {
            _factMainService = factMainService;
        }

        public List<MyCampaignsModel> GetMyCampaigns(Profiles advisor)
        {
            List<MyCampaignsModel> listOfMyCampaignModels = new List<MyCampaignsModel>();
            AmazonS3Service awsService = new AmazonS3Service();
            MamlFunctions mamlController = new MamlFunctions();
            List<Campaigns> savedCampaigns = _factMainService.FindAllCampaigns().Where(p => p.UserId == advisor.UserId && p.Status == "Saved").ToList();
            foreach (var s in savedCampaigns)
            {
                AmazonResponse ar = awsService.DownloadMaml(s.CampaignFileURL);
                if (ar.Successful)
                {
                    string mamlXML = ar.Message;
                    MyCampaignsModel myCampaignsModel = new MyCampaignsModel();
                    myCampaignsModel.Status = "";
                    myCampaignsModel.DbStatus = s.Status;
                    myCampaignsModel.CampaignName = s.CampaignName;
                    myCampaignsModel.DateSubmitted = s.DateSubmitted;
                    myCampaignsModel.Maml = mamlXML;
                    myCampaignsModel.Program = mamlController.FromMAMLString(mamlXML);
                    myCampaignsModel.CreatorName = advisor.FirstName + " " + advisor.LastName;
                    myCampaignsModel.Url = s.CampaignFileURL;
                    listOfMyCampaignModels.Add(myCampaignsModel);
                }
            }
            Byte[] progCallData =
                Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":""" + advisor.MFMasterPartnerTicket + @""",""Ticket"":""" +
                advisor.MFTicket + @""",""UserPassword"":""" + advisor.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) +
                @"""},""SearchString"":"""",""MaxRows"":500,""SortExpression"":"""",""StartRowIndex"":0,""State"":255}");
            JObject progResultPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/GetProgramList", progCallData,
                                                                                             "application/json", "POST", "", ""));
            if (progResultPost["Result"]["ErrorMessage"].ToString() != "")
            {
            }
            else
            {
                for (int i = 0; i <= progResultPost["ProgramList"].Count() - 1; i++)
                {
                    string creatorId = progResultPost["ProgramList"][i]["CreatorID"].ToString();
                    if (creatorId == advisor.MFUserID)
                    {
                        string pName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                        try
                        {
                            Campaigns dbCampaign = _factMainService.FindAllCampaigns().Where(x => x.CampaignName == pName && x.UserId == advisor.UserId).FirstOrDefault();
                            if (dbCampaign != null)
                            {
                                if (dbCampaign.Status == "Saved")
                                {
                                    foreach (MyCampaignsModel savedCampaign in listOfMyCampaignModels)
                                    {
                                        if (savedCampaign.CampaignName == pName)
                                        {
                                            savedCampaign.CreatorName = progResultPost["ProgramList"][i]["CreatorName"].ToString();
                                            savedCampaign.Id = Convert.ToInt32(progResultPost["ProgramList"][i]["ProgramID"].ToString());
                                            savedCampaign.CampaignName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                                            savedCampaign.Status = progResultPost["ProgramList"][i]["StateName"].ToString();
                                            savedCampaign.State = Convert.ToInt32(progResultPost["ProgramList"][i]["State"].ToString());
                                            savedCampaign.DateSubmitted = Convert.ToDateTime(progResultPost["ProgramList"][i]["PublishDate"].ToString());
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    MyCampaignsModel myCampaignsModel = new MyCampaignsModel();
                                    myCampaignsModel.CreatorName = progResultPost["ProgramList"][i]["CreatorName"].ToString();
                                    myCampaignsModel.Id = Convert.ToInt32(progResultPost["ProgramList"][i]["ProgramID"].ToString());
                                    myCampaignsModel.CampaignName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                                    myCampaignsModel.Status = progResultPost["ProgramList"][i]["StateName"].ToString();
                                    myCampaignsModel.State = Convert.ToInt32(progResultPost["ProgramList"][i]["State"].ToString());
                                    myCampaignsModel.DateSubmitted = Convert.ToDateTime(progResultPost["ProgramList"][i]["PublishDate"].ToString());
                                    myCampaignsModel.Maml = dbCampaign.CampaignFileURL;
                                    myCampaignsModel.Token = dbCampaign.ApprovalToken;
                                    myCampaignsModel.DbStatus = dbCampaign.Status;
                                    listOfMyCampaignModels.Add(myCampaignsModel);
                                }
                            }
                        }
                        catch (Exception ex)
                        {
                        }
                    }
                }
            }
            return listOfMyCampaignModels.OrderByDescending(x => x.DateSubmitted).ToList();
        }
    }
} 
