﻿using FACT.Domain.Models.MamlModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.ModelBinding;
using System.Xml;

namespace FACT.Web.Business
{
    public class MamlFunctions
    {
        public MamlProgramModel FromMAMLString(string mamlXMl)
        {
            MamlProgramModel result = new MamlProgramModel();
            if (mamlXMl != "")
            {
                XmlDocument campXMLLoad = new XmlDocument();
                campXMLLoad.LoadXml(mamlXMl);
                XmlElement root = campXMLLoad.DocumentElement;
                XmlNodeList campaignElementNodes = root.GetElementsByTagName("CampaignElement");
                XmlNodeList campaignConnectionsNodes = root.GetElementsByTagName("CampaignConnection");
                XmlNodeList campaigns = root.GetElementsByTagName("Campaign");
                XmlElement campaign = (XmlElement)campaigns.Item(0);
                //TODO Deserialize ...
                result.Name = root.GetAttribute("Name");
                result.Version = root.GetAttribute("Version");
                result.dbId = root.GetAttribute("DbId") != "" ? Convert.ToInt32(root.GetAttribute("DbId")) : 0;
                result.CheckOutId = Convert.ToInt32(root.GetAttribute("CheckOutId"));
                result.State = root.GetAttribute("State");
                result.Campaign = new MamlProgramCampaignModel();
                result.Campaign.CampaignElements = new List<MamlProgramCampaignElementModel>();
                result.Campaign.Name = campaign.GetAttribute("Name");
                result.Campaign.Id = campaign.GetAttribute("Id") != "" ? Convert.ToInt32(campaign.GetAttribute("Id")) : 0;
                result.Campaign.DbId = campaign.GetAttribute("DbId") != "" ? Convert.ToInt32(campaign.GetAttribute("DbId")) : 0;
                result.Campaign.CheckOutId = campaign.GetAttribute("CheckOutId") != "" ? Convert.ToInt32(campaign.GetAttribute("CheckOutId")) : 0;
                result.Campaign.State = campaign.GetAttribute("State");
                MamlProgramCampaignElementModel campaignElementModel = null;
                XmlElement campaignElementXml;
                for (int campaignElemntIdx = 0; campaignElemntIdx <= campaignElementNodes.Count - 1; campaignElemntIdx++)
                {
                    campaignElementXml = (XmlElement)campaignElementNodes.Item(campaignElemntIdx);
                    campaignElementModel = new MamlProgramCampaignElementModel();
                    campaignElementModel.Id = campaignElementXml.GetAttribute("Id") != "" ? Convert.ToInt32(campaignElementXml.GetAttribute("Id")) : 0;
                    campaignElementModel.DbId = campaignElementXml.GetAttribute("DbId") != "" ? Convert.ToInt32(campaignElementXml.GetAttribute("DbId")) : 0;
                    campaignElementModel.CheckOutId = campaignElementXml.GetAttribute("CheckOutId") != "" ? Convert.ToInt32(campaignElementXml.GetAttribute("CheckOutId")) : 0;
                    campaignElementModel.Version = campaignElementXml.GetAttribute("Version");
                    campaignElementModel.State = campaignElementXml.GetAttribute("State");
                    campaignElementModel.Type = campaignElementXml.GetAttribute("Type");
                    campaignElementModel.Category = campaignElementXml.GetAttribute("Category");
                    campaignElementModel.Name = campaignElementXml.GetAttribute("Name");
                    campaignElementModel.Position = campaignElementXml.GetAttribute("Position");
                    campaignElementModel.SharedCheckOut = campaignElementXml.GetAttribute("SharedCheckout");
                    campaignElementModel.RuleGroup = campaignElementXml.GetAttribute("RuleGroup");
                    campaignElementModel.Optional = campaignElementXml.GetAttribute("Optional");
                    campaignElementModel.Delete = campaignElementXml.GetAttribute("Delete");
                    campaignElementModel.Readonly = campaignElementXml.GetAttribute("ReadOnly");
                    double itemCost = 0;
                    campaignElementModel.itemCost = Double.TryParse(campaignElementXml.GetAttribute("itemCost"), out itemCost) ? itemCost : 0;
                    XmlElement ScheduleXml;
                    XmlNodeList Schedules;
                    XmlElement PropertiesXml = null;
                    XmlElement ChildXml;
                    try
                    {
                        MamlProgramCampaignElementScheduleModel scheduleModel = null;
                        switch (campaignElementModel.Type)
                        {
                            case "ContactList":
                                XmlElement filterxml = (XmlElement)campaignElementXml.GetElementsByTagName("Filter").Item(0);
                                campaignElementModel.Filter = new MamlFilterModel();
                                campaignElementModel.Filter.CriteriaJoinOperator = filterxml.GetAttribute("CriteriaJoinOperator");
                                campaignElementModel.Filter.Criteria = new List<MamlFilterCriteriaModel>();
                                var FilterCriteriasXml = filterxml.GetElementsByTagName("Criteria");
                                for (int FilterCriteriasIdx = 0; FilterCriteriasIdx <= FilterCriteriasXml.Count - 1; FilterCriteriasIdx++)
                                {
                                    XmlElement FilterCriteriaXml = (XmlElement)FilterCriteriasXml.Item(FilterCriteriasIdx);
                                    MamlFilterCriteriaModel FilterCriteria = new MamlFilterCriteriaModel();
                                    FilterCriteria.Row = FilterCriteriaXml.GetAttribute("Row") != "" ? FilterCriteriaXml.GetAttribute("Row") : "0";
                                    FilterCriteria.Field = FilterCriteriaXml.GetAttribute("Field");
                                    FilterCriteria.Operator = FilterCriteriaXml.GetAttribute("Operator");
                                    FilterCriteria.Value = FilterCriteriaXml.GetAttribute("Value");
                                    campaignElementModel.Filter.Criteria.Add(FilterCriteria);
                                }
                                XmlElement HookedEventsXml = (XmlElement)campaignElementXml.GetElementsByTagName("HookedEvents").Item(0);
                                campaignElementModel.HookedEvents_TriggerFireContext = HookedEventsXml.GetAttribute("TriggerFireContext");
                                break;
                            case "Email":
                                if (campaignElementModel.Schedules == null)
                                {
                                    campaignElementModel.Schedules = new List<MamlProgramCampaignElementScheduleModel>();
                                }
                                Schedules = campaignElementXml.GetElementsByTagName("Schedule");
                                for (int scheduleIdx = 0; scheduleIdx <= Schedules.Count - 1; scheduleIdx++)
                                {
                                    ScheduleXml = (XmlElement)Schedules.Item(scheduleIdx);
                                    scheduleModel = new MamlProgramCampaignElementScheduleModel();
                                    scheduleModel.Id = ScheduleXml.GetAttribute("Id") != "" ? Convert.ToInt32(ScheduleXml.GetAttribute("Id")) : 0;
                                    scheduleModel.DbId = ScheduleXml.GetAttribute("DbId") != "" ? Convert.ToInt32(ScheduleXml.GetAttribute("DbId")) : 0;
                                    scheduleModel.Status = ScheduleXml.GetAttribute("Status");
                                    scheduleModel.TargetAudiences = ScheduleXml.GetAttribute("TargetAudiences");
                                    scheduleModel.Type = ScheduleXml.GetAttribute("Type");
                                    scheduleModel.TimeZone = ScheduleXml.GetAttribute("TimeZone");
                                    scheduleModel.EmailNotification = ScheduleXml.GetAttribute("EmailNotification");
                                    scheduleModel.ApprovalRequired = ScheduleXml.GetAttribute("ApprovalRequired");
                                    scheduleModel.ContactScope = ScheduleXml.GetAttribute("ContactScope");
                                    XmlElement scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Subject").Item(0);
                                    scheduleModel.Subject = scheduleChildXml.InnerText;
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Body").Item(0);
                                    scheduleModel.Body = scheduleChildXml.InnerText;
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Start").Item(0);
                                    scheduleModel.StartDate = scheduleChildXml.GetAttribute("DateTime");
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Recurrence").Item(0);
                                    if (scheduleChildXml != null)
                                    {
                                        XmlElement scheduleChild1Xml = (XmlElement)scheduleChildXml.GetElementsByTagName("Interval").Item(0);
                                        scheduleModel.Recurrence = new MamlProgramCampaignElementScheduleRecurrenceModel();
                                        scheduleModel.Recurrence.Interval_Day = scheduleChild1Xml.GetAttribute("Day");
                                        scheduleModel.Recurrence.Interval_Month = scheduleChild1Xml.GetAttribute("Month");
                                        scheduleModel.Recurrence.RecurrenceString = scheduleChildXml.OuterXml;
                                    }
                                }
                                if (scheduleModel != null)
                                {
                                    campaignElementModel.Schedules.Add(scheduleModel);
                                }
                                XmlElement messageXml = null;
                                XmlNodeList messagesXml = campaignElementXml.GetElementsByTagName("Message");
                                campaignElementModel.Properties = new MamlProgramCampaignElementPropertyModel();
                                campaignElementModel.Properties.Messages = new MamlProgramCampaignElementPropertyMessagesModel();
                                campaignElementModel.Properties.Messages.Message = new MamlProgramCampaignElementPropertyMessageModel();
                                messageXml = (XmlElement)messagesXml.Item(0);
                                campaignElementModel.Properties.Messages.Message.Id = Convert.ToInt32(messageXml.GetAttribute("Id"));
                                ChildXml = (XmlElement)messageXml.GetElementsByTagName("FromName").Item(0);
                                campaignElementModel.Properties.Messages.Message.FromName = ChildXml.InnerText;
                                ChildXml = (XmlElement)messageXml.GetElementsByTagName("ToAddress").Item(0);
                                campaignElementModel.Properties.Messages.Message.ToAddress = ChildXml.InnerText;
                                ChildXml = (XmlElement)messageXml.GetElementsByTagName("Subject").Item(0);
                                campaignElementModel.Properties.Messages.Message.Subject = ChildXml.InnerText;
                                ChildXml = (XmlElement)messageXml.GetElementsByTagName("HtmlContent").Item(0);
                                if (ChildXml.InnerText != "")
                                {
                                    campaignElementModel.Properties.Messages.Message.HtmlContent_URL = ChildXml.InnerText;
                                }
                                else
                                {
                                    int attrCount = ChildXml.Attributes.Count;
                                    bool hasURL = false;
                                    for (int i = 0; i <= attrCount - 1; i++)
                                    {
                                        if (ChildXml.Attributes.Item(i).Name == "URL")
                                        {
                                            hasURL = true;
                                            break;
                                        }
                                    }
                                    campaignElementModel.Properties.Messages.Message.HtmlContent_URL = hasURL ? ChildXml.GetAttribute("URL") : ChildXml.InnerText;
                                }
                                ChildXml = (XmlElement)messageXml.GetElementsByTagName("ReplyTo").Item(0);
                                campaignElementModel.Properties.Messages.Message.ReplyTo = ChildXml.InnerText;
                                ChildXml = (XmlElement)messageXml.GetElementsByTagName("FromAddress").Item(0);
                                campaignElementModel.Properties.Messages.Message.FromAddress = ChildXml.InnerText;
                                ChildXml = (XmlElement)messageXml.GetElementsByTagName("TextContent").Item(0);
                                campaignElementModel.Properties.Messages.Message.TextContent = ChildXml.InnerText;
                                break;
                            case "DirectMail":
                                if (campaignElementModel.Schedules == null)
                                {
                                    campaignElementModel.Schedules = new List<MamlProgramCampaignElementScheduleModel>();
                                }
                                Schedules = campaignElementXml.GetElementsByTagName("Schedule");
                                for (int scheduleIdx = 0; scheduleIdx <= Schedules.Count - 1; scheduleIdx++)
                                {
                                    ScheduleXml = (XmlElement)Schedules.Item(scheduleIdx);
                                    scheduleModel = new MamlProgramCampaignElementScheduleModel();
                                    scheduleModel.Id = ScheduleXml.GetAttribute("Id") != "" ? Convert.ToInt32(ScheduleXml.GetAttribute("Id")) : 0;
                                    scheduleModel.DbId = ScheduleXml.GetAttribute("DbId") != "" ? Convert.ToInt32(ScheduleXml.GetAttribute("DbId")) : 0;
                                    scheduleModel.Status = ScheduleXml.GetAttribute("Status");
                                    scheduleModel.TargetAudiences = ScheduleXml.GetAttribute("TargetAudiences");
                                    scheduleModel.Type = ScheduleXml.GetAttribute("Type");
                                    scheduleModel.TimeZone = ScheduleXml.GetAttribute("TimeZone");
                                    scheduleModel.EmailNotification = ScheduleXml.GetAttribute("EmailNotification");
                                    scheduleModel.ApprovalRequired = ScheduleXml.GetAttribute("ApprovalRequired");
                                    scheduleModel.ContactScope = ScheduleXml.GetAttribute("ContactScope");
                                    XmlElement scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Subject").Item(0);
                                    scheduleModel.Subject = scheduleChildXml.InnerText;
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Body").Item(0);
                                    scheduleModel.Body = scheduleChildXml.InnerText;
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Start").Item(0);
                                    scheduleModel.StartDate = scheduleChildXml.GetAttribute("DateTime");
                                    if (ScheduleXml.GetElementsByTagName("Recurrence") != null && ScheduleXml.GetElementsByTagName("Recurrence").Count > 0)
                                    {
                                        scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Recurrence").Item(0);
                                        if (scheduleChildXml.GetElementsByTagName("Interval") != null && scheduleChildXml.GetElementsByTagName("Interval").Count > 0)
                                        {
                                            scheduleChildXml = (XmlElement)scheduleChildXml.GetElementsByTagName("Interval").Item(0);
                                            scheduleModel.Recurrence = new MamlProgramCampaignElementScheduleRecurrenceModel();
                                            scheduleModel.Recurrence.Interval_Day = scheduleChildXml.GetAttribute("Day");
                                            scheduleModel.Recurrence.Interval_Month = scheduleChildXml.GetAttribute("Month");
                                        }
                                    }
                                }
                                if (scheduleModel != null)
                                {
                                    campaignElementModel.Schedules.Add(scheduleModel);
                                }
                                messagesXml = campaignElementXml.GetElementsByTagName("Message");
                                campaignElementModel.Properties = new MamlProgramCampaignElementPropertyModel();
                                campaignElementModel.Properties.Messages = new MamlProgramCampaignElementPropertyMessagesModel();
                                campaignElementModel.Properties.Messages.Message = new MamlProgramCampaignElementPropertyMessageModel();
                                messageXml = (XmlElement)messagesXml.Item(0);
                                campaignElementModel.Properties.Messages.Message.Type = messageXml.GetAttribute("Type");
                                campaignElementModel.Properties.Messages.Message.Encoding = messageXml.GetAttribute("Encoding");
                                XmlNodeList ColumnsXml = campaignElementXml.GetElementsByTagName("Column");
                                if (ColumnsXml != null)
                                {
                                    for (int ColumnIdx = 0; ColumnIdx <= ColumnsXml.Count - 1; ColumnIdx++)
                                    {
                                        if (campaignElementModel.Properties.Messages.Message.SelectedColumns == null)
                                        {
                                            campaignElementModel.Properties.Messages.Message.SelectedColumns = new List<KeyValuePair<String, String>>();
                                        }
                                        XmlElement ColumnXml = (XmlElement)ColumnsXml.Item(ColumnIdx);
                                        string dbgAlias = ColumnXml.GetAttribute("Alias");
                                        string dngInnerText = ColumnXml.InnerText;
                                        KeyValuePair<string, string> selectedColumn = new KeyValuePair<String, String>(dbgAlias, dngInnerText);
                                        campaignElementModel.Properties.Messages.Message.SelectedColumns.Add(selectedColumn);
                                    }
                                }
                                campaignElementModel.Properties.Messages.EmailNotification = new MamlProgramCampaignElementPropertyEmailNotificationeModel();
                                XmlElement EmailNotificationXml = (XmlElement)campaignElementXml.GetElementsByTagName("EmailNotification").Item(0);
                                ChildXml = (XmlElement)EmailNotificationXml.GetElementsByTagName("FromDisplayName").Item(0);
                                campaignElementModel.Properties.Messages.EmailNotification.FromDisplayName = ChildXml.InnerText;
                                ChildXml = (XmlElement)EmailNotificationXml.GetElementsByTagName("ToAddress").Item(0);
                                campaignElementModel.Properties.Messages.EmailNotification.ToAddress = ChildXml.InnerText;
                                ChildXml = (XmlElement)EmailNotificationXml.GetElementsByTagName("Subject").Item(0);
                                campaignElementModel.Properties.Messages.EmailNotification.Subject = ChildXml.InnerText;
                                ChildXml = (XmlElement)EmailNotificationXml.GetElementsByTagName("Body").Item(0);
                                campaignElementModel.Properties.Messages.EmailNotification.Body = ChildXml.InnerText;
                                break;
                            case "OutboundApplication":
                                if (campaignElementModel.Schedules == null)
                                {
                                    campaignElementModel.Schedules = new List<MamlProgramCampaignElementScheduleModel>();
                                }
                                Schedules = campaignElementXml.GetElementsByTagName("Schedule");
                                for (int scheduleIdx = 0; scheduleIdx <= Schedules.Count - 1; scheduleIdx++)
                                {
                                    ScheduleXml = (XmlElement)Schedules.Item(scheduleIdx);
                                    scheduleModel = new MamlProgramCampaignElementScheduleModel();
                                    scheduleModel.Id = ScheduleXml.GetAttribute("Id") != "" ? Convert.ToInt32(ScheduleXml.GetAttribute("Id")) : 0;
                                    scheduleModel.DbId = ScheduleXml.GetAttribute("DbId") != "" ? Convert.ToInt32(ScheduleXml.GetAttribute("DbId")) : 0;
                                    scheduleModel.Status = ScheduleXml.GetAttribute("Status");
                                    scheduleModel.Type = ScheduleXml.GetAttribute("Type");
                                    scheduleModel.TimeZone = ScheduleXml.GetAttribute("TimeZone");
                                    scheduleModel.EmailNotification = ScheduleXml.GetAttribute("EmailNotification");
                                    scheduleModel.ApprovalRequired = ScheduleXml.GetAttribute("ApprovalRequired");
                                    XmlElement scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Subject").Item(0);
                                    scheduleModel.Subject = scheduleChildXml.InnerText;
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Body").Item(0);
                                    scheduleModel.Body = scheduleChildXml.InnerText;
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Start").Item(0);
                                    scheduleModel.StartDate = scheduleChildXml.GetAttribute("DateTime");
                                }
                                if (scheduleModel != null)
                                {
                                    campaignElementModel.Schedules.Add(scheduleModel);
                                }
                                campaignElementModel.Properties = new MamlProgramCampaignElementPropertyModel();
                                campaignElementModel.Properties.Configs = new List<KeyValuePair<String, String>>();
                                PropertiesXml = (XmlElement)campaignElementXml.GetElementsByTagName("Properties").Item(0);
                                XmlNodeList ConfigsXml = campaignElementXml.GetElementsByTagName("Config");
                                campaignElementModel.Properties.ServiceTypeID = PropertiesXml.GetAttribute("ServiceTypeID");
                                campaignElementModel.Properties.ApplicationVersion = PropertiesXml.GetAttribute("ApplicationVersion");
                                for (int ConfigIdx = 0; ConfigIdx <= ConfigsXml.Count - 1; ConfigIdx++)
                                {
                                    XmlElement ConfigXml = (XmlElement)ConfigsXml.Item(ConfigIdx);
                                    campaignElementModel.Properties.Configs.Add(new KeyValuePair<String, String>(ConfigXml.GetAttribute("Key"), ConfigXml.InnerText));
                                }
                                break;
                            case "Microsite":
                                if (campaignElementModel.Schedules == null)
                                {
                                    campaignElementModel.Schedules = new List<MamlProgramCampaignElementScheduleModel>();
                                }
                                Schedules = campaignElementXml.GetElementsByTagName("Schedule");
                                for (int scheduleIdx = 0; scheduleIdx <= Schedules.Count - 1; scheduleIdx++)
                                {
                                    ScheduleXml = (XmlElement)Schedules.Item(scheduleIdx);
                                    scheduleModel = new MamlProgramCampaignElementScheduleModel();
                                    scheduleModel.Id = ScheduleXml.GetAttribute("Id") != "" ? Convert.ToInt32(ScheduleXml.GetAttribute("Id")) : 0;
                                    scheduleModel.DbId = ScheduleXml.GetAttribute("DbId") != "" ? Convert.ToInt32(ScheduleXml.GetAttribute("DbId")) : 0;
                                    scheduleModel.Status = ScheduleXml.GetAttribute("Status");
                                    scheduleModel.Type = ScheduleXml.GetAttribute("Type");
                                    scheduleModel.TimeZone = ScheduleXml.GetAttribute("TimeZone");
                                    scheduleModel.EmailNotification = ScheduleXml.GetAttribute("EmailNotification");
                                    scheduleModel.ApprovalRequired = ScheduleXml.GetAttribute("ApprovalRequired");
                                    XmlElement scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Subject").Item(0);
                                    scheduleModel.Subject = scheduleChildXml.InnerText;
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Body").Item(0);
                                    scheduleModel.Body = scheduleChildXml.InnerText;
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("Start").Item(0);
                                    scheduleModel.StartDate = scheduleChildXml.GetAttribute("DateTime");
                                    scheduleChildXml = (XmlElement)ScheduleXml.GetElementsByTagName("End").Item(0);
                                    scheduleModel.EndDate = scheduleChildXml.GetAttribute("DateTime");
                                }
                                if (scheduleModel != null)
                                {
                                    campaignElementModel.Schedules.Add(scheduleModel);
                                }
                                //TODO CallbackTouchPoints
                                campaignElementModel.CallbackTouchPoints = new List<MamlProgramCampaignElementCallBackTuchPointModel>();
                                XmlNodeList CallbackTouchPointsXml = campaignElementXml.GetElementsByTagName("CallbackTouchPoint");
                                for (int CallbackTouchPointIdx = 0; CallbackTouchPointIdx <= CallbackTouchPointsXml.Count - 1; CallbackTouchPointIdx++)
                                {
                                    XmlElement CallbackTouchPointXml = (XmlElement)CallbackTouchPointsXml.Item(CallbackTouchPointIdx);
                                    MamlProgramCampaignElementCallBackTuchPointModel CallbackTouchPoint = new MamlProgramCampaignElementCallBackTuchPointModel();
                                    CallbackTouchPoint.Id = CallbackTouchPointXml.GetAttribute("Id") != "" ? Convert.ToInt32(CallbackTouchPointXml.GetAttribute("Id")) : 0;
                                    CallbackTouchPoint.DbId = CallbackTouchPointXml.GetAttribute("DbId") != "" ? Convert.ToInt32(CallbackTouchPointXml.GetAttribute("DbId")) : 0;
                                    CallbackTouchPoint.Callback = CallbackTouchPointXml.GetAttribute("Callback");
                                    campaignElementModel.CallbackTouchPoints.Add(CallbackTouchPoint);
                                }
                                //TODO ChildElements
                                campaignElementModel.ChildElements = new List<MamlProgramCampaignElementChildElementModel>();
                                XmlNodeList ChildElemntsXml = campaignElementXml.GetElementsByTagName("ChildElement");
                                for (int ChildElementIdx = 0; ChildElementIdx <= ChildElemntsXml.Count - 1; ChildElementIdx++)
                                {
                                    XmlElement ChildElementXml = (XmlElement)ChildElemntsXml.Item(ChildElementIdx);
                                    MamlProgramCampaignElementChildElementModel ChildElement = new MamlProgramCampaignElementChildElementModel();
                                    ChildElement.Id = ChildElementXml.GetAttribute("Id") != "" ? Convert.ToInt32(ChildElementXml.GetAttribute("Id")) : 0;
                                    ChildElement.DbId = ChildElementXml.GetAttribute("DbId") != "" ? Convert.ToInt32(ChildElementXml.GetAttribute("DbId")) : 0;
                                    ChildElement.Type = ChildElementXml.GetAttribute("Type");
                                    ChildElement.IsVersion = ChildElementXml.GetAttribute("IsVersion");
                                    ChildElement.Name = ChildElementXml.GetAttribute("Name");
                                    ChildElement.Position = ChildElementXml.GetAttribute("Position");
                                    if (ChildElementXml.GetAttribute("RuleGroup") != null)
                                    {
                                        ChildElement.RuleGroup = ChildElementXml.GetAttribute("RuleGroup");
                                    }
                                    XmlElement ChildElementPropertyXml = (XmlElement)ChildElementXml.GetElementsByTagName("Properties").Item(0);
                                    ChildElement.Properties = new MamlProgramCampaignElementChildElementPropertyModel(); ;
                                    ChildElement.Properties.PageType = ChildElementPropertyXml.GetAttribute("PageType");
                                    ChildElement.Properties.IsHomePage = ChildElementPropertyXml.GetAttribute("IsHomePage");
                                    ChildElement.Properties.SkipPage = ChildElementPropertyXml.GetAttribute("SkipPage");
                                    ChildElement.Properties.IsLoginRequired = ChildElementPropertyXml.GetAttribute("IsLoginRequired");
                                    XmlElement htmlElement = (XmlElement)ChildElementPropertyXml.GetElementsByTagName("HtmlContent").Item(0);
                                    string lUrl = htmlElement.GetAttribute("URL");
                                    if (lUrl == "")
                                    {
                                        lUrl = htmlElement.InnerText;
                                    }
                                    ChildElement.Properties.HtmlContent = lUrl;
                                    ChildElement.Flow = new MamlProgramCampaignElementChildElementFlowModel();
                                    ChildElement.Flow.FlowNextElements = new List<MamlProgramCampaignElementChildElementFlowNextElementModel>();
                                    XmlElement ChildElementFlowXml = (XmlElement)ChildElementXml.GetElementsByTagName("Flow").Item(0);
                                    ChildElement.Flow.DefaultNextChildId = ChildElementFlowXml.GetAttribute("DefaultNextChildId");
                                    ChildElement.Flow.DefaultNextChildName = ChildElementFlowXml.GetAttribute("DefaultNextChildName");
                                    XmlNodeList FlowNextElementsXml = ChildElementFlowXml.GetElementsByTagName("FlowNextElement");
                                    for (int FlowNextElementIdx = 0; FlowNextElementIdx <= FlowNextElementsXml.Count - 1; FlowNextElementIdx++)
                                    {
                                        XmlElement FlowNextElementXml = (XmlElement)FlowNextElementsXml.Item(FlowNextElementIdx);
                                        MamlProgramCampaignElementChildElementFlowNextElementModel FlowNextElement = new MamlProgramCampaignElementChildElementFlowNextElementModel();
                                        FlowNextElement.Id = FlowNextElementXml.GetAttribute("Id") != "" ? Convert.ToInt32(FlowNextElementXml.GetAttribute("Id")) : 0;
                                        FlowNextElement.Name = FlowNextElementXml.GetAttribute("Name");
                                        XmlElement FlowNextElementFiltersXml = (XmlElement)FlowNextElementXml.GetElementsByTagName("Filter").Item(0);
                                        FlowNextElement.Filter = new MamlFilterModel();
                                        FlowNextElement.Filter.CriteriaJoinOperator = FlowNextElementFiltersXml.GetAttribute("CriteriaJoinOperator");
                                        FlowNextElement.Filter.Criteria = new List<MamlFilterCriteriaModel>();
                                        XmlNodeList FlowNextElementFilterCriteriasXml = FlowNextElementFiltersXml.GetElementsByTagName("Criteria");
                                        for (int FlowNextElementFilterCriteriasIdx = 0; FlowNextElementFilterCriteriasIdx <= FlowNextElementFilterCriteriasXml.Count - 1;
                                            FlowNextElementFilterCriteriasIdx++)
                                        {
                                            XmlElement FlowNextElementFilterCriteriaXml = (XmlElement)FlowNextElementFilterCriteriasXml.Item(FlowNextElementFilterCriteriasIdx);
                                            MamlFilterCriteriaModel FlowNextElementFilterCriteria = new MamlFilterCriteriaModel();
                                            FlowNextElementFilterCriteria.Row = FlowNextElementFilterCriteriaXml.GetAttribute("Row") != "" ?
                                                FlowNextElementFilterCriteriaXml.GetAttribute("Row") : "0";
                                            FlowNextElementFilterCriteria.Field = FlowNextElementFilterCriteriaXml.GetAttribute("Field");
                                            FlowNextElementFilterCriteria.Operator = FlowNextElementFilterCriteriaXml.GetAttribute("Operator");
                                            FlowNextElementFilterCriteria.Value = FlowNextElementFilterCriteriaXml.GetAttribute("Value");
                                            FlowNextElement.Filter.Criteria.Add(FlowNextElementFilterCriteria);
                                        }
                                        ChildElement.Flow.FlowNextElements.Add(FlowNextElement);
                                    }
                                    campaignElementModel.ChildElements.Add(ChildElement);
                                }
                                //TODO ChildConnections
                                campaignElementModel.ChildConnections = new List<MamlProgramCampaignElementChildConnectionModel>();
                                XmlNodeList ChildConnectionsXml = campaignElementXml.GetElementsByTagName("ChildConnection");
                                for (int ChildConnectionIdx = 0; ChildConnectionIdx <= ChildConnectionsXml.Count - 1; ChildConnectionIdx++)
                                {
                                    XmlElement ChildConnectionXml = (XmlElement)ChildConnectionsXml.Item(ChildConnectionIdx);
                                    MamlProgramCampaignElementChildConnectionModel ChildConnection = new MamlProgramCampaignElementChildConnectionModel();
                                    ChildConnection.StartElementId = ChildConnectionXml.GetAttribute("StartElementId") != "" ?
                                        Convert.ToInt32(ChildConnectionXml.GetAttribute("StartElementId")) : 0;
                                    ChildConnection.EndElementId = ChildConnectionXml.GetAttribute("EndElementId") != "" ?
                                        Convert.ToInt32(ChildConnectionXml.GetAttribute("EndElementId")) : 0;
                                    campaignElementModel.ChildConnections.Add(ChildConnection);
                                }
                                //TODO Properties
                                if (campaignElementXml.HasChildNodes)
                                {
                                    for (int i = 0; i <= campaignElementXml.ChildNodes.Count - 1; i++)
                                    {
                                        XmlElement nodeXml = (XmlElement)campaignElementXml.ChildNodes.Item(i);
                                        if (nodeXml.Name == "Properties")
                                        {
                                            PropertiesXml = nodeXml;
                                        }
                                    }
                                }
                                campaignElementModel.Properties = new MamlProgramCampaignElementPropertyModel();
                                campaignElementModel.Properties.BaseUrlCollection = new List<MamlProgramCampaignElementPropertyBaseUrlModel>();
                                XmlNodeList BaseUrlCollectionXml = PropertiesXml.GetElementsByTagName("BaseUrl");
                                for (int baseUrlIdx = 0; baseUrlIdx <= BaseUrlCollectionXml.Count - 1; baseUrlIdx++)
                                {
                                    XmlElement BaseUrlXml = (XmlElement)BaseUrlCollectionXml.Item(baseUrlIdx);
                                    MamlProgramCampaignElementPropertyBaseUrlModel BaseUrl = new MamlProgramCampaignElementPropertyBaseUrlModel();
                                    BaseUrl.Id = BaseUrlXml.GetAttribute("Id") != "" ? Convert.ToInt32(BaseUrlXml.GetAttribute("Id")) : 0;
                                    BaseUrl.DbId = BaseUrlXml.GetAttribute("DbId") != "" ? Convert.ToInt32(BaseUrlXml.GetAttribute("DbId")) : 0;
                                    BaseUrl.Outbound_Id = BaseUrlXml.GetAttribute("Outbound_Id") != "" ? Convert.ToInt32(BaseUrlXml.GetAttribute("Outbound_Id")) : 0;
                                    BaseUrl.Outbound_DbId = BaseUrlXml.GetAttribute("Outbound_DbId") != "" ? Convert.ToInt32(BaseUrlXml.GetAttribute("Outbound_DbId")) : 0;
                                    BaseUrl.Parent_Id = BaseUrlXml.GetAttribute("Parent_Id") != "" ? Convert.ToInt32(BaseUrlXml.GetAttribute("Parent_Id")) : 0;
                                    BaseUrl.URL = BaseUrlXml.GetAttribute("URL");
                                    BaseUrl.Extension = BaseUrlXml.GetAttribute("Extension");
                                    BaseUrl.PurlPosition = BaseUrlXml.GetAttribute("PurlPosition");
                                    BaseUrl.Domain = BaseUrlXml.GetAttribute("Domain");
                                    BaseUrl.SSL = BaseUrlXml.GetAttribute("SSL");
                                    BaseUrl.SearchEngineAllowed = BaseUrlXml.GetAttribute("SearchEngineAllowed");
                                    campaignElementModel.Properties.BaseUrlCollection.Add(BaseUrl);
                                }
                                campaignElementModel.Properties.Settings = new MamlProgramCampaignElementPropertySettingsModel();
                                XmlElement SettingsXml = (XmlElement)PropertiesXml.GetElementsByTagName("Settings").Item(0);
                                campaignElementModel.Properties.Settings.SessionTimeout = SettingsXml.GetAttribute("SessionTimeout");
                                XmlElement InvalidPurlXml = (XmlElement)SettingsXml.GetElementsByTagName("InvalidPurl").Item(0);
                                XmlElement PageNotFoundXml = (XmlElement)SettingsXml.GetElementsByTagName("PageNotFound").Item(0);
                                XmlElement ErrorXml = (XmlElement)SettingsXml.GetElementsByTagName("Error").Item(0);
                                campaignElementModel.Properties.Settings.InvalidPurl = InvalidPurlXml.InnerText;
                                campaignElementModel.Properties.Settings.PageNotFound = PageNotFoundXml.InnerText;
                                campaignElementModel.Properties.Settings.Error = ErrorXml.InnerText;
                                break;
                            default:
                                break;
                        }
                    }
                    catch (Exception ex)
                    {
                        //TODO add code for exception handling
                        //modelState.AddModelError(String.Empty, ex);
                    }
                }
                //TODO RuleGroupSet 
                if (campaignElementModel != null)
                {
                    result.Campaign.CampaignElements.Add(campaignElementModel);
                }
                //TODO CampaignConnections 
                result.Campaign.CampaignConnections = new List<MamlProgramCampaignConnectionModel>();
                for (int campaignConnectionIdx = 0; campaignConnectionIdx <= campaignConnectionsNodes.Count - 1; campaignConnectionIdx++)
                {
                    XmlElement ccXml = (XmlElement)campaignConnectionsNodes.Item(campaignConnectionIdx);
                    var ccObj = new MamlProgramCampaignConnectionModel();
                    ccObj.Id = campaignConnectionIdx;
                    ccObj.StartElementId = ccXml.GetAttribute("StartElementId") != "" ? Convert.ToInt32(ccXml.GetAttribute("StartElementId")) : 0;
                    ccObj.EndElementId = ccXml.GetAttribute("EndElementId") != "" ? Convert.ToInt32(ccXml.GetAttribute("EndElementId")) : 0;
                    result.Campaign.CampaignConnections.Add(ccObj);
                }
                //TODO RuleGroupSet 
                result.RuleGroupSet = new List<MamlProgramRuleGroupModel>();
                XmlNodeList ruleGroups = root.GetElementsByTagName("RuleGroup");
                for (int rgIdx = 0; rgIdx <= ruleGroups.Count - 1; rgIdx++)
                {
                    XmlElement rgXml = (XmlElement)ruleGroups.Item(rgIdx);
                    var rgObj = new MamlProgramRuleGroupModel();
                    rgObj.ContentVariables = new List<String>();
                    rgObj.Name = rgXml.GetAttribute("Name");
                    rgObj.Description = rgXml.GetAttribute("Description");
                    XmlNodeList cv = rgXml.GetElementsByTagName("ContentVariable");
                    if (cv == null)
                    {
                        for (int cvIdx = 0; cvIdx <= cv.Count - 1; cvIdx++)
                        {
                            XmlElement cvObj = (XmlElement)cv.Item(cvIdx);
                            rgObj.ContentVariables.Add(cvObj.GetAttribute("Name"));
                        }
                    }
                    result.RuleGroupSet.Add(rgObj);
                }
            }
            return result;
        }
    }
}