﻿using FACT.Domain.Models;
using FACT.Domain.Models.EntityModels;
using FACT.Service;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace FACT.Web.Business
{
    public class CRMFunctions : IDisposable
    {
        private GlobalX objConnection = new GlobalX();
        private bool disposed = false;

        public string getSFAuthToken(string strCode, string strRedirect, Profiles Profile, IFactMainService _factMainService)
        {
            if (string.IsNullOrEmpty(strRedirect))
            {
                strRedirect = "https://campaigns.lplmod.com/SalesforceImport";
            }
            Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://login.salesforce.com/services/oauth2/token?grant_type=authorization_code&client_id=3MVG9A2kN3Bn17hsIqu9MmPk5RFxwdxCzuowa9ofrbVhZmzWu3HGUbPY7QAwIZftKFRkqNTalQlt_UHV2clTd&client_secret=1697002824243212611" +
                "&code=" + strCode + "&redirect_uri=" + strRedirect, null, "text/json", "POST", "", "Bearer");
            if (tmp.GetType() == typeof(MessageError))
            {
                MessageError msg = new MessageError();
                msg = (MessageError)tmp;
                return msg.ErrorText;
            }
            else
            {
                var sfAuth = JObject.FromObject(tmp);
                if (Profile != null)
                {
                    Profile.SalesforceAuthToken = sfAuth["access_token"].ToString();
                    Profile.SalesforceRefreshToken = sfAuth["refresh_token"].ToString();
                    _factMainService.UpdateProfiles(Profile);
                }
                HttpContext.Current.Response.Redirect(strRedirect, false);
                return null;
            }
        }

        public string getVRAuthToken(string strCode, string strRedirect, Profiles Profile)
        {
            if (string.IsNullOrEmpty(strRedirect))
            {
                strRedirect = "https://campaigns.lplmod.com/Profile.aspx";
            }
            if (Profile == null)
            {
            }
            Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://vrapi.verticalresponse.com/api/v1/oauth/access_token?client_id=" + ConfigurationManager.AppSettings["VRClientId"] +
                "&client_secret=" + ConfigurationManager.AppSettings["VRClientSecret"] + "&code=" + strCode + "&redirect_uri=" + strRedirect, null, "text/json", "POST", "", "Bearer");
            if (tmp.GetType() == typeof(MessageError))
            {
                MessageError msg = new MessageError();
                msg = (MessageError)tmp;
                return msg.ErrorText;
            }
            else
            {
                HttpContext.Current.Response.Redirect(strRedirect);
                return "";
            }
        }

        public string UpdateRedtailLogin(string strAPIKey, string strUsername, string strPassword)
        {
            try
            {
                string rtCreds = strAPIKey + ":" + strUsername + ":" + strPassword;
                Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("http://api2.redtailtechnology.com/crm/v1/rest/authentication", null, "text/json", "GET", rtCreds, "Basic");
                if (tmp.GetType() == typeof(MessageError))
                {
                    MessageError msg = new MessageError();
                    msg = (MessageError)tmp;
                    return msg.ErrorText;
                }
                else
                {
                    HttpContext.Current.Response.Redirect("Profile");
                    return "";
                }
            }
            catch (Exception)
            {
                return "Invalid Redtail Login";
            }
        }

        public string getCCAuthToken(string strCode, string strUsername)
        {
            if (string.IsNullOrEmpty(strUsername))
            {
            }
            try
            {
                Object tmp = GlobalFunctions.ProcessJSONHTTPRequest("https://oauth2.constantcontact.com/oauth2/oauth/token?grant_type=authorization_code&client_id=y9h9z2pzpe54j526bf6ckx67&client_secret=xvXKuXCjXrP3H7HxmXPEhhby&code=" +
                    strCode + "&redirect_uri=https://campaigns.lplmod.com/Profile.aspx", null, "text/json", "POST", "", "Bearer");
                if (tmp.GetType() == typeof(MessageError))
                {
                    MessageError msg = new MessageError();
                    msg = (MessageError)tmp;
                    return msg.ErrorText;
                }
                else
                {
                    HttpContext.Current.Response.Redirect("Profile");
                    return "";
                }
            }
            catch (WebException ex)
            {
                return ex.Message;
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
        }
    }
}