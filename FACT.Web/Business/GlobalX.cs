﻿using FACT.Domain.Models;
using FACT.Domain.Models.EntityModels;
using FACT.Service;
using FACT.Web.Controllers;
using FACT.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Cryptography;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;
using System.Xml;

namespace FACT.Web.Business
{
    public class GlobalX : IDisposable
    {
        private bool disposed = false;

        public GlobalX()
        {
            this.disposed = false;
        }

        public string PutHTTPRequestSync(string uri, string requestBody, string contentType, Dictionary<string, string> headers)
        {
            if (headers == null)
            {
            }
            Uri xURI = new Uri(uri);
            try
            {
                ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls11;
                var client = new RestClient(xURI);
                var request = new RestRequest("", Method.PUT);
                //SetupRequestHeader(request, contentType, headers, authType)
                request.RequestFormat = contentType.ToUpper().IndexOf("XML") != 0 ? DataFormat.Xml : DataFormat.Json;
                if (requestBody != "")
                {
                    request.AddParameter(contentType, requestBody, ParameterType.RequestBody);
                }
                var response = client.Execute(request);
                JsonSerializerSettings jcSettings = new JsonSerializerSettings();
                jcSettings.NullValueHandling = NullValueHandling.Ignore;
                jcSettings.MissingMemberHandling = MissingMemberHandling.Ignore;
                return JsonConvert.DeserializeObject(response.Content, jcSettings).ToString();
            }
            catch (Exception ex)
            {
                GlobalFunctions.showPrompt(ex.Message, true);
                return null;
            }
        }

        public string setActiveLink()
        {
            var strPath = HttpContext.Current.Request.Url.AbsolutePath;
            return strPath;
        }

        public Byte[] FromBase64(string base64)
        {
            if (base64 == null)
            {
                throw new ArgumentNullException("base64");
            }
            return Convert.FromBase64String(base64);
        }

        public string cleanString(string strValue)
        {
            string strreturnValue = " ";
            if (strValue != "")
            {
                strreturnValue = strValue.Replace(",", "").Replace("\r", "").Replace("\n", "");
            }
            return strreturnValue;
        }

        public string CheckNull(string fieldValue)
        {
            if (fieldValue.Equals(DBNull.Value))
            {
                return "";
            }
            else
            {
                if (fieldValue == "N/A")
                {
                    return "value for N/A";
                }
                else
                {
                    return fieldValue;
                }
            }
        }

        public int lineCount(string filePATH_fileNAME, bool hasHeader)
        {
            int myLineCount = 0;
            StreamReader sr = new StreamReader(filePATH_fileNAME);
            while (true)
            {
                string line;
                if ((line = sr.ReadLine()) != null)
                {
                    myLineCount += 1;
                }
                else
                {
                    break;
                }
            }
            sr.Dispose();
            if (hasHeader)
            {
                myLineCount = myLineCount - 1;
            }
            return myLineCount;
        }

        public string GenerateHash(string SourceText)
        {
            //Create an encoding object to ensure the encoding standard for the source text
            UnicodeEncoding Ue = new UnicodeEncoding();
            //Retrieve a byte array based on the source text
            var ByteSourceText = Ue.GetBytes(SourceText);
            //Instantiate an MD5 Provider object
            MD5CryptoServiceProvider Md5 = new MD5CryptoServiceProvider();
            //Compute the hash value from the source
            var ByteHash = Md5.ComputeHash(ByteSourceText);
            //And convert it to String format for return
            return Convert.ToBase64String(ByteHash);
        }

        public bool IsEmailValid(string email)
        {
            RegexUtilities validateEmail = new RegexUtilities();
            return validateEmail.IsValidEmail(email);
        }

        public bool checkProperty(JObject objectt, string propertyName)
        {
            if (objectt != null)
            {
                return objectt[propertyName] != null;
            }
            else
            {
                return false;
            }
        }

        public int CheckDomain(string email, IFactMainService _factMainService)
        {
            string pattern = @"^[a-zA-Z][\w\.-]*[a-zA-Z0-9]@[a-zA-Z0-9][\w\.-]*[a-zA-Z0-9]\.[a-zA-Z][a-zA-Z\.]*[a-zA-Z]$";
            Match emailAddressMatch = Regex.Match(email, pattern);
            if (emailAddressMatch.Success)
            {
                List<EmailDomains> emailDomains = _factMainService.FindAllEmailDomains().ToList();
                foreach (var item in emailDomains)
                {
                    if ((item.Domain == email.Split('@')[1]) && (item.isBlacklisted != true))
                    {
                        return 2;
                    }
                    else if ((item.Domain == email.Split('@')[1]) && (item.isBlacklisted = true))
                    {
                        return 0;
                    }
                }
                return 1;
            }
            else
            {
                return 0;
            }
        }

        public async Task<SendGrid.Response> SendMail(string emailFrom, string emailFromName, string subject, bool templateExists, string templatePath, string emailTo,
            Object parameters)
        {
            SendGridService sendgridService = new SendGridService();
            sendgridService.SetSender(emailFrom, emailFromName);
            sendgridService.SetRecipient(emailTo);
            sendgridService.SetContent(templateExists, templatePath, parameters, subject);
            return await sendgridService.Send();
        }

        public Object GetPropertyValue(Object obj, string PropName)
        {
            Type objType = obj.GetType();
            PropertyInfo pInfo = objType.GetProperty(PropName);
            Object PropValue = pInfo.GetValue(obj, System.Reflection.BindingFlags.GetProperty, null, null, null);
            return PropValue;
        }

        public void ShowError(string title, string message, string type, string returnUrl, IFactMainService _factMainService, bool logError = false)
        {
            ErrorModelsController errorController = new ErrorModelsController(_factMainService);
            errorController.ShowError(title, message, type, returnUrl, logError);
        }

        public string firstLetterToCapital(string val)
        {
            if (!string.IsNullOrEmpty(val))
            {
                return val.Substring(0, 1).ToUpper() + val.Substring(1, val.Length - 1).ToLower();
            }
            else
            {
                return "";
            }
        }

        public string camelCase(string val)
        {
            if (!string.IsNullOrEmpty(val))
            {
                string[] res = val.Split(new Char[] {});
                for (int i = 0; i <= res.Count() - 1; i++)
                {
                    res[i] = firstLetterToCapital(res[i]);
                }
                return String.Join(" ", res);
            }
            else
            {
                return "";
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
        }
    }
}