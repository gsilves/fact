﻿using AuthorizeNet.Api.Contracts.V1;
using FACT.Domain.Models;
using FACT.Domain.Models.Controllers;
using FACT.Domain.Models.EntityModels;
using FACT.Domain.Models.MiscModels;
using FACT.Service;
using FACT.Web.Models;
using FACT.Web.Services;
using Newtonsoft.Json;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Web;
using System.Web.Mvc;
using System.Web.UI.WebControls;
using System.Xml;
using System.Xml.Linq;

namespace FACT.Web.Business
{
    public static class ControllerFunctions
    {
        private static AuthorizeNetService aNetApi = new AuthorizeNetService();
        private static int intCSVLimit = 500;
        private static int intContactLimit = 500;

        public static List<MyCampaignsModel> GetMyCampaigns(Profiles advisor, IFactMainService _factMainService)
        {
            List<MyCampaignsModel> listOfMyCampaignModels = new List<MyCampaignsModel>();
            AmazonS3Service awsService = new AmazonS3Service();
            MamlFunctions mamlController = new MamlFunctions();
            List<Campaigns> savedCampaigns = _factMainService.FindAllCampaigns().Where(p => p.UserId == advisor.UserId && p.Status == "Saved").ToList();
            foreach (var s in savedCampaigns)
            {
                AmazonResponse ar = awsService.DownloadMaml(s.CampaignFileURL);
                if (ar.Successful)
                {
                    string mamlXML = ar.Message;
                    MyCampaignsModel myCampaignsModel = new MyCampaignsModel();
                    myCampaignsModel.Status = "";
                    myCampaignsModel.DbStatus = s.Status;
                    myCampaignsModel.CampaignName = s.CampaignName;
                    myCampaignsModel.DateSubmitted = s.DateSubmitted;
                    myCampaignsModel.Maml = mamlXML;
                    myCampaignsModel.Program = mamlController.FromMAMLString(mamlXML);
                    myCampaignsModel.CreatorName = advisor.FirstName + " " + advisor.LastName;
                    myCampaignsModel.Url = s.CampaignFileURL;
                    listOfMyCampaignModels.Add(myCampaignsModel);
                }
            }
            byte[] progCallData =
                Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":""" + advisor.MFMasterPartnerTicket + @""",""Ticket"":""" +
                advisor.MFTicket + @""",""UserPassword"":""" + advisor.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) +
                @"""},""SearchString"":"""",""MaxRows"":500,""SortExpression"":"""",""StartRowIndex"":0,""State"":255}");
            JObject progResultPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/GetProgramList", progCallData,
                                                                                             "application/json", "POST", "", ""));
            if (progResultPost["Result"]["ErrorMessage"].ToString() != "")
            {
            }
            else
            {
                for (int i = 0; i <= progResultPost["ProgramList"].Count() - 1; i++)
                {
                    string creatorId = progResultPost["ProgramList"][i]["CreatorID"].ToString();
                    if (creatorId == advisor.MFUserID)
                    {
                        string pName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                        try
                        {
                            Campaigns dbCampaign = _factMainService.FindAllCampaigns().Where(x => x.CampaignName == pName && x.UserId == advisor.UserId).First();
                            if (dbCampaign != null)
                            {
                                if (dbCampaign.Status == "Saved")
                                {
                                    foreach (MyCampaignsModel savedCampaign in listOfMyCampaignModels)
                                    {
                                        if (savedCampaign.CampaignName == pName)
                                        {
                                            savedCampaign.CreatorName = progResultPost["ProgramList"][i]["CreatorName"].ToString();
                                            savedCampaign.Id = Convert.ToInt32(progResultPost["ProgramList"][i]["ProgramID"].ToString());
                                            savedCampaign.CampaignName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                                            savedCampaign.Status = progResultPost["ProgramList"][i]["StateName"].ToString();
                                            savedCampaign.State = Convert.ToInt32(progResultPost["ProgramList"][i]["State"]);
                                            savedCampaign.DateSubmitted = Convert.ToDateTime(progResultPost["ProgramList"][i]["PublishDate"].ToString());
                                            break;
                                        }
                                    }
                                }
                                else
                                {
                                    MyCampaignsModel myCampaignsModel = new MyCampaignsModel();
                                    myCampaignsModel.CreatorName = progResultPost["ProgramList"][i]["CreatorName"].ToString();
                                    myCampaignsModel.Id = Convert.ToInt32(progResultPost["ProgramList"][i]["ProgramID"].ToString());
                                    myCampaignsModel.CampaignName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                                    myCampaignsModel.Status = progResultPost["ProgramList"][i]["StateName"].ToString();
                                    myCampaignsModel.State = Convert.ToInt32(progResultPost["ProgramList"][i]["State"].ToString());
                                    myCampaignsModel.DateSubmitted = Convert.ToDateTime(progResultPost["ProgramList"][i]["PublishDate"].ToString());
                                    myCampaignsModel.Maml = dbCampaign.CampaignFileURL;
                                    myCampaignsModel.Token = dbCampaign.ApprovalToken;
                                    myCampaignsModel.DbStatus = dbCampaign.Status;
                                    listOfMyCampaignModels.Add(myCampaignsModel);
                                }
                            }
                        }
                        catch (Exception)
                        {
                        }
                    }
                }
            }
            return listOfMyCampaignModels.OrderByDescending(x => x.DateSubmitted).ToList();
        }

        public static string generateDisclosure(Profiles profile, IFactMainService _factMainService)
        {
            string res = "";
            Disclosures objDisclosure = _factMainService.FindAllDisclosures().SingleOrDefault(p => p.UserType == profile.UserType && p.IAType == profile.IAType);
            if (objDisclosure != null)
            {
                if (objDisclosure.isVariable)
                {
                    objDisclosure.DisclosureText = objDisclosure.DisclosureText.Replace("[INSERT RIA NAME HERE]", profile.ApprovedRIAName).Replace("[FSC/RAA/SPF/WFS]", profile.BDID);
                }
                res = objDisclosure.DisclosureText;
            }
            else
            {
                res = "No disclosure text!";
            }
            return res;
        }

        public static void StopAllCampaigns(long userId, IFactMainService _factMainService)
        {
            Profiles Profile = _factMainService.FindProfileById(userId);
            MindfireFunctions objMF = new MindfireFunctions();
            objMF.AuthenticateAccount(Profile.Email, Profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), ref Profile);
            byte[] progCallData = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":""" + Profile.MFMasterPartnerTicket +
                @""",""Ticket"":""" + Profile.MFTicket + @""",""UserPassword"":""" + Profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) +
                @"""},""SearchString"":"""",""MaxRows"":500,""SortExpression"":"""",""StartRowIndex"":0,""State"":255}");
            JObject progResultPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/GetProgramList", progCallData,
                "application/json", "POST", "", ""));
            if (progResultPost["Result"]["ErrorMessage"].ToString() != "")
            {
            }
            else
            {
                for (int i = 0; i <= progResultPost["ProgramList"].Count() - 1; i++)
                {
                    string creatorId = progResultPost["ProgramList"][i]["CreatorID"].ToString();
                    if (creatorId == Profile.MFUserID)
                    {
                        string pName = progResultPost["ProgramList"][i]["ProgramName"].ToString();
                        string programID = progResultPost["ProgramList"][i]["ProgramID"].ToString();
                        byte[] campaignCallData = Encoding.UTF8.GetBytes(@"{""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" +
                            Profile.MFTicket + @""",""UserPassword"":""" + Profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) +
                            @"""},""MamlFormat"":0,""ProgramID"":" + programID + "}");
                        JObject campaignResultPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/CheckoutProgram", campaignCallData,
                            "application/json", "POST", "", ""));
                        if (campaignResultPost["Result"]["ErrorMessage"].ToString() != "")
                        {
                        }
                        else
                        {
                            string mamlXML = string.Empty;
                            string byteString = string.Empty;
                            for (int x = 0; x <= campaignResultPost["Maml"].Count() - 1; x++)
                            {
                                mamlXML = mamlXML + (char)(Convert.ToInt32(campaignResultPost["Maml"][x].ToString()));
                            }
                            XmlDocument campXML = new XmlDocument();
                            campXML.LoadXml(mamlXML);
                            XmlElement rootLoad = campXML.DocumentElement;
                            Campaigns campaignChangedStatus = _factMainService.FindAllCampaigns().Where(c => c.CampaignName == pName).SingleOrDefault();
                            if (campaignChangedStatus != null)
                            {
                                if (rootLoad.Attributes["State"].Value == "Start")
                                {
                                    rootLoad.Attributes["State"].Value = "Stop";
                                    //*** rootLoad.Item["Campaign"].Attributes["State"].Value = "Stop";
                                    
                                }
                                var encodedData = GlobalFunctions.Convert64(campXML.InnerXml);
                                for (int x = 0; x <= encodedData.Length - 1; x++)
                                {
                                    byteString = byteString + encodedData[x] + ",";
                                    byteString = byteString + Environment.NewLine;
                                }
                                byteString = byteString.Remove(byteString.LastIndexOf(","));
                                byte[] progCallData2 =
                                    Encoding.UTF8.GetBytes(@"{""Maml"":[" + byteString +
                                    @"],""MamlFormat"":0,""CheckoutAfterCheckin"":false,""Comments"":""Edited Campaign"",""Credentials"":{""IsEncrypted"":false,""PartnerPassword"":"""",""PartnerTicket"":"""",""Ticket"":""" +
                                    Profile.MFTicket + @""",""UserPassword"":""" + Profile.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])) + @"""}}");
                                JObject campaignStopPost = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest("https://studio.mdl.io/REST/programservice/CheckinProgram",
                                    progCallData2, "application/json", "POST", "", ""));
                                if (campaignStopPost["Result"]["ErrorMessage"].ToString() != "")
                                {
                                }
                                else
                                {
                                    if (campaignChangedStatus != null)
                                    {
                                        campaignChangedStatus.Status = "Stop";
                                        campaignChangedStatus.LaunchDate = DateTime.Now;
                                        _factMainService.UpdateCampaigns(campaignChangedStatus);
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }

        public static bool HasSubscriptions(string authorizeNetId)
        {
            if (authorizeNetId == null)
            {
                authorizeNetId = "";
            }
            getCustomerProfileResponse tmp = aNetApi.GetCustomerProfile(authorizeNetId);
            List<ARBGetSubscriptionResponse> subscriptions = new List<ARBGetSubscriptionResponse>();
            if (tmp != null && !(tmp.subscriptionIds == null) && !(tmp.subscriptionIds.Count() == 0))
            {
                foreach (var subId in tmp.subscriptionIds)
                {
                    subscriptions.Add(aNetApi.GetSubscriptionDetails(subId));
                }
            }
            else
            {
                return false;
            }
            int countOfActiveSubs = subscriptions.Where(x => x.subscription.status == 0).ToList().Count;
            return (countOfActiveSubs > 0);
        }

        public static string decodeBDID(string BDID)
        {
            if (BDID.ToUpper().Contains("SAGEPOINT"))
            {
                return "SPF";
            }
            else if (BDID.ToUpper().Contains("ROYAL"))
            {
                return "RAA";
            }
            else if (BDID.ToUpper().Contains("FSC SECURITIES"))
            {
                return "FSC";
            }
            else if (BDID.ToUpper().Contains("WOODBURY"))
            {
                return "WFS";
            }
            else if (BDID.ToUpper().Contains("ADVISOR GROUP"))
            {
                return "AG";
            }
            else
            {
                return "0";
            }
        }

        public static string generatePersonaDisclosure(Personas _persona, Profiles _profile, IFactMainService _factMainService)
        {
            string res = "";
            Disclosures objDisclosure = _factMainService.FindAllDisclosures().SingleOrDefault(p => p.UserType == _profile.UserType && p.IAType == _persona.IAType);
            if (objDisclosure != null)
            {
                if (objDisclosure.isVariable)
                {
                    objDisclosure.DisclosureText = objDisclosure.DisclosureText.Replace("[INSERT RIA NAME HERE]", _persona.ApprovedRIAName).Replace("[FSC/RAA/SPF/WFS]", _persona.BDID);
                }
                res = objDisclosure.DisclosureText;
            }
            else
            {
                res = "No disclosure text!";
            }
            return res;
        }

        public static bool IsImage(HttpPostedFileBase file)
        {
            return file.ContentType == "image/gif" || file.ContentType == "image/jpeg" || file.ContentType == "image/png";
        }

        private static SalesForceImportViewModel FillCustomFields(SalesForceImportViewModel sfImports)
        {
            sfImports.intCSVLimit = intCSVLimit.ToString();
            sfImports.intContactLimit = intContactLimit.ToString();
            var customFields = Enumerable.Empty<CustomFieldAdmin>();
            try
            {
                customFields = ProcessSalesForceServiceRequestAdmin(service => 
                service.GetCustomFieldsAdmin(SessionManager.SalesForce.Authentication, SoqlDataTable.Contact, SoqlDataTable.Lead));
            }
            catch (Exception exception)
            {
                Tuple<String, Object[]> extraMessageFormat = MessageHelper.GetCurrentRequestInformationMessageFormat();
                this.EventLogger.LogError(exception, extraMessageFormat.Item1, extraMessageFormat.Item2);
            }
            IEnumerable<ListItem> contactCustomFields = customFields.Where(cf => cf.Table == SoqlDataTable.Contact).Select(cf => new ListItem(cf.Label, cf.Id)).ToArray();
            IEnumerable<ListItem> leadCustomFields = customFields.Where(cf => cf.Table == SoqlDataTable.Lead).Select(cf => new ListItem(cf.Label, cf.Id).ToArray();
            var contactOptions = contactCustomFields.Select(purpose => new SelectListItem()
            {
                Text = purpose.Text,
                Value = purpose.Value
            }).ToList();
            var leadOptions = leadCustomFields.Select(purpose => new SelectListItem()
            {
                Text = purpose.Text,
                Value = purpose.Value
            }).ToList();
            SelectListItem firstCustom = new SelectListItem();
            firstCustom.Value = "0";
            firstCustom.Text = "Select Custom Contact";
            contactOptions.Add(firstCustom);
            sfImports.CustomFieldsContact = contactOptions;
            SelectListItem firstLead = new SelectListItem();
            firstLead.Value = "0";
            firstLead.Text = "Select Custom Lead";
            leadOptions.Add(firstLead);
            sfImports.CustomFieldsLead = leadOptions;
            sfImports.SelectedContact = "0";
            sfImports.SelectedLead = "0";
            return sfImports;
        }

        private static void ProcessSalesForceServiceRequestAdmin<TResult>(Func<SFServiceAdmin, TResult> request)
        {
            SFServiceAdmin _sfServiceAdmin = new SFServiceAdmin();
            if (SessionManagerAdmin.SalesForceAdmin.AccessTokenAvailableAdmin)
            {
                try
                {
                    return request[_sfServiceAdmin];
                }
                catch (SalesForceSessionExceptionAdmin exception)
                {
                    SessionManagerAdmin.SalesForceAdmin.AuthenticationAdmin = _sfServiceAdmin.GetAuthenticationInformationAdmin(SessionManagerAdmin.SalesForceAdmin.AuthenticationAdmin.RefreshToken);
                    return request(_sfServiceAdmin);
                }
            }
            else
            {
                SessionManagerAdmin.SalesForceAdmin.AuthenticationAdmin = _sfServiceAdmin.GetAuthenticationInformationAdmin(SessionManagerAdmin.SalesForceAdmin.AuthenticationAdmin.RefreshToken);
                return request(_sfServiceAdmin);
            }
        }
    }
}