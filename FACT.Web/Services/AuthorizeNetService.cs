﻿using AuthorizeNet.Api.Contracts.V1;
using AuthorizeNet.Api.Controllers;
using AuthorizeNet.Api.Controllers.Bases;
using FACT.Domain.Models.Profile;
using FACT.Web.Enums;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net;
using System.Web;

namespace FACT.Web.Services
{
    public class AuthorizeNetService
    {
        public string API_LOGIN_ID = ConfigurationManager.AppSettings["AuthNetApiLoginId_" + ConfigurationManager.AppSettings["AuthNetEnvironment"]];
        public string TRANSACTION_KEY = ConfigurationManager.AppSettings["AuthNetTransactionKey_" + ConfigurationManager.AppSettings["AuthNetEnvironment"]];

        /// <summary>
        /// Constructor sets up the evironment variables for Authorize.net so we don//t have to repeat that code in every
        /// method, and turns on TLS 1.2 which is required by Authorize.net.
        /// </summary>
        public AuthorizeNetService()
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;

            string environment = ConfigurationManager.AppSettings["AuthNetEnvironment"];
            API_LOGIN_ID = ConfigurationManager.AppSettings["AuthNetApiLoginId_" + environment];
            TRANSACTION_KEY = ConfigurationManager.AppSettings["AuthNetTransactionKey_" + environment];
            switch (environment)
            {
                case "SANDBOX":
                    ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.SANDBOX;
                    break;
                case "PRODUCTION":
                    ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = AuthorizeNet.Environment.PRODUCTION;
                    break;
                default:
                    break;
            }
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = API_LOGIN_ID,
                Item = TRANSACTION_KEY,
                ItemElementName = ItemChoiceType.transactionKey
            };
        }

        /// <summary>
        /// This is a constructor that takes parameters for setting up authorisation data. This constructor is intended for 
        /// testing, application developers should almost certanly use parameterless constructor.
        /// </summary>
        public AuthorizeNetService(string loginKey, string transactionKey, AuthorizeNet.Environment environment)
        {
            ServicePointManager.SecurityProtocol = SecurityProtocolType.Tls12;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.RunEnvironment = environment;
            ApiOperationBase<ANetApiRequest, ANetApiResponse>.MerchantAuthentication = new merchantAuthenticationType()
            {
                name = loginKey,
                Item = transactionKey,
                ItemElementName = ItemChoiceType.transactionKey
            };
        }

        /// <summary>
        /// Creates a new customer over Authorize.net API. It returns a <see cref="createCustomerProfileResponse"/>  
        /// object with data populated.
        /// </summary>
        /// <param name="email">Email/username of local user</param>
        /// <param name="userId">ID of local user to be set in AuthoriseNet as "dscription"</param>
        /// <returns>
        /// createCustomerProfileResponse object containing created customer or error data
        /// </returns>
        public createCustomerProfileResponse CreateCustomer(string email, string userId)
        {
            createCustomerProfileRequest request = new createCustomerProfileRequest()
            {
                profile = new customerProfileType
                {
                    description = userId,
                    email = email
                },
                validationMode = validationModeEnum.none
            };
            createCustomerProfileController controller = new createCustomerProfileController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public getCustomerProfileResponse GetCustomerProfile(string customerProfileId)
        {
            getCustomerProfileRequest request = new getCustomerProfileRequest
            {
                customerProfileId = customerProfileId,
                unmaskExpirationDate = true,
                unmaskExpirationDateSpecified = true
            };
            getCustomerProfileController controller = new getCustomerProfileController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public updateCustomerProfileResponse UpdateCustomerProfile(string _customerProfileId, string strEmail)
        {
            updateCustomerProfileRequest request = new updateCustomerProfileRequest
            {
                profile = new customerProfileExType
                {
                    customerProfileId = _customerProfileId,
                    email = strEmail
                }
            };
            updateCustomerProfileController controller = new updateCustomerProfileController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public deleteCustomerProfileResponse DeleteCustomer(int customerProfileId)
        {
            deleteCustomerProfileRequest request = new deleteCustomerProfileRequest
            {
                customerProfileId = customerProfileId.ToString()
            };
            deleteCustomerProfileController controller = new deleteCustomerProfileController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public createCustomerPaymentProfileResponse AddCreditCard(AddCardVM model, string customerProfileId)
        {
            createCustomerPaymentProfileRequest request = new createCustomerPaymentProfileRequest
            {
                customerProfileId = customerProfileId,
                paymentProfile = new customerPaymentProfileType
                {
                    billTo = new customerAddressType
                    {
                        firstName = model.FirstName,
                        lastName = model.LastName,
                        address = model.Address,
                        city = model.City,
                        state = model.State,
                        zip = model.Zip,
                        country = model.Country,
                        company = model.Company,
                        phoneNumber = model.PhoneNumber,
                        faxNumber = model.FaxNumber
                    },
                    payment = new paymentType
                    {
                        Item = new creditCardType
                        {
                            cardNumber = model.CardNumber,
                            expirationDate = String.Format("{0:D4}-{1:D2}", model.Year, model.Month),
                            cardCode = model.CCV
                        }
                    }
                },
                validationMode = validationModeEnum.testMode
            };
            createCustomerPaymentProfileController controller = new createCustomerPaymentProfileController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public deleteCustomerPaymentProfileResponse RemoveCreditCard(string customerProfileId, string customerPaymentProfileId)
        {
            deleteCustomerPaymentProfileRequest request = new deleteCustomerPaymentProfileRequest
            {
                customerProfileId = customerProfileId,
                customerPaymentProfileId = customerPaymentProfileId
            };
            deleteCustomerPaymentProfileController controller = new deleteCustomerPaymentProfileController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public ANetApiResponse ChargeCreditCard(List<KeyValuePair<string, decimal>> items, int quantity, customerAddressType billingAddress)
        {
            decimal amount = 0;
            List<lineItemType> lineItems = new List<lineItemType>();
            int no = 1;
            foreach (KeyValuePair<string, decimal> item in items)
            {
                amount = amount + item.Value;
                lineItems.Add(new lineItemType
                {
                    itemId = Convert.ToString(no),
                    name = item.Key,
                    quantity = quantity,
                    unitPrice = item.Value
                });
                no += 1;
            }
            //Dim paymentType = New paymentType With {.Item = creditCard}
            transactionRequestType transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureContinueTransaction.ToString(),
                amount = amount,
                billTo = billingAddress,
                lineItems = lineItems.ToArray()
            };
            createTransactionRequest request = new createTransactionRequest
            {
                transactionRequest = transactionRequest
            };
            createTransactionController controller = new createTransactionController(request);
            controller.Execute();
            var response = controller.GetApiResponse();
            return response;
        }

        public createTransactionResponse ChargeCustomerProfile(string customerProfileId, string customerPaymentProfileId, List<KeyValuePair<string, decimal>> items,
            double quantity)
        {
            decimal amount = 0;
            List<lineItemType> lineItems = new List<lineItemType>();
            int no = 1;
            JArray aLineItems = new JArray();
            foreach (KeyValuePair<string, decimal> item in items)
            {
                JObject jLineItem = new JObject();
                jLineItem["itemId"] = Convert.ToString(no);
                jLineItem["name"] = item.Key;
                jLineItem["quantity"] = quantity;
                jLineItem["unitPrice"] = item.Value;
                aLineItems.Add(jLineItem);
                amount += item.Value * (decimal)quantity;
                lineItems.Add(new lineItemType
                {
                    itemId = Convert.ToString(no),
                    name = item.Key,
                    quantity = (decimal)quantity,
                    unitPrice = item.Value
                });
                no += 1;
            }
            customerProfilePaymentType profileToCharge = new customerProfilePaymentType();
            profileToCharge.customerProfileId = customerProfileId;
            profileToCharge.paymentProfile = new paymentProfile
            {
                paymentProfileId = customerPaymentProfileId
            };
            transactionRequestType transactionRequest = new transactionRequestType
            {
                transactionType = transactionTypeEnum.authCaptureTransaction.ToString(),
                amount = amount,
                profile = profileToCharge
            };
            createTransactionRequest request = new createTransactionRequest
            {
                transactionRequest = transactionRequest
            };
            createTransactionController controller = new createTransactionController(request);
            controller.Execute();
            createTransactionResponse response = controller.GetApiResponse();
            return response;
        }

        public ARBCreateSubscriptionResponse CreateTransaction(string customerProfileId, string customerPaymentProfileId, decimal ammount, string transactionName)
        {
            ARBCreateSubscriptionRequest request = new ARBCreateSubscriptionRequest
            {
                subscription = new ARBSubscriptionType
                {
                    amount = ammount,
                    name = transactionName,
                    paymentSchedule = new paymentScheduleType
                    {
                        interval = new paymentScheduleTypeInterval
                        {
                            length = 1,
                            unit = ARBSubscriptionUnitEnum.months
                        },
                        startDate = DateTime.Now.AddDays(1), // Start date should be at the minimum two months after the account creation (free period)
                        totalOccurrences = 9999
                    },
                    profile = new customerProfileIdType
                    {
                        customerProfileId = customerProfileId,
                        customerPaymentProfileId = customerPaymentProfileId
                    }
                }
            };
            ARBCreateSubscriptionController controller = new ARBCreateSubscriptionController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public ARBCreateSubscriptionResponse CreateSubscription(string customerProfileId, string customerPaymentProfileId, SubscriptionPeriod period)
        {
            // Depending on the payment selection, you can subscribe for a year for lower price, or start a regular monthly 
            // subscription. Fields //amount// and //length// need to be set based on //monthly// parameter.
            ARBCreateSubscriptionRequest request = new ARBCreateSubscriptionRequest
            {
                subscription = new ARBSubscriptionType
                {
                    amount = period == SubscriptionPeriod.Monthly ? 29 : 299,
                    paymentSchedule = new paymentScheduleType
                    {
                        interval = new paymentScheduleTypeInterval
                        {
                            length = period == SubscriptionPeriod.Monthly ? (short)1 : (short)12,
                            unit = ARBSubscriptionUnitEnum.months
                        },
                        startDate = DateTime.Now.AddDays(1), // Start date should be at the minimum two months after the account creation (free period)
                        totalOccurrences = 9999
                    },
                    profile = new customerProfileIdType
                    {
                        customerProfileId = customerProfileId,
                        customerPaymentProfileId = customerPaymentProfileId
                    }
                }
            };
            ARBCreateSubscriptionController controller = new ARBCreateSubscriptionController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public ARBGetSubscriptionResponse GetSubscriptionDetails(string subscriptionId)
        {
            ARBGetSubscriptionRequest request = new ARBGetSubscriptionRequest
            {
                subscriptionId = subscriptionId,
                refId = subscriptionId
            };
            ARBGetSubscriptionController controller = new ARBGetSubscriptionController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public ARBCancelSubscriptionResponse CancelSubscription(string subscriptionId)
        {
            ARBCancelSubscriptionRequest request = new ARBCancelSubscriptionRequest
            {
                subscriptionId = subscriptionId
            };
            ARBCancelSubscriptionController controller = new ARBCancelSubscriptionController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public getTransactionDetailsResponse GetTransactionDetails(string transactionId)
        {
            getTransactionDetailsRequest request = new getTransactionDetailsRequest
            {
                transId = transactionId
            };
            getTransactionDetailsController controller = new getTransactionDetailsController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public getTransactionListResponse GetTransactionListForCustomer(string customerProfileId, string customerPaymentProfileId)
        {
            try
            {
                getTransactionListForCustomerRequest request = new getTransactionListForCustomerRequest
                {
                    customerProfileId = customerProfileId,
                    customerPaymentProfileId = customerPaymentProfileId
                };
                getTransactionListForCustomerController controller = new getTransactionListForCustomerController(request);
                controller.Execute();
                return controller.GetApiResponse();
            }
            catch (Exception)
            {
                return null;
            }
        }

        public List<getTransactionListResponse> GetTransactions(string customerProfileId)
        {
            List<getTransactionListResponse> result = new List<getTransactionListResponse>();
            try
            {
                var customerProfile = GetCustomerProfile(customerProfileId);
                foreach (var paymentProfile in customerProfile.profile.paymentProfiles)
                {
                    var profileTransactionList = GetTransactionListForCustomer(customerProfileId, paymentProfile.customerPaymentProfileId);
                    if (profileTransactionList != null)
                    {
                        result.Add(profileTransactionList);
                    }
                }
                return result;
            }
            catch (Exception)
            {
                return result;
            }
        }

        public getHostedProfilePageResponse GetHostedPage(string customerProfileId)
        {
            List<settingType> hostedProfileSettings = new List<settingType>();
            getHostedProfilePageRequest request = new getHostedProfilePageRequest()
            {
                customerProfileId = customerProfileId,
                hostedProfileSettings = hostedProfileSettings.ToArray()
            };
            getHostedProfilePageController controller = new getHostedProfilePageController(request);
            controller.Execute();
            return controller.GetApiResponse();
        }

        public string GetHostedPageToken(string customerProfileId)
        {
            var HostedPage = GetHostedPage(customerProfileId);
            return HostedPage == null ? "" : HostedPage.token;
        }
    }
}