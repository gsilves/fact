﻿using FACT.Domain.Models;
using FACT.Domain.Models.EntityModels;
using FACT.Domain.Models.MiscModels;
using FACT.Service;
using FACT.Web.Business;
using FACT.Web.Controllers;
using FACT.Web.Enums;
using FACT.Web.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Reflection;
using System.Threading.Tasks;
using System.Web;
using System.Web.ModelBinding;

namespace FACT.Web.Services
{
    public class SSOAccountService : IDisposable
    {
        private ApplicationUserManager _userManager;
        private ApplicationSignInManager _signInManager;
        HttpContext context = (HttpContext)HttpContext.Current.Session["context"];
        private bool disposed;

        public async Task<SSOAccountResult> AddSSOAccount(Profiles model, ModelStateDictionary modelState, ErrorModelsController errController, string userName,
            string email, string strPassword, IFactMainService _factMainService)
        {
            bool aspNetUsersModified = false;
            bool profileModified = false;
            bool authorizeNetModified = false;
            bool mindfireModified = false;
            bool contactsModified = false;
            bool companyLogoUploaded = false;
            bool companyLogoSelected = false;
            bool profilePhotoUploaded = false;
            bool signatureImageUploaded = false;
            List<KeyValuePair<string, string>> list = new List<KeyValuePair<String, String>>();
            SSOAccountResult resultSSO = new SSOAccountResult();
            var user = new ApplicationUser() {
                UserName = userName,
                Email = email
            };
            AuthorizeNetService authorizeNetService = new AuthorizeNetService();
            AmazonS3Service awsService = new AmazonS3Service();
            MindfireFunctions objMF = new MindfireFunctions();
            int? intSubaccount = -1;
            try
            {
                // AspNetUsers user Creation
                IdentityResult createUserResult;
                createUserResult = await UserManager.CreateAsync(user, strPassword);
                if (createUserResult.Succeeded)
                {
                    aspNetUsersModified = true;
                    model.UserId = user.Id;
                    model.DisclosureText = ControllerFunctions.generateDisclosure(model, _factMainService);
                    //Making sure that model.ApprovedRIAName is empty if IAType is "Registered Representative" or "Corporate IA"
                    if (model.IAType == null || model.IAType == (int)IAType.NONE || model.IAType == (int)IAType.CRIA)
                    {
                        model.ApprovedRIAName = "";
                    }
                    // Profile creation
                    _factMainService.AddProfile(model);
                    profileModified = true;
                    //AuthorizeNet CreateCustomer
                    var anResult = authorizeNetService.CreateCustomer(model.Email, user.Id);
                    if (anResult.messages.resultCode == AuthorizeNet.Api.Contracts.V1.messageTypeEnum.Ok)
                    {
                        authorizeNetModified = true;
                        user.AuthorizeNetId = Convert.ToInt32(anResult.customerProfileId);
                        await UserManager.UpdateAsync(user);
                        bool isMFUser = objMF.CheckMindfireUser(model.Email, strPassword);
                        if (!isMFUser)
                        {
                            //Create Mindfire account
                            intSubaccount = objMF.CreateMFSubAccount("Advisor Group-" + model.AGUserId.ToString() + model.RepCode, model.Email, model.FirstName, 
                                model.LastName, strPassword);
                            if (intSubaccount != null)
                            {
                                mindfireModified = true;
                                model.MFAccountID = intSubaccount.ToString();
                                objMF.SetAuthorizedPartner();
                                objMF.AddDomain(model.MFAccountID + ".agmycmo.com");
                            }
                            else
                            {
                                modelState.AddModelError("", "Campaign Engine Account Error. Please contact support for assistance.");
                                intSubaccount = -1;
                            }
                        }
                        if (modelState.IsValid)
                        {
                            objMF.AuthenticateAccount(model.Email, strPassword, ref model);
                            //upload images to S3
                            if (model.IAType == null)
                            {
                                model.IAType = (int)IAType.NONE;
                            }
                            if (!string.IsNullOrEmpty(model.CompanyLogo))
                            {
                                string[] stringSeparators = { AmazonS3Service.Bucket + "/" };
                                var a = model.CompanyLogo.Split(stringSeparators, StringSplitOptions.None);
                                if (a.Length > 1)
                                {
                                    model.CompanyLogo = a[1];
                                }
                                companyLogoUploaded = true;
                                companyLogoSelected = true;
                            }
                            else if (model.CompanyLogoFile != null)
                            {
                                var result = awsService.UploadCompanyLogoImage(model.CompanyLogoFile);
                                if (result.Successful)
                                {
                                    model.CompanyLogo = result.Path;
                                    companyLogoUploaded = true;
                                }
                                else
                                {
                                    modelState.AddModelError("", result.Message);
                                }
                            }
                            else
                            {

                            }

                            if (model.ProfilePhotoFile != null)
                            {
                                var result = awsService.UploadProfileImage(model.ProfilePhotoFile);
                                if (result.Successful)
                                {
                                    model.ProfilePhoto = result.Path;
                                    profilePhotoUploaded = true;
                                }
                                else
                                {
                                    modelState.AddModelError("", result.Message);
                                }
                            }
                            if (model.SignatureImageFile != null)
                            {
                                var result = awsService.UploadSignatureImage(model.SignatureImageFile);
                                if (result.Successful)
                                {
                                    model.SignatureImage = result.Path;
                                    signatureImageUploaded = true;
                                }
                                else
                                {
                                    modelState.AddModelError("", result.Message);
                                }
                            }
                            //Update profile properties in DB
                            _factMainService.UpdateProfiles(model);
                            //Create contact with same info as new User
                            MyContactsModel myContactsModel = new MyContactsModel();
                            myContactsModel.Email = (model.Email == null) ? "" : model.Email;
                            myContactsModel.AccountID = (model.MFAccountID == null) ? "" : model.MFAccountID;
                            myContactsModel.Address1 = (model.BranchAddressLine1 == null) ? "" : model.BranchAddressLine1;
                            myContactsModel.Address2 = (model.BranchAddressLine2 == null) ? "" : model.BranchAddressLine2;
                            myContactsModel.City = (model.BranchCity == null) ? "" : model.BranchCity;
                            myContactsModel.Company = (model.Company == null) ? "" : model.Company;
                            myContactsModel.FacebookAccount = (model.FacebookURL == null) ? "" : model.FacebookURL;
                            myContactsModel.Fax = (model.BranchFaxNumber == null) ? "" : model.BranchFaxNumber;
                            myContactsModel.FirstName = (model.FirstName == null) ? "" : model.FirstName;
                            myContactsModel.Gender = (model.Gender == null) ? "" : model.Gender;
                            myContactsModel.LastName = (model.LastName == null) ? "" : model.LastName;
                            myContactsModel.LinkedInAccount = (model.LinkedInURL == null) ? "" : model.LinkedInURL;
                            myContactsModel.MiddleName = (model.MiddleName == null) ? "" : model.MiddleName;
                            myContactsModel.Phone = (model.BranchPhone == null) ? "" : model.BranchPhone;
                            myContactsModel.PhotoURL = (model.ProfilePhoto == null) ? "" : model.ProfilePhoto;
                            myContactsModel.State = (model.BranchState == null) ? "" : model.BranchState;
                            myContactsModel.Title = (model.JobTitle == 0) ? "" : model.JobTitle.ToString();
                            myContactsModel.TwitterAccount = (model.TwitterURL == null) ? "" : model.TwitterURL;
                            myContactsModel.Website = (model.Website == null) ? "" : model.Website;
                            myContactsModel.Zip = (model.BranchZip == null) ? "" : model.BranchZip;
                            myContactsModel.PURL = (model.FirstName == null) ? "" : (model.FirstName + model.LastName == null) ? "" : model.LastName;
                            foreach (PropertyInfo p in myContactsModel.GetType().GetProperties())
                            {
                                if (p.CanRead)
                                {
                                    Object val = p.GetValue(myContactsModel);
                                    string name = p.Name;
                                    var pair = new KeyValuePair<String, String>(name, val.ToString());
                                    list.Add(pair);
                                }
                            }
                            JObject x = objMF.CreateContact(model.MFTicket, strPassword, model.MFMasterPartnerTicket, list);
                            if (string.IsNullOrEmpty(x["Result"]["ErrorCode"].ToString()))
                            {
                                contactsModified = true;
                                JToken contactId = x["ContactID"];
                                JToken purl = x["Purl"];
                            }
                            else
                            {
                                JToken errMsg = x["Result"]["ErrorMessage"];
                                modelState.AddModelError("", errMsg.ToString());
                            }
                            //*** Await db.SaveChangesAsync()
                            user.AuthorizeNetId = Convert.ToInt32(anResult.customerProfileId);
                            user.Active = true;
                            await UserManager.UpdateAsync(user);
                            await SignInManager.SignInAsync(user, isPersistent: false, rememberBrowser: false);
                            resultSSO.ProfileModel = null;
                            resultSSO.ResultStr = "HomeIndex";
                            return resultSSO;
                        }
                        else
                        {
                            //Profile and AspNetUsers records created, AuthorizeNet Customer created
                            if (profileModified)
                            {
                                _factMainService.DeleteProfile(model);
                            }
                            if (aspNetUsersModified)
                            {
                                await UserManager.DeleteAsync(user);
                            }
                            var response = authorizeNetService.DeleteCustomer(Convert.ToInt32(anResult.customerProfileId));
                            resultSSO.ProfileModel = model;
                            resultSSO.ResultStr = "ViewACSModel";
                            return resultSSO;
                        }
                    }
                    else
                    {
                        //Profile and AspNetUsers records created
                        if (profileModified)
                        {
                            _factMainService.DeleteProfile(model);
                        }
                        if (aspNetUsersModified)
                        {
                            await UserManager.DeleteAsync(user);
                        }
                        AddErrors(new IdentityResult(new List<String> { anResult.messages.resultCode.ToString() }), modelState);
                        //Return View("acs", model)
                        resultSSO.ProfileModel = model;
                        resultSSO.ResultStr = "ViewACSModel";
                        return resultSSO;
                    }
                }
                else
                {
                    //AspNetUsers new user creation failed.
                    //No other changes yet.
                    AddErrors(createUserResult, modelState);
                    resultSSO.ProfileModel = model;
                    resultSSO.ResultStr = "ViewACSModel";
                    return resultSSO;
                }
            }
            catch (Exception ex)
            {
                //ModelState.AddModelError("", ex.StackTrace)
                if (authorizeNetModified)
                {
                    authorizeNetService.DeleteCustomer((int)user.AuthorizeNetId);
                }
                if (profileModified)
                {
                    _factMainService.DeleteProfile(model);
                }
                if (aspNetUsersModified)
                {
                    UserManager.Delete(user);
                }
                if (contactsModified)
                {
                    objMF.DeleteContact(model.MFTicket, model.GetMFPassword(Convert.ToBoolean(ConfigurationManager.AppSettings["MFPasswordMODAct"])), model.MFMasterPartnerTicket, list);
                }
                if (companyLogoUploaded)
                {
                    if (!companyLogoSelected)
                    {
                        awsService.DeleteCompanyLogoImage(model.CompanyLogo);
                    }
                    model.CompanyLogo = "";
                }
                if (profilePhotoUploaded)
                {
                    awsService.DeleteProfileImage(model.ProfilePhoto);
                    model.ProfilePhoto = "";
                }
                if (signatureImageUploaded)
                {
                    awsService.DeleteSignatureImage(model.SignatureImage);
                    model.SignatureImage = "";
                }
                if (mindfireModified)
                {
                    modelState.AddModelError("", "Mindfire Account Created is not linked to any Profile! MF Account Id: " + intSubaccount);
                }
                modelState.AddModelError("", ex);
                resultSSO.ProfileModel = null;
                resultSSO.ResultStr = "Err";
                resultSSO.ExErr = ex;
                return resultSSO;
            }
        }

        protected ApplicationUserManager UserManager
        {
            get
            {
                if (_userManager == null)
                {
                    context.GetOwinContext().GetUserManager<ApplicationUserManager>();
                }
                return _userManager ?? context.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
            set
            {
                _userManager = value;
            }
        }

        protected ApplicationSignInManager SignInManager
        {
            get
            {
                return _signInManager ?? context.GetOwinContext().Get<ApplicationSignInManager>();
            }
            set
            {
                _signInManager = value;
            }
        }

        private void AddErrors(IdentityResult result, ModelStateDictionary modelState)
        {
            foreach (var error in result.Errors)
            {
                modelState.AddModelError("", error);
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
        }
    }
}