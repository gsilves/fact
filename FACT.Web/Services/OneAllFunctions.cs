﻿using FACT.Domain.Models.OneAllModels;
using FACT.Service;
using FACT.Web.Business;
using Newtonsoft.Json.Linq;
using RestSharp;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Web;

namespace FACT.Web.Services
{
    public class OneAllFunctions : IDisposable
    {
        public string PublicKey;
        public string PrivateKey;
        public string SO;
        private string authToken;
        public string apiURL = ConfigurationManager.AppSettings["OAAPIEndpoint"];
        private bool disposed = false;

        public OneAllFunctions()
        {
            PublicKey = ConfigurationManager.AppSettings["OApublicKey"].ToString();
            PrivateKey = ConfigurationManager.AppSettings["OAPrivateKey"].ToString();
            SO = PublicKey + ":" + PrivateKey;
            authToken = GlobalFunctions.ToBase64(GlobalFunctions.Convert64(SO));
        }

        public OneAllUser GetUser(string userToken)
        {
            RestClient client = new RestClient(apiURL);
            RestRequest request = new RestRequest("users/" + userToken + ".json", Method.GET);
            OneAllUser userString;
            request.AddHeader("Content-type", "application/json");
            request.AddHeader("Authorization", "Basic " + authToken);
            var response = (UserResponse)client.Execute(request);
            if (response.Response.result != null)
            {
                userString = response.Response.result.data.user;
            }
            else
            {
                userString = null;
            }
            return userString;
        }

        public JObject ReturnIdentityDetails(string identityToken)
        {
            JObject oaResult_post = JObject.FromObject(GlobalFunctions.ProcessJSONHTTPRequest(apiURL + "/identities/" + identityToken +
                ".json", null, "application/json", "GET", SO, "Basic"));
            if (oaResult_post["response"]["request"]["status"]["code"] == null)
            {
                return oaResult_post;
            }
            else
            {
                return (JObject)"Error!!!!";
            }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (!this.disposed)
            {
                if (disposing)
                {

                }
            }
        }
    }
}