﻿using Amazon.Runtime;
using Amazon.Runtime.CredentialManagement;
using Amazon.S3;
using Amazon.S3.Model;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.IO;
using System.Linq;
using System.Text;
using System.Web;

namespace FACT.Web.Services
{
    public class AmazonS3Service
    {
        private IAmazonS3 AmazonClient { get; set; }

        public const string Bucket = "mycmo";

        public AmazonS3Service()
        {
            AWSCredentials credentials = null;
            CredentialProfile credentialsProfile = null;
            SharedCredentialsFile credentialsFile = new SharedCredentialsFile(ConfigurationManager.AppSettings["S3CredentialsFile"]);
            bool readProfile = credentialsFile.TryGetProfile(ConfigurationManager.AppSettings["S3CredentialsProfile"], out credentialsProfile);
            bool readCredentials = AWSCredentialsFactory.TryGetAWSCredentials(credentialsProfile, credentialsFile, out credentials);

            if (readProfile && readCredentials)
            {
                AmazonClient = new AmazonS3Client(credentials, Amazon.RegionEndpoint.USEast1);
            }
            else
            {
                throw new Exception("Could not create AWS S3 client.");
            }
        }

        private bool HasImageExtension(string fileName)
        {
            return (fileName.EndsWith(".png") || fileName.EndsWith(".jpg") || fileName.EndsWith(".jpeg") || fileName.EndsWith(".jfif") ||
                    fileName.EndsWith(".bmp") || fileName.EndsWith(".gif") || fileName.EndsWith(".tif") || fileName.EndsWith(".tiff"));
        }

        public IEnumerable<string> GetFolderContent(string folder)
        {
            List<string> result = new List<string>();
            try
            {
                ListObjectsV2Request getListRequest = new ListObjectsV2Request()
                {
                    BucketName = Bucket,
                    Prefix = folder,
                    MaxKeys = 1000
                };
                ListObjectsV2Response Response = AmazonClient.ListObjectsV2(getListRequest);
                if (Response != null)
                {
                    foreach (var entry in Response.S3Objects)
                    {
                        //TODO check if entry.key is image file and if so add it tot result list.
                        if (HasImageExtension(entry.Key))
                        {
                            result.Add("https://s3-us-west-2.amazonaws.com/" + Bucket + "/" + entry.Key);
                        }
                    }
                }
                return result;
            }
            catch (Exception ex)
            {
                return new List<String>();
            }
        }

        public AmazonResponse UploadCampaignTemplate(string content, string campaignTemplateFileName)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                UTF8Encoding enc = new UTF8Encoding();
                byte[] arrayData = enc.GetBytes(content);
                ms.Write(arrayData, 0, arrayData.Length);
                string fileName = campaignTemplateFileName.Replace("~", "CampaignTemplateFiles");
                var uploadTemplateRequest = new Amazon.S3.Model.PutObjectRequest()
                {
                    BucketName = Bucket,
                    Key = fileName,
                    ContentType = "text/xml",
                    InputStream = ms,
                    CannedACL = S3CannedACL.PublicRead
                };
                ms.Seek(0, SeekOrigin.Begin);
                AmazonClient.PutObject(uploadTemplateRequest);
                ms.Close();
                return new AmazonResponse { Successful = true, Path = fileName };
            }
            catch (AmazonS3Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = ex.ErrorCode };
            }
            catch (Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = "Unkown error" };
            }
        }


        public AmazonResponse DownloadCampaignTemplate(string campaignTemplateFileName)
        {
            try
            {
                var downloadTemplateRequest = new Amazon.S3.Model.GetObjectRequest()
                {
                    BucketName = Bucket,
                    Key = campaignTemplateFileName
                };
                GetObjectResponse response = AmazonClient.GetObject(downloadTemplateRequest);
                StreamReader sr = new StreamReader(response.ResponseStream);
                return new AmazonResponse { Successful = true, Message = sr.ReadToEnd() };
            }
            catch (AmazonS3Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = ex.ErrorCode };
            }
            catch (Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = "Unkown error" };
            }
        }

        public AmazonResponse UploadMaml(string maml, string campaignName)
        {
            try
            {
                MemoryStream ms = new MemoryStream();
                UTF8Encoding enc = new UTF8Encoding();
                byte[] arrayData = enc.GetBytes(maml);
                ms.Write(arrayData, 0, arrayData.Length);
                string fileName = "Campaigns/" + campaignName + ".maml";
                var uploadMamlRequest = new Amazon.S3.Model.PutObjectRequest()
                {
                    BucketName = Bucket,
                    Key = fileName,
                    ContentType = "text/xml",
                    InputStream = ms,
                    CannedACL = S3CannedACL.PublicRead
                };
                ms.Seek(0, SeekOrigin.Begin);
                AmazonClient.PutObject(uploadMamlRequest);
                ms.Close();
                return new AmazonResponse { Successful = true, Path = fileName };
            }
            catch (AmazonS3Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = ex.ErrorCode };
            }
            catch (Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = "Unkown error" };
            }
        }


        public AmazonResponse DownloadMaml(string maml)
        {
            try
            {
                var downloadMamlRequest = new Amazon.S3.Model.GetObjectRequest()
                {
                    BucketName = Bucket,
                    Key = maml
                };
                var response = AmazonClient.GetObject(downloadMamlRequest);
                StreamReader sr = new StreamReader(response.ResponseStream);
                return new AmazonResponse { Successful = true, Message = sr.ReadToEnd() };
            }
            catch (AmazonS3Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = ex.ErrorCode };
            }
            catch (Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = "Unkown error" };
            }
        }

        public AmazonResponse DeleteMaml(string fileName)
        {
            try
            {
                var deleteMamlRequest = new Amazon.S3.Model.DeleteObjectRequest()
                {
                    BucketName = Bucket,
                    Key = fileName
                };
                AmazonClient.DeleteObject(deleteMamlRequest);
                return new AmazonResponse { Successful = true, Path = fileName };
            }
            catch (AmazonS3Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = ex.ErrorCode };
            }
            catch (Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = "Unkown error" };
            }
        }

        private AmazonResponse UploadImage(string bucketName, HttpPostedFileBase image, string fileName)
        {
            try
            {
                var uploadImageRequest = new Amazon.S3.Model.PutObjectRequest()
                {
                    BucketName = bucketName,
                    Key = fileName,
                    ContentType = image.ContentType,
                    InputStream = image.InputStream,
                    CannedACL = S3CannedACL.PublicRead
                };
                AmazonClient.PutObject(uploadImageRequest);
                return new AmazonResponse { Successful = true, Path = fileName };
            }
            catch (AmazonS3Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = ex.ErrorCode };
            }
            catch (Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = "Unkown error" };
            }
        }

        public AmazonResponse UploadProfileImage(HttpPostedFileBase image)
        {
            string filename = Guid.NewGuid().ToString("N") + Path.GetExtension(image.FileName);
            return UploadImage(Bucket, image, "AdvisorFiles/ProfilePhotos/" + filename);
        }

        public AmazonResponse UploadSignatureImage(HttpPostedFileBase image)
        {
            string filename = Guid.NewGuid().ToString("N") + Path.GetExtension(image.FileName);
            return UploadImage(Bucket, image, "AdvisorFiles/SignatureImages/" + filename);
        }

        public AmazonResponse UploadCompanyLogoImage(HttpPostedFileBase image)
        {
            string filename = Guid.NewGuid().ToString("N") + Path.GetExtension(image.FileName);
            return UploadImage(Bucket, image, "AdvisorFiles/CompanyLogos/" + filename);
        }

        private AmazonResponse DeleteImage(string bucketName, string fileName)
        {
            try
            {
                var deleteImageRequest = new Amazon.S3.Model.DeleteObjectRequest()
                {
                    BucketName = bucketName,
                    Key = fileName
                };
                AmazonClient.DeleteObject(deleteImageRequest);
                return new AmazonResponse { Successful = true, Path = fileName };
            }
            catch (AmazonS3Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = ex.ErrorCode };
            }
            catch (Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = "Unkown error" };
            }
        }


        public AmazonResponse DeleteProfileImage(string imageName)
        {
            return DeleteImage(Bucket, imageName);
        }

        public AmazonResponse DeleteSignatureImage(string imageName)
        {
            return DeleteImage(Bucket, imageName);
        }

        public AmazonResponse DeleteCompanyLogoImage(string imageName)
        {
            return DeleteImage(Bucket, imageName);
        }

        public AmazonResponse UploadPdf(MemoryStream file, string fileName)
        {
            try
            {
                MemoryStream mStream = new MemoryStream();
                file.WriteTo(mStream);
                mStream.Seek(0, SeekOrigin.Begin);
                fileName = "Pdf/" + fileName + ".pdf";
                var uploadPdfRequest = new Amazon.S3.Model.PutObjectRequest()
                {
                    BucketName = Bucket,
                    Key = fileName,
                    ContentType = "application/pdf",
                    InputStream = mStream,
                    CannedACL = S3CannedACL.PublicRead
                };
                mStream.Seek(0, SeekOrigin.Begin);
                AmazonClient.PutObject(uploadPdfRequest);
                mStream.Close();
                return new AmazonResponse { Successful = true, Path = "https://mycmo.s3.amazonaws.com/" + fileName };
            }
            catch (AmazonS3Exception ex)
            {
                return new AmazonResponse { Successful = false, Message = ex.ErrorCode };
            }
            catch (Exception ex)
            { 
                        return new AmazonResponse { Successful = false, Message = ex.Message };
            }
        }
    }

    public class AmazonResponse
    {
        public string Path { get; set; }
        public string Message { get; set; }
        public bool Successful { get; set; }
    }
}