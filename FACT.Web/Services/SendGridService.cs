﻿using RazorEngine;
using RazorEngine.Templating;
using SendGrid;
using SendGrid.Helpers.Mail;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Web;

namespace FACT.Web.Services
{
    public class SendGridService
    {
        private SendGridClient Client;
        private EmailAddress Sender;
        private EmailAddress Recipient;
        private string HtmlContent;
        private string Subject;

        public void SetSender(string email, string name = null)
        {
            Sender = new EmailAddress(email, name);
        }

        public void SetRecipient(string email, string name = null)
        {
            Recipient = new EmailAddress(email, name);
        }

        public void SetContent(bool templateExists, string templateVirtualPath, Object parameters, string title)
        {
            if (!templateExists)
            {
                HtmlContent = templateVirtualPath;
            } else
            {
                var path = HttpContext.Current.Server.MapPath(templateVirtualPath);
                var template = System.IO.File.ReadAllText(path);
                HtmlContent = Engine.Razor.RunCompile(template, path, null, parameters);
            }
            Subject = title;
        }

        public SendGridService()
        {
            var apiKey = ConfigurationManager.AppSettings["SendGridApiKey"];
            Client = new SendGridClient(apiKey);
        }

        public async Task<Response> Send()
        {
            var msg = MailHelper.CreateSingleEmail(Sender, Recipient, Subject, null, HtmlContent);
            return await Client.SendEmailAsync(msg);
        }
    }
}