﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace FACT.Web.Enums
{
    public enum IAType
    {
        [Required]
        [Display(Name = "Registered Representative")]
        NONE,

        [Display(Name = "Corporate IA")]
        CRIA,

        [Display(Name = "Dual IA")]
        Dual,

        [Display(Name = "Independent IA")]
        IRIA
    }
}