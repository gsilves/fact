﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.Enums
{
    public enum PersonaType
    {
        Individual,
        Company
    }
}