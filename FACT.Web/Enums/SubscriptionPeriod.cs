﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace FACT.Web.Enums
{
    public enum SubscriptionPeriod
    {
        Monthly,
        Yearly,
        Once
    }
}