﻿using FACT.Domain.IRepositories;
using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Repository.Repositories
{
    public class CtSubscriptionctCampaignsRepository : Repository<CtSubscriptionctCampaigns>, IctSubscriptionctCampaignsRepository
    {
        public CtSubscriptionctCampaignsRepository(FormsContext context) : base(context)
        {
        }
    }
}
