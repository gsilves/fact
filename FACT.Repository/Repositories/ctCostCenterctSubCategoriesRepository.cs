﻿using FACT.Domain.IRepositories;
using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Repository.Repositories
{
    public class CtCostCenterctSubCategoriesRepository : Repository<CtCostCenterctSubCategories>, IctCostCenterctSubCategoriesRepository
    {
        public CtCostCenterctSubCategoriesRepository(FormsContext context) : base(context)
        {
        }
    }
}
