﻿using FACT.Domain;
using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Repository
{
    public abstract class Repository<T> : IRepository<T> where T : BaseEntity
    {
        protected FormsContext _entities;
        protected readonly IDbSet<T> _dbset;

        public Repository(FormsContext context)
        {
            _entities = context;
            _dbset = context.Set<T>();
        }

        /// <summary>
        /// adds a record to the entity
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Add(T entity)
        {
            _dbset.Add(entity);
        }

        /// <summary>
        /// deletes a record from the entity
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Delete(T entity)
        {
            _dbset.Remove(entity);
        }

        /// <summary>
        /// returns an IEnumerable of all records
        /// </summary>
        /// <returns></returns>
        public virtual IEnumerable<T> FindAll()
        {
            return _dbset.AsEnumerable<T>();
        }

        /// <summary>
        /// returns an IEnumerable of all records inside where scope
        /// </summary>
        /// <param name="predicate"></param>
        /// <returns></returns>
        public IEnumerable<T> FindById(Expression<Func<T, bool>> predicate)
        {
            IEnumerable<T> query = _dbset.Where(predicate).AsEnumerable();
            return query;
        }

        /// <summary>
        /// updates a record in the entity
        /// </summary>
        /// <param name="entity"></param>
        public virtual void Update(T entity)
        {
            _entities.Entry(entity).State = System.Data.Entity.EntityState.Modified;
        }

        /// <summary>
        /// saves all changes performed
        /// </summary>
        public virtual void Save()
        {
            _entities.SaveChanges();
        }
    }
}
