﻿using FACT.Domain.Models.EntityModels;
using FACT.Repository.Configuration;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Repository
{
    public partial class FormsContext : BaseFormsContext<FormsContext>
    {
        public DbSet<AspNetRoles> AspNetRoles { get; set; }
        public DbSet<AspNetUserClaims> AspNetUserClaims { get; set; }
        public DbSet<AspNetUserLogins> AspNetUserLogins { get; set; }
        public DbSet<AspNetUserRoles> AspNetUserRoles { get; set; }
        public DbSet<AspNetUsers> AspNetUsers { get; set; }
        public DbSet<Campaigns> Campaigns { get; set; }
        public DbSet<CtCampaigns> CtCampaigns { get; set; }
        public DbSet<CtCampaignTemplates> CtCampaignTemplates { get; set; }
        public DbSet<CtCategories> CtCategories { get; set; }
        public DbSet<CtCostCenterctCategories> CtCostCenterctCategories { get; set; }
        public DbSet<CtCostCenterctSubCategories> CtCostCenterctSubCategories { get; set; }
        public DbSet<CtCostCenters> CtCostCenters { get; set; }
        public DbSet<CtSubCategories> CtSubCategories { get; set; }
        public DbSet<CtSubscriptionctCampaigns> CtSubscriptionctCampaigns { get; set; }
        public DbSet<CtSubscriptions> CtSubscriptions { get; set; }
        public DbSet<Delegates> Delegates { get; set; }
        public DbSet<Disclosures> Disclosures { get; set; }
        public DbSet<EmailDomains> EmailDomains { get; set; }
        public DbSet<ErrorModels> ErrorModels { get; set; }
        public DbSet<EventDetails> EventDetails { get; set; }
        public DbSet<ExpirationNotifications> ExpirationNotifications { get; set; }
        public DbSet<Personas> Personas { get; set; }
        public DbSet<Profiles> Profiles { get; set; }
        public DbSet<Settings> Settings { get; set; }
        public DbSet<SignatureBlocks> SignatureBlocks { get; set; }
        public DbSet<Subscriptions> Subscriptions { get; set; }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Configurations.Add(new AspNetUserLoginsConfiguration());
            modelBuilder.Configurations.Add(new CampaignsConfiguration());
            modelBuilder.Configurations.Add(new CtCostCenterctCategoriesConfiguration());
            modelBuilder.Configurations.Add(new CtCostCenterctSubCategoriesConfiguration());
            modelBuilder.Configurations.Add(new CtSubscriptionctCampaignsConfiguration());
            modelBuilder.Configurations.Add(new ProfilesConfiguration());
        }

        public override int SaveChanges()
        {
            return base.SaveChanges();
        }
    }
}
