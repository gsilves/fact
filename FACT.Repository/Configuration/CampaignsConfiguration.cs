﻿using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Repository.Configuration
{
    public class CampaignsConfiguration : EntityTypeConfiguration<Campaigns>
    {
        public CampaignsConfiguration()
        {
            ToTable("Campaigns");
            Property(g => g.Id).HasColumnName("Campaign_Id");
        }
    }
}
