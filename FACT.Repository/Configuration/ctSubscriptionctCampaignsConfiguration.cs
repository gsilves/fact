﻿using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Repository.Configuration
{
    public class CtSubscriptionctCampaignsConfiguration : EntityTypeConfiguration<CtSubscriptionctCampaigns>
    {
        public CtSubscriptionctCampaignsConfiguration()
        {
            ToTable("ctSubscriptionctCampaigns");
            Property(g => g.Id).HasColumnName("ctSubscription_Id").HasMaxLength(128);
        }
    }
}
