﻿using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Repository.Configuration
{
    public class CtCostCenterctCategoriesConfiguration : EntityTypeConfiguration<CtCostCenterctCategories>
    {
        public CtCostCenterctCategoriesConfiguration()
        {
            ToTable("ctCostCenterctCategories");
            Property(g => g.Id).HasColumnName("ctCostCenter_Id").HasMaxLength(128);
        }
    }
}
