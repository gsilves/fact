﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Repository
{
    public class UnitOfWork : IUnitOfWork
    {
        /// <summary>
        /// The DbContent
        /// </summary>
        private FormsContext _dbContext;

        /// <summary>
        /// Initializes a new instance of the UnitOfWork Class
        /// </summary>
        /// <param name="context">The object context</param>
        public UnitOfWork(FormsContext context)
        {
            _dbContext = context;
        }

        /// <summary>
        /// Save all pending changes
        /// </summary>
        /// <returns>The number of objects in an Added, Modified, or Deleted state</returns>
        public int Commit()
        {
            //save changes with the default options
            return _dbContext.SaveChanges();
        }

        /// <summary>
        /// Disposes the current object
        /// </summary>
        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        /// <summary>
        /// Disposes all external resources
        /// </summary>
        /// <param name="disposing">The dispose indicator</param>
        private void Dispose(bool disposing)
        {
            if (disposing)
            {
                if (_dbContext != null)
                {
                    _dbContext.Dispose();
                    _dbContext = null;
                }
            }
        }
    }
}
