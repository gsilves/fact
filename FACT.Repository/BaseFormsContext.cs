﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Repository
{
    public abstract class BaseFormsContext<TContext> : DbContext where TContext : DbContext
    {
        static BaseFormsContext()
        {
            Database.SetInitializer<TContext>(null);
        }

        protected BaseFormsContext() : base(ConfigurationManager.ConnectionStrings["DefaultConnection"].ConnectionString)
        {
        }
    }
}
