﻿using FACT.Domain.IRepositories;
using FACT.Domain.Models.EntityModels;
using FACT.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Service
{
    public class FactMainService : IFactMainService
    {
        private readonly IErrorModelsRepository _errorModelsRepository;
        private readonly IEmailDomainsRepository _emailDomainsRepository;
        private readonly IProfilesRepository _profilesRepository;
        private readonly IUnitOfWork _uow;
        private readonly ICampaignsRepository _campaignsRepository;
        private readonly IPersonasRepository _personasRepository;
        private readonly IDisclosuresRepository _disclosuresRepository;
        private readonly IDelegatesRepository _delegatesRepository;
        private readonly ISubscriptionsRepository _subscriptionsRepository;
        private readonly ISettingsRepository _settingsRepository;

        public FactMainService(IErrorModelsRepository ErrorModelsRepository, IUnitOfWork uow, IEmailDomainsRepository emailDomainsRepository,
            IProfilesRepository profilesRepository, ICampaignsRepository campaignsRepository, IPersonasRepository personasRepository, 
            IDisclosuresRepository disclosuresRepository, IDelegatesRepository delegatesRepository, ISubscriptionsRepository subscriptionsRepository, 
            ISettingsRepository settingsRepository)
        {
            _errorModelsRepository = ErrorModelsRepository;
            _uow = uow;
            _emailDomainsRepository = emailDomainsRepository;
            _profilesRepository = profilesRepository;
            _campaignsRepository = campaignsRepository;
            _personasRepository = personasRepository;
            _disclosuresRepository = disclosuresRepository;
            _delegatesRepository = delegatesRepository;
            _subscriptionsRepository = subscriptionsRepository;
            _settingsRepository = settingsRepository;
        }

        #region Campaigns
        public void AddCampaigns(Campaigns model)
        {
            _campaignsRepository.Add(model);
            _uow.Commit();
        }

        public IList<Campaigns> FindAllCampaigns()
        {
            return _campaignsRepository.FindAll().ToList();
        }

        public void UpdateCampaigns(Campaigns model)
        {
            _campaignsRepository.Update(model);
            _uow.Commit();
        }
        #endregion

        #region Delegates
        public List<Delegates> FindAllDelegates()
        {
            return _delegatesRepository.FindAll().ToList();
        }

        public Delegates FindDelegatesById(string id)
        {
            var qry = _delegatesRepository.FindById(x => x.Id == id).FirstOrDefault();
            return qry;
        }
        #endregion

        #region Disclosures
        public List<Disclosures> FindAllDisclosures()
        {
            var qry = _disclosuresRepository.FindAll();
            return qry.ToList();
        }
        #endregion

        #region EmailDomains
        public IList<EmailDomains> FindAllEmailDomains()
        {
            return _emailDomainsRepository.FindAll().ToList();
        }
        #endregion EmailDomians

        #region ErrorModels
        public void AddErrorModel(ErrorModels model)
        {
            _errorModelsRepository.Add(model);
            _uow.Commit();
        }

        public IList<ErrorModels> FindAllErrorModels()
        {
            var model = _errorModelsRepository.FindAll();
            return model.ToList();
        }

        public ErrorModels FindErrorModelById(int id)
        {
            var qry = _errorModelsRepository.FindById(x => x.Id == id).FirstOrDefault();
            return qry;
        }

        public void DeleteErrorModels(ErrorModels model)
        {
            _errorModelsRepository.Delete(model);
            _uow.Commit();
        }

        public void UpdateErrorModels(ErrorModels model)
        {
            _errorModelsRepository.Update(model);
            _uow.Commit();
        }
        #endregion ErrorModels

        #region Personas
        public void AddPersona(Personas model)
        {
            _personasRepository.Add(model);
            _uow.Commit();
        }

        public void DeletePersonas(Personas model)
        {
            _personasRepository.Delete(model);
            _uow.Commit();
        }

        public List<Personas> FindAllPersonas()
        {
            return _personasRepository.FindAll().ToList();
        }

        public Personas FindPersonasById(string id)
        {
            var qry = _personasRepository.FindById(x => x.Id == id).FirstOrDefault();
            return qry;
        }

        public void UpdatePersonas(Personas model)
        {
            _personasRepository.Update(model);
            _uow.Commit();
        }
        #endregion

        #region Profiles
        public List<Profiles> FindAllProfiles()
        {
            return _profilesRepository.FindAll().ToList();
        }

        public void AddProfile(Profiles model)
        {
            _profilesRepository.Add(model);
            _uow.Commit();
        }

        public void DeleteProfile(Profiles model)
        {
            _profilesRepository.Delete(model);
            _uow.Commit();
        }

        public Profiles FindProfileById(long id)
        {
            var qry = _profilesRepository.FindById(x => x.Id == id).FirstOrDefault();
            return qry;
        }

        public void UpdateProfiles(Profiles model)
        {
            _profilesRepository.Update(model);
            _uow.Commit();
        }
        #endregion Profiles

        #region Settings
        public List<Settings> FindAllSettings()
        {
            return _settingsRepository.FindAll().ToList();
        }
        #endregion

        #region Subscriptions
        public List<Subscriptions> FindAllSubscriptions()
        {
            return _subscriptionsRepository.FindAll().ToList();
        }
        #endregion
    }
}
