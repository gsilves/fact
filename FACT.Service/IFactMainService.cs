﻿using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Service
{
    public interface IFactMainService
    {
        #region Campaigns
        void AddCampaigns(Campaigns model);
        IList<Campaigns> FindAllCampaigns();
        void UpdateCampaigns(Campaigns model);
        #endregion

        #region Delegates
        List<Delegates> FindAllDelegates();
        Delegates FindDelegatesById(string id);
        #endregion

        #region Disclosures
        List<Disclosures> FindAllDisclosures();
        #endregion

        #region EmailDomains
        IList<EmailDomains> FindAllEmailDomains();
        #endregion EmailDomains

        #region ErrorModels
        void AddErrorModel(ErrorModels model);
        IList<ErrorModels> FindAllErrorModels();
        ErrorModels FindErrorModelById(int id);
        void DeleteErrorModels(ErrorModels model);
        void UpdateErrorModels(ErrorModels model);
        #endregion ErrorModels

        #region Personas
        void AddPersona(Personas model);
        void DeletePersonas(Personas model);
        List<Personas> FindAllPersonas();
        Personas FindPersonasById(string id);
        void UpdatePersonas(Personas model);
        #endregion

        #region Profiles
        List<Profiles> FindAllProfiles();
        void AddProfile(Profiles model);
        void DeleteProfile(Profiles model);
        Profiles FindProfileById(long id);
        void UpdateProfiles(Profiles model);
        #endregion Profiles

        #region Settings
        List<Settings> FindAllSettings();
        #endregion

        #region Subscriptions
        List<Subscriptions> FindAllSubscriptions();
        #endregion
    }
}
