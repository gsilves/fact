﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Model
{
    public class Settings
    {
        public long Id { get; set; }
        public bool ApprovalRequired { get; set; }
        public bool IsCreditChargeable { get; set; }
        public bool IsSubscriptionSignable { get; set; }
    }
}
