﻿using FACT.Domain.Models.EntityModels;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.IRepositories
{
    public interface IEventDetailsRepository : IRepository<EventDetails>
    {
    }
}
