﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain
{
    public interface IRepository<T> where T : BaseEntity
    {
        IEnumerable<T> FindAll();
        void Add(T entity);
        void Delete(T entity);
        void Update(T entity);
        IEnumerable<T> FindById(Expression<Func<T, bool>> predicate);
    }
}
