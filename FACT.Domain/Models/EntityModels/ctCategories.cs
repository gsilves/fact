﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("ctCategories")]
    public partial class CtCategories : Entity<string>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string ThumbNail { get; set; }
    }
}
