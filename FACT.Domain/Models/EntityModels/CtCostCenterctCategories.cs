﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("ctCostCenterctCategories")]
    public partial class CtCostCenterctCategories : Entity<string>
    {
        [Required]
        public string CtCategory_Id { get; set; }
    }
}
