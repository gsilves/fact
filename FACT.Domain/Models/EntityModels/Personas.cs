﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace FACT.Domain.Models.EntityModels
{
    [Table("Personas")]
    public partial class Personas : Entity<string>
    {
        [MaxLength(128)]
        public string UserId { get; set; }

        [Required]
        public Int32 PersonaType { get; set; }

        [MaxLength(50)]
        public string FirstName { get; set; }

        [MaxLength(50)]
        public string LastName { get; set; }

        [MaxLength(100)]
        public string Email { get; set; }

        public Int32? JobTitle { get; set; }

        [MaxLength(50)]
        public string Website { get; set; }

        [MaxLength(15)]
        public string PhoneNumber { get; set; }

        [MaxLength(15)]
        public string MobilePhone { get; set; }

        [MaxLength(50)]
        public string Company { get; set; }

        [MaxLength(6)]
        public string MastheadColor { get; set; }

        [MaxLength(255)]
        public string SignatureImage { get; set; }

        [MaxLength(255)]
        public string ProfilePhoto { get; set; }

        [MaxLength(255)]
        public string CompanyLogo { get; set; }

        [Required]
        [MaxLength(50)]
        public string Address { get; set; }

        [Required]
        [MaxLength(50)]
        public string City { get; set; }

        [Required]
        public Int32? State { get; set; }

        [Required]
        [MaxLength(12)]
        public string Zip { get; set; }

        public Int32 Designation1 { get; set; }

        public Int32 Designation2 { get; set; }

        public Int32 Designation3 { get; set; }

        public Int32 Designation4 { get; set; }

        [Required]
        public bool emailSPFApproved { get; set; }

        public string BDID { get; set; }

        public string ApprovedDBAName { get; set; }

        public string BusinessRegistrationType { get; set; }

        public string TeamName { get; set; }

        public string AGUserId { get; set; }

        public int IAType { get; set; }

        public string ApprovedRIAName { get; set; }

        public string OAConnectionToken { get; set; }

        public string OAUserToken { get; set; }

        [Required]
        public bool DefaultPersona { get; set; }

        public string DisclosureText { get; set; }

        public string Name { get; set; }

        [NotMapped]
        public HttpPostedFileBase SignatureImageFile { get; set; }

        [NotMapped]
        public HttpPostedFileBase ProfilePhotoFile { get; set; }

        [NotMapped]
        public HttpPostedFileBase CompanyLogoFile { get; set; }

        [NotMapped]
        public Boolean SignatureImageDelete { get; set; }

        [NotMapped]
        public Boolean ProfilePhotoDelete { get; set; }

        [NotMapped]
        public Boolean CompanyLogoDelete { get; set; }

        public string FacebookURL { get; set; }

        public string LinkedInURL { get; set; }

        public string TwitterURL { get; set; }

        public string YouTubeURL { get; set; }
    }
}
