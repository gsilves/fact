﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("SignatureBlocks")]
    public partial class SignatureBlocks : Entity<int>
    {
        [Required]
        [MaxLength(50)]
        public string Title { get; set; }

        [Required]
        public string HTML { get; set; }
    }
}
