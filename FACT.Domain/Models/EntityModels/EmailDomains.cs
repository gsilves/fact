﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("EmailDomains")]
    public partial class EmailDomains : Entity<int>
    {
        [Required]
        public string Domain { get; set; }

        [Required]
        public bool isBlacklisted { get; set; }
    }
}
