﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using FACT.ExtendFramework;

namespace FACT.Domain.Models
{
    public class MyContactsModel
    {
        public string AccountID { get; set; } 
        public string Prefix { get; set; }

        [Required]
        [Display(Name="First Name")]
        public string FirstName { get; set; }

        [Required]
        [Display(Name="Last Name")]
        public string LastName { get; set; }

        [Display(Name="Middle Name")]
        public string MiddleName { get; set; } 
        public string Suffix { get; set; }

        [DataType(DataType.EmailAddress)]
        public string Email { get; set; } 
        public string Company { get; set; } 
        public string Title { get; set; } 
        public string Phone { get; set; } 
        public string Country { get; set; } 
        public string Website { get; set; } 
        
        [Display(Name="Facebook Account")]
        public string FacebookAccount { get; set; } 
        [Display(Name="LinkedIn Account")]
        public string LinkedInAccount { get; set; } 
        [Display(Name="Twitter Account")]
        public string TwitterAccount { get; set; } 
        public Nullable<DateTime> Anniversary { get; set; }

        [Display(Name="Birth Date")]
        public Nullable<DateTime> BirthDate { get; set; } 
        public string Gender { get; set; } 
        public string Extension { get; set; } 
        public string Mobile { get; set; } 
        public string Fax { get; set; } 
        public string Address1 { get; set; } 
        public string Address2 { get; set; } 
        public string City { get; set; } 
        public string State { get; set; } 
        public string Zip { get; set; } 
        public string ZipPlus4 { get; set; } 
        public string Password { get; set; } 
        public string PhotoURL { get; set; }

        [Display(Name="Delivery Point Indicator")]
        public string DeliveryPointIndicator { get; set; } 
        public int Id { get; set; }

        [Display(Name="Group Name")]
        public string GroupName { get; set; }

        [Display(Name="Contact Source")]
        public string ContactSource { get; set; } 
        public string PURL { get; set; } 
    }
}
