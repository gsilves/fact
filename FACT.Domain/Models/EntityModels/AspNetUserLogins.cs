﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("AspNetUserLogins")]
    public partial class AspNetUserLogins : Entity<string>
    {
        [Required]
        [MaxLength(128)]
        public string ProviderKey { get; set; }

        [Required]
        [MaxLength(128)]
        public string UserId { get; set; }
    }
}
