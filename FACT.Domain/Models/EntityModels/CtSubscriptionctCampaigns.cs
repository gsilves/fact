﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("ctSubscriptionctCampaigns")]
    public partial class CtSubscriptionctCampaigns : Entity<string>
    {
        [Required]
        public string CtCampaign_Id { get; set; }
    }
}
