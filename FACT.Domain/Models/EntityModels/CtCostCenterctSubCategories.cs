﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("ctCostCenterctSubCategories")]
    public partial class CtCostCenterctSubCategories : Entity<string>
    {
        [Required]
        public string CtSubCategory_Id { get; set; }
    }
}
