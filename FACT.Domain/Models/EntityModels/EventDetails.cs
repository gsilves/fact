﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("EventDetails")]
    public partial class EventDetails : Entity<string>
    {
        [Required]
        public DateTime EventDate { get; set; }

        [Required]
        public DateTime EventTime { get; set; }

        public string EventLocation { get; set; }

        public string EventAddress1 { get; set; }

        public string EventAddress2 { get; set; }

        public string EventCity { get; set; }

        public string EventState { get; set; }

        public string EventZip { get; set; }

        [Required]
        public string Campaign_Id { get; set; }
    }
}
