﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("AspNetUsers")]
    public partial class AspNetUsers : Entity<string>
    {
        public Int32 AuthorizeNetId { get; set; }

        [MaxLength(256)]
        public string Email { get; set; }

        [Required]
        public bool EmailConfirmed { get; set; }

        public string PasswordHash { get; set; }

        public string SecurityStamp { get; set; }

        public string PhoneNumber { get; set; }

        [Required]
        public bool PhoneNumberConfirmed { get; set; }

        [Required]
        public bool TwoFactorEnabled { get; set; }

        public DateTime LockoutEndDateUtc { get; set; }

        [Required]
        public bool LockoutEnabled { get; set; }

        [Required]
        public Int32 AccessFailedCount { get; set; }

        [Required]
        [MaxLength(256)]
        public string UserName { get; set; }

        public DateTime Created { get; set; }

        public bool Active { get; set; }
    }
}
