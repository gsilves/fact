﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("Disclosures")]
    public partial class Disclosures : Entity<string>
    {
        public string DisclosureText { get; set; }

        public string UserType { get; set; }

        [Required]
        public Int32 IAType { get; set; }

        [Required]
        public bool isVariable { get; set; }

        [Required]
        public Int32 State { get; set; }
    }
}
