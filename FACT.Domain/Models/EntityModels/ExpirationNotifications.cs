﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("ExpirationNotifications")]
    public partial class ExpirationNotifications : Entity<int>
    {
        [Required]
        public Int32 CardId { get; set; }
    }
}
