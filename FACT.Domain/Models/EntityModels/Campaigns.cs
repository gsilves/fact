﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("Campaigns")]
    public partial class Campaigns : Entity<long>
    {
        [Required]
        public DateTime LaunchDate { get; set; }

        public string Status { get; set; }

        public string CampaignFileURL { get; set; }

        [MaxLength(128)]
        public string UserId { get; set; }

        public string CampaignName { get; set; }

        [Required]
        public DateTime DateSubmitted { get; set; }

        public string ApprovalToken { get; set; }

        public string SFCaseObjectId { get; set; }

        public Int32 PersonaId { get; set; }

        public bool IsEdit { get; set; }

        [Required]
        public Int32 CampaignType { get; set; }
    }
}
