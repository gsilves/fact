﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("ctCampaigns")]
    public partial class CtCampaigns : Entity<string>
    {
        public string Name { get; set; }

        public string Description { get; set; }

        public string ThumbNail { get; set; }

        public string Maml { get; set; }

        public string Mode { get; set; }

        public string Feed { get; set; }

        public string Filter { get; set; }

        [Required]
        public string CtSubCategoryId { get; set; }

        [Required]
        public Int32 CampaignType_EventOrStandard { get; set; }
    }
}
