﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace FACT.Domain.Models.EntityModels
{
    [Table("Profiles")]
    public partial class Profiles : Entity<long>
    {
        [MaxLength(128)]
        public string UserId { get; set; }

        public string AGUserId { get; set; }

        public string UserType { get; set; }

        public string BDID { get; set; }

        public int? IAType { get; set; }

        public string BusinessType { get; set; }

        public string UserName { get; set; }

        public string FirstName { get; set; }

        public string Gender { get; set; }

        public string MiddleName { get; set; }

        public string LastName { get; set; }

        public string Suffix { get; set; }

        public string Email { get; set; }

        public string RepCode { get; set; }

        public string StateSecuritiesRegistration { get; set; }

        public string StateIARegistrations { get; set; }

        public string ExamsPassed { get; set; }

        public string Status { get; set; }

        public string ARInsuranceNumber { get; set; }

        [MaxLength(6)]
        public string MastheadColor { get; set; }

        public string MailingAddressLine1 { get; set; }

        public string MailingAddressLine2 { get; set; }

        public string MailingAddressLine3 { get; set; }

        public string MailingCity { get; set; }

        public string MailingState { get; set; }

        public string MailingZip { get; set; }

        public string ApprovedDBAName { get; set; }

        public string ApprovedRIAName { get; set; }

        public string BusinessRegistrationType { get; set; }

        public Int32 Designation1 { get; set; }

        public Int32 Designation2 { get; set; }

        public Int32 Designation3 { get; set; }

        public Int32 Designation4 { get; set; }

        public string Company { get; set; }

        public int? JobTitle { get; set; }

        public string MFParentAccountID { get; set; }

        public string MFAccountID { get; set; }

        public string MFUserID { get; set; }

        public string MFDomain { get; set; }

        public string MFTicket { get; set; }

        public string MFMasterPartnerTicket { get; set; }

        public string MFUserToken { get; set; }

        public string MFReportingToken { get; set; }

        public string PrimeCell { get; set; }

        public string PrimePhone { get; set; }

        public string PrimeFaxNumber { get; set; }

        public string OAConnectionToken { get; set; }

        public string OAUserToken { get; set; }

        public string ConfirmPassword { get; set; }

        public string ExternalID { get; set; }

        public string Branch { get; set; }

        public string BranchName { get; set; }

        public string BranchAddressLine1 { get; set; }

        public string BranchAddressLine2 { get; set; }

        public string BranchAddressLine3 { get; set; }

        public string BranchCity { get; set; }

        public string BranchState { get; set; }

        public string BranchZip { get; set; }

        public string BranchPhone { get; set; }

        public string BranchFaxNumber { get; set; }

        public string RedtailAPIKey { get; set; }

        public string RedtailPassword { get; set; }

        public string RedtailUsername { get; set; }

        public string SalesforceAuthToken { get; set; }

        public string SalesforceInstance { get; set; }

        public string SalesforceRefreshToken { get; set; }

        public string OSJRepCode { get; set; }

        public string OSJName { get; set; }

        public string SupervisorRepCode { get; set; }

        public string SupervisorName { get; set; }

        public string TCAgreeDate { get; set; }

        public string TermsAndConditions { get; set; }

        public string Website { get; set; }

        public string PlanID { get; set; }

        [MaxLength(255)]
        public string SignatureImage { get; set; }

        [MaxLength(255)]
        public string ProfilePhoto { get; set; }

        [MaxLength(255)]
        public string CompanyLogo { get; set; }

        [Required]
        public bool emailSPFApproved { get; set; }

        public string DisclosureText { get; set; }

        public string FacebookURL { get; set; }

        public string LinkedInURL { get; set; }

        public string TwitterURL { get; set; }

        public string YouTubeURL { get; set; }

        public string TimeZoneInfoId { get; set; }

        [NotMapped]
        public SelectList TimeZones { get; set; }

        [Display(Name = "Signature image")]
        [NotMapped]
        public HttpPostedFileBase SignatureImageFile { get; set; }

        [Display(Name = "Profile photo")]
        [NotMapped]
        public HttpPostedFileBase ProfilePhotoFile { get; set; }

        [Display(Name = "Company logo")]
        [NotMapped]
        public HttpPostedFileBase CompanyLogoFile { get; set; }

            //if ModACTAlgorithm is true, we are using ModACT Algorithm for generating MFPassword.
        public string GetMFPassword(bool ModACTAlgorithm)
        {
            if ((ModACTAlgorithm == false) && (!ModACTAlgorithm))
            {
                return AGUserId.ToLower() + RepCode + "_Connect9";
            }
            else
            {
                if (Email == "jfroehlke@rangedelivers.com")
                {
                    return "LPLConnect101!";
                }
                else
                {
                    return UserName + "_Connect101";
                }
            }
        }
    }
}
