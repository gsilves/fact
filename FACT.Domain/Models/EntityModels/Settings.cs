﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("Settings")]
    public partial class Settings : Entity<int>
    {
        public bool ApprovalRequired { get; set; }
        public bool IsCreditChargeable { get; set; }
        public bool IsSubscriptionSignable { get; set; }
    }
}
