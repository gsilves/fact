﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("AspNetUserClaims")]
    public partial class AspNetUserClaims : Entity<int>
    {
        [Required]
        [MaxLength(128)]
        public string UserId { get; set; }

        public string ClaimType { get; set; }

        public string ClaimValue { get; set; }
    }
}
