﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("AspNetRoles")]
    public partial class AspNetRoles : Entity<string>
    {
        [Required]
        [MaxLength(256)]
        public string Name { get; set; }
    }
}
