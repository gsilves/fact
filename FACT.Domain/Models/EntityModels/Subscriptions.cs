﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("Subscriptions")]
    public partial class Subscriptions : Entity<int>
    {
        [Required]
        public string CustomerProfileId { get; set; }

        [Required]
        [MaxLength(20)]
        public string TransactionId { get; set; }

        [Required]
        public DateTime SubmitTimeUTC { get; set; }

        [Required]
        public Int32 Status { get; set; }

        [Required]
        public Decimal Amount { get; set; }

        [Required]
        public Int32 Duration { get; set; }
    }
}
