﻿using FACT.Domain.Models.MiscModels;
using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("Delegates")]
    public partial class Delegates : Entity<string>
    {
        public Delegates()
        {
            DateAdded = DateTime.Now;
        }

        [Required]
        [MaxLength(128)]
        public string AccountId { get; set; }

        public ApplicationUser Account { get; set; }

        [Required]
        [MaxLength(128)]
        [ForeignKey("Parent")]
        public string ParentId { get; set; }

        public ApplicationUser Parent { get; set; }

        public string FirstName { get; set; }

        public string LastName { get; set; }

        [Required]
        public DateTime DateAdded { get; set; }
    }
}
