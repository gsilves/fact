﻿using FACT.ExtendFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.EntityModels
{
    [Table("ErrorModels")]
    public partial class ErrorModels : Entity<int>
    {
        public string Title { get; set; }

        public string Message { get; set; }

        public string Type { get; set; }

        [Required]
        public DateTime Timestamp { get; set; }
    }
}
