﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models
{
    public class MessageError
    {
        public string ErrorText { get; set; }
    }
}
