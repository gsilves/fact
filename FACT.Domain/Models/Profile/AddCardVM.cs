﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.Profile
{
    public class AddCardVM
    {
        // Card information
        public string CardNumber { get; set; }
        public int? Year { get; set; }
        public int? Month { get; set; }
        public string CCV { get; set; }
        // Billing information
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Address { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string Country { get; set; }
        public string PhoneNumber { get; set; }
        public string FaxNumber { get; set; }
        public string Company { get; set; }
        public string Redirect { get; set; }
    }
}
