﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models
{
    public class MasterMFAccount
    {
        public string Email { get; set; }
        public string PartnerGuid { get; set; }
        public string PartnerPassword { get; set; }
        public string Password { get; set; }
        public string ParentAccountId { get; set; }
        public string AccountId { get; set; }
        public string PartnerTicket { get; set; }
        public string Ticket { get; set; }
        public string UserId { get; set; }
        public string UserToken { get; set; }
    }
}
