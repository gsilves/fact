﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.OneAllModels
{
    public class Response
    {
        public Request request { get; set; }
        public Result result { get; set; }
    }
}
