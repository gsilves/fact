﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.OneAllModels
{
    public class URL
    {
        public string value { get; set; }
        public string type { get; set; }
    }
}
