﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.OneAllModels
{
    public class Identity
    {
        public string identity_token { get; set; }
        public DateTime date_creation { get; set; }
        public DateTime date_last_update { get; set; }
        public string provider { get; set; }
        public string provider_identity_uid { get; set; }
        public Source source { get; set; }
        public string id { get; set; }
        public string displayName { get; set; }
        public Name name { get; set; }
        public string preferredUsername { get; set; }
        public DateTime profileCreated { get; set; }
        public string profileUrl { get; set; }
        public string thumbnailUrl { get; set; }
        public string pictureUrl { get; set; }
        public string currentLocation { get; set; }
        public string aboutMe { get; set; }
        public List<URL> urls { get; set; }
        public List<Account> accounts { get; set; }
        public List<Photo> photos { get; set; }
        public string gender { get; set; }
        public string utcOffset { get; set; }
        public string birthday { get; set; }
        public List<Email> emails { get; set; }
    }
}
