﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.OneAllModels
{
    public class Name
    {
        public string formatted { get; set; }
        public string givenName { get; set; }
        public string familyName { get; set; }
    }
}
