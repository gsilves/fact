﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.OneAllModels
{
    public class Photo
    {
        public string value { get; set; }
        public string size { get; set; }
    }
}
