﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.OneAllModels
{
    public class Source
    {
        public string name { get; set; }
        public string key { get; set; }
        public AccessToken access_token { get; set; }
    }
}
