﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.OneAllModels
{
    public class Request
    {
        public string date { get; set; }
        public string resource { get; set; }
        public Status status { get; set; }
    }
}
