﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.OneAllModels
{
    public class OneAllUser
    {
        public string user_token { get; set; }
        public string date_creation { get; set; }
        public string date_last_login { get; set; }
        public string num_logins { get; set; }
        public List<Identity> identities { get; set; }
    }
}
