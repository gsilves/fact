﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.OneAllModels
{
    public class Account
    {
        public string domain { get; set; }
        public string userid { get; set; }
        public string username { get; set; }
    }
}
