﻿using Newtonsoft.Json;
using System.Collections.Generic;

namespace FACT.Domain.Models.MiscModels
{
    public class RestResponsePost
    {
        [JsonProperty("id")]
        public string Id { get; set; }

        [JsonProperty("errors")]
        public List<string> Errors { get; set; }

        [JsonProperty("success")]
        public bool Success { get; set; }
    }
}
