﻿using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.MiscModels
{
    public class ApplicationUser : IdentityUser
    {
        public ApplicationUser()
        {
            Created = DateTime.Now;
        }

        // We need an additional id field for users to link them to SparkPay customer profiles.
        public virtual int? AuthorizeNetId { get; set; }

        public bool Active { get; set; }

        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            //Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            //Add custom user claims here
            return userIdentity;
        }

        public DateTime? Created { get; set; }
    }
}
