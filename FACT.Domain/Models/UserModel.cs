﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models
{
    public class UserModel
    {
        public string Id { get; set; }
        public string UserOrPersona { get; set; }
        public string Username { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public string Email { get; set; }
        public bool Active { get; set; }
        public bool SPFSetup { get; set; }
        public bool Created { get; set; }
        public string UserType { get; set; }
        public string BDID { get; set; }
        public string BusinessType { get; set; }
        public string Gender { get; set; }
        public string IAType { get; set; }
        public string StateSecuritiesRegistration { get; set; }
        public string RepCode { get; set; }
        public string StateIARegistrations { get; set; }
        public string ExamsPassed { get; set; }
        public string ARInsuranceNumber { get; set; }
        public string MailingAddressLine1 { get; set; }
        public string MailingAddressLine2 { get; set; }
        public string MailingCity { get; set; }
        public string MailingZip { get; set; }
        public string ApprovedDBAName { get; set; }
        public string ApprovedRIAName { get; set; }
        public string BusinessRegistrationType { get; set; }
        public string CompanyLogo { get; set; }
        public string Company { get; set; }
        public string MFAccountID { get; set; }
        public string MFDomain { get; set; }
        public string PrimeCell { get; set; }
        public string PrimePhone { get; set; }
        public string PrimeFaxNumber { get; set; }
        public string Branch { get; set; }
        public string BranchName { get; set; }
        public string BranchAddressLine1 { get; set; }
        public string BranchAddressLine2 { get; set; }
        public string BranchAddressLine3 { get; set; }
        public string BranchCity { get; set; }
        public string BranchState { get; set; }
        public string BranchZip { get; set; }
        public string BranchPhone { get; set; }
        public string BranchFaxNumber { get; set; }
        public string ProfilePhoto { get; set; }
        public string RedtailAPIKey { get; set; }
        public string RedtailPassword { get; set; }
        public string RedtailUserName { get; set; }
        public string SalesForceAuthToken { get; set; }
        public string SalesforceInstance { get; set; }
        public string SalesForceRefreshToken { get; set; }
        public string SignatureImage { get; set; }
        public string OSJRepCode { get; set; }
        public string OSJName { get; set; }
        public string SupervisorRepCode { get; set; }
        public string SupervisorName { get; set; }
        public string TcAgreeDate { get; set; }
        public string TermsAndConditions { get; set; }
        public string WebSite { get; set; }
        public string PlanID { get; set; }
        public string MFTicket { get; set; }
        public string AGUserID { get; set; }
        public string MastheadColor { get; set; }
        public string DisclosureText { get; set; }
        public string emailSPFApproved { get; set; }
        public string FacebookURL { get; set; }
        public string LinkedInURL { get; set; }
        public string TwitterURL { get; set; }
        public string YouTubeURL { get; set; }
        public string TimeZoneInfoId { get; set; }
        public string MFReportingToken { get; set; }
        public string Designation1 { get; set; }
        public string Designation2 { get; set; }
        public string Designation3 { get; set; }
        public string Designation4 { get; set; }
        public string JobTitle { get; set; }
    }
}
