﻿using FACT.Domain.Models.MamlModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models
{
    public class MyCampaignsModel
    {
        [NotMapped]
        public int Id { get; set; }
        [NotMapped]
        [Display(Name = "Campaign Name")]
        public string CampaignName { get; set; }
        [NotMapped]
        [Display(Name = "Date Submitted")]
        public DateTime DateSubmitted { get; set; }
        [NotMapped]
        public string Status { get; set; }
        [NotMapped]
        public string DbStatus { get; set; }
        [NotMapped]
        public string Details { get; set; }
        [NotMapped]
        [Display(Name = "Creator Name")]
        public string CreatorName { get; set; }
        [NotMapped]
        public MamlProgramModel Program { get; set; }
        [NotMapped]
        public string Token { get; set; }
        [NotMapped]
        public string Maml { get; set; }
        [NotMapped]
        public string Url { get; set; }
        [NotMapped]
        public int State { get; set; }
        [NotMapped]
        public long PersonaId { get; set; }
    }
}
