﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementChildElementPropertyModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [Display(Name = "Page Type")]
        [XmlAttribute(nameof(PageType))]
        public string PageType { get; set; }

        [Display(Name = "Is Homepage")]
        [XmlAttribute(nameof(IsHomePage))]
        public string IsHomePage { get; set; }

        [Display(Name = "Skip Page")]
        [XmlAttribute(nameof(SkipPage))]
        public string SkipPage { get; set; }

        [Display(Name = "Is Login Required")]
        [XmlAttribute(nameof(IsLoginRequired))]
        public string IsLoginRequired { get; set; }

        [Display(Name = "Html Content")]
        [XmlAttribute(nameof(HtmlContent))]
        public string HtmlContent { get; set; }
    }
}
