﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementPropertySettingsModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [Display(Name = "Session Timeout")]
        [XmlAttribute(nameof(SessionTimeout))]
        public string SessionTimeout { get; set; }

        [Display(Name = "Invalid Purl")]
        [XmlAttribute(nameof(InvalidPurl))]
        public string InvalidPurl { get; set; }

        [Display(Name = "Page Not Found")]
        [XmlAttribute(nameof(PageNotFound))]
        public string PageNotFound { get; set; }

        [XmlAttribute(nameof(Error))]
        public string Error { get; set; }
    }
}
