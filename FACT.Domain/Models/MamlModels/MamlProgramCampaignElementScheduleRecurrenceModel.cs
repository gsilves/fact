﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementScheduleRecurrenceModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public string Id { get; set; }

        [XmlAttribute(nameof(Frequency))]
        public string Frequency { get; set; }

        [XmlAttribute(nameof(Interval_Month))]
        public string Interval_Month { get; set; }

        [XmlAttribute(nameof(Interval_Day))]
        public string Interval_Day { get; set; }

        public string RecurrenceString { get; set; }
    }
}
