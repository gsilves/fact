﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementChildConnectionModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [Display(Name = "Start Element Id")]
        [XmlAttribute(nameof(StartElementId))]
        public int StartElementId { get; set; }

        [Display(Name = "End Element Id")]
        [XmlAttribute(nameof(EndElementId))]
        public int EndElementId { get; set; }
    }
}
