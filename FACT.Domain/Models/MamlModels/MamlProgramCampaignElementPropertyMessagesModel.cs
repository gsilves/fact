﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    [Bind(Include = "Message")]
    public class MamlProgramCampaignElementPropertyMessagesModel
    {

        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [XmlElement(nameof(Message))]
        public MamlProgramCampaignElementPropertyMessageModel Message { get; set; }

        [Display(Name = "Email Notification")]
        [XmlElement(nameof(EmailNotification))]
        public MamlProgramCampaignElementPropertyEmailNotificationeModel EmailNotification { get; set; }
    }
}
