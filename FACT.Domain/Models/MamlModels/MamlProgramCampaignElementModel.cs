﻿using FACT.Domain.Models.EntityModels;
using FACT.Domain.Models.OneAllModels;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [XmlAttribute(nameof(DbId))]
        public int DbId { get; set; }

        [Display(Name = "Checkout Id")]
        [XmlAttribute(nameof(CheckOutId))]
        public int CheckOutId { get; set; }

        [XmlAttribute(nameof(Version))]
        public string Version { get; set; }

        [XmlAttribute(nameof(State))]
        public string State { get; set; }

        [XmlAttribute(nameof(Type))]
        public string Type { get; set; }

        [XmlAttribute(nameof(Category))]
        public string Category { get; set; }

        [XmlAttribute(nameof(Name))]
        public string Name { get; set; }

        [XmlAttribute(nameof(Position))]
        public string Position { get; set; }

        [Display(Name = "Shared Checkout")]
        [XmlAttribute(nameof(SharedCheckOut))]
        public string SharedCheckOut { get; set; }

        // Here ve have name of RuleGroup from RuleGroupSet in MamlProgramModel
        [Display(Name = "Rule Group")]
        [XmlAttribute(nameof(RuleGroup))]
        public string RuleGroup { get; set; }

        [Display(Name = "Item Cost")]
        [XmlAttribute(nameof(itemCost))]
        public double itemCost { get; set; }

        [Display(Name = "Readonly")]
        [XmlAttribute(nameof(Readonly))]
        public string Readonly { get; set; }

        [XmlAttribute(nameof(Optional))]
        public string Optional { get; set; }

        [XmlAttribute(nameof(Delete))]
        public string Delete { get; set; }

        //TODO this is not exactly as is in XML. Filter is Element in XML not Attribute
        [Display(Name = "Filter")]
        [XmlAttribute(nameof(Filter))]
        public MamlFilterModel Filter { get; set; }

        //TODO this is not exactly as is in XML. Filter is Element in XML not Attribute
        [Display(Name = "Hooked Events Trigger Fire Context")]
        [XmlAttribute(nameof(HookedEvents_TriggerFireContext))]
        public string HookedEvents_TriggerFireContext { get; set; }

        [XmlElement(nameof(Schedules))]
        public virtual ICollection<MamlProgramCampaignElementScheduleModel> Schedules { get; set; }

        [XmlElement(nameof(Properties))]
        public MamlProgramCampaignElementPropertyModel Properties { get; set; }

        [Display(Name = "Callback TouchPoints")]
        [XmlElement(nameof(CallbackTouchPoints))]
        public virtual ICollection<MamlProgramCampaignElementCallBackTuchPointModel> CallbackTouchPoints { get; set; }

        [Display(Name = "Child Elements")]
        [XmlElement(nameof(ChildElements))]
        public virtual ICollection<MamlProgramCampaignElementChildElementModel> ChildElements { get; set; }

        [Display(Name = "Child Connections")]
        [XmlElement(nameof(ChildConnections))]
        public virtual ICollection<MamlProgramCampaignElementChildConnectionModel> ChildConnections { get; set; }

        [NotMapped]
        public string HtmlContent { get; set; }

        [NotMapped]
        public string Disabled { get; set; }

        [NotMapped]
        public OneAllUser SocialUser { get; set; }

        [NotMapped]
        public Profiles UserProfile { get; set; }

        [NotMapped]
        public string FriendlyType
        {
            get
            {
                switch (Type)
                {
                    case "Microsite":
                        return "Landing Page";
                    case "OutboundApplication":
                        return "Social Post";
                    case "DirectMail":
                        return "Print And Mail";
                    default:
                        return Type;
                }
            }
        }
    }
}
