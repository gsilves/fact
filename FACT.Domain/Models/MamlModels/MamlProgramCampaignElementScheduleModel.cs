﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementScheduleModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [XmlAttribute(nameof(DbId))]
        public int DbId { get; set; }

        [XmlAttribute(nameof(Status))]
        public string Status { get; set; }

        [Display(Name = "Target Audiences")]
        [XmlAttribute(nameof(TargetAudiences))]
        public string TargetAudiences { get; set; }

        [XmlAttribute(nameof(Type))]
        public string Type { get; set; }

        [Display(Name = "Time Zone")]
        [XmlAttribute(nameof(TimeZone))]
        public string TimeZone { get; set; }

        [Display(Name = "Email Notification")]
        [XmlAttribute(nameof(EmailNotification))]
        public string EmailNotification { get; set; }

        [Display(Name = "Approval Required")]
        [XmlAttribute(nameof(ApprovalRequired))]
        public string ApprovalRequired { get; set; }

        [Display(Name = "Contact Scope")]
        [XmlAttribute(nameof(ContactScope))]
        public string ContactScope { get; set; }

        [XmlAttribute(nameof(Subject))]
        public string Subject { get; set; }

        [XmlAttribute(nameof(Body))]
        public string Body { get; set; }

        [Display(Name = "Start Date")]
        [XmlAttribute(nameof(StartDate))]
        public string StartDate { get; set; }

        [Display(Name = "End Date")]
        [XmlAttribute(nameof(EndDate))]
        public string EndDate { get; set; }

        [XmlElement(nameof(Recurrence))]
        public MamlProgramCampaignElementScheduleRecurrenceModel Recurrence { get; set; }
    }
}
