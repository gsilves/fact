﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementPropertyBaseUrlModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [XmlAttribute(nameof(DbId))]
        public int DbId { get; set; }

        [XmlAttribute(nameof(URL))]
        public string URL { get; set; }

        [XmlAttribute(nameof(Extension))]
        public string Extension { get; set; }

        [Display(Name = "Outbound Id")]
        [XmlAttribute(nameof(Outbound_Id))]
        public int Outbound_Id { get; set; }

        [Display(Name = "Outbound DbId")]
        [XmlAttribute(nameof(Outbound_DbId))]
        public int Outbound_DbId { get; set; }

        [Display(Name = "Parent Id")]
        [XmlAttribute(nameof(Parent_Id))]
        public int Parent_Id { get; set; }

        [Display(Name = "Purl Position")]
        [XmlAttribute(nameof(PurlPosition))]
        public string PurlPosition { get; set; }

        [XmlAttribute(nameof(Domain))]
        public string Domain { get; set; }

        [XmlAttribute(nameof(SSL))]
        public string SSL { get; set; }

        [Display(Name = "Search Engine Allowed")]
        [XmlAttribute(nameof(SearchEngineAllowed))]
        public string SearchEngineAllowed { get; set; }
    }
}
