﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    [Bind(Include = "Messages")]
    public class MamlProgramCampaignElementPropertyModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public string Id { get; set; }

        [Display(Name = "Service Type ID")]
        [XmlAttribute(nameof(ServiceTypeID))]
        public string ServiceTypeID { get; set; }

        [Display(Name = "Application Version")]
        [XmlAttribute(nameof(ApplicationVersion))]
        public string ApplicationVersion { get; set; }

        [Display(Name = "Page Type")]
        [XmlAttribute(nameof(PageType))]
        public string PageType { get; set; }

        [Display(Name = "Is Homepage")]
        [XmlAttribute(nameof(IsHomePage))]
        public string IsHomePage { get; set; }

        [Display(Name = "Skip Page")]
        [XmlAttribute(nameof(SkipPage))]
        public string SkipPage { get; set; }

        [Display(Name = "Is Login Required")]
        [XmlAttribute(nameof(IsLoginRequired))]
        public string IsLoginRequired { get; set; }

        [XmlAttribute(nameof(Messages))]
        public MamlProgramCampaignElementPropertyMessagesModel Messages { get; set; }

        [XmlElement(nameof(Configs))]
        public virtual ICollection<KeyValuePair<string, string>> Configs { get; set; }

        [Display(Name = "Base Url Collection")]
        [XmlElement(nameof(BaseUrlCollection))]
        public virtual ICollection<MamlProgramCampaignElementPropertyBaseUrlModel> BaseUrlCollection { get; set; }

        [XmlElement(nameof(Settings))]
        public virtual MamlProgramCampaignElementPropertySettingsModel Settings { get; set; }

    }
}
