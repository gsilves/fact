﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    [Bind(Include = "Id,Type,ToAddress,FromName,FromAddress,ReplyTo,Subject,HtmlContent_URL,TextContent,SelectedColumns")]
    public class MamlProgramCampaignElementPropertyMessageModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [XmlAttribute(nameof(Type))]
        public string Type { get; set; }

        [XmlAttribute(nameof(Encoding))]
        public string Encoding { get; set; }

        [Display(Name = "To Address")]
        [XmlAttribute(nameof(ToAddress))]
        public string ToAddress { get; set; }

        [Display(Name = "From Name")]
        [XmlAttribute(nameof(FromName))]
        public string FromName { get; set; }

        [Display(Name = "From Address")]
        [XmlAttribute(nameof(FromAddress))]
        public string FromAddress { get; set; }

        [Display(Name = "Reply To")]
        [XmlAttribute(nameof(ReplyTo))]
        public string ReplyTo { get; set; }

        [XmlAttribute(nameof(Subject))]
        public string Subject { get; set; }

        [Display(Name = "Html Content URL")]
        [XmlAttribute(nameof(HtmlContent_URL))]
        public string HtmlContent_URL { get; set; }

        [Display(Name = "Text Content")]
        [XmlAttribute(nameof(TextContent))]
        public string TextContent { get; set; }

        [Display(Name = "Selected Columns")]
        [XmlElement(nameof(SelectedColumns))]
        public virtual ICollection<KeyValuePair<string, string>> SelectedColumns { get; set; }
    }
}
