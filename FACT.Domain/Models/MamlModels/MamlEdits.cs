﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlEdits
    {
        public MamlEdits()
        {
            Elements = new List<ElementEdits>();
        }

        public ElementEdits AddElementEdits(int id)
        {
            ElementEdits ee = new ElementEdits(id);
            Elements.Add(ee);
            return ee;
        }

        [NotMapped]
        public string MamlXML { get; set; }

        [NotMapped]
        public string CampaignMaml { get; set; }

        [NotMapped]
        public string CampaignName { get; set; }

        [NotMapped]
        public string ContactFilter { get; set; }

        [NotMapped]
        public int PersonaId { get; set; }

        [NotMapped]
        public string Action { get; set; }

        [NotMapped]
        public List<ElementEdits> Elements { get; set; }
    }
}
