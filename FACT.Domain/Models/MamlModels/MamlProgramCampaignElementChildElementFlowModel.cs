﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementChildElementFlowModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [Display(Name = "Default Next Child Name")]
        [XmlAttribute(nameof(DefaultNextChildName))]
        public string DefaultNextChildName { get; set; }

        [Display(Name = "Default Next Child Id")]
        [XmlAttribute(nameof(DefaultNextChildId))]
        public string DefaultNextChildId { get; set; }

        [Display(Name = "Flow Next Elements")]
        [XmlElement(nameof(FlowNextElements))]
        public virtual ICollection<MamlProgramCampaignElementChildElementFlowNextElementModel> FlowNextElements { get; set; }
    }
}
