﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    [Bind(Include = "Id,CriteriaJoinOperator,Criteria")]
    public class MamlFilterModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [Display(Name = "Criteria Join Operator")]
        [XmlAttribute(nameof(CriteriaJoinOperator))]
        public string CriteriaJoinOperator { get; set; }

        [XmlElement(nameof(Criteria))]
        public virtual ICollection<MamlFilterCriteriaModel> Criteria { get; set; }
    }
}
