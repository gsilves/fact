﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramModel
    {
        [XmlAttribute(nameof(Name))]
        public string Name { get; set; }

        [XmlAttribute(nameof(Version))]
        public string Version { get; set; }

        [Key]
        [XmlAttribute(nameof(dbId))]
        public int dbId { get; set; }

        [Display(Name = "CheckOut Id")]
        [XmlAttribute(nameof(CheckOutId))]
        public int CheckOutId { get; set; }

        [XmlAttribute(nameof(State))]
        public string State { get; set; }

        [XmlElement(nameof(Campaign))]
        public MamlProgramCampaignModel Campaign { get; set; }

        [Display(Name = "Rule Group Set")]
        [XmlElement(nameof(RuleGroupSet))]
        public virtual ICollection<MamlProgramRuleGroupModel> RuleGroupSet { get; set; }
    }
}
