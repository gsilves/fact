﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementPropertyEmailNotificationeModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [Display(Name = "From Display Name")]
        [XmlAttribute(nameof(FromDisplayName))]
        public string FromDisplayName { get; set; }

        [Display(Name = "To Address")]
        [XmlAttribute(nameof(ToAddress))]
        public string ToAddress { get; set; }

        [XmlAttribute(nameof(Subject))]
        public string Subject { get; set; }

        [XmlAttribute(nameof(Body))]
        public string Body { get; set; }
    }
}
