﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramRuleGroupModel
    {
        [XmlAttribute(nameof(Name))]
        [Key]
        public string Name { get; set; }

        [XmlAttribute(nameof(Description))]
        public string Description { get; set; }

        [Display(Name = "Content Variables")]
        [XmlElement(nameof(ContentVariables))]
        public virtual ICollection<string> ContentVariables { get; set; }
    }
}
