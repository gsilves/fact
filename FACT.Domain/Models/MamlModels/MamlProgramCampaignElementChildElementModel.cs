﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignElementChildElementModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [XmlAttribute(nameof(DbId))]
        public int DbId { get; set; }

        [XmlAttribute(nameof(Type))]
        public string Type { get; set; }

        [XmlAttribute(nameof(IsVersion))]
        [Display(Name = "Is Version")]
        public string IsVersion { get; set; }

        [XmlAttribute(nameof(Name))]
        public string Name { get; set; }

        [XmlAttribute(nameof(Position))]
        public string Position { get; set; }

        [Display(Name = "Rule Group")]
        [XmlAttribute(nameof(RuleGroup))]
        public string RuleGroup { get; set; }

        [XmlElement(nameof(Properties))]
        public MamlProgramCampaignElementChildElementPropertyModel Properties { get; set; }

        [XmlElement(nameof(Flow))]
        public MamlProgramCampaignElementChildElementFlowModel Flow { get; set; }
    }
}
