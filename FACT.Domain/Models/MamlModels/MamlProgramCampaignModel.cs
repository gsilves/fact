﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    public class MamlProgramCampaignModel
    {
        [XmlAttribute(nameof(Id))]
        [Key]
        public int Id { get; set; }

        [XmlAttribute(nameof(DbId))]
        public int DbId { get; set; }

        [XmlAttribute(nameof(CheckOutId))]
        [Display(Name = "CheckOut Id")]
        public int CheckOutId { get; set; }

        [XmlAttribute(nameof(Name))]
        public string Name { get; set; }

        [XmlAttribute(nameof(State))]
        public string State { get; set; }

        public int MamlProgramCampaignElementModelId { get; set; }

        [Display(Name = "Campaign Elements")]
        [XmlElement(nameof(CampaignElements))]
        public virtual ICollection<MamlProgramCampaignElementModel> CampaignElements { get; set; }

        public int MamlProgramCampaignConnectionModelId { get; set; }

        [Display(Name = "Campaign Connections")]
        [XmlElement(nameof(CampaignConnections))]
        public virtual List<MamlProgramCampaignConnectionModel> CampaignConnections { get; set; }
    }
}
