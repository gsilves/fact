﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.MamlModels
{
    public class ElementEdits
    {
        public ElementEdits(int id)
        {
            elementId = id;
        }

        [NotMapped]
        public int elementId { get; set; }

        [NotMapped]
        public DateTime LounchDate { get; set; }

        [NotMapped]
        public string FromAddress { get; set; }

        [NotMapped]
        public string FromName { get; set; }

        [NotMapped]
        public string EmailReplyTo { get; set; }

        [NotMapped]
        public string EmailSubject { get; set; }

        [NotMapped]
        public string LetterFeed { get; set; }

        [NotMapped]
        public string SocialAccount { get; set; }

        [NotMapped]
        public string LinkUrl { get; set; }

        [NotMapped]
        public string LinkCaption { get; set; }

        [NotMapped]
        public string LinkImage { get; set; }

        [NotMapped]
        public string MessageBody { get; set; }

        [NotMapped]
        public string HTMLContent { get; set; }

        [NotMapped]
        public string TextContent { get; set; }
    }
}
