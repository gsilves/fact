﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.Mvc;
using System.Xml.Serialization;

namespace FACT.Domain.Models.MamlModels
{
    [Bind(Include = "Row,Field,Operator,Value")]
    public class MamlFilterCriteriaModel
    {
        [XmlAttribute(nameof(Row))]
        [Key]
        public string Row { get; set; }

        [XmlAttribute(nameof(Field))]
        public string Field { get; set; }

        [XmlAttribute(nameof(Operator))]
        public string Operator { get; set; }

       [XmlAttribute(nameof(Value))]
        public string Value { get; set; }
    }
}
