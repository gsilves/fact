﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models
{
    public class Settingsstatic
    {
        public static long Id { get; set; }
        public static bool ApprovalRequired { get; set; }
        public static bool IsCreditChargeable { get; set; }
        public static bool IsSubscriptionSignable { get; set; }
    }
}
