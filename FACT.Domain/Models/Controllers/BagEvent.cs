﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.Controllers
{
    public class BagEvent
    {
        public string title { get; set; }
        public string start { get; set; }
    }
}
