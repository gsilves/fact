﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.Controllers
{
    public class PreviewContacts
    {
        public string RTTagGroups { get; set; }
        public string GroupName { get; set; }
        public string MFListName { get; set; }
        public string NotificationEmail { get; set; }
        public string CSV { get; set; }
        public List<ContactsModel> ListOfContacts { get; set; }
    }
}
