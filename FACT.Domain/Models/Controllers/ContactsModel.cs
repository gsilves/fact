﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace FACT.Domain.Models.Controllers
{
    public class ContactsModel
    {
        [Key]
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public int AccountID { get; set; }
        public string Email { get; set; }
        public string Company { get; set; }
        public string CellPhone { get; set; }
        public string Address1 { get; set; }
        public string Address2 { get; set; }
        public string City { get; set; }
        public string State { get; set; }
        public string Zip { get; set; }
        public string WebSite { get; set; }
        public DateTime Birthdate { get; set; }
        public DateTime Anniversary { get; set; }
    }
}
